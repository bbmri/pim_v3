# Deploying PIM
Notes: PIM provides support for [Sentry](https://sentry.io/welcome/), a frontend error tracking webservice.
Provide ```SENTRY_DSN``` to make use of this feature.
Also, provide your [letsencrypt](https://letsencrypt.org/) email for HTTPS support (```LETSENCRYPT_EMAIL```).

To deploy PIM in your own network, execute the following:
```
cd /pim/server/docker/site
export SENTRY_DSN=YOUR_SENTY_DSN LETSENCRYPT_EMAIL=YOUR_LETSENCRYPT_EMAIL && sudo docker-compose up
```
PIM will be available at port 80.