#  Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
#
#  PIM is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Lesser General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  PIM is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Lesser General Public License for more details.

from flask_restplus import fields

from server.api.restplus import api

port = api.model('Port', {
    'title': fields.String(required=False, description='Port title (UI)', default=''),
    'description': fields.String(required=False, description='Port description (UI)', default=''),
    'customData': fields.Raw(required=False, description='Port custom data (JSON)'),

    'name': fields.String(required=True, description='Port name', default='')
})

node = api.model('Node', {
    'title': fields.String(required=False, description='Node title (UI)', default=''),
    'description': fields.String(required=False, description='Node description (UI)', default=''),
    'customData': fields.Raw(required=False, description='Node custom data (JSON)'),

    'name': fields.String(required=True, description='Node name', default=''),
    'type': fields.String(required=False, description='Type of node'),
    'inPorts': fields.List(fields.Nested(port)),
    'outPorts': fields.List(fields.Nested(port))
})

# Tree node (workaround because restplus 0.9.2 does not natively support self referential models)
tree_node = api.inherit('TreeNode', node, {
    'children': fields.List(fields.Nested(api.models['Node']))
})

link = api.model('Link', {
    'title': fields.String(required=False, description='Link title (UI)', default=''),
    'description': fields.String(required=False, description='Link description (UI)', default=''),
    'customData': fields.Raw(required=False, description='Link custom data (JSON)'),

    'fromPort': fields.String(required=True, description='From port path (e.g. /root/node_a/from_port_a)', default=''),
    'toPort': fields.String(required=True, description='To port path (e.g. /root/node_a/from_port_b)', default=''),
    'type': fields.String(required=False, description='Data type associated with this link', default='')
})

status_type = api.model('statusType', {
    'title': fields.String(required=True, description='Status type title (e.g. idle, success)', default=''),
    'description': fields.String(required=False, description='Status type description', default=''),
    'color': fields.String(required=False, description='Status color (hex)', default='')
})

default_status_types = list()

default_status_types.append({'title': 'idle', 'description': 'Jobs that are waiting for input', 'color': '#ffffff'})
default_status_types.append({'title': 'running', 'description': 'Jobs that are running', 'color': '#ffffff'})
default_status_types.append({'title': 'success', 'description': 'Jobs that finished successfully', 'color': '#ffffff'})
default_status_types.append({'title': 'failed', 'description': 'Jobs that have failed', 'color': '#ffffff'})
default_status_types.append({'title': 'cancelled', 'description': 'Jobs which were cancelled', 'color': '#ffffff'})
default_status_types.append({'title': 'undefined', 'description': 'Jobs with an undefined state', 'color': '#ffffff'})

run = api.model('Run', {
    'name': fields.String(required=True, description='Unique run name', default=''),
    'title': fields.String(required=False, description='Run title (UI)', default=''),
    'description': fields.String(required=False, description='Run description (UI)', default=''),
    'customData': fields.Raw(required=False, description='Run custom data (JSON)'),
    'user': fields.String(required=True, description='Owner of the run', default=''),
    'assignedTo': fields.List(fields.String(), required=True, description='Assigned to role groups'),
    'root': fields.Nested(tree_node, required=True, description='Root node'),
    'links': fields.List(fields.Nested(link), required=True, description='Node connections'),
    'statusTypes': fields.List(fields.Nested(status_type), default=default_status_types, required=False, description='Status types map')
})

job = api.model('JobPut', {
    'path': fields.String(required=True, description='Job path (e.g. /root/node_a/node_b)', default=''),
    'title': fields.String(required=False, description='Job title', default=''),
    'description': fields.String(required=False, description='Job description', default=''),
    'status': fields.Integer(required=False, default=0, description='Job status'),
    'customData': fields.Raw(required=False, description='Job custom data')
})
