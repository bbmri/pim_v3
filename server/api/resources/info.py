#  Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
#
#  PIM is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Lesser General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  PIM is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Lesser General Public License for more details.

import sys, os, subprocess

from flask_restplus import Resource

from server.api.restplus import api, PimJsonResponse, PimExceptionResponse

ns = api.namespace('info', description='Info endpoints')


def get_git_revision_hash():
    return subprocess.check_output(['git', 'rev-parse', 'HEAD']).decode(sys.stdout.encoding).strip()


def get_git_revision_info(fmt):
    return subprocess.check_output(['git', 'log', '-1', '--date=iso', '--pretty=%{}'.format(fmt)]).decode(sys.stdout.encoding).strip()


@ns.route('/')
class Info(Resource):
    @api.response(200, 'Info successfully fetched')
    def get(self):
        """
        Get api information
        """

        try:
            home_dir = os.getenv("HOME")

            try:
                file = open('./../deploy_info'.format(home_dir), 'r')

                last_updated = file.readline()
                current_revision = file.readline()
            except IOError as e:
                last_updated = None
                current_revision = None

            deployment = {'lastUpdated': last_updated, 'currentRevision': current_revision}

            return PimJsonResponse(200, version=2, deployment=deployment)
        except Exception as e:
            return PimExceptionResponse(message='Unable to fetch info due to an unhandled error.')
