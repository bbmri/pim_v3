#  Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
#
#  PIM is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Lesser General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  PIM is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Lesser General Public License for more details.

from flask import g
from flask_restplus import Resource

from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.orm.exc import *

from server.api.restplus import api, PimJsonResponse, PimExceptionResponse

import server.orm as orm

import server.database.database as database

ns = api.namespace('runs', description='Runs endpoints')


@ns.route('/<string:run_name>/graph')
@api.doc(params={'run_name': 'The unique run name'})
class Graph(Resource):
    @api.response(200, 'Graph successfully fetched')
    @api.response(500, 'Unable to fetch graph due to an internal server error')
    def get(self, run_name):
        """
        Get graph
        """

        try:
            # Check run existence
            run = orm.Run.query.filter(orm.Run.name == run_name).one()

            nodes = database.get_nodes(run)
            links = database.get_links(run)

            return PimJsonResponse(200, nodes=nodes, links=links)
        except NoResultFound as e:
            g.db_session.rollback()
            message = 'Unable to fetch graph. Run does not exist: {}.'.format(run_name)
            return PimJsonResponse(404, message=message, error=str(e))
        except SQLAlchemyError as e:
            g.db_session.rollback()
            return PimExceptionResponse(message='Unable to fetch graph due to database error.')
        except Exception as e:
            return PimExceptionResponse(message='Unable to fetch graph due to an unhandled error.')
