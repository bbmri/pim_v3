#  Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
#
#  PIM is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Lesser General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  PIM is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Lesser General Public License for more details.

import datetime, json

from flask import request, g, current_app
from flask_restplus import Resource, reqparse

from sqlalchemy import desc
from sqlalchemy.exc import SQLAlchemyError

import server.api.model as model

import server.orm as orm
import server.serialization as serialization
from server.api.restplus import api, PimJsonResponse, PimExceptionResponse

from server.database.updatejobs import UpdateJobs

ns = api.namespace('runs', description='Runs endpoints')

get_runs_args = reqparse.RequestParser()
get_runs_args.add_argument('name_filter', type=str, required=False, default='')
get_runs_args.add_argument('user_filter', type=str, required=False, default='')
get_runs_args.add_argument('status_filter', type=str, required=False, default='["idle","running","success","failed","cancelled","undefined"]')
get_runs_args.add_argument('sort_column', type=str, required=False, default='name')
get_runs_args.add_argument('sort_direction', type=str, required=False, default='ascending')
get_runs_args.add_argument('page_size', type=int, required=False, default=10)
get_runs_args.add_argument('page_index', type=int, required=False, default=0)


@ns.route('/')
class RunsCollection(Resource):
    @api.expect(get_runs_args)
    @api.response(200, 'Run summaries successfully fetched')
    @api.response(500, 'Unable to fetch run summaries due to an internal server error')
    def get(self):
        """
        Get runs
        """

        try:
            # Request parameters
            args = get_runs_args.parse_args()

            # Sorting/pagination parameters
            name_filter = args['name_filter']
            user_filter = args['user_filter']
            sort_column = args['sort_column']
            sort_direction = args['sort_direction']
            page_index = args['page_index']
            page_size = args['page_size']

            sort_columns = [
                'status',
                'name',
                'user',
                'no_jobs',
                'progress',
                'duration',
                'created',
                'modified']

            status_map = {
                'idle':      0,
                'running':   1,
                'success':   2,
                'failed':    3,
                'cancelled': 4,
                'undefined': 5,
            }

            if sort_column in sort_columns:
                # Obtain sorting ORM column
                column = getattr(orm.Run, sort_column)

                # Sorting
                if sort_direction == 'descending':
                    runs_query = orm.Run.query.order_by(desc(column))
                else:
                    runs_query = orm.Run.query.order_by(column)
            else:
                runs_query = orm.Run.query

            # Obtain number of runs in total
            filtered_runs = runs_query

            # Filter out by name, if supplied
            if name_filter != '':
                filtered_runs = filtered_runs.filter(orm.Run.name.ilike('%{}%'.format(name_filter.replace('_', '\_'))))

            # Filter out by user, if supplied
            if user_filter != '':
                filtered_runs = filtered_runs.filter(orm.Run.user.ilike('%{}%'.format(user_filter).replace('_', '\_')))

            # Possibly filter by run status
            if 'status_filter' in args:
                status_filter = []

                for status_type in json.loads(args['status_filter']):
                    status_filter.append(status_map[status_type])

                filtered_runs = filtered_runs.filter(orm.Run.status.in_(tuple(status_filter)))

            # Paginate
            paginated_runs = filtered_runs.limit(page_size).offset(page_index * page_size)

            # Number of runs in total
            no_runs = orm.Run.query.count()

            runs = serialization.RunShortSchema(many=True).dump(paginated_runs).data
            return PimJsonResponse(200, runs=runs, noRuns=no_runs, noFilteredRuns=filtered_runs.count())
        except SQLAlchemyError as e:
            g.db_session.rollback()
            return PimExceptionResponse(message='Unable to fetch run summaries due to database error.')
        except Exception as e:
            return PimExceptionResponse(message='Unable to retrieve run summaries due to an unhandled error.')

    @api.expect(model.run)
    @api.response(200, 'Run successfully added')
    @api.response(400, 'Unable to add run due to a bad request')
    @api.response(500, 'Unable to add run due to an internal server error')
    def post(self):
        """
        Add run
        """

        try:
            now = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')

            payload = request.get_json()

            # Use name for the title if the title is undefined
            if payload['title'] == '':
                payload['title'] = payload['name']

            # Avoid posting an existing run
            if orm.Run.query.filter(orm.Run.name == payload['name']).first():
                return PimJsonResponse(400, message='Run {} already exists'.format(payload['name']))

            # Serialize JSON run to ORM run
            run = serialization.RunSchema().load(payload).data

            def add_node(json_node, parent_orm_node=None, level=0):

                # Use name for the title if the title is undefined
                if json_node['title'] == '':
                    json_node['title'] = json_node['name']

                node_orm = serialization.NodeSchema().load(json_node).data

                if parent_orm_node:
                    node_orm.path = '{}/{}'.format(parent_orm_node.path, json_node['name'])
                    node_orm.parent = parent_orm_node
                else:
                    node_orm.path = '{}/{}'.format(payload['name'], json_node['name'])

                node_orm.level = level
                node_orm.aggregates = [0 for i in range(current_app.aggregation_no_items)]

                # Append the node
                run.nodes.append(node_orm)

                if not parent_orm_node:
                    run.root = node_orm

                # Add input ports to node
                for in_port_json in json_node['inPorts']:

                    # Use name for the title if the title is undefined
                    if in_port_json['title'] == '':
                        in_port_json['title'] = in_port_json['name']

                    port_orm = serialization.InPortSchema().load(in_port_json).data
                    port_orm.path = '{}/{}'.format(node_orm.path, in_port_json['name'])
                    node_orm.in_ports.append(port_orm)

                # Add output ports to node
                for out_port_json in json_node['outPorts']:

                    # Use name for the title if the title is undefined
                    if out_port_json['title'] == '':
                        out_port_json['title'] = out_port_json['name']

                    port_orm = serialization.OutPortSchema().load(out_port_json).data
                    port_orm.path = '{}/{}'.format(node_orm.path, out_port_json['name'])
                    node_orm.out_ports.append(port_orm)

                for child in json_node['children']:
                    add_node(child, node_orm, level + 1)

            add_node(payload['root'])

            def common_ancestor_node(from_port_id, to_port_id):
                from_segs = from_port_id.split('/')[:-1]
                to_segs = to_port_id.split('/')[:-1]

                no_segs_min = min(len(from_segs), len(to_segs))
                result = list()

                for seg_id in range(no_segs_min):
                    if from_segs[seg_id] == to_segs[seg_id]:
                        result.append(from_segs[seg_id])
                    else:
                        break

                return '/'.join(result)

            def add_link(link_json):

                # Use name for the title if the title is undefined
                if link_json['title'] == '':
                    link_json['title'] = link_json['name']

                link_orm = serialization.LinkSchema().load(link_json).data
                link_orm.from_port_id = '{}/{}'.format(payload['name'], link_orm.from_port_id)
                link_orm.to_port_id = '{}/{}'.format(payload['name'], link_orm.to_port_id)

                link_orm.common_ancestor = common_ancestor_node(link_orm.from_port_id, link_orm.to_port_id)
                run.links.append(link_orm)

            for link in payload['links']:
                add_link(link)

            run.name = payload['name']
            run.created = now
            run.modified = now

            g.db_session.add(run)
            g.db_session.commit()

            master_job = dict(path='root/master'.format(), title='master', description='Master job', status=0)

            UpdateJobs.update_jobs(run.id, run.name, now, [master_job])

            return PimJsonResponse(200, message='Run added.')
        except SQLAlchemyError as e:
            g.db_session.rollback()
            return PimExceptionResponse(message='Unable to add run due to a database error.')
        except Exception as e:
            return PimExceptionResponse(message='Unable to add run due to an unhandled error.')