
#  Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
#
#  PIM is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Lesser General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  PIM is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Lesser General Public License for more details.

import datetime

from flask import g
from flask_restplus import Resource, reqparse

from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.orm.exc import *

from server.api.restplus import api, PimJsonResponse, PimExceptionResponse

import server.orm as orm

import server.database.database as database

ns = api.namespace('runs', description='Runs endpoints')

get_status_args = reqparse.RequestParser()
get_status_args.add_argument('reference_timestamp', required=False, type=str, default='', help='Only return updated run status')


@ns.route('/<string:run_name>/status')
@api.doc(params={'run_name': 'The unique run name'})
class Status(Resource):
    @api.expect(get_status_args)
    @api.response(200, 'Run status successfully fetched')
    @api.response(400, 'Unable to fetch run status due to a bad request')
    @api.response(500, 'Unable to fetch run status due to an internal server error')
    def get(self, run_name):
        """
        Get run status
        """

        try:
            # Check run existence
            run = orm.Run.query.filter(orm.Run.name == run_name).one()

            args = get_status_args.parse_args()

            beginning_of_time = datetime.datetime.fromtimestamp(0).strftime('%Y-%m-%d %H:%M:%S.%f')
            now = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')
            reference_timestamp = beginning_of_time if args['reference_timestamp'] == '' else args['reference_timestamp']

            status = database.get_status(run, reference_timestamp)

            return PimJsonResponse(200, status=status, timestamp=now)
        except NoResultFound as e:
            message = 'Unable to fetch run status. Run does not exist: {}.'.format(run_name)
            return PimJsonResponse(404, message=message, error=str(e))
        except SQLAlchemyError as e:
            g.db_session.rollback()
            return PimExceptionResponse(message='Unable to fetch run status due to database error.')
        except Exception as e:
            return PimExceptionResponse(message='Unable to fetch run status due to an unhandled error.')
