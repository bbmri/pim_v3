
#  Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
#
#  PIM is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Lesser General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  PIM is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Lesser General Public License for more details.

from flask import g
from flask_restplus import Resource

from server.api.restplus import api, PimJsonResponse, PimExceptionResponse

import server.orm as orm

import server.serialization as serialization

from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.orm.exc import *

from server.database.deleterun import DeleteRun

ns = api.namespace('runs', description='Runs endpoints')


@ns.route('/<string:run_name>')
@api.doc(params={'run_name': 'The unique run name'})
class RunItem(Resource):
    @api.response(200, 'Run summary successfully fetched')
    @api.response(404, 'Unable to fetch run summary because the run does not exist')
    @api.response(500, 'Unable to fetch run summary due to an internal server error')
    def get(self, run_name):
        """
        Get run summary
        """

        try:
            run = serialization.RunShortSchema().dump(orm.Run.query.filter(orm.Run.name == run_name).one()).data
            return PimJsonResponse(200, run=run)
        except NoResultFound as e:
            message = 'Unable to fetch run. Run does not exist: {}.'.format(run_name)
            return PimJsonResponse(404, message=message, error=str(e))
        except SQLAlchemyError as e:
            g.db_session.rollback()
            return PimExceptionResponse(message='Unable to fetch run due to a database error.')
        except Exception as e:
            return PimExceptionResponse(message='Unable to fetch run summary due to an unhandled error.')

    @api.response(200, 'Run successfully deleted')
    @api.response(404, 'Unable to delete run due to a bad request')
    @api.response(500, 'Unable to delete run due to an internal server error')
    def delete(self, run_name):
        """
        Delete run
        """

        try:
            run = g.db_session.query(orm.Run).filter(orm.Run.name == run_name).one()

            DeleteRun.delete_run(run.id)

            return PimJsonResponse(200, message='Run deleted.')
        except NoResultFound as e:
            message = 'Unable to delete run. Run does not exist: {}.'.format(run_name)
            return PimJsonResponse(404, message=message, error=str(e))
        except SQLAlchemyError as e:
            g.db_session.rollback()
            return PimExceptionResponse(message='Unable to delete run due to a database error.')
        except Exception as e:
            return PimExceptionResponse(message='Unable to delete run due to an unhandled error.')
