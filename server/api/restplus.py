
#  Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
#
#  PIM is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Lesser General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  PIM is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Lesser General Public License for more details.

import sys, logging, traceback

from flask_restplus import Api

import time

from flask import jsonify

from flask.wrappers import Response

log = logging.getLogger(__name__)

api = Api(version='1.0', title='PIM API',  description='API documentation for the Pipeline Inspection and Monitoring web micro service ', doc="/documentation")


class PimJsonResponse(Response):
    def __init__(self, status_code, **kwargs):
        super().__init__()

        # Set status code
        self.status_code = status_code

        # Set empty data to be sure
        self.set_data({})

        # Store python object which will be converted later to a JSON string in finalize()
        self.content = kwargs

    def finalize(self, time_start):
        """Finalizes the JSON response object

        Augment the response object with compute elasped time and convert the object to JSON representation.

        :param time_start: Time when the request started
        """

        # Determine elapsed time
        time_end = time.time()

        # Set elapsed time in response object
        self.content['apiRequest'] = dict()

        # Save duration
        self.content['apiRequest']['duration'] = 1000.0 * (time_end - time_start)

        # Set JSON content
        self.set_data(jsonify(self.content).response[0])


class PimExceptionResponse(PimJsonResponse):
    def __init__(self, **kwargs):
        exc_type, exc_value, exc_traceback = sys.exc_info()

        kwargs['exception'] = dict()

        kwargs['exception']['type'] = str(exc_type)
        kwargs['exception']['value'] = str(exc_value)
        kwargs['exception']['trace'] = traceback.format_exc().splitlines()

        super().__init__(500, **kwargs)
