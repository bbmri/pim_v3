# Backend
This directory contains the implementation of the PIM backend.

## Organization
The frontend files are organized as follows:
```
.
├── api                         # REST API
│   └── resources               # Endpoints of the REST API
├── config                      # Configurations
├── database                    # Database
├── docker                      # Docker environment
│   ├── reverse-proxy           # Optional reverse proxy        
│   └── site                    # PIM website
├── test                        # Testing environment
│   ├── cases                   # Unit test cases
│   └── workflowengines         # Workflow engines
│       ├── fastr               # FASTR network examples
│       └── simulator           # Workflow engine simulator
└── views                       # Flask views

```

## Deploying PIM
Please follow [this](docker/README.md) guide for deploying PIM.