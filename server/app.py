#  Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
#
#  PIM is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Lesser General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  PIM is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Lesser General Public License for more details.

import argparse
import configparser
import logging
import os
import time

from flask import Flask, Blueprint, g, current_app
from raven.contrib.flask import Sentry
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
import server.orm as orm
from server.api.restplus import api, PimJsonResponse

# Resources
from server.views.home import home_bp
from server.api.resources.run import ns as run_ns
from server.api.resources.runs import ns as runs_ns
from server.api.resources.status import ns as status_ns
from server.api.resources.jobs import ns as jobs_ns
from server.api.resources.job import ns as job_ns
from server.api.resources.graph import ns as graph_ns
from server.api.resources.info import ns as info_ns


werkzeug_logger = logging.getLogger('werkzeug')

# Sentry instance
if 'SENTRY_DSN' in os.environ:
    sentry = Sentry(dsn=os.environ['SENTRY_DSN'])

from flask_restplus import Api as rpapi
import flask


@property
def specs_url(self):
    """Fixes issue where swagger-ui makes a call to swagger.json over HTTP (mixed-content problem).
       This can ONLY be used on servers that actually use HTTPS.  On servers that use HTTP,
       this code should not be used at all.
    """
    return flask.url_for(self.endpoint('specs'), _external=True, _scheme='https')


def create_app(**kwargs):
    """Creates a custom PIM-Flask application

    :param kwargs: PIM-Flask configuration
    :return: Flask application
    """

    # Defaults from kwargs
    flask_debug             = bool(kwargs.get('flask_debug', False))
    flask_profile           = bool(kwargs.get('flask_profile', False))
    database_uri            = kwargs.get('database_uri', '')
    aggregation_no_items    = int(kwargs.get('aggregation_no_items', 6))

    # Create flask application object
    app = Flask(__name__)


    # Configure application
    app.config['SWAGGER_UI_DOC_EXPANSION']          = 'list'
    app.config['SWAGGER_UI_JSONEDITOR']             = False
    app.config['RESTPLUS_VALIDATE']                 = True
    app.config['RESTPLUS_MASK_SWAGGER']             = False
    app.config['ERROR_404_HELP']                    = False
    app.config['SQLALCHEMY_DATABASE_URI']           = database_uri
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS']    = False
    # app.config['SQLALCHEMY_ECHO']                   = True
    app.config['SQLALCHEMY_POOL_SIZE']              = 20
    app.config['JSONIFY_PRETTYPRINT_REGULAR']       = False

    # Get application context
    if 'APP_CONTEXT' in os.environ:
        app_context = os.environ['APP_CONTEXT']

        # Override the default specs URL with one that returns an HTTPS scheme
        if app_context.startswith('production'):
            rpapi.specs_url = specs_url

        # Enable application debugging in the context of development
        if app_context == 'development':
            flask_debug = True

    # Set the number of items per node to aggregate
    app.aggregation_no_items = aggregation_no_items

    # Create api blueprint
    api_bp = Blueprint('api', __name__, url_prefix='/api')

    # Initialize rest api blueprint
    api.init_app(api_bp)

    # Register blueprints
    app.register_blueprint(api_bp)

    # Set application logger
    app.log = logging.getLogger('app')

    # If the user selects the profiling option, then we need to do a little extra setup
    if flask_profile:
        from werkzeug.contrib.profiler import ProfilerMiddleware

        app.config['PROFILE']   = True
        app.wsgi_app            = ProfilerMiddleware(app.wsgi_app, restrictions=[30])
        app.debug               = True

    # Flask debugging on/off
    app.debug = flask_debug

    app.db_engine = create_engine(database_uri, convert_unicode=True, pool_size=30, max_overflow=0)

    app.db_engine.pool._use_threadlocal = True

    orm.Base.metadata.create_all(bind=app.db_engine)

    # Obtain the type of Flask application configuration
    config = kwargs.get('config', '')

    # Initialize Sentry in production Flask applications only
    if config == 'production' and sentry is not None:
        sentry.init_app(app)

    @app.before_request
    def before_request():
        g.time_start = time.time()
        g.db_session = scoped_session(sessionmaker(autocommit=False, autoflush=False, bind=current_app.db_engine))

        orm.Base.query = g.db_session.query_property()

    @app.after_request
    def per_request_callbacks(response):
        g.db_session.remove()

        time_start = g.get('time_start')

        if isinstance(response, PimJsonResponse):
            response.finalize(time_start)

        return response

    return app


def create_app_from_config(config=''):
    """Creates a PIM-Flask application from a template

    :param config: Type of application configuration
    :param kwargs: PIM-Flask configuration name
    :return: Flask application
    """

    # Available configurations
    configs = ['development', 'production', 'unit_test_ci', 'unit_test_local']

    if config not in configs:
        raise Exception('{} not available!'.format(config))

    # Create the config parser
    config_parser = configparser.ConfigParser()

    # Get absolute PIM server directory name
    server_path = os.path.dirname(__file__)

    # Read from configuration file
    config_parser.read('{}/config/{}.cfg'.format(server_path, config))

    arguments = dict()

    # Add Flask configuration type to the arguments
    arguments['config'] = config

    # Establish database settings
    arguments['flask_debug']            = bool(config_parser.get('flask', 'debug'))
    arguments['flask_testing']          = bool(config_parser.get('flask', 'testing'))
    arguments['database_uri']           = config_parser.get('database', 'uri')
    arguments['aggregation_no_items']   = config_parser.get('aggregation', 'no_items')

    return create_app(**arguments)


def main():
    """Stand-alone PIM application

    """

    # Set up the command-line arguments parser
    parser = argparse.ArgumentParser(description='PIM - Webserver')

    # Create Flask option group and options
    flask_group = parser.add_argument_group('Flask', 'Flask options')

    flask_group.add_argument('--flask_host', help='Hostname of the Flask app', default='0.0.0.0')
    flask_group.add_argument('--flask_port', type=int, help='Port for the Flask app ', default=5000)
    flask_group.add_argument('--flask_debug', action='store_true', dest='flask_debug', help='Flask debugging')
    flask_group.add_argument('--flask_profile', action='store_true', dest='flask_profile', help='Flask profiling')
    flask_group.add_argument('--flask_werkzeug_logging', action='store_true', dest='flask_werkzeug_logging', help='Flask Werkzeug logging')

    # Create database option group
    database_group = parser.add_argument_group('Database', 'Database options')

    # Add options
    database_group.add_argument('--database_uri', dest='database_uri', default='', help='Database URI')

    # Create aggregates option group
    aggregates_group = parser.add_argument_group('Aggregation', 'Aggregation options')

    # Add options
    aggregates_group.add_argument('--aggregation_no_items', type=int, dest='aggregation_no_items', default=6,
                             help='Number of aggregation items')

    # Parse the command line arguments
    arguments = parser.parse_args()

    # Create the flask application and set up the database
    app = create_app(**vars(arguments))

    # Enable/disable werkzeug logging
    if not arguments.flask_werkzeug_logging:
        werkzeug_logger.disabled = True

    # Run the Flask web server
    app.run(debug=arguments.flask_debug, host=arguments.flask_host, port=int(arguments.flask_port))


if __name__ == "__main__":
    main()
