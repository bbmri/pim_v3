#  Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
#
#  PIM is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Lesser General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  PIM is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Lesser General Public License for more details.

from .testnode import TestNode
from .testlink import TestLink


class TestRun:
    """
    Run class for testing purposes
    """

    def __init__(self, **kwargs):
        self.name = kwargs.get('name', '')
        self.title = kwargs.get('title', '')
        self.description = kwargs.get('description', '')
        self.user = kwargs.get('user', '')
        self.custom_data = kwargs.get('custom_data', dict())
        self.assigned_to = kwargs.get('assigned_to', list())
        self.root = TestNode(name='root', type='group', title='Root', description='Root node', no_jobs=0)
        self.links = list()

        self.jobs = list()

    def __getstate__(self):
        return dict(name=self.name, title=self.title, description=self.description, user=self.user,
                    customData=self.custom_data, assignedTo=self.assigned_to, root=self.root, links=self.links)

    def __repr__(self):
        return str(self.__dict__)

    @staticmethod
    def ports(port_names):
        ports = list()

        for port_name in port_names:
            ports.append({'name': port_name, 'title': port_name})

        return ports

    def connect(self, from_node, from_port, to_node, to_port):
        self.links.append(TestLink(type='Image',
                                   title='Link',
                                   from_port='{}/{}'.format(from_node.path(), from_port),
                                   to_port='{}/{}'.format(to_node.path(), to_port)))

    @staticmethod
    def simulator_run(name='unit_test_run', title='Test run', description='Run for testing', user='unittest'):
        run = TestRun(name=name, user=user, title=title, description=description)

        sources = run.root.add_child(name='sources', title='Sources', no_jobs=0)

        image = sources.add_child(name='image', title='Source Image', type='SourceNode',
                                  out_ports=TestRun.ports(['dicom_image']))

        process = run.root.add_child(name='process', title='Convert', no_jobs=0)

        constant = process.add_child(name='constant', title='Constant', type='ConstantNode',
                                     out_ports=TestRun.ports(['constant']))
        convert = process.add_child(name='convert', title='DICOM to Nifti', type='ConverterNode',
                                    in_ports=TestRun.ports(['constant', 'dicom_image']),
                                    out_ports=TestRun.ports(['nifti_image']))

        sinks = run.root.add_child(name='sinks', title='Sinks', no_jobs=0)

        nifti_sink = sinks.add_child(name='save_nifti', title='Save Nifti', type='SinkNode',
                                     in_ports=TestRun.ports(['nifti_image']))

        # Connect nodes
        run.connect(image, 'dicom_image', convert, 'dicom_image')
        run.connect(constant, 'constant', convert, 'constant')
        run.connect(convert, 'nifti_image', nifti_sink, 'nifti_image')

        # Add master job
        run.root.add_job('master')

        run.jobs.extend(run.root.jobs)

        # Add leaf node jobs
        run.jobs.extend(image.jobs)
        run.jobs.extend(constant.jobs)
        run.jobs.extend(convert.jobs)
        run.jobs.extend(nifti_sink.jobs)

        return run

    @staticmethod
    def test_run(name='unit_test_run', title='Test run', description='Run for testing', user='unittest'):
        run = TestRun(name=name, user=user, title=title, description=description)

        child_a = run.root.add_child(name='child_a', )

        child_a.add_child(name='child_a')
        child_a.add_child(name='child_b')

        child_b = run.root.add_child(name='child_b')

        child_b.add_child(name='child_a')
        child_b.add_child(name='child_b')
        child_b.add_child(name='child_c')

        return run
