#  Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
#
#  PIM is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Lesser General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  PIM is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Lesser General Public License for more details.

class TestJob:
    """
    Job class for testing purposes
    """

    def __init__(self, **kwargs):
        self.title = kwargs.get('title', '')
        self.path = '{}/{}'.format(kwargs.get('path', ''), self.title)
        self.description = kwargs.get('description', '')
        self.custom_data = kwargs.get('custom_data', dict())
        self.status = kwargs.get('status', 0)

    def __getstate__(self):
        return dict(path=self.path, title=self.title, description=self.description, customData=self.custom_data,
                    status=self.status)

    def __repr__(self):
        return str(self.__dict__)
