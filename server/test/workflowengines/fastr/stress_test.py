#  Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
#
#  PIM is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Lesser General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  PIM is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Lesser General Public License for more details.

import subprocess
import atexit
import time
import sys

processes = list()


def run_network(name, no_samples):
    return subprocess.Popen(['python', 'network.py', name, str(no_samples)])


def create_processes(no_processes, no_samples):
    for i in range(no_processes):
        name = 'fastr_stress_test_{}'.format(i)
        process = run_network(name, no_samples)
        processes.append(process)
        time.sleep(1)


def wait_for_processes():
    while True:
        process_status = [process.poll() for process in processes]

        if all([x is not None for x in process_status]):
            break


def terminate_processes():
    print('terminate_processes')

    for process in processes:
        process.terminate()


if __name__ == '__main__':
    create_processes(int(sys.argv[1]), int(sys.argv[2]))
    wait_for_processes()

atexit.register(terminate_processes)