#  Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
#
#  PIM is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Lesser General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  PIM is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Lesser General Public License for more details.

from server.test.testrun import TestRun
from server.test.testjob import TestJob
from server.test.api import API

api = API('http://localhost:8080')

api.http_delete('api/runs/test')

run = TestRun.test_run(name='test', user='simulator')

api.http_post('api/runs/', data=run)

master_job = TestJob(node='root', title='master', status=1)

api.http_put('api/runs/{}/jobs'.format(run.name), data=[master_job])

master_job.status = 3

api.http_put('api/runs/{}/jobs'.format(run.name), data=[master_job])

