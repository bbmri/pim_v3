#  Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
#
#  PIM is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Lesser General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  PIM is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Lesser General Public License for more details.

import random, datetime

from random_words import LoremIpsum

li = LoremIpsum()

from server.test.testrun import TestRun
from server.test.testjob import TestJob
from server.test.api import API

api = API('http://localhost:8080')

import time
# runs = api.http_get('api/runs', params=dict(sort_column='no_jobs', page_size=1000))

# print(runs)


def delete_all_runs():
    runs = api.http_get('api/runs', params=dict(page_size=1000))['json']['runs']

    for run in runs:
        if run['name'].startswith('run'):
            api.http_delete('api/runs/{}'.format(run['name']))


batch_size = 5
timings = list()


def add_runs():
    for i in range(1):
        name = 'run_{}_jobs'.format(i)
        run = TestRun.test_run(name=name, user='unit_test_{}'.format(random.randint(0, 2)))

        api.http_post('api/runs/', data=run)

        batches = [run.jobs[i:i + batch_size] for i in range(0, len(run.jobs), batch_size)]

        for batch in batches:
            time.sleep(0.25)
            api.http_put('api/runs/{}/jobs'.format(name), data=batch)
            # timings.append(response['json']['apiRequest']['duration'])

        run.root.finalize_jobs()

        batches = [run.jobs[i:i + batch_size] for i in range(0, len(run.jobs), batch_size)]

        for batch in batches:
            time.sleep(1)
            api.http_put('api/runs/{}/jobs'.format(name), data=batch)


delete_all_runs()
add_runs()