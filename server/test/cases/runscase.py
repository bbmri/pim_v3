#  Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
#
#  PIM is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Lesser General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  PIM is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Lesser General Public License for more details.

import random, math

from .basecase import BaseTestCase
from ..testrun import TestRun
from ..testjob import TestJob


class RunsTestCase(BaseTestCase):

    def test_runs_get(self):
        """
        Test if get endpoint works properly
        """

        self.http_post('runs', data=TestRun.test_run('run_a'))
        self.http_post('runs', data=TestRun.test_run('run_b'))
        self.http_post('runs', data=TestRun.test_run('run_c'))

        runs = self.http_get('runs/')

        self.assertEqual(runs['noRuns'], 3)

    def test_filter_by_run_name(self):
        """
        Test if filtering by run name works properly
        """

        config = ['Lorem', 'ipsum', 'dolor', 'sit', 'amet']

        for id, name in enumerate(config):
            self.http_post('runs', data=TestRun.test_run(name=name))
            self.http_put('runs/{}/jobs'.format(name), data=[TestJob(path='root', title='master', status=0)])

        self.assertEqual(1, len(self.http_get('runs/', params=dict(name_filter='ip'))['runs']))
        self.assertEqual(1, len(self.http_get('runs/', params=dict(name_filter='IP'))['runs']))
        self.assertEqual(2, len(self.http_get('runs/', params=dict(name_filter='lo'))['runs']))
        self.assertEqual(2, len(self.http_get('runs/', params=dict(name_filter='LO'))['runs']))
        self.assertEqual(2, len(self.http_get('runs/', params=dict(name_filter='lor'))['runs']))
        self.assertEqual(2, len(self.http_get('runs/', params=dict(name_filter='LOR'))['runs']))
        self.assertEqual(1, len(self.http_get('runs/', params=dict(name_filter='met'))['runs']))
        self.assertEqual(3, len(self.http_get('runs/', params=dict(name_filter='m'))['runs']))

    def test_filter_by_run_user(self):
        """
        Test if filtering by run user works properly
        """

        config = ['piet', 'klaas', 'anton', 'willem', 'hendrik', 'simon', 'piet', 'klaas', 'anton', 'klaas']

        for id, user in enumerate(config):
            name = 'run_{}'.format(id)

            self.http_post('runs', data=TestRun.test_run(name=name, user=user))
            self.http_put('runs/{}/jobs'.format(name), data=[TestJob(path='root', title='master', status=0)])

        self.assertEqual(2, len(self.http_get('runs/', params=dict(user_filter='piet'))['runs']))
        self.assertEqual(3, len(self.http_get('runs/', params=dict(user_filter='klaas'))['runs']))
        self.assertEqual(2, len(self.http_get('runs/', params=dict(user_filter='anton'))['runs']))
        self.assertEqual(1, len(self.http_get('runs/', params=dict(user_filter='willem'))['runs']))
        self.assertEqual(1, len(self.http_get('runs/', params=dict(user_filter='hendrik'))['runs']))
        self.assertEqual(1, len(self.http_get('runs/', params=dict(user_filter='simon'))['runs']))

    def test_filter_by_run_status(self):
        """
        Test if filtering by run status works properly
        """

        config = {
            'run_a': 4,
            'run_b': 0,
            'run_c': 2,
            'run_d': 3,
            'run_e': 1,
            'run_f': 4
        }

        for name, status in config.items():
            self.http_post('runs', data=TestRun.test_run(name))
            self.http_put('runs/{}/jobs'.format(name), data=[TestJob(path='root', title='master', status=status)])

        self.assertEqual(1, len(self.http_get('runs/', params=dict(status_filter='["idle"]'))['runs']))
        self.assertEqual(1, len(self.http_get('runs/', params=dict(status_filter='["running"]'))['runs']))
        self.assertEqual(1, len(self.http_get('runs/', params=dict(status_filter='["success"]'))['runs']))
        self.assertEqual(1, len(self.http_get('runs/', params=dict(status_filter='["failed"]'))['runs']))
        self.assertEqual(2, len(self.http_get('runs/', params=dict(status_filter='["cancelled"]'))['runs']))
        self.assertEqual(2, len(self.http_get('runs/', params=dict(status_filter='["running", "failed"]'))['runs']))

    def test_sort_by_run_status(self):
        """
        Test if sorting by run status works properly
        """

        config = {
            'run_a': 4,
            'run_b': 0,
            'run_c': 2,
            'run_d': 3,
            'run_e': 1,
            'run_f': 4
        }

        ascending = sorted(config.values())
        descending = sorted(config.values(), reverse=True)

        for name, status in config.items():
            self.http_post('runs', data=TestRun.test_run(name))
            self.http_put('runs/{}/jobs'.format(name), data=[TestJob(path='root', title='master', status=status)])

        runs = self.http_get('runs/', params=dict(sort_column='status', sort_direction='ascending'))['runs']

        for i in ascending:
            self.assertEqual(ascending[i], runs[i]['status'])

        runs = self.http_get('runs/', params=dict(sort_column='status', sort_direction='descending'))['runs']

        for i in descending:
            self.assertEqual(descending[i], runs[i]['status'])

    def test_sort_by_run_name(self):
        """
        Test if sorting by run name works properly
        """

        config = ['gamma', 'alpha', 'ypsilon', 'beta', 'zeta', 'kappa']

        ascending = sorted(config)
        descending = sorted(config, reverse=True)

        for name in config:
            self.http_post('runs', data=TestRun.test_run(name))
            self.http_put('runs/{}/jobs'.format(name), data=[TestJob(path='root', title='master', status=0)])

        runs = self.http_get('runs/', params=dict(sort_column='name', sort_direction='ascending'))['runs']

        for id, name in enumerate(ascending):
            self.assertEqual(ascending[id], runs[id]['name'])

        runs = self.http_get('runs/', params=dict(sort_column='name', sort_direction='descending'))['runs']

        for id, name in enumerate(descending):
            self.assertEqual(descending[id], runs[id]['name'])

    def test_sort_by_run_user(self):
        """
        Test if sorting by run user works properly
        """

        config = ['piet', 'klaas', 'anton', 'willem', 'hendrik', 'simon']

        ascending = sorted(config)
        descending = sorted(config, reverse=True)

        for user in config:
            run_name = '{}_run'.format(user)

            self.http_post('runs', data=TestRun.test_run(name=run_name, user=user))
            self.http_put('runs/{}/jobs'.format(run_name), data=[TestJob(path='root', title='master', status=0)])

        runs = self.http_get('runs/', params=dict(sort_column='user', sort_direction='ascending'))['runs']

        for id, user in enumerate(ascending):
            self.assertEqual(ascending[id], runs[id]['user'])

        runs = self.http_get('runs/', params=dict(sort_column='user', sort_direction='descending'))['runs']

        for id, user in enumerate(descending):
            self.assertEqual(descending[id], runs[id]['user'])

    def test_sort_by_run_no_jobs(self):
        """
        Test if sorting by run number of jobs works properly
        """

        config = [50, 56, 35, 71, 12, 3]

        ascending = sorted(config)
        descending = sorted(config, reverse=True)

        for no_jobs in config:
            run_name = 'run_with_{}_jobs'.format(no_jobs)

            self.http_post('runs', data=TestRun.test_run(name=run_name))
            self.http_put('runs/{}/jobs'.format(run_name), data=[TestJob(path='root', title='master', status=0)])

            jobs = list()

            for i in range(no_jobs):
                jobs.append(TestJob(path='root', title='job_{}'.format(i), status=0))

            self.http_put('runs/{}/jobs'.format(run_name), data=jobs)

        runs = self.http_get('runs/', params=dict(sort_column='no_jobs', sort_direction='ascending'))['runs']

        for id, no_jobs in enumerate(ascending):
            self.assertEqual(ascending[id], runs[id]['noJobs'])

        runs = self.http_get('runs/', params=dict(sort_column='no_jobs', sort_direction='descending'))['runs']

        for id, no_jobs in enumerate(descending):
            self.assertEqual(descending[id], runs[id]['noJobs'])

    def test_sort_by_run_progress(self):
        """
        Test if sorting by run progress works properly
        """

        config = list()
        no_runs = 10
        no_jobs_run = 10

        for i in range(no_runs):
            run_name = 'run_{}'.format(i)

            self.http_post('runs', data=TestRun.test_run(name=run_name))
            self.http_put('runs/{}/jobs'.format(run_name), data=[TestJob(path='root', title='master', status=0)])

            aggregates = [0 for a in range(6)]

            jobs = list()

            for j in range(no_jobs_run):
                status = random.randint(0, 4)
                jobs.append(TestJob(path='root', title='job_{}'.format(j), status=status))
                aggregates[status] += 1

            self.http_put('runs/{}/jobs'.format(run_name), data=jobs)

            finished = aggregates[2] + aggregates[3]
            progress = finished / no_jobs_run

            config.append(progress)

        ascending = sorted(config)
        descending = sorted(config, reverse=True)

        runs = self.http_get('runs/', params=dict(sort_column='progress', sort_direction='ascending'))['runs']

        for id, i in enumerate(ascending):
            self.assertEqual(ascending[id], runs[id]['progress'])

        runs = self.http_get('runs/', params=dict(sort_column='progress', sort_direction='descending'))['runs']

        for id, i in enumerate(descending):
            self.assertEqual(descending[id], runs[id]['progress'])

    def test_pagination(self):
        """
        Test if pagination works properly
        """

        no_runs = 40

        for i in range(no_runs):
            run_name = 'run_{}'.format(i)

            self.http_post('runs', data=TestRun.test_run(name='run_{}'.format(i)))
            self.http_put('runs/{}/jobs'.format(run_name), data=[TestJob(path='root', title='master', status=0)])

        page_size = 6
        no_pages = math.floor(no_runs / page_size)
        no_runs_page = [page_size for i in range(no_pages)]
        no_runs_page.append(no_runs - (no_pages * page_size))

        for page_id in range(no_pages + 1):
            runs = self.http_get('runs/', params=dict(page_size=page_size, page_index=page_id))['runs']
            self.assertEqual(no_runs_page[page_id], len(runs))
