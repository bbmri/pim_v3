#  Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
#
#  PIM is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Lesser General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  PIM is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Lesser General Public License for more details.

from .basecase import BaseTestCase
from ..testrun import TestRun
from ..testjob import TestJob


class RunTestCase(BaseTestCase):
    def test_run_post(self):
        run = TestRun.test_run()

        self.http_post('runs', data=run)

    def test_run_get(self):
        run = TestRun.test_run()

        self.http_post('runs', data=run)
        self.http_get('runs/{}'.format(run.name))

    def test_run_delete(self):
        run = TestRun.test_run()

        self.http_post('runs', data=run)
        self.http_get('runs/{}'.format(run.name))
        self.http_delete('runs/{}'.format(run.name))
        self.http_delete('runs/{}'.format(run.name), expected_status_code=404)
        self.http_get('runs/{}'.format(run.name), expected_status_code=404)

    def test_run_status(self):
        run = TestRun.test_run()

        self.http_post('runs', data=run)

        master_job = TestJob(path='root', title='master', status=1)

        self.http_put('runs/{}/jobs'.format(run.name), data=[master_job])

        self.assertEqual(1, self.http_get('runs/{}'.format(run.name))['run']['status'])

        master_job.status = 3

        self.http_put('runs/{}/jobs'.format(run.name), data=[master_job])

        self.assertEqual(3, self.http_get('runs/{}'.format(run.name))['run']['status'])

    def test_run_name(self):
        pass

    def test_run_user(self):
        pass

    def test_run_number_of_jobs(self):
        run = TestRun.test_run()

        self.http_post('runs', data=run)

        jobs = list()

        jobs.append(TestJob(path='root/child_a/child_a', title='job_0', status=3))
        jobs.append(TestJob(path='root/child_a/child_b', title='job_0', status=2))
        jobs.append(TestJob(path='root/child_b/child_a', title='job_0', status=1))
        jobs.append(TestJob(path='root/child_b/child_b', title='job_0', status=1))
        jobs.append(TestJob(path='root/child_b/child_c', title='job_0', status=4))
        jobs.append(TestJob(path='root/child_b/child_c', title='job_1', status=0))

        self.http_put('runs/{}/jobs'.format(run.name), data=jobs)

        self.assertEqual(6, self.http_get('runs/{}'.format(run.name))['run']['noJobs'])

    def test_run_progress(self):
        run = TestRun.test_run()

        self.http_post('runs', data=run)

        jobs = list()

        jobs.append(TestJob(path='root/child_a/child_a', title='job_0', status=3))
        jobs.append(TestJob(path='root/child_a/child_b', title='job_0', status=2))
        jobs.append(TestJob(path='root/child_b/child_a', title='job_0', status=1))
        jobs.append(TestJob(path='root/child_b/child_b', title='job_0', status=1))
        jobs.append(TestJob(path='root/child_b/child_c', title='job_0', status=4))
        jobs.append(TestJob(path='root/child_b/child_c', title='job_1', status=0))

        self.http_put('runs/{}/jobs'.format(run.name), data=jobs)

        self.assertAlmostEqual(1 / 3, self.http_get('runs/{}'.format(run.name))['run']['progress'])

    def test_run_aggregates(self):
        run = TestRun.test_run()

        self.http_post('runs', data=run)

        jobs = list()

        jobs.append(TestJob(path='root/child_a/child_a', title='job_0', status=3))
        jobs.append(TestJob(path='root/child_a/child_b', title='job_0', status=2))
        jobs.append(TestJob(path='root/child_b/child_a', title='job_0', status=1))
        jobs.append(TestJob(path='root/child_b/child_b', title='job_0', status=1))
        jobs.append(TestJob(path='root/child_b/child_c', title='job_0', status=4))
        jobs.append(TestJob(path='root/child_b/child_c', title='job_1', status=0))

        self.http_put('runs/{}/jobs'.format(run.name), data=jobs)

        self.assertEqual([1, 2, 1, 1, 1, 0], self.http_get('runs/{}'.format(run.name))['run']['aggregates'])
