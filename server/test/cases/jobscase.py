#  Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
#
#  PIM is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Lesser General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  PIM is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Lesser General Public License for more details.

from .basecase import BaseTestCase
from ..testrun import TestRun
from ..testjob import TestJob


class JobsTestCase(BaseTestCase):

    def test_put(self):
        run = TestRun.test_run()

        self.http_post('runs', data=run)

        jobs = list()

        for i in range(10):
            jobs.append(TestJob(path='root/child_a', title='job_{}'.format(i), status=0))

        self.http_put('runs/{}/jobs'.format(run.name), data=jobs)

    def test_get(self):
        run = TestRun.test_run()

        self.http_post('runs', data=run)

        jobs = list()

        jobs.append(TestJob(path='root/child_a', title='job', status=0))

        self.http_put('runs/{}/jobs'.format(run.name), data=jobs)
        self.http_get('runs/{}/job'.format(run.name), params=dict(path='root/child_a/job'))

    def test_jobs_custom_data(self):
        run = TestRun.test_run()

        self.http_post('runs', data=run)

        job_with_custom_data = TestJob(path='root/child_a', title='job', status=0, custom_data=dict(logs=dict(a='Log A')))

        self.http_put('runs/{}/jobs'.format(run.name), data=[job_with_custom_data])

        job_with_custom_data = TestJob(path='root/child_a', title='job', status=0, custom_data=dict(logs=dict(b='Log B')))

        # self.http_put('runs/{}/jobs'.format(run.name), data=[job_with_custom_data])

        # job = self.http_get('runs/{}/job'.format(run.name), params=dict(path='root/child_a/job'))

        # print(job)
        # self.assertEqual(True, 'a' in job['job']['customData']['logs'])
        # self.assertEqual(True, 'b' in job['job']['customData']['logs'])
