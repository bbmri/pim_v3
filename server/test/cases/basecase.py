#  Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
#
#  PIM is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Lesser General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  PIM is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Lesser General Public License for more details.

import json, unittest

from server.app import *
from server.test.api import API


class BaseTestCase(unittest.TestCase):
    def setUp(self):
        """
        This method is run once before each test method is executed
        """

        # Get PIM config type from environment
        pim_config = os.environ.get('PIM_CONFIG', 'unit_test_ci')

        # Disable werkzeug logging
        werkzeug_logger.disabled = True

        # Create testing Flask application
        self.app = create_app_from_config(pim_config)

        self.test_client = self.app.test_client()

        self.api = API()

        # Create tables
        orm.Base.metadata.create_all(bind=self.app.db_engine)

    def tearDown(self):
        """
        This method is run once after each test method is executed
        """

        # Remove tables
        orm.Base.metadata.drop_all(bind=self.app.db_engine)

    def http_post(self, endpoint, **kwargs):
        """
        Post HTTP request

        :param endpoint: Endpoint URL
        :param kwargs
        """

        expected_status_code = kwargs.get('expected_status_code', 200)
        data = kwargs.get('data', None)

        http_request = self.api.http_post('/api/{}/'.format(endpoint), data=data, test_case=self)

        result = json.loads(http_request.get_data(as_text=True)) if http_request.status_code is 200 else None

        self.assertEqual(expected_status_code, http_request.status_code, msg=result)

        return result

    def http_put(self, endpoint, **kwargs):
        """
        Put HTTP request

        :param endpoint: Endpoint URL
        :param kwargs: Options
        """

        expected_status_code = kwargs.get('expected_status_code', 200)
        data = kwargs.get('data', None)

        http_request = self.api.http_put('/api/{}'.format(endpoint), data=data, test_case=self)

        result = json.loads(http_request.get_data(as_text=True))# if http_request.status_code is 200 else None

        self.assertEqual(expected_status_code, http_request.status_code, msg=result)

        return result

    def http_get(self, endpoint, **kwargs):
        """
        Get HTTP request

        :param endpoint: Endpoint URL
        :param kwargs: Options
        """

        expected_status_code = kwargs.get('expected_status_code', 200)

        http_request = self.api.http_get('/api/{}'.format(endpoint), test_case=self, params=kwargs.get('params', None))

        result = json.loads(http_request.get_data(as_text=True)) if http_request.status_code is 200 else None

        self.assertEqual(expected_status_code, http_request.status_code, msg=result)

        return result

    def http_delete(self, endpoint, **kwargs):
        """
        Delete HTTP request

        :param endpoint: Endpoint URL
        :param kwargs: Options
        """

        expected_status_code = kwargs.get('expected_status_code', 200)

        http_request = self.test_client.delete('/api/{}'.format(endpoint))

        result = json.loads(http_request.get_data(as_text=True)) if http_request.status_code is 200 else None

        self.assertEqual(expected_status_code, http_request.status_code, msg=result)

        return result
