#  Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
#
#  PIM is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Lesser General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  PIM is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Lesser General Public License for more details.

from .basecase import BaseTestCase
from ..testrun import TestRun
from ..testjob import TestJob


class AggregatesTestCase(BaseTestCase):
    def _status(self, run_name):
        return self.http_get('runs/{}/status'.format(run_name))

    def _assert_status(self, run_name, node_name, expected_aggregates):
        self.assertEqual(expected_aggregates, self._status(run_name)['status'][node_name])

    def test_jobs(self):
        run = TestRun.test_run()

        self.http_post('runs', data=run)

        self._assert_status(run.name, 'root', [0, 0, 0, 0, 0, 0])

        jobs = list()

        jobs.append(TestJob(path='root/child_a/child_a', title='job_0', status=3))
        jobs.append(TestJob(path='root/child_a/child_b', title='job_0', status=2))
        jobs.append(TestJob(path='root/child_b/child_a', title='job_0', status=1))
        jobs.append(TestJob(path='root/child_b/child_b', title='job_0', status=1))
        jobs.append(TestJob(path='root/child_b/child_c', title='job_0', status=4))
        jobs.append(TestJob(path='root/child_b/child_c', title='job_1', status=0))

        self.http_put('runs/{}/jobs'.format(run.name), data=jobs)

        self._assert_status(run.name, 'root', [1, 2, 1, 1, 1, 0])
        self._assert_status(run.name, 'root/child_a', [0, 0, 1, 1, 0, 0])
        self._assert_status(run.name, 'root/child_a/child_a', [0, 0, 0, 1, 0, 0])
        self._assert_status(run.name, 'root/child_a/child_b', [0, 0, 1, 0, 0, 0])
        self._assert_status(run.name, 'root/child_b', [1, 2, 0, 0, 1, 0])
        self._assert_status(run.name, 'root/child_b/child_a', [0, 1, 0, 0, 0, 0])
        self._assert_status(run.name, 'root/child_b/child_b', [0, 1, 0, 0, 0, 0])
        self._assert_status(run.name, 'root/child_b/child_c', [1, 0, 0, 0, 1, 0])

    def test_multiple_identical_jobs(self):
        run = TestRun.test_run()

        self.http_post('runs', data=run)

        self._assert_status(run.name, 'root', [0, 0, 0, 0, 0, 0])

        jobs = [TestJob(path='root/child_a/child_a', title='job_0', status=3) for j in range(3)]

        self.http_put('runs/{}/jobs'.format(run.name), data=jobs)

        self._assert_status(run.name, 'root', [0, 0, 0, 1, 0, 0])

    def test_multiple_identical_jobs_with_varying_status(self):
        run = TestRun.test_run()

        self.http_post('runs', data=run)

        self._assert_status(run.name, 'root', [0, 0, 0, 0, 0, 0])

        jobs = list()

        jobs.append(TestJob(path='root/child_a/child_a', title='job_0', status=0))
        jobs.append(TestJob(path='root/child_a/child_a', title='job_0', status=1))
        jobs.append(TestJob(path='root/child_a/child_a', title='job_0', status=2))

        self.http_put('runs/{}/jobs'.format(run.name), data=jobs)

        self._assert_status(run.name, 'root', [0, 0, 1, 0, 0, 0])

    def test_run_modified(self):
        run = TestRun.test_run()

        self.http_post('runs', data=run)

        self._assert_status(run.name, 'root', [0, 0, 0, 0, 0, 0])

        job = TestJob(path='root/child_a/child_a', title='job', status=3)

        self.http_put('runs/{}/jobs'.format(run.name), data=[job])

        run_result = self.http_get('runs/{}'.format(run.name))
        job_result = self.http_get('runs/{}/job'.format(run.name), params=dict(path=job.path))

        self.assertEqual(run_result['run']['modified'], job_result['job']['modified'])

    def test_reference_timestamp(self):
        run = TestRun.test_run()

        self.http_post('runs', data=run)

        # self._assert_status(run.name, 'root', [0, 0, 0, 0, 0, 0])

        test_job_a = TestJob(path='root/child_a/child_a', title='job', status=3)

        self.http_put('runs/{}/jobs'.format(run.name), data=[test_job_a])

        status_a = self._status(run.name)

        self.assertEqual(8, len(self._status(run.name)['status']))

        test_job_b = TestJob(path='root/child_a', title='job', status=3)

        self.http_put('runs/{}/jobs'.format(run.name), data=[test_job_b])

        status_b = self.http_get('runs/{}/status'.format(run.name),
                                     params=dict(reference_timestamp=status_a['timestamp']))

        self.assertEqual(2, len(status_b['status']))

        self.http_put('runs/{}/jobs'.format(run.name), data=[TestJob(path='root/child_a', title='job', status=0)])

        self.http_get('runs/{}/status'.format(run.name),
                                 params=dict(reference_timestamp=status_a['timestamp']))

        test_job_c = TestJob(path='root/child_b/child_c', title='job', status=1)

        self.http_put('runs/{}/jobs'.format(run.name), data=[test_job_c])

        status_c = self.http_get('runs/{}/status'.format(run.name),
                                     params=dict(reference_timestamp=status_a['timestamp']))

        self.assertEqual(4, len(status_c['status']))
