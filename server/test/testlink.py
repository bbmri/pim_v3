#  Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
#
#  PIM is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Lesser General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  PIM is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Lesser General Public License for more details.

from random_words import RandomNicknames, RandomWords, LoremIpsum

rn = RandomNicknames()
rw = RandomWords()
li = LoremIpsum()


class TestLink:
    """
    Link class for testing purposes
    """

    def __init__(self, **kwargs):
        self.data_type = kwargs.get('data_type', 'undefined')
        self.title = kwargs.get('title', rn.random_nick(gender='u'))
        self.description = kwargs.get('description', li.get_sentence())
        self.custom_data = kwargs.get('custom_data', dict())
        self.from_port = kwargs.get('from_port', '')
        self.to_port = kwargs.get('to_port', '')

    def __getstate__(self):
        return dict(dataType=self.data_type, title=self.title, description=self.description, customData=self.custom_data,
                    fromPort=self.from_port, toPort=self.to_port)

    def __repr__(self):
        return str(self.__dict__)
