#  Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
#
#  PIM is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Lesser General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  PIM is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Lesser General Public License for more details.

import json, requests, jsonpickle

from urllib.parse import urlencode


class API:
    def __init__(self, pim_host=''):
        """
        :param pim_host: PIM host URL
        """

        self.pim_host = pim_host

    def http_post(self, endpoint, **kwargs):
        """
        Post HTTP request

        :param endpoint: Endpoint URL
        :param kwargs Optional arguments
        """

        test_case = kwargs.get('test_case', None)
        data = kwargs.get('data', None)
        json_data = jsonpickle.encode(data, unpicklable=False, max_depth=100)

        if test_case is not None:
            headers = [('Content-Type', 'application/json'), ('Content-Length', len(json_data))]

            return test_case.test_client.post(endpoint, headers=headers, data=json_data)
        else:
            url = '{}/{}'.format(self.pim_host, endpoint)
            response = requests.post(url, json=json.loads(json_data))
            return dict(response=response, json=json.loads(response.text) if response.status_code is 200 else None)

    def http_put(self, endpoint, **kwargs):
        """
        Put HTTP request

        :param endpoint: Endpoint URL
        :param kwargs: Optional arguments
        """

        test_case = kwargs.get('test_case', None)
        data = kwargs.get('data', None)
        json_data = jsonpickle.encode(data, unpicklable=False)

        if test_case is not None:
            headers = [('Content-Type', 'application/json'), ('Content-Length', len(json_data))]
            return test_case.test_client.put(endpoint, headers=headers, data=json_data)
        else:
            url = '{}/{}'.format(self.pim_host, endpoint)
            response = requests.put(url, json=json.loads(json_data))
            return dict(response=response, json=json.loads(response.text) if response.status_code is 200 else None)

    def http_get(self, endpoint, **kwargs):
        """
        Get HTTP request

        :param endpoint: Endpoint URL
        :param kwargs: Optional arguments
        """

        test_case = kwargs.get('test_case', None)
        params = kwargs.get('params', None)

        if test_case is not None:
            url = endpoint

            if params is not None:
                url = '{}?{}'.format(url, urlencode(params))

            return test_case.test_client.get(url)
        else:
            response = requests.get('{}/{}'.format(self.pim_host, endpoint), params=params)
            return dict(response=response, json=json.loads(response.text) if response.status_code is 200 else None)

    def http_delete(self, endpoint, **kwargs):
        """
        Delete HTTP request

        :param endpoint: Endpoint URL
        :param kwargs: Optional arguments
        """

        test_case = kwargs.get('test_case', None)

        if test_case is not None:
            return self.test_client.delete('/api/{}'.format(endpoint))
        else:
            response = requests.delete('{}/{}'.format(self.pim_host, endpoint))
            return dict(response=response, json=json.loads(response.text) if response.status_code is 200 else None)
