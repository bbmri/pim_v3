#  Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
#
#  PIM is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Lesser General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  PIM is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Lesser General Public License for more details.

import random

from .testjob import TestJob

from random_words import RandomNicknames, RandomWords, LoremIpsum

rn = RandomNicknames()
rw = RandomWords()
li = LoremIpsum()


class PDF:
    def __init__(self, probabilities):
        self.pdf = []

        for id, sum in enumerate(probabilities):
            for i in range(sum):
                self.pdf.append(id)

    def sample(self):
        return random.choice(self.pdf)


class Probabilities:
    def __init__(self, probabilities):
        self.p = probabilities


pdfs = list()

pdfs.append(PDF([100, 10, 1, 1, 0, 0]))
pdfs.append(PDF([0, 0, 100, 5, 1, 0]))
pdfs.append(PDF([0, 0, 0, 10, 0, 0]))
pdfs.append(PDF([3, 10, 50, 5, 10, 0]))
pdfs.append(PDF([0, 0, 0, 0, 0, 1]))


class TestNode:
    """
    Node class for testing purposes
    """

    def __init__(self, **kwargs):
        self.name = kwargs.get('name', '')
        self.title = kwargs.get('title', rn.random_nick(gender='u'))
        self.type = kwargs.get('type', rw.random_word())
        self.description = kwargs.get('description', li.get_sentence())
        self.custom_data = kwargs.get('custom_data', dict())
        self.children = list()
        self.in_ports = kwargs.get('in_ports', list())
        self.out_ports = kwargs.get('out_ports', list())
        self.parent = kwargs.get('parent', None)
        self.jobs = list()
        self.random_pdf = random.choice(pdfs)

        for i in range(kwargs.get('no_jobs', 100)):
            self.add_job('job_{}'.format(i))

    def add_child(self, **kwargs):
        kwargs['parent'] = self

        child = TestNode(**kwargs)
        self.children.append(child)

        return child

    def add_job(self, title):
        self.jobs.append(TestJob(path=self.path(), title=title, status=0))

    def finalize_jobs(self):
        for job in self.jobs:
            job.status = self.random_pdf.sample()

        for child in self.children:
            child.finalize_jobs()

    def path(self):
        """
        Computes the absolute node path
        """

        if not self.parent:
            return self.name

        parent_path = self.parent.path()

        return '{}/{}'.format(parent_path, self.name)

    def __getstate__(self):
        return dict(name=self.name, title=self.title, type=self.type, description=self.description, customData=self.custom_data,
                    children=self.children, inPorts=self.in_ports, outPorts=self.out_ports)

    def __repr__(self):
        return str(self.__dict__)
