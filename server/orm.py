
#  Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
#
#  PIM is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Lesser General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  PIM is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Lesser General Public License for more details.

import datetime

from sqlalchemy import Column, String, Text, DateTime, ForeignKey, Float, Integer, ARRAY, func
from sqlalchemy.orm import relationship, backref, column_property
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import select, and_, cast, case, not_
from sqlalchemy_utils.types.json import JSONType

Base = declarative_base()


class Node(Base):
    __tablename__ = "nodes"

    id          = Column(Integer(), primary_key=True, autoincrement=True, index=True)
    path        = Column(String(256), nullable=True, unique=True, index=True)
    title       = Column(String(128), nullable=True)
    description = Column(Text(), nullable=True)
    created     = Column(DateTime, default=datetime.datetime.utcnow(), index=True)
    modified    = Column(DateTime, default=datetime.datetime.utcnow(), index=True)
    custom_data = Column(JSONType)
    type        = Column(String(128), index=True)
    parent_id   = Column(Integer(), ForeignKey('nodes.id'), index=True)
    run_id      = Column(Integer(), ForeignKey('runs.id', ondelete="CASCADE"), index=True)
    level       = Column(Integer())
    aggregates  = Column(ARRAY(Integer))

    children    = relationship('Node', backref=backref('parent', remote_side='Node.id'))
    jobs        = relationship('Job', backref='node', cascade='all, delete')
    in_ports    = relationship('InPort', backref='node', cascade='all, delete')
    out_ports   = relationship('OutPort', backref='node', cascade='all, delete')


class Job(Base):
    __tablename__ = "jobs"

    id              = Column(Integer(), primary_key=True, autoincrement=True, index=True)
    path            = Column(String(256), nullable=True, unique=True, index=True)
    title           = Column(String(128), nullable=True)
    description     = Column(Text(), nullable=True)
    created         = Column(DateTime, default=datetime.datetime.utcnow(), index=True)
    modified        = Column(DateTime, default=datetime.datetime.utcnow(), index=True)
    custom_data     = Column(JSONType)
    run_id          = Column(Integer(), ForeignKey('runs.id', ondelete="CASCADE"), nullable=False, index=True)
    node_id         = Column(Integer(), index=True)
    node_path       = Column(String(256), ForeignKey('nodes.path'), nullable=False, index=True)
    status          = Column(Integer, index=True)
    duration        = column_property(modified - created)


class Run(Base):
    __tablename__ = "runs"

    id          = Column(Integer(), primary_key=True, autoincrement=True)
    name        = Column(String(128), nullable=False, unique=True)
    title       = Column(String(128), nullable=True)
    description = Column(Text(), nullable=True)
    created     = Column(DateTime, default=datetime.datetime.utcnow(), index=True)
    modified    = Column(DateTime, default=datetime.datetime.utcnow(), index=True)
    custom_data = Column(JSONType)
    user        = Column(String(128), nullable=True)

    nodes   = relationship('Node', backref='run', order_by='Node.id', cascade='all, delete')
    links   = relationship('Link', backref='run', cascade='all, delete')

    nodes_tbl = Node.__table__
    jobs_tbl = Job.__table__

    finished = nodes_tbl.c.aggregates[3] + nodes_tbl.c.aggregates[4]

    # Total number of jobs in total: idle + running + success + failed + cancelled + undefined
    total = nodes_tbl.c.aggregates[1] + nodes_tbl.c.aggregates[2] + nodes_tbl.c.aggregates[3] + \
            nodes_tbl.c.aggregates[4] + nodes_tbl.c.aggregates[5] + nodes_tbl.c.aggregates[6]

    # Compute progress and avoid division by zero
    progress = case([(total == 0, 0)], else_=(finished / cast(total, Float)))

    # Create query to obtain the progress using the root aggregate node
    aggregate_progress_qry = select([progress]).where(
        and_(nodes_tbl.c.run_id == id, nodes_tbl.c.level == 0)).select_from(nodes_tbl)

    # Create duration column
    duration = column_property(modified - created)

    jobs_qry = select([jobs_tbl.c.id]).where(jobs_tbl.c.run_id == id)

    # Create query to count the number of jobs
    no_jobs_qry = select([func.count()]).select_from(jobs_tbl).where(and_(jobs_tbl.c.run_id == id, not_(jobs_tbl.c.path.like('%/root/master%'))))

    # Create number of jobs column
    no_jobs = column_property(no_jobs_qry)

    # Create progress column
    progress = column_property(aggregate_progress_qry)

    status_qry = select([jobs_tbl.c.status], and_(jobs_tbl.c.run_id == id, jobs_tbl.c.path.like('%/root/master%'))).as_scalar()

    status = column_property(status_qry)


class InPort(Base):
    __tablename__ = 'in_ports'

    id          = Column(Integer(), primary_key=True, autoincrement=True)
    path        = Column(String(256), nullable=False, unique=True)
    title       = Column(String(128), nullable=True)
    description = Column(Text(), nullable=True)
    created     = Column(DateTime, default=datetime.datetime.utcnow())
    modified    = Column(DateTime, default=datetime.datetime.utcnow())
    custom_data = Column(JSONType)
    node_id     = Column(Integer(), ForeignKey('nodes.id', ondelete="CASCADE"))

    links = relationship('Link', backref='to_port')


class OutPort(Base):
    __tablename__ = 'out_ports'

    id          = Column(Integer(), primary_key=True, autoincrement=True)
    path        = Column(String(256), nullable=False, unique=True)
    title       = Column(String(128), nullable=True)
    description = Column(Text(), nullable=True)
    created     = Column(DateTime, default=datetime.datetime.utcnow())
    modified    = Column(DateTime, default=datetime.datetime.utcnow())
    custom_data = Column(JSONType)
    node_id     = Column(Integer(), ForeignKey('nodes.id', ondelete="CASCADE"))

    links = relationship('Link', backref='from_port')


class Link(Base):
    __tablename__ = "links"

    id                  = Column(Integer(), primary_key=True, autoincrement=True)
    title               = Column(String(128), nullable=True)
    description         = Column(Text(), nullable=True)
    created             = Column(DateTime, default=datetime.datetime.utcnow())
    modified            = Column(DateTime, default=datetime.datetime.utcnow())
    custom_data         = Column(JSONType)
    run_id              = Column(Integer(), ForeignKey('runs.id', ondelete="CASCADE"), nullable=False)
    from_port_id        = Column(String(256), ForeignKey('out_ports.path'), nullable=False)
    to_port_id          = Column(String(256), ForeignKey('in_ports.path'), nullable=False)
    data_type           = Column(String(128), nullable=True)
    common_ancestor     = Column(String(256), ForeignKey('nodes.path'), nullable=False)


class LinkNodeRef(Base):
    """Temporary table for the links endpoint"""

    __tablename__ = "link_node_refs"

    id          = Column(Integer(), primary_key=True, autoincrement=True)
    node_id     = Column(Integer(), nullable=True, index=True)
    node_path   = Column(String(256), nullable=False, index=True)
    created     = Column(DateTime, default=datetime.datetime.utcnow())