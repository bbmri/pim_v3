#  Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
#
#  PIM is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Lesser General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  PIM is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Lesser General Public License for more details.

from flask import g, current_app

import server.orm as orm

# SQL Alchemy imports
from sqlalchemy import select, and_, bindparam
from sqlalchemy.orm import aliased


def deep_merge(source, destination):
    for key, value in source.items():
        if isinstance(value, dict):
            node = destination.setdefault(key, {})
            deep_merge(value, node)
        else:
            destination[key] = value

    return destination


class UpdateNode:
    def __init__(self, row):
        """
        Initialize update node from database record

        :param node: Node record
        """

        self.data = row

        self.jobs = list()

    @staticmethod
    def unset_job_status(nodes, node, status, job):
        # root/master job is not considered in the aggregation process
        if job.path != 'root/master':
            node.data['aggregates'][status] -= 1

        parent_id = node.data['parent_id']

        if parent_id is not None:
            UpdateNode.unset_job_status(nodes, nodes[parent_id], status, job)

    @staticmethod
    def set_job_status(nodes, node, job):
        job_hist = '{}{}'.format(job.absolute_path, job.status)

        if job_hist in node.jobs:
            return

        # root/master job is not considered in the aggregation process
        if job.path != 'root/master':
            node.data['aggregates'][job.status] += 1

        parent_id = node.data['parent_id']

        if parent_id is not None:
            UpdateNode.set_job_status(nodes, nodes[parent_id], job)

        node.jobs.append(job_hist)

    def __str__(self):
        return str(self.__dict__)


class UpdateJob:
    def __init__(self, run_name, run_id, api_job, time_stamp, nodes, node_ids, existing_jobs):
        """
        Initialize update job from REST API job

        :param run_name: Name of the containing run
        :param api_job: REST API job
        :param time_stamp: Timestamp
        """

        self.run_name = run_name
        self.run_id = run_id
        self.path = api_job.get('path', '')
        self.absolute_path = '{}/{}'.format(run_name, self.path)
        self.node_path = '/'.join(self.absolute_path.split('/')[:-1])
        self.node_id = node_ids[self.node_path]
        self.time_stamp = time_stamp

        self.should_insert = self.absolute_path not in existing_jobs
        self.status_changed = False

        if self.should_insert:
            self.title = api_job.get('title', '')
            self.description = api_job.get('description', None)
            self.custom_data = api_job.get('customData', dict())
            self.status = api_job.get('status', None)

            if self.title == '':
                self.title = self.path.split('/')[-1]

            if self.status is not None:
                UpdateNode.set_job_status(nodes, nodes[self.node_id], self)
        else:
            existing_job = existing_jobs[self.absolute_path]

            if existing_job['status'] is not None:
                UpdateNode.unset_job_status(nodes, nodes[self.node_id], existing_job['status'], self)

            if 'title' in api_job and api_job['title'] is not '':
                self.title = api_job['title']
            else:
                self.title = existing_job['title']

            self.description = api_job.get('description', existing_job['description'])

            if 'customData' in api_job:
                # if existing_job['custom_data'] is not None:
                self.custom_data = existing_job['custom_data']

                deep_merge(api_job['customData'], self.custom_data)

                # if 'master' in self.path:
                #     current_app.logger.info(self.custom_data)
            else:
                self.custom_data = existing_job['custom_data']

            if 'status' in api_job:
                if api_job['status'] is not existing_job['status']:
                    self.status_changed = True
                    self.status = api_job['status']
                else:
                    self.status = existing_job['status']
            else:
                self.status = existing_job['status']

            if self.status is not None:
                UpdateNode.set_job_status(nodes, nodes[self.node_id], self)

    def update_record(self):
        update_job = dict()

        update_job['_path'] = self.absolute_path
        update_job['title'] = self.title
        update_job['description'] = self.description
        update_job['modified'] = self.time_stamp
        update_job['custom_data'] = self.custom_data
        update_job['status'] = self.status

        return update_job

    def insert_record(self):
        insert_job = dict()

        insert_job['path'] = self.absolute_path
        insert_job['title'] = self.title
        insert_job['description'] = self.description
        insert_job['created'] = self.time_stamp
        insert_job['modified'] = self.time_stamp
        insert_job['custom_data'] = self.custom_data
        insert_job['run_id'] = self.run_id
        insert_job['node_id'] = self.node_id
        insert_job['node_path'] = self.node_path
        insert_job['status'] = self.status

        return insert_job

    def __str__(self):
        return str(self.__dict__)


class UpdateJobs:
    """
    Helper class for updating the job status and status aggregation in the database
    """

    @staticmethod
    def __jobs(run_id, run_name, api_jobs, time_stamp, nodes, node_ids, existing_jobs):
        return [UpdateJob(run_name, run_id, api_job, time_stamp, nodes, node_ids, existing_jobs) for api_job in api_jobs]

    @staticmethod
    def __nodes(run_id, run_name, api_jobs):
        nodes = dict()

        leaf_node_ids = set()

        for api_job in api_jobs:
            node_id = '{}/{}'.format(run_name, '/'.join(api_job['path'].split('/')[:-1]))
            leaf_node_ids.add(node_id)

        nodes_tbl = orm.Node.__table__

        nodes_alias = aliased(orm.Node, name='nodes_alias')

        status_cte = select([
            nodes_tbl.c.id,
            nodes_tbl.c.parent_id,
            nodes_tbl.c.aggregates]). \
            where(and_(nodes_tbl.c.run_id == run_id, nodes_tbl.c.path.in_(tuple(leaf_node_ids)))). \
            cte(recursive=True)

        nodes_cte_alias = status_cte.alias()

        status_cte = status_cte.union_all(
            select([
                nodes_alias.id,
                nodes_alias.parent_id,
                nodes_cte_alias.c.aggregates]).where(nodes_alias.id == nodes_cte_alias.c.parent_id))

        for row in g.db_session.execute(select([nodes_tbl], nodes_tbl.c.id == status_cte.c.id)):
            node = dict(row)

            nodes[row[0]] = UpdateNode(node)

        return nodes

    @staticmethod
    def _existing_jobs(run_name, api_jobs):
        jobs_tbl = orm.Job.__table__

        job_paths = set()

        for api_job in api_jobs:
            job_paths.add('{}/{}'.format(run_name, api_job['path']))

        existing_jobs = dict()

        for row in g.db_session.execute(select([jobs_tbl], jobs_tbl.c.path.in_(tuple(job_paths)))):
            existing_jobs[row.path] = dict(status=row.status, title=row.title, description=row.description,
                                           custom_data=row.custom_data)

        return existing_jobs

    @staticmethod
    def _node_ids(run_name, api_jobs):
        nodes_tbl = orm.Node.__table__

        node_paths = set()

        for api_job in api_jobs:
            node_path = '/'.join(api_job['path'].split('/')[:-1])
            node_paths.add('{}/{}'.format(run_name, node_path))

        node_ids = dict()

        for row in g.db_session.execute(
                select([nodes_tbl.c.path, nodes_tbl.c.id], nodes_tbl.c.path.in_(tuple(node_paths)))):
            node_ids[row.path] = row.id

        return node_ids

    @staticmethod
    def _insert_jobs(jobs):
        jobs_tbl = orm.Job.__table__

        insert_jobs = list()
        paths = list()

        for job in jobs:
            if job.should_insert and (job.path not in paths):
                insert_jobs.append(job.insert_record())
                paths.append(job.path)

        if len(insert_jobs) > 0:
            g.db_session.execute(jobs_tbl.insert().values(insert_jobs))
            g.db_session.commit()

    @staticmethod
    def _update_jobs(jobs):
        jobs_tbl = orm.Job.__table__

        update_jobs = list()

        for job in jobs:
            update_jobs.append(job.update_record())

        if len(update_jobs) > 0:
            update_jobs_qry = jobs_tbl.update() \
                .where(jobs_tbl.c.path == bindparam('_path'))

            g.db_session.execute(update_jobs_qry, update_jobs)
            g.db_session.commit()

    @staticmethod
    def _update_nodes(nodes, time_stamp):
        update_nodes = list()

        for id, node in nodes.items():
            update_nodes.append(dict(_id=node.data['id'], aggregates=node.data['aggregates'], modified=time_stamp))

        if len(update_nodes) > 0:
            update_nodes_qry = orm.Node.__table__.update() \
                .where(orm.Node.__table__.c.id == bindparam('_id')) \
                .values({'aggregates': bindparam('aggregates'), 'modified': bindparam('modified')})

            g.db_session.execute(update_nodes_qry, update_nodes)
            g.db_session.commit()

    @staticmethod
    def _flag_run_modified(run_id, time_stamp):
        flag_run_modified_qry = orm.Run.__table__ \
            .update() \
            .values(dict(modified=time_stamp)) \
            .where(orm.Run.__table__.c.id == run_id)

        g.db_session.execute(flag_run_modified_qry)
        g.db_session.commit()

    @staticmethod
    def sanitize_api_jobs(api_jobs):
        """
        Sanitizes the API jobs payload (removal of duplicates)

        :param api_jobs:
        """

        jobs = dict()

        for api_job in api_jobs:
            if api_job['path'] not in jobs:
                jobs[api_job['path']] = dict()

            job = jobs[api_job['path']]

            if 'path' in api_job:
                job['path'] = api_job['path']

            if 'title' in api_job:
                job['title'] = api_job['title']

            if 'description' in api_job:
                job['description'] = api_job['description']

            if 'status' in api_job:
                job['status'] = api_job['status']

            if 'customData' in api_job:
                if 'customData' in job:
                    if 'customData' in api_job:
                        job['customData'] = deep_merge(api_job['customData'], job['customData'])
                else:
                    job['customData'] = api_job['customData']

        return jobs.values()

    @staticmethod
    def update_jobs(run_id, run_name, time_stamp, api_jobs):
        """
        Updates the jobs, as received from the REST API, in the database
        If necessary, this function will create new jobs in the database if non-existent. It will also aggregate the
        node status for the nodes that are affected by the jobs.

        :param run_id: Index of the run that contains the jobs
        :param run_name: Name of the run that contains the jobs
        :param time_stamp: Time stamp at which the jobs are updated (also used in the database)
        :param api_jobs: List of jobs as received from the REST API
        """

        nodes = UpdateJobs.__nodes(run_id, run_name, api_jobs)

        node_paths = set()

        for api_job in api_jobs:
            node_path = '/'.join(api_job['path'].split('/')[:-1])
            node_paths.add('{}/{}'.format(run_name, node_path))

        node_ids = UpdateJobs._node_ids(run_name, api_jobs)

        existing_jobs = UpdateJobs._existing_jobs(run_name, api_jobs)

        jobs = UpdateJobs.__jobs(run_id, run_name, api_jobs, time_stamp, nodes, node_ids, existing_jobs)

        UpdateJobs._insert_jobs(jobs)
        UpdateJobs._update_jobs(jobs)
        UpdateJobs._update_nodes(nodes, time_stamp)
        UpdateJobs._flag_run_modified(run_id, time_stamp)
