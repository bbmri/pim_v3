#  Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
#
#  PIM is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Lesser General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  PIM is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Lesser General Public License for more details.

from flask import g

import server.orm as orm


class DeleteRun:
    """
    Helper class for removing a run from the database
    """

    @staticmethod
    def delete_run(run_id):
        g.db_session.query(orm.Run).filter(orm.Run.id == run_id).delete(synchronize_session=False)
        g.db_session.commit()
