#  Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
#
#  PIM is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Lesser General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  PIM is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Lesser General Public License for more details.

from .updatejobs import *


def get_status(run, reference_timestamp=None):
    """Get run status from database

    :type run: Run for which to get the status
    :type reference_timestamp: Filter out node status that are newer than the reference timestamp
    :rtype: Dictionary that maps node path to node status
    """

    nodes_tbl = orm.Node.__table__

    updated_nodes_qry = select([nodes_tbl.c.path, nodes_tbl.c.modified, nodes_tbl.c.aggregates]) \
        .where(and_(nodes_tbl.c.run_id == run.id, nodes_tbl.c.modified > reference_timestamp))

    aggregates = dict()

    # Build a dictionary that maps node path to node status
    for update_node in g.db_session.execute(updated_nodes_qry):
        aggregates['/'.join(update_node.path.split('/')[1:])] = update_node.aggregates

    return aggregates


def get_nodes(run):
    """Get run nodes from database

    :type run: Run for which to fetch the nodes
    :rtype: Dictionary with nodes
    """

    # Tables
    nodes_tbl = orm.Node.__table__
    in_ports_tbl = orm.InPort.__table__
    out_ports_tbl = orm.OutPort.__table__

    # Basic filter on run id
    filter = (nodes_tbl.c.run_id == run.id)

    # Create query to fetch nodes
    nodes_qry = select([nodes_tbl]) \
        .where(filter)

    nodes = dict()

    def remove_run_from_path(path):
        return '/'.join(path.split('/')[1:])

    def port_name(path):
        return path.split('/')[-1]

    # Build a dictionary that maps node path to node properties
    for record in g.db_session.execute(nodes_qry):
        node = dict()

        node_id = remove_run_from_path(record.path)

        node['id'] = node_id
        node['path'] = node['id']
        node['title'] = record.title
        node['description'] = record.description
        node['customData'] = record.custom_data
        node['type'] = record.type
        node['children'] = list()
        node['level'] = record.level

        children_qry = select([nodes_tbl.c.path]) \
            .where(nodes_tbl.c.parent_id == record.id)

        for child in g.db_session.execute(children_qry):
            node['children'].append('/'.join(child.path.split('/')[1:]))

        node['inPorts'] = dict()
        node['outPorts'] = dict()

        in_ports_qry = select(
            [in_ports_tbl.c.path, in_ports_tbl.c.title, in_ports_tbl.c.description, in_ports_tbl.c.custom_data]) \
            .where(in_ports_tbl.c.node_id == record.id)

        out_ports_qry = select(
            [out_ports_tbl.c.path, out_ports_tbl.c.title, out_ports_tbl.c.description, out_ports_tbl.c.custom_data]) \
            .where(out_ports_tbl.c.node_id == record.id)

        for p in g.db_session.execute(in_ports_qry):
            node['inPorts'][port_name(p.path)] = dict(name=port_name(p.path), title=p.title, description=p.description,
                                                      customData=p.custom_data)

        for p in g.db_session.execute(out_ports_qry):
            node['outPorts'][port_name(p.path)] = dict(name=port_name(p.path), title=p.title, description=p.description,
                                                       customData=p.custom_data)

        nodes[node_id] = node

    return nodes


def get_links(run):
    """Get run links from database

    :type run: Run for which to fetch the nodes
    :rtype: Dictionary with links
    """

    # Tables
    links_tbl = orm.Link.__table__

    # Basic filter on run id
    filter = (links_tbl.c.run_id == run.id)

    # Create query to fetch links
    links_qry = select([links_tbl]) \
        .where(filter)

    links = dict()

    for row in g.db_session.execute(links_qry):
        link = dict()

        link['title'] = row.title
        link['description'] = row.description
        link['customData'] = row.custom_data
        link['fromNode'] = '/'.join(row.from_port_id.split('/')[1:-1])
        link['toNode'] = '/'.join(row.to_port_id.split('/')[1:-1])
        link['fromPort'] = row.from_port_id.split('/')[-1]
        link['toPort'] = row.to_port_id.split('/')[-1]
        link['dataType'] = row.data_type
        link['implicit'] = False

        link_id = '{}_{}_{}_{}'.format(link['fromNode'], link['fromPort'], link['toNode'], link['toPort'])

        links[link_id.replace('/', '_')] = link

    return links
