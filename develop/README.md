## PIM development environment
The most convenient way to develop PIM is to use the Docker Compose [environment](docker-compose.yml).
This environment has all the necessary services to interactively implement, test and debug PIM.
The main components of the PIM development environment are:
* [PostgreSQL](https://www.postgresql.org/) database
* [Flask](http://flask.pocoo.org/) REST API
* [WebPack](https://webpack.js.org/) [development server](https://webpack.js.org/configuration/dev-server/)

Launch the environment with:
```
cd /pim/develop
sudo docker-compose up
```
PIM will be available at ```localhost:8080```

Upon first launch, the database is populated with two demo pipelines:
* ```failing_macro_top_level_2019-05-13T08-29-04```
* ```tissue_pipeline_2019-01-31T21-41-53```

The [Swagger](https://swagger.io/) REST API documentation will be available at: ```http://localhost:8080/api/documentation```

#### Frontend development
The files for backend development are located in ```/pim/app```. Changing the content of JavaScript files triggers a re-load of the [WebPack development server](https://webpack.js.org/configuration/dev-server/).

#### Backend development
The files for backend development are located in ```/pim/server```. The backend server is based on a Flask application with debug turned on, so changing the content of *.py files will trigger a re-load of the Flask application. 