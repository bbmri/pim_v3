--
-- PostgreSQL database dump
--

-- Dumped from database version 11.1 (Debian 11.1-1.pgdg90+1)
-- Dumped by pg_dump version 11.1 (Debian 11.1-1.pgdg90+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: aggregates; Type: TABLE; Schema: public; Owner: pim
--

CREATE TABLE public.aggregates (
    id integer NOT NULL,
    status integer[],
    node_id integer
);


ALTER TABLE public.aggregates OWNER TO pim;

--
-- Name: aggregates_id_seq; Type: SEQUENCE; Schema: public; Owner: pim
--

CREATE SEQUENCE public.aggregates_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.aggregates_id_seq OWNER TO pim;

--
-- Name: aggregates_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pim
--

ALTER SEQUENCE public.aggregates_id_seq OWNED BY public.aggregates.id;


--
-- Name: in_ports; Type: TABLE; Schema: public; Owner: pim
--

CREATE TABLE public.in_ports (
    id integer NOT NULL,
    path character varying(256) NOT NULL,
    title character varying(128),
    description text,
    created timestamp without time zone,
    modified timestamp without time zone,
    custom_data json,
    node_id integer
);


ALTER TABLE public.in_ports OWNER TO pim;

--
-- Name: in_ports_id_seq; Type: SEQUENCE; Schema: public; Owner: pim
--

CREATE SEQUENCE public.in_ports_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.in_ports_id_seq OWNER TO pim;

--
-- Name: in_ports_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pim
--

ALTER SEQUENCE public.in_ports_id_seq OWNED BY public.in_ports.id;


--
-- Name: jobs; Type: TABLE; Schema: public; Owner: pim
--

CREATE TABLE public.jobs (
    id integer NOT NULL,
    path character varying(256),
    title character varying(128),
    description text,
    created timestamp without time zone,
    modified timestamp without time zone,
    custom_data json,
    run_id integer NOT NULL,
    node_id integer,
    node_path character varying(256) NOT NULL,
    status integer
);


ALTER TABLE public.jobs OWNER TO pim;

--
-- Name: jobs_id_seq; Type: SEQUENCE; Schema: public; Owner: pim
--

CREATE SEQUENCE public.jobs_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.jobs_id_seq OWNER TO pim;

--
-- Name: jobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pim
--

ALTER SEQUENCE public.jobs_id_seq OWNED BY public.jobs.id;


--
-- Name: link_node_refs; Type: TABLE; Schema: public; Owner: pim
--

CREATE TABLE public.link_node_refs (
    id integer NOT NULL,
    node_id integer,
    node_path character varying(256) NOT NULL,
    created timestamp without time zone
);


ALTER TABLE public.link_node_refs OWNER TO pim;

--
-- Name: link_node_refs_id_seq; Type: SEQUENCE; Schema: public; Owner: pim
--

CREATE SEQUENCE public.link_node_refs_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.link_node_refs_id_seq OWNER TO pim;

--
-- Name: link_node_refs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pim
--

ALTER SEQUENCE public.link_node_refs_id_seq OWNED BY public.link_node_refs.id;


--
-- Name: links; Type: TABLE; Schema: public; Owner: pim
--

CREATE TABLE public.links (
    id integer NOT NULL,
    title character varying(128),
    description text,
    created timestamp without time zone,
    modified timestamp without time zone,
    custom_data json,
    run_id integer NOT NULL,
    from_port_id character varying(256) NOT NULL,
    to_port_id character varying(256) NOT NULL,
    data_type character varying(128),
    common_ancestor character varying(256) NOT NULL
);


ALTER TABLE public.links OWNER TO pim;

--
-- Name: links_id_seq; Type: SEQUENCE; Schema: public; Owner: pim
--

CREATE SEQUENCE public.links_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.links_id_seq OWNER TO pim;

--
-- Name: links_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pim
--

ALTER SEQUENCE public.links_id_seq OWNED BY public.links.id;


--
-- Name: nodes; Type: TABLE; Schema: public; Owner: pim
--

CREATE TABLE public.nodes (
    id integer NOT NULL,
    path character varying(256),
    title character varying(128),
    description text,
    created timestamp without time zone,
    modified timestamp without time zone,
    custom_data json,
    type character varying(128),
    parent_id integer,
    run_id integer,
    level integer,
    aggregates integer[]
);


ALTER TABLE public.nodes OWNER TO pim;

--
-- Name: nodes_id_seq; Type: SEQUENCE; Schema: public; Owner: pim
--

CREATE SEQUENCE public.nodes_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.nodes_id_seq OWNER TO pim;

--
-- Name: nodes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pim
--

ALTER SEQUENCE public.nodes_id_seq OWNED BY public.nodes.id;


--
-- Name: out_ports; Type: TABLE; Schema: public; Owner: pim
--

CREATE TABLE public.out_ports (
    id integer NOT NULL,
    path character varying(256) NOT NULL,
    title character varying(128),
    description text,
    created timestamp without time zone,
    modified timestamp without time zone,
    custom_data json,
    node_id integer
);


ALTER TABLE public.out_ports OWNER TO pim;

--
-- Name: out_ports_id_seq; Type: SEQUENCE; Schema: public; Owner: pim
--

CREATE SEQUENCE public.out_ports_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.out_ports_id_seq OWNER TO pim;

--
-- Name: out_ports_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pim
--

ALTER SEQUENCE public.out_ports_id_seq OWNED BY public.out_ports.id;


--
-- Name: runs; Type: TABLE; Schema: public; Owner: pim
--

CREATE TABLE public.runs (
    id integer NOT NULL,
    name character varying(128) NOT NULL,
    title character varying(128),
    description text,
    created timestamp without time zone,
    modified timestamp without time zone,
    custom_data json,
    "user" character varying(128)
);


ALTER TABLE public.runs OWNER TO pim;

--
-- Name: runs_id_seq; Type: SEQUENCE; Schema: public; Owner: pim
--

CREATE SEQUENCE public.runs_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.runs_id_seq OWNER TO pim;

--
-- Name: runs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pim
--

ALTER SEQUENCE public.runs_id_seq OWNED BY public.runs.id;


--
-- Name: aggregates id; Type: DEFAULT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.aggregates ALTER COLUMN id SET DEFAULT nextval('public.aggregates_id_seq'::regclass);


--
-- Name: in_ports id; Type: DEFAULT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.in_ports ALTER COLUMN id SET DEFAULT nextval('public.in_ports_id_seq'::regclass);


--
-- Name: jobs id; Type: DEFAULT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.jobs ALTER COLUMN id SET DEFAULT nextval('public.jobs_id_seq'::regclass);


--
-- Name: link_node_refs id; Type: DEFAULT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.link_node_refs ALTER COLUMN id SET DEFAULT nextval('public.link_node_refs_id_seq'::regclass);


--
-- Name: links id; Type: DEFAULT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.links ALTER COLUMN id SET DEFAULT nextval('public.links_id_seq'::regclass);


--
-- Name: nodes id; Type: DEFAULT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.nodes ALTER COLUMN id SET DEFAULT nextval('public.nodes_id_seq'::regclass);


--
-- Name: out_ports id; Type: DEFAULT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.out_ports ALTER COLUMN id SET DEFAULT nextval('public.out_ports_id_seq'::regclass);


--
-- Name: runs id; Type: DEFAULT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.runs ALTER COLUMN id SET DEFAULT nextval('public.runs_id_seq'::regclass);


--
-- Data for Name: aggregates; Type: TABLE DATA; Schema: public; Owner: pim
--

COPY public.aggregates (id, status, node_id) FROM stdin;
\.


--
-- Data for Name: in_ports; Type: TABLE DATA; Schema: public; Owner: pim
--

COPY public.in_ports (id, path, title, description, created, modified, custom_data, node_id) FROM stdin;
3685	tissue_pipeline_2019-01-31T21-41-53/root/segmentation/fast_tissue_segmentation/image	image	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "NiftiImageFile", "input_group": "default", "dimension_names": ["subjects"]}	5600
3686	tissue_pipeline_2019-01-31T21-41-53/root/segmentation/fast_tissue_segmentation/classes	classes	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "Int", "input_group": "default", "dimension_names": []}	5600
3687	tissue_pipeline_2019-01-31T21-41-53/root/segmentation/fast_tissue_segmentation/iterations	iterations	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "Int", "input_group": "default", "dimension_names": []}	5600
3688	tissue_pipeline_2019-01-31T21-41-53/root/segmentation/calculate_hard_segmentation/gray_matter	gray_matter	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "NiftiImageFileCompressed", "input_group": "default", "dimension_names": ["subjects"]}	5601
3689	tissue_pipeline_2019-01-31T21-41-53/root/segmentation/calculate_hard_segmentation/white_matter	white_matter	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "NiftiImageFileCompressed", "input_group": "default", "dimension_names": ["subjects"]}	5601
3690	tissue_pipeline_2019-01-31T21-41-53/root/segmentation/calculate_hard_segmentation/cerebral_spinal_fluid	cerebral_spinal_fluid	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "NiftiImageFileCompressed", "input_group": "default", "dimension_names": ["subjects"]}	5601
3691	tissue_pipeline_2019-01-31T21-41-53/root/segmentation/mask_t1_with_brain_mask/input_datatype	input_datatype	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "__FSLMaths_5.0.9_interface__input_datatype__Enum__", "input_group": "default", "dimension_names": []}	5602
3692	tissue_pipeline_2019-01-31T21-41-53/root/segmentation/mask_t1_with_brain_mask/image1	image1	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "NiftiImageFile", "input_group": "default", "dimension_names": ["subjects"]}	5602
3693	tissue_pipeline_2019-01-31T21-41-53/root/segmentation/mask_t1_with_brain_mask/operator1	operator1	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "String", "input_group": "default", "dimension_names": ["const_mask_t1_with_brain_mask_operator1_0"]}	5602
3694	tissue_pipeline_2019-01-31T21-41-53/root/segmentation/mask_t1_with_brain_mask/operator1_string	operator1_string	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "String", "input_group": "default", "dimension_names": []}	5602
3695	tissue_pipeline_2019-01-31T21-41-53/root/segmentation/mask_t1_with_brain_mask/operator1_number	operator1_number	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "Float", "input_group": "default", "dimension_names": []}	5602
3696	tissue_pipeline_2019-01-31T21-41-53/root/segmentation/mask_t1_with_brain_mask/image2	image2	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "NiftiImageFile", "input_group": "default", "dimension_names": ["subjects"]}	5602
3697	tissue_pipeline_2019-01-31T21-41-53/root/segmentation/mask_t1_with_brain_mask/operator2	operator2	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "String", "input_group": "default", "dimension_names": []}	5602
3698	tissue_pipeline_2019-01-31T21-41-53/root/segmentation/mask_t1_with_brain_mask/operator2_string	operator2_string	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "String", "input_group": "default", "dimension_names": []}	5602
3699	tissue_pipeline_2019-01-31T21-41-53/root/segmentation/mask_t1_with_brain_mask/operator2_number	operator2_number	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "Float", "input_group": "default", "dimension_names": []}	5602
3700	tissue_pipeline_2019-01-31T21-41-53/root/segmentation/mask_t1_with_brain_mask/image3	image3	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "NiftiImageFile", "input_group": "default", "dimension_names": []}	5602
3701	tissue_pipeline_2019-01-31T21-41-53/root/segmentation/mask_t1_with_brain_mask/operator3	operator3	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "String", "input_group": "default", "dimension_names": []}	5602
3702	tissue_pipeline_2019-01-31T21-41-53/root/segmentation/mask_t1_with_brain_mask/operator3_string	operator3_string	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "String", "input_group": "default", "dimension_names": []}	5602
3703	tissue_pipeline_2019-01-31T21-41-53/root/segmentation/mask_t1_with_brain_mask/operator3_number	operator3_number	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "Float", "input_group": "default", "dimension_names": []}	5602
3704	tissue_pipeline_2019-01-31T21-41-53/root/segmentation/mask_t1_with_brain_mask/image4	image4	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "NiftiImageFile", "input_group": "default", "dimension_names": []}	5602
3705	tissue_pipeline_2019-01-31T21-41-53/root/segmentation/mask_t1_with_brain_mask/operator4	operator4	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "String", "input_group": "default", "dimension_names": []}	5602
3706	tissue_pipeline_2019-01-31T21-41-53/root/segmentation/mask_t1_with_brain_mask/operator4_string	operator4_string	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "String", "input_group": "default", "dimension_names": []}	5602
3707	tissue_pipeline_2019-01-31T21-41-53/root/segmentation/mask_t1_with_brain_mask/operator4_number	operator4_number	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "Float", "input_group": "default", "dimension_names": []}	5602
3708	tissue_pipeline_2019-01-31T21-41-53/root/segmentation/mask_t1_with_brain_mask/image5	image5	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "NiftiImageFile", "input_group": "default", "dimension_names": []}	5602
3709	tissue_pipeline_2019-01-31T21-41-53/root/segmentation/mask_t1_with_brain_mask/operator5	operator5	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "String", "input_group": "default", "dimension_names": []}	5602
3710	tissue_pipeline_2019-01-31T21-41-53/root/segmentation/mask_t1_with_brain_mask/operator5_string	operator5_string	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "String", "input_group": "default", "dimension_names": []}	5602
3711	tissue_pipeline_2019-01-31T21-41-53/root/segmentation/mask_t1_with_brain_mask/operator5_number	operator5_number	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "Float", "input_group": "default", "dimension_names": []}	5602
3712	tissue_pipeline_2019-01-31T21-41-53/root/segmentation/mask_t1_with_brain_mask/output_datatype	output_datatype	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "__FSLMaths_5.0.9_interface__output_datatype__Enum__", "input_group": "default", "dimension_names": []}	5602
3713	tissue_pipeline_2019-01-31T21-41-53/root/statistics/wm_volume/input	input	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "CsvFile", "input_group": "default", "dimension_names": ["subjects"]}	5604
3714	tissue_pipeline_2019-01-31T21-41-53/root/statistics/gm_volume/input	input	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "CsvFile", "input_group": "default", "dimension_names": ["subjects"]}	5605
3715	tissue_pipeline_2019-01-31T21-41-53/root/statistics/brain_volume/input	input	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "CsvFile", "input_group": "default", "dimension_names": ["subjects"]}	5606
3716	tissue_pipeline_2019-01-31T21-41-53/root/statistics/get_wm_volume/label	label	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "NiftiImageFileCompressed", "input_group": "default", "dimension_names": ["subjects"]}	5607
3717	tissue_pipeline_2019-01-31T21-41-53/root/statistics/get_wm_volume/image	image	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "NiftiImageFileCompressed", "input_group": "default", "dimension_names": ["subjects"]}	5607
3718	tissue_pipeline_2019-01-31T21-41-53/root/statistics/get_wm_volume/mask	mask	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "NiftiImageFileCompressed", "input_group": "default", "dimension_names": []}	5607
3719	tissue_pipeline_2019-01-31T21-41-53/root/statistics/get_wm_volume/volume	volume	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "String", "input_group": "default", "dimension_names": ["const_get_wm_volume_volume_0"]}	5607
3720	tissue_pipeline_2019-01-31T21-41-53/root/statistics/get_wm_volume/stddev	stddev	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "String", "input_group": "default", "dimension_names": []}	5607
3721	tissue_pipeline_2019-01-31T21-41-53/root/statistics/get_wm_volume/mean	mean	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "String", "input_group": "default", "dimension_names": []}	5607
3722	tissue_pipeline_2019-01-31T21-41-53/root/statistics/get_wm_volume/selection	selection	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "TxtFile", "input_group": "default", "dimension_names": ["const_get_wm_volume_selection_0"]}	5607
3723	tissue_pipeline_2019-01-31T21-41-53/root/statistics/get_wm_volume/ddof	ddof	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "Int", "input_group": "default", "dimension_names": []}	5607
3724	tissue_pipeline_2019-01-31T21-41-53/root/statistics/get_wm_volume/flatten	flatten	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "Boolean", "input_group": "default", "dimension_names": ["const_get_wm_volume_flatten_0"]}	5607
3725	tissue_pipeline_2019-01-31T21-41-53/root/statistics/get_gm_volume/label	label	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "NiftiImageFileCompressed", "input_group": "default", "dimension_names": ["subjects"]}	5608
3726	tissue_pipeline_2019-01-31T21-41-53/root/statistics/get_gm_volume/image	image	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "NiftiImageFileCompressed", "input_group": "default", "dimension_names": ["subjects"]}	5608
3727	tissue_pipeline_2019-01-31T21-41-53/root/statistics/get_gm_volume/mask	mask	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "NiftiImageFileCompressed", "input_group": "default", "dimension_names": []}	5608
3728	tissue_pipeline_2019-01-31T21-41-53/root/statistics/get_gm_volume/volume	volume	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "String", "input_group": "default", "dimension_names": ["const_get_gm_volume_volume_1"]}	5608
3729	tissue_pipeline_2019-01-31T21-41-53/root/statistics/get_gm_volume/stddev	stddev	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "String", "input_group": "default", "dimension_names": []}	5608
3730	tissue_pipeline_2019-01-31T21-41-53/root/statistics/get_gm_volume/mean	mean	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "String", "input_group": "default", "dimension_names": []}	5608
3731	tissue_pipeline_2019-01-31T21-41-53/root/statistics/get_gm_volume/selection	selection	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "TxtFile", "input_group": "default", "dimension_names": ["const_get_gm_volume_selection_1"]}	5608
3732	tissue_pipeline_2019-01-31T21-41-53/root/statistics/get_gm_volume/ddof	ddof	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "Int", "input_group": "default", "dimension_names": []}	5608
3733	tissue_pipeline_2019-01-31T21-41-53/root/statistics/get_gm_volume/flatten	flatten	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "Boolean", "input_group": "default", "dimension_names": ["const_get_gm_volume_flatten_1"]}	5608
3734	tissue_pipeline_2019-01-31T21-41-53/root/statistics/get_brain_volume/label	label	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "NiftiImageFileCompressed", "input_group": "default", "dimension_names": ["subjects"]}	5609
3735	tissue_pipeline_2019-01-31T21-41-53/root/statistics/get_brain_volume/image	image	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "NiftiImageFileCompressed", "input_group": "default", "dimension_names": ["subjects"]}	5609
3736	tissue_pipeline_2019-01-31T21-41-53/root/statistics/get_brain_volume/mask	mask	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "NiftiImageFileCompressed", "input_group": "default", "dimension_names": []}	5609
3737	tissue_pipeline_2019-01-31T21-41-53/root/statistics/get_brain_volume/volume	volume	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "String", "input_group": "default", "dimension_names": ["const_get_brain_volume_volume_0"]}	5609
3738	tissue_pipeline_2019-01-31T21-41-53/root/statistics/get_brain_volume/stddev	stddev	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "String", "input_group": "default", "dimension_names": []}	5609
3739	tissue_pipeline_2019-01-31T21-41-53/root/statistics/get_brain_volume/mean	mean	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "String", "input_group": "default", "dimension_names": []}	5609
3740	tissue_pipeline_2019-01-31T21-41-53/root/statistics/get_brain_volume/selection	selection	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "TxtFile", "input_group": "default", "dimension_names": ["const_get_brain_volume_selection_0"]}	5609
3741	tissue_pipeline_2019-01-31T21-41-53/root/statistics/get_brain_volume/ddof	ddof	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "Int", "input_group": "default", "dimension_names": []}	5609
3742	tissue_pipeline_2019-01-31T21-41-53/root/statistics/get_brain_volume/flatten	flatten	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "Boolean", "input_group": "default", "dimension_names": ["const_get_brain_volume_flatten_0"]}	5609
3743	tissue_pipeline_2019-01-31T21-41-53/root/statistics/mask_hammers_labels/left_hand	left_hand	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "NiftiImageFileCompressed", "input_group": "default", "dimension_names": ["subjects"]}	5610
3744	tissue_pipeline_2019-01-31T21-41-53/root/statistics/mask_hammers_labels/right_hand	right_hand	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "NiftiImageFileCompressed", "input_group": "default", "dimension_names": ["subjects"]}	5610
3745	tissue_pipeline_2019-01-31T21-41-53/root/statistics/mask_hammers_labels/operator	operator	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "String", "input_group": "default", "dimension_names": ["const_mask_hammers_labels_operator_0"]}	5610
3746	tissue_pipeline_2019-01-31T21-41-53/root/dicom_to_nifti/t1_dicom_to_nifti/dicom_image	dicom_image	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "DicomImageFile", "input_group": "default", "dimension_names": ["subjects"]}	5624
3747	tissue_pipeline_2019-01-31T21-41-53/root/dicom_to_nifti/t1_dicom_to_nifti/file_order	file_order	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "String", "input_group": "default", "dimension_names": ["const_t1_dicom_to_nifti_file_order_0"]}	5624
3748	tissue_pipeline_2019-01-31T21-41-53/root/dicom_to_nifti/t1_dicom_to_nifti/threshold	threshold	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "Number", "input_group": "default", "dimension_names": []}	5624
3749	tissue_pipeline_2019-01-31T21-41-53/root/dicom_to_nifti/reformat_t1/image	image	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "NiftiImageFileCompressed", "input_group": "default", "dimension_names": ["subjects"]}	5625
3750	tissue_pipeline_2019-01-31T21-41-53/root/hammers_atlas/hammers_t1w/input	input	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "NiftiImageFileCompressed", "dimension_names": ["hammers_t1w"]}	5627
3751	tissue_pipeline_2019-01-31T21-41-53/root/hammers_atlas/hammers_brains_r5/input	input	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "NiftiImageFileCompressed", "dimension_names": ["hammers_brains_r5"]}	5628
3752	tissue_pipeline_2019-01-31T21-41-53/root/hammers_atlas/hammers_brains/input	input	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "NiftiImageFileCompressed", "dimension_names": ["hammers_brains"]}	5629
3753	tissue_pipeline_2019-01-31T21-41-53/root/hammers_atlas/hammers_labels/input	input	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "NiftiImageFileCompressed", "dimension_names": ["hammers_labels"]}	5630
3754	tissue_pipeline_2019-01-31T21-41-53/root/subjects/t1w_dicom/input	input	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "DicomImageFile", "dimension_names": ["t1w_dicom"]}	5631
3755	tissue_pipeline_2019-01-31T21-41-53/root/output/t1_nifti_images/input	input	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "NiftiImageFileCompressed", "input_group": "default", "dimension_names": ["subjects"]}	5632
3756	tissue_pipeline_2019-01-31T21-41-53/root/output/gm_map_output/input	input	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "NiftiImageFile", "input_group": "default", "dimension_names": ["subjects"]}	5633
3757	tissue_pipeline_2019-01-31T21-41-53/root/output/wm_map_output/input	input	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "NiftiImageFile", "input_group": "default", "dimension_names": ["subjects"]}	5634
3758	tissue_pipeline_2019-01-31T21-41-53/root/output/multi_atlas_segm/input	input	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "NiftiImageFile", "input_group": "default", "dimension_names": ["subjects"]}	5635
3759	tissue_pipeline_2019-01-31T21-41-53/root/output/hammer_brain_mask/input	input	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "NiftiImageFile", "input_group": "default", "dimension_names": ["subjects"]}	5636
3760	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/bet/image	image	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "NiftiImageFile", "input_group": "default", "dimension_names": ["subjects"]}	5637
3761	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/bet/fraction_threshold	fraction_threshold	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "Float", "input_group": "default", "dimension_names": ["const_bet_fraction_threshold_0"]}	5637
3762	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/bet/fraction_gradient	fraction_gradient	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "Float", "input_group": "default", "dimension_names": []}	5637
3763	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/bet/head_radius	head_radius	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "Float", "input_group": "default", "dimension_names": []}	5637
3764	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/bet/center_of_gravity	center_of_gravity	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "Int", "input_group": "default", "dimension_names": []}	5637
3765	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/bet/apply_threshold	apply_threshold	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "Boolean", "input_group": "default", "dimension_names": []}	5637
3766	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/bet/robust_estimation	robust_estimation	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "Boolean", "input_group": "default", "dimension_names": []}	5637
3767	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/bet/eye_cleanup	eye_cleanup	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "Boolean", "input_group": "default", "dimension_names": []}	5637
3768	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/bet/bias_cleanup	bias_cleanup	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "Boolean", "input_group": "default", "dimension_names": ["const_bet_bias_cleanup_0"]}	5637
3769	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/bet/small_fov	small_fov	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "Boolean", "input_group": "default", "dimension_names": []}	5637
3770	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/bet/apply_4D	apply_4D	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "Boolean", "input_group": "default", "dimension_names": []}	5637
3771	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/bet/run_bet2_surf	run_bet2_surf	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "Boolean", "input_group": "default", "dimension_names": []}	5637
3772	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/bet/T2_image	T2_image	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "NiftiImageFile", "input_group": "default", "dimension_names": []}	5637
3773	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/convert_brain/image	image	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "ITKImageFile", "input_group": "default", "dimension_names": ["subjects"]}	5638
3774	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/convert_brain/component_type	component_type	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "__PxCastConvert_0.3.2_interface__component_type__Enum__", "input_group": "default", "dimension_names": ["const_convert_brain_component_type_0"]}	5638
3775	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/convert_brain/compression_flag	compression_flag	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "Boolean", "input_group": "default", "dimension_names": []}	5638
3776	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/threshold_brain/image	image	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "ITKImageFile", "input_group": "default", "dimension_names": ["subjects"]}	5639
3777	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/threshold_brain/method	method	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "__PxThresholdImage_0.3.0_interface__method__Enum__", "input_group": "default", "dimension_names": []}	5639
3778	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/threshold_brain/mask	mask	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "ITKImageFile", "input_group": "default", "dimension_names": []}	5639
3779	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/threshold_brain/lower_threshold	lower_threshold	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "Float", "input_group": "default", "dimension_names": []}	5639
3780	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/threshold_brain/upper_threshold	upper_threshold	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "Float", "input_group": "default", "dimension_names": []}	5639
3781	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/threshold_brain/inside_value	inside_value	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "Float", "input_group": "default", "dimension_names": []}	5639
3782	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/threshold_brain/outside_value	outside_value	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "Float", "input_group": "default", "dimension_names": []}	5639
3783	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/threshold_brain/number_of_thresholds	number_of_thresholds	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "UnsignedInt", "input_group": "default", "dimension_names": []}	5639
3784	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/threshold_brain/number_of_histbins	number_of_histbins	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "UnsignedInt", "input_group": "default", "dimension_names": []}	5639
3785	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/threshold_brain/radius	radius	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "UnsignedInt", "input_group": "default", "dimension_names": []}	5639
3786	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/threshold_brain/number_of_controlpoints	number_of_controlpoints	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "UnsignedInt", "input_group": "default", "dimension_names": []}	5639
3787	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/threshold_brain/number_of_levels	number_of_levels	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "UnsignedInt", "input_group": "default", "dimension_names": []}	5639
3788	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/threshold_brain/number_of_samples	number_of_samples	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "UnsignedInt", "input_group": "default", "dimension_names": []}	5639
3789	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/threshold_brain/spline_order	spline_order	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "UnsignedInt", "input_group": "default", "dimension_names": []}	5639
3790	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/threshold_brain/power	power	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "Float", "input_group": "default", "dimension_names": []}	5639
3791	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/threshold_brain/sigma	sigma	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "Float", "input_group": "default", "dimension_names": []}	5639
3792	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/threshold_brain/number_of_iterations	number_of_iterations	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "UnsignedInt", "input_group": "default", "dimension_names": []}	5639
3793	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/threshold_brain/mask_value	mask_value	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "UnsignedInt", "input_group": "default", "dimension_names": []}	5639
3794	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/threshold_brain/mixture_type	mixture_type	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "__PxThresholdImage_0.3.0_interface__mixture_type__Enum__", "input_group": "default", "dimension_names": []}	5639
3795	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/threshold_brain/compression_flag	compression_flag	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "Boolean", "input_group": "default", "dimension_names": []}	5639
3796	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/dilate_brainmask/image	image	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "ITKImageFile", "input_group": "default", "dimension_names": ["subjects"]}	5640
3797	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/dilate_brainmask/operation	operation	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "__PxMorphology_0.3.2_interface__operation__Enum__", "input_group": "default", "dimension_names": ["const_dilate_brainmask_operation_0"]}	5640
3798	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/dilate_brainmask/operation_type	operation_type	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "__PxMorphology_0.3.2_interface__operation_type__Enum__", "input_group": "default", "dimension_names": ["const_dilate_brainmask_operation_type_0"]}	5640
3799	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/dilate_brainmask/compression_flag	compression_flag	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "Boolean", "input_group": "default", "dimension_names": []}	5640
3800	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/dilate_brainmask/radius	radius	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "Float", "input_group": "default", "dimension_names": ["const_dilate_brainmask_radius_0"]}	5640
3801	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/dilate_brainmask/boundary_condition	boundary_condition	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "Float", "input_group": "default", "dimension_names": []}	5640
3802	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/dilate_brainmask/foreground_background_value	foreground_background_value	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "Float", "input_group": "default", "dimension_names": []}	5640
3803	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/dilate_brainmask/gradient_algorithm_enum	gradient_algorithm_enum	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "__PxMorphology_0.3.2_interface__gradient_algorithm_enum__Enum__", "input_group": "default", "dimension_names": []}	5640
3804	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/dilate_brainmask/component_type	component_type	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "String", "input_group": "default", "dimension_names": []}	5640
3805	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/n4_bias_correction/image	image	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "NiftiImageFileCompressed", "input_group": "default", "dimension_names": ["subjects"]}	5641
3806	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/n4_bias_correction/mask	mask	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "NiftiImageFileCompressed", "input_group": "default", "dimension_names": ["subjects"]}	5641
3807	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/n4_bias_correction/n4_parameters	n4_parameters	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "String", "input_group": "default", "dimension_names": []}	5641
3808	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/brain_mask_registation/fixed_image	fixed_image	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "ITKImageFile", "input_group": "default", "dimension_names": ["subjects"]}	5642
3809	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/brain_mask_registation/moving_image	moving_image	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "ITKImageFile", "input_group": "atlas", "dimension_names": ["hammers_atlas"]}	5642
3810	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/brain_mask_registation/parameters	parameters	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "ElastixParameterFile", "input_group": "default", "dimension_names": ["const_brain_mask_registation_parameters_0"]}	5642
3811	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/brain_mask_registation/fixed_mask	fixed_mask	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "ITKImageFile", "input_group": "default", "dimension_names": []}	5642
3812	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/brain_mask_registation/moving_mask	moving_mask	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "ITKImageFile", "input_group": "default", "dimension_names": []}	5642
3813	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/brain_mask_registation/initial_transform	initial_transform	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "ElastixTransformFile", "input_group": "default", "dimension_names": []}	5642
3814	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/brain_mask_registation/priority	priority	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "__Elastix_4.8_interface__priority__Enum__", "input_group": "default", "dimension_names": []}	5642
3815	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/brain_mask_registation/threads	threads	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "Int", "input_group": "default", "dimension_names": ["const_brain_mask_registation_threads_0"]}	5642
3816	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/t1w_registration/fixed_image	fixed_image	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "ITKImageFile", "input_group": "default", "dimension_names": ["subjects"]}	5643
3817	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/t1w_registration/moving_image	moving_image	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "ITKImageFile", "input_group": "default", "dimension_names": ["hammers_atlas"]}	5643
3818	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/t1w_registration/parameters	parameters	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "ElastixParameterFile", "input_group": "default", "dimension_names": ["const_t1w_registration_parameters_0"]}	5643
3819	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/t1w_registration/fixed_mask	fixed_mask	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "ITKImageFile", "input_group": "default", "dimension_names": ["subjects"]}	5643
3820	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/t1w_registration/moving_mask	moving_mask	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "ITKImageFile", "input_group": "default", "dimension_names": ["hammers_atlas"]}	5643
3821	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/t1w_registration/initial_transform	initial_transform	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "ElastixTransformFile", "input_group": "default", "dimension_names": ["subjects", "hammers_atlas"]}	5643
3822	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/t1w_registration/priority	priority	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "__Elastix_4.8_interface__priority__Enum__", "input_group": "default", "dimension_names": []}	5643
3823	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/t1w_registration/threads	threads	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "Int", "input_group": "default", "dimension_names": ["const_t1w_registration_threads_0"]}	5643
3824	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/hammers_brain_transform/transform	transform	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "ElastixTransformFile", "input_group": "default", "dimension_names": ["subjects", "hammers_atlas"]}	5644
3825	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/hammers_brain_transform/image	image	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "ITKImageFile", "input_group": "default", "dimension_names": ["hammers_atlas"]}	5644
3826	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/hammers_brain_transform/points	points	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "TransformixPointsInputType", "input_group": "default", "dimension_names": []}	5644
3827	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/hammers_brain_transform/determinant_of_jacobian_flag	determinant_of_jacobian_flag	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "Boolean", "input_group": "default", "dimension_names": []}	5644
3828	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/hammers_brain_transform/jacobian_matrix_flag	jacobian_matrix_flag	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "Boolean", "input_group": "default", "dimension_names": []}	5644
3829	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/hammers_brain_transform/priority	priority	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "__Transformix_4.8_interface__priority__Enum__", "input_group": "default", "dimension_names": []}	5644
3830	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/hammers_brain_transform/threads	threads	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "Int", "input_group": "default", "dimension_names": ["const_hammers_brain_transform_threads_0"]}	5644
3831	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/label_registration/transform	transform	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "ElastixTransformFile", "input_group": "default", "dimension_names": ["subjects", "hammers_atlas"]}	5645
3832	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/label_registration/image	image	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "ITKImageFile", "input_group": "default", "dimension_names": ["hammers_atlas"]}	5645
3833	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/label_registration/points	points	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "TransformixPointsInputType", "input_group": "default", "dimension_names": []}	5645
3834	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/label_registration/determinant_of_jacobian_flag	determinant_of_jacobian_flag	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "Boolean", "input_group": "default", "dimension_names": []}	5645
3835	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/label_registration/jacobian_matrix_flag	jacobian_matrix_flag	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "Boolean", "input_group": "default", "dimension_names": []}	5645
3836	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/label_registration/priority	priority	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "__Transformix_4.8_interface__priority__Enum__", "input_group": "default", "dimension_names": []}	5645
3837	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/label_registration/threads	threads	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "Int", "input_group": "default", "dimension_names": ["const_label_registration_threads_0"]}	5645
3838	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/combine_labels/images	images	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "ITKImageFile", "input_group": "default", "dimension_names": ["subjects"]}	5646
3839	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/combine_labels/method	method	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "__PxCombineSegmentations_0.3.2_interface__method__Enum__", "input_group": "default", "dimension_names": ["const_combine_labels_method_0"]}	5646
3840	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/combine_labels/number_of_classes	number_of_classes	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "Int", "input_group": "default", "dimension_names": ["const_combine_labels_number_of_classes_0"]}	5646
3841	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/combine_labels/original_labels	original_labels	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "Int", "input_group": "default", "dimension_names": []}	5646
3842	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/combine_labels/substitute_labels	substitute_labels	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "Int", "input_group": "default", "dimension_names": []}	5646
3843	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/combine_brains/images	images	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "ITKImageFile", "input_group": "default", "dimension_names": ["subjects"]}	5647
3844	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/combine_brains/method	method	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "__PxCombineSegmentations_0.3.2_interface__method__Enum__", "input_group": "default", "dimension_names": ["const_combine_brains_method_0"]}	5647
3845	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/combine_brains/number_of_classes	number_of_classes	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "Int", "input_group": "default", "dimension_names": ["const_combine_brains_number_of_classes_0"]}	5647
3846	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/combine_brains/original_labels	original_labels	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "Int", "input_group": "default", "dimension_names": []}	5647
3847	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/combine_brains/substitute_labels	substitute_labels	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "Int", "input_group": "default", "dimension_names": []}	5647
3848	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/threshold_labels/image	image	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "ITKImageFile", "input_group": "default", "dimension_names": ["subjects"]}	5664
3849	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/threshold_labels/method	method	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "__PxThresholdImage_0.3.0_interface__method__Enum__", "input_group": "default", "dimension_names": []}	5664
3850	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/threshold_labels/mask	mask	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "ITKImageFile", "input_group": "default", "dimension_names": []}	5664
3851	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/threshold_labels/lower_threshold	lower_threshold	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "Float", "input_group": "default", "dimension_names": []}	5664
3852	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/threshold_labels/upper_threshold	upper_threshold	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "Float", "input_group": "default", "dimension_names": ["const_threshold_labels_upper_threshold_0"]}	5664
3853	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/threshold_labels/inside_value	inside_value	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "Float", "input_group": "default", "dimension_names": []}	5664
3854	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/threshold_labels/outside_value	outside_value	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "Float", "input_group": "default", "dimension_names": []}	5664
3855	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/threshold_labels/number_of_thresholds	number_of_thresholds	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "UnsignedInt", "input_group": "default", "dimension_names": []}	5664
3856	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/threshold_labels/number_of_histbins	number_of_histbins	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "UnsignedInt", "input_group": "default", "dimension_names": []}	5664
3857	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/threshold_labels/radius	radius	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "UnsignedInt", "input_group": "default", "dimension_names": []}	5664
3858	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/threshold_labels/number_of_controlpoints	number_of_controlpoints	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "UnsignedInt", "input_group": "default", "dimension_names": []}	5664
3859	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/threshold_labels/number_of_levels	number_of_levels	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "UnsignedInt", "input_group": "default", "dimension_names": []}	5664
3860	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/threshold_labels/number_of_samples	number_of_samples	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "UnsignedInt", "input_group": "default", "dimension_names": []}	5664
3861	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/threshold_labels/spline_order	spline_order	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "UnsignedInt", "input_group": "default", "dimension_names": []}	5664
3862	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/threshold_labels/power	power	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "Float", "input_group": "default", "dimension_names": []}	5664
3863	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/threshold_labels/sigma	sigma	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "Float", "input_group": "default", "dimension_names": []}	5664
3864	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/threshold_labels/number_of_iterations	number_of_iterations	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "UnsignedInt", "input_group": "default", "dimension_names": []}	5664
3865	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/threshold_labels/mask_value	mask_value	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "UnsignedInt", "input_group": "default", "dimension_names": []}	5664
3866	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/threshold_labels/mixture_type	mixture_type	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "__PxThresholdImage_0.3.0_interface__mixture_type__Enum__", "input_group": "default", "dimension_names": []}	5664
3867	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/threshold_labels/compression_flag	compression_flag	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "Boolean", "input_group": "default", "dimension_names": []}	5664
3868	tissue_pipeline_2019-01-31T21-41-53/root/qc_overview/t1_with_gm_outline_sink/input	input	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "PngImageFile", "input_group": "default", "dimension_names": ["subjects"]}	5666
3869	tissue_pipeline_2019-01-31T21-41-53/root/qc_overview/t1_with_wm_outline_sink/input	input	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "PngImageFile", "input_group": "default", "dimension_names": ["subjects"]}	5667
3870	tissue_pipeline_2019-01-31T21-41-53/root/qc_overview/t1_with_hammers_sink/input	input	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "PngImageFile", "input_group": "default", "dimension_names": ["subjects"]}	5668
3871	tissue_pipeline_2019-01-31T21-41-53/root/qc_overview/t1_qc_sink/input	input	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "PngImageFile", "input_group": "default", "dimension_names": ["subjects"]}	5669
3872	tissue_pipeline_2019-01-31T21-41-53/root/qc_overview/t1_with_brain_sink/input	input	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "PngImageFile", "input_group": "default", "dimension_names": ["subjects"]}	5670
3873	tissue_pipeline_2019-01-31T21-41-53/root/qc_overview/t1_qc/image	image	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "NiftiImageFile", "input_group": "default", "dimension_names": ["subjects"]}	5671
3874	tissue_pipeline_2019-01-31T21-41-53/root/qc_overview/t1_with_gm_outline/image	image	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "NiftiImageFile", "input_group": "default", "dimension_names": ["subjects"]}	5672
3875	tissue_pipeline_2019-01-31T21-41-53/root/qc_overview/t1_with_gm_outline/segmentation	segmentation	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "NiftiImageFile", "input_group": "default", "dimension_names": ["subjects"]}	5672
3876	tissue_pipeline_2019-01-31T21-41-53/root/qc_overview/t1_with_wm_outline/image	image	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "NiftiImageFile", "input_group": "default", "dimension_names": ["subjects"]}	5673
3877	tissue_pipeline_2019-01-31T21-41-53/root/qc_overview/t1_with_wm_outline/segmentation	segmentation	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "NiftiImageFile", "input_group": "default", "dimension_names": ["subjects"]}	5673
3878	tissue_pipeline_2019-01-31T21-41-53/root/qc_overview/t1_with_hammers/image	image	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "NiftiImageFile", "input_group": "default", "dimension_names": ["subjects"]}	5674
3879	tissue_pipeline_2019-01-31T21-41-53/root/qc_overview/t1_with_hammers/segmentation	segmentation	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "NiftiImageFile", "input_group": "default", "dimension_names": ["subjects"]}	5674
3880	tissue_pipeline_2019-01-31T21-41-53/root/qc_overview/t1_with_brain/image	image	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "NiftiImageFile", "input_group": "default", "dimension_names": ["subjects"]}	5675
3881	tissue_pipeline_2019-01-31T21-41-53/root/qc_overview/t1_with_brain/segmentation	segmentation	\N	2019-04-02 09:39:49.659406	2019-04-02 09:39:49.659433	{"datatype": "NiftiImageFile", "input_group": "default", "dimension_names": ["subjects"]}	5675
3934	test/root/process/convert/constant	constant	\N	2019-04-07 10:43:17.688387	2019-04-07 10:43:17.688424	null	5972
3935	test/root/process/convert/dicom_image	dicom_image	\N	2019-04-07 10:43:17.688387	2019-04-07 10:43:17.688424	null	5972
3936	test/root/sinks/save_nifti/nifti_image	nifti_image	\N	2019-04-07 10:43:17.688387	2019-04-07 10:43:17.688424	null	5973
4621	failing_macro_top_level_2019-05-13T08-29-04/root/source_a/input	input	\N	2019-05-13 06:27:40.176648	2019-05-13 06:27:40.176693	{"datatype": "Int", "dimension_names": ["source_a"]}	6520
4622	failing_macro_top_level_2019-05-13T08-29-04/root/source_b/input	input	\N	2019-05-13 06:27:40.176648	2019-05-13 06:27:40.176693	{"datatype": "Int", "dimension_names": ["source_b"]}	6521
4623	failing_macro_top_level_2019-05-13T08-29-04/root/source_c/input	input	\N	2019-05-13 06:27:40.176648	2019-05-13 06:27:40.176693	{"datatype": "Int", "dimension_names": ["source_c"]}	6522
4624	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/source_1/input	input	\N	2019-05-13 06:27:40.176648	2019-05-13 06:27:40.176693	{"datatype": "Int", "dimension_names": ["source_1"]}	6527
4625	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/source_2/input	input	\N	2019-05-13 06:27:40.176648	2019-05-13 06:27:40.176693	{"datatype": "Int", "dimension_names": ["source_2"]}	6528
4626	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/source_3/input	input	\N	2019-05-13 06:27:40.176648	2019-05-13 06:27:40.176693	{"datatype": "Int", "dimension_names": ["source_3"]}	6529
4627	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sink_1/input	input	\N	2019-05-13 06:27:40.176648	2019-05-13 06:27:40.176693	{"input_group": "default", "datatype": "Int", "dimension_names": ["source_1"]}	6530
4628	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sink_2/input	input	\N	2019-05-13 06:27:40.176648	2019-05-13 06:27:40.176693	{"input_group": "default", "datatype": "Int", "dimension_names": ["source_3"]}	6531
4629	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sink_3/input	input	\N	2019-05-13 06:27:40.176648	2019-05-13 06:27:40.176693	{"input_group": "default", "datatype": "Int", "dimension_names": ["source_1"]}	6532
4630	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sink_4/input	input	\N	2019-05-13 06:27:40.176648	2019-05-13 06:27:40.176693	{"input_group": "default", "datatype": "Int", "dimension_names": ["source_1"]}	6533
4631	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sink_5/input	input	\N	2019-05-13 06:27:40.176648	2019-05-13 06:27:40.176693	{"input_group": "default", "datatype": "Int", "dimension_names": ["source_1"]}	6534
4632	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/step_1/in_1	in_1	\N	2019-05-13 06:27:40.176648	2019-05-13 06:27:40.176693	{"input_group": "default", "datatype": "Int", "dimension_names": ["source_1"]}	6535
4633	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/step_1/in_2	in_2	\N	2019-05-13 06:27:40.176648	2019-05-13 06:27:40.176693	{"input_group": "default", "datatype": "Int", "dimension_names": ["source_2"]}	6535
4634	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/step_1/fail_1	fail_1	\N	2019-05-13 06:27:40.176648	2019-05-13 06:27:40.176693	{"input_group": "default", "datatype": "Boolean", "dimension_names": []}	6535
4635	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/step_1/fail_2	fail_2	\N	2019-05-13 06:27:40.176648	2019-05-13 06:27:40.176693	{"input_group": "default", "datatype": "Boolean", "dimension_names": ["const_step_1_fail_2_0"]}	6535
4636	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/step_2/in_1	in_1	\N	2019-05-13 06:27:40.176648	2019-05-13 06:27:40.176693	{"input_group": "default", "datatype": "Int", "dimension_names": ["source_3"]}	6536
4637	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/step_2/in_2	in_2	\N	2019-05-13 06:27:40.176648	2019-05-13 06:27:40.176693	{"input_group": "default", "datatype": "Int", "dimension_names": ["source_1"]}	6536
4638	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/step_2/fail_1	fail_1	\N	2019-05-13 06:27:40.176648	2019-05-13 06:27:40.176693	{"input_group": "default", "datatype": "Boolean", "dimension_names": ["const_step_2_fail_1_0"]}	6536
4639	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/step_2/fail_2	fail_2	\N	2019-05-13 06:27:40.176648	2019-05-13 06:27:40.176693	{"input_group": "default", "datatype": "Boolean", "dimension_names": []}	6536
4640	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/step_3/in_1	in_1	\N	2019-05-13 06:27:40.176648	2019-05-13 06:27:40.176693	{"input_group": "default", "datatype": "Int", "dimension_names": ["source_1"]}	6537
4641	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/step_3/in_2	in_2	\N	2019-05-13 06:27:40.176648	2019-05-13 06:27:40.176693	{"input_group": "default", "datatype": "Int", "dimension_names": ["source_3"]}	6537
4642	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/step_3/fail_1	fail_1	\N	2019-05-13 06:27:40.176648	2019-05-13 06:27:40.176693	{"input_group": "default", "datatype": "Boolean", "dimension_names": []}	6537
4643	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/step_3/fail_2	fail_2	\N	2019-05-13 06:27:40.176648	2019-05-13 06:27:40.176693	{"input_group": "default", "datatype": "Boolean", "dimension_names": []}	6537
4644	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/range/value	value	\N	2019-05-13 06:27:40.176648	2019-05-13 06:27:40.176693	{"input_group": "default", "datatype": "Int", "dimension_names": ["source_1"]}	6538
4645	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sum/values	values	\N	2019-05-13 06:27:40.176648	2019-05-13 06:27:40.176693	{"input_group": "default", "datatype": "Number", "dimension_names": ["source_1"]}	6539
4646	failing_macro_top_level_2019-05-13T08-29-04/root/add/left_hand	left_hand	\N	2019-05-13 06:27:40.176648	2019-05-13 06:27:40.176693	{"input_group": "default", "datatype": "Int", "dimension_names": []}	6524
4647	failing_macro_top_level_2019-05-13T08-29-04/root/add/right_hand	right_hand	\N	2019-05-13 06:27:40.176648	2019-05-13 06:27:40.176693	{"input_group": "default", "datatype": "Int", "dimension_names": ["const_add_right_hand_0"]}	6524
4648	failing_macro_top_level_2019-05-13T08-29-04/root/sink/input	input	\N	2019-05-13 06:27:40.176648	2019-05-13 06:27:40.176693	{"input_group": "default", "datatype": "Int", "dimension_names": ["source_a"]}	6525
\.


--
-- Data for Name: jobs; Type: TABLE DATA; Schema: public; Owner: pim
--

COPY public.jobs (id, path, title, description, created, modified, custom_data, run_id, node_id, node_path, status) FROM stdin;
575577	tissue_pipeline_2019-01-31T21-41-53/root/master	master		2019-04-03 10:16:01.588243	2019-04-03 10:16:01.588243	{}	624	5591	tissue_pipeline_2019-01-31T21-41-53/root	2
620857	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sink_1/failing_macro_top_level_failing_macro_failing_network___sink_1___sample_1_0	failing_macro_top_level_failing_macro_failing_network___sink_1___sample_1_0		2019-05-13 06:29:19.974458	2019-05-13 06:29:19.974458	{"sample_id": ["sample_1_0"], "sample_index": [0], "errors": []}	692	6530	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sink_1	2
620858	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sink_1/failing_macro_top_level_failing_macro_failing_network___sink_1___sample_1_1	failing_macro_top_level_failing_macro_failing_network___sink_1___sample_1_1		2019-05-13 06:29:19.974458	2019-05-13 06:29:19.974458	{"sample_id": ["sample_1_1"], "sample_index": [1], "errors": []}	692	6530	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sink_1	2
620859	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sink_1/failing_macro_top_level_failing_macro_failing_network___sink_1___sample_1_2	failing_macro_top_level_failing_macro_failing_network___sink_1___sample_1_2		2019-05-13 06:29:19.974458	2019-05-13 06:29:19.974458	{"sample_id": ["sample_1_2"], "sample_index": [2], "errors": []}	692	6530	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sink_1	2
620860	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sink_1/failing_macro_top_level_failing_macro_failing_network___sink_1___sample_1_3	failing_macro_top_level_failing_macro_failing_network___sink_1___sample_1_3		2019-05-13 06:29:19.974458	2019-05-13 06:29:19.974458	{"sample_id": ["sample_1_3"], "sample_index": [3], "errors": []}	692	6530	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sink_1	2
620861	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sink_2/failing_macro_top_level_failing_macro_failing_network___sink_2___sample_1_0	failing_macro_top_level_failing_macro_failing_network___sink_2___sample_1_0		2019-05-13 06:29:19.974458	2019-05-13 06:29:19.974458	{"sample_id": ["sample_1_0"], "sample_index": [0], "errors": []}	692	6531	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sink_2	2
620862	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sink_2/failing_macro_top_level_failing_macro_failing_network___sink_2___sample_1_1	failing_macro_top_level_failing_macro_failing_network___sink_2___sample_1_1		2019-05-13 06:29:19.974458	2019-05-13 06:29:19.974458	{"sample_id": ["sample_1_1"], "sample_index": [1], "errors": []}	692	6531	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sink_2	2
620863	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sink_2/failing_macro_top_level_failing_macro_failing_network___sink_2___sample_1_2	failing_macro_top_level_failing_macro_failing_network___sink_2___sample_1_2		2019-05-13 06:29:19.974458	2019-05-13 06:29:19.974458	{"sample_id": ["sample_1_2"], "sample_index": [2], "errors": []}	692	6531	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sink_2	2
620812	failing_macro_top_level_2019-05-13T08-29-04/root/master	master		2019-05-13 06:29:04.967625	2019-05-13 06:29:24.977399	{"__log__": {"2019-05-13 06:29:04.817062": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "pimreporter", "function": "pim_register_run", "lineno": 461, "message": "Registering failing_macro_top_level_2019-05-13T08-29-04 with PIM at http://localhost:8080/api/runs/"}, "2019-05-13 06:29:04.947627": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "pimreporter", "function": "pim_register_run", "lineno": 476, "message": "Run registered in PIM at http://localhost:8080/runs/failing_macro_top_level_2019-05-13T08-29-04"}, "2019-05-13 06:29:05.115013": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "networkchunker", "function": "chunck_network", "lineno": 146, "message": "Adding sink to candidates (blocking False)"}, "2019-05-13 06:29:05.116581": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "noderun", "function": "create_job", "lineno": 579, "message": "Creating job for node fastr:///networks/failing_macro_top_level/0.0/runs/failing_macro_top_level_2019-05-13T08-29-04/nodelist/source_a sample id <SampleId ('sample_1',)>, index <SampleIndex (0)>"}, "2019-05-13 06:29:05.127001": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "basepluginmanager", "function": "test_plugin", "lineno": 78, "message": "Could not load plugin file /home/thomas/workspaces/fastr/fastr/resources/plugins/targetplugins/singularitytarget.py\\nCould not find executable \\"singularity\\" on PATH: ['/home/thomas/workspaces/venv_fastr_dev/bin', '/home/thomas/.local/bin', '/usr/local/sbin', '/usr/local/bin', '/usr/sbin', '/usr/bin', '/sbin', '/bin', '/usr/games', '/usr/local/games', '/snap/bin']"}, "2019-05-13 06:29:05.134828": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "basepluginmanager", "function": "test_plugin", "lineno": 78, "message": "Could not load plugin file /home/thomas/workspaces/fastr/fastr/resources/plugins/executionplugins/slurmexecution.py\\nCould not find executable \\"sbatch\\" on PATH: ['/home/thomas/workspaces/venv_fastr_dev/bin', '/home/thomas/.local/bin', '/usr/local/sbin', '/usr/local/bin', '/usr/sbin', '/usr/bin', '/sbin', '/bin', '/usr/games', '/usr/local/games', '/snap/bin']"}, "2019-05-13 06:29:05.138152": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "noderun", "function": "create_job", "lineno": 579, "message": "Creating job for node fastr:///networks/failing_macro_top_level/0.0/runs/failing_macro_top_level_2019-05-13T08-29-04/nodelist/source_b sample id <SampleId ('sample_1',)>, index <SampleIndex (0)>"}, "2019-05-13 06:29:05.139639": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "noderun", "function": "create_job", "lineno": 579, "message": "Creating job for node fastr:///networks/failing_macro_top_level/0.0/runs/failing_macro_top_level_2019-05-13T08-29-04/nodelist/source_c sample id <SampleId ('sample_1',)>, index <SampleIndex (0)>"}, "2019-05-13 06:29:05.141408": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "noderun", "function": "create_job", "lineno": 579, "message": "Creating job for node fastr:///networks/failing_macro_top_level/0.0/runs/failing_macro_top_level_2019-05-13T08-29-04/nodelist/const_add_right_hand_0 sample id <SampleId ('id_0',)>, index <SampleIndex (0)>"}, "2019-05-13 06:29:05.158981": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "networkrun", "function": "execute", "lineno": 607, "message": "Waiting for execution to finish..."}, "2019-05-13 06:29:07.230595": {"process_name": "MainProcess", "thread_name": "CallbackProcessor-0", "level_name": "INFO", "module": "networkrun", "function": "job_finished", "lineno": 726, "message": "Finished job failing_macro_top_level___source_a___sample_1 with status JobState.finished"}, "2019-05-13 06:29:07.234995": {"process_name": "MainProcess", "thread_name": "CallbackProcessor-0", "level_name": "INFO", "module": "networkrun", "function": "job_finished", "lineno": 726, "message": "Finished job failing_macro_top_level___source_b___sample_1 with status JobState.finished"}, "2019-05-13 06:29:07.240850": {"process_name": "MainProcess", "thread_name": "CallbackProcessor-0", "level_name": "INFO", "module": "networkrun", "function": "job_finished", "lineno": 726, "message": "Finished job failing_macro_top_level___source_c___sample_1 with status JobState.finished"}, "2019-05-13 06:29:08.162601": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "networkrun", "function": "execute", "lineno": 618, "message": "Chunk execution finished!"}, "2019-05-13 06:29:08.201574": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "networkchunker", "function": "chunck_network", "lineno": 146, "message": "Adding sink_1 to candidates (blocking False)"}, "2019-05-13 06:29:08.201778": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "networkchunker", "function": "chunck_network", "lineno": 146, "message": "Adding step_3 to candidates (blocking False)"}, "2019-05-13 06:29:08.202087": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "networkchunker", "function": "chunck_network", "lineno": 146, "message": "Adding sink_2 to candidates (blocking False)"}, "2019-05-13 06:29:08.202380": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "networkchunker", "function": "chunck_network", "lineno": 146, "message": "Adding range to candidates (blocking False)"}, "2019-05-13 06:29:08.202527": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "networkchunker", "function": "chunck_network", "lineno": 146, "message": "Adding sink_3 to candidates (blocking False)"}, "2019-05-13 06:29:08.203026": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "networkchunker", "function": "chunck_network", "lineno": 146, "message": "Adding sink_5 to candidates (blocking False)"}, "2019-05-13 06:29:08.203717": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "noderun", "function": "create_job", "lineno": 579, "message": "Creating job for node fastr:///networks/failing_macro_top_level/0.0/runs/failing_macro_top_level_2019-05-13T08-29-04/nodelist/failing_macro/network_run/nodelist/const_step_1_fail_2_0 sample id <SampleId ('id_0',)>, index <SampleIndex (0)>"}, "2019-05-13 06:29:08.204879": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "noderun", "function": "create_job", "lineno": 579, "message": "Creating job for node fastr:///networks/failing_macro_top_level/0.0/runs/failing_macro_top_level_2019-05-13T08-29-04/nodelist/failing_macro/network_run/nodelist/const_step_1_fail_2_0 sample id <SampleId ('id_1',)>, index <SampleIndex (1)>"}, "2019-05-13 06:29:08.205810": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "noderun", "function": "create_job", "lineno": 579, "message": "Creating job for node fastr:///networks/failing_macro_top_level/0.0/runs/failing_macro_top_level_2019-05-13T08-29-04/nodelist/failing_macro/network_run/nodelist/const_step_1_fail_2_0 sample id <SampleId ('id_2',)>, index <SampleIndex (2)>"}, "2019-05-13 06:29:08.206560": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "noderun", "function": "create_job", "lineno": 579, "message": "Creating job for node fastr:///networks/failing_macro_top_level/0.0/runs/failing_macro_top_level_2019-05-13T08-29-04/nodelist/failing_macro/network_run/nodelist/const_step_1_fail_2_0 sample id <SampleId ('id_3',)>, index <SampleIndex (3)>"}, "2019-05-13 06:29:08.207659": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "noderun", "function": "create_job", "lineno": 579, "message": "Creating job for node fastr:///networks/failing_macro_top_level/0.0/runs/failing_macro_top_level_2019-05-13T08-29-04/nodelist/failing_macro/network_run/nodelist/const_step_2_fail_1_0 sample id <SampleId ('id_0',)>, index <SampleIndex (0)>"}, "2019-05-13 06:29:08.208536": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "noderun", "function": "create_job", "lineno": 579, "message": "Creating job for node fastr:///networks/failing_macro_top_level/0.0/runs/failing_macro_top_level_2019-05-13T08-29-04/nodelist/failing_macro/network_run/nodelist/const_step_2_fail_1_0 sample id <SampleId ('id_1',)>, index <SampleIndex (1)>"}, "2019-05-13 06:29:08.209244": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "noderun", "function": "create_job", "lineno": 579, "message": "Creating job for node fastr:///networks/failing_macro_top_level/0.0/runs/failing_macro_top_level_2019-05-13T08-29-04/nodelist/failing_macro/network_run/nodelist/const_step_2_fail_1_0 sample id <SampleId ('id_2',)>, index <SampleIndex (2)>"}, "2019-05-13 06:29:08.209910": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "noderun", "function": "create_job", "lineno": 579, "message": "Creating job for node fastr:///networks/failing_macro_top_level/0.0/runs/failing_macro_top_level_2019-05-13T08-29-04/nodelist/failing_macro/network_run/nodelist/const_step_2_fail_1_0 sample id <SampleId ('id_3',)>, index <SampleIndex (3)>"}, "2019-05-13 06:29:08.211920": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "noderun", "function": "execute", "lineno": 473, "message": "Generating jobs for node \\"step_1\\" with dimensions: [source_1: 4]"}, "2019-05-13 06:29:08.214724": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "noderun", "function": "create_job", "lineno": 579, "message": "Creating job for node fastr:///networks/failing_macro_top_level/0.0/runs/failing_macro_top_level_2019-05-13T08-29-04/nodelist/failing_macro/network_run/nodelist/step_1 sample id <SampleId ('sample_1_0',)>, index <SampleIndex (0)>"}, "2019-05-13 06:29:08.215621": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "noderun", "function": "create_job", "lineno": 579, "message": "Creating job for node fastr:///networks/failing_macro_top_level/0.0/runs/failing_macro_top_level_2019-05-13T08-29-04/nodelist/failing_macro/network_run/nodelist/step_1 sample id <SampleId ('sample_1_1',)>, index <SampleIndex (1)>"}, "2019-05-13 06:29:08.216501": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "noderun", "function": "create_job", "lineno": 579, "message": "Creating job for node fastr:///networks/failing_macro_top_level/0.0/runs/failing_macro_top_level_2019-05-13T08-29-04/nodelist/failing_macro/network_run/nodelist/step_1 sample id <SampleId ('sample_1_2',)>, index <SampleIndex (2)>"}, "2019-05-13 06:29:08.217244": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "noderun", "function": "create_job", "lineno": 579, "message": "Creating job for node fastr:///networks/failing_macro_top_level/0.0/runs/failing_macro_top_level_2019-05-13T08-29-04/nodelist/failing_macro/network_run/nodelist/step_1 sample id <SampleId ('sample_1_3',)>, index <SampleIndex (3)>"}, "2019-05-13 06:29:08.219065": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "noderun", "function": "execute", "lineno": 473, "message": "Generating jobs for node \\"step_2\\" with dimensions: [source_3: 4]"}, "2019-05-13 06:29:08.221649": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "noderun", "function": "create_job", "lineno": 579, "message": "Creating job for node fastr:///networks/failing_macro_top_level/0.0/runs/failing_macro_top_level_2019-05-13T08-29-04/nodelist/failing_macro/network_run/nodelist/step_2 sample id <SampleId ('sample_1_0',)>, index <SampleIndex (0)>"}, "2019-05-13 06:29:08.222386": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "noderun", "function": "create_job", "lineno": 579, "message": "Creating job for node fastr:///networks/failing_macro_top_level/0.0/runs/failing_macro_top_level_2019-05-13T08-29-04/nodelist/failing_macro/network_run/nodelist/step_2 sample id <SampleId ('sample_1_1',)>, index <SampleIndex (1)>"}, "2019-05-13 06:29:08.223092": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "noderun", "function": "create_job", "lineno": 579, "message": "Creating job for node fastr:///networks/failing_macro_top_level/0.0/runs/failing_macro_top_level_2019-05-13T08-29-04/nodelist/failing_macro/network_run/nodelist/step_2 sample id <SampleId ('sample_1_2',)>, index <SampleIndex (2)>"}, "2019-05-13 06:29:08.224222": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "noderun", "function": "create_job", "lineno": 579, "message": "Creating job for node fastr:///networks/failing_macro_top_level/0.0/runs/failing_macro_top_level_2019-05-13T08-29-04/nodelist/failing_macro/network_run/nodelist/step_2 sample id <SampleId ('sample_1_3',)>, index <SampleIndex (3)>"}, "2019-05-13 06:29:08.226354": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "noderun", "function": "execute", "lineno": 473, "message": "Generating jobs for node \\"step_3\\" with dimensions: [source_1: 4]"}, "2019-05-13 06:29:08.228667": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "noderun", "function": "create_job", "lineno": 579, "message": "Creating job for node fastr:///networks/failing_macro_top_level/0.0/runs/failing_macro_top_level_2019-05-13T08-29-04/nodelist/failing_macro/network_run/nodelist/step_3 sample id <SampleId ('sample_1_0',)>, index <SampleIndex (0)>"}, "2019-05-13 06:29:08.229907": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "noderun", "function": "create_job", "lineno": 579, "message": "Creating job for node fastr:///networks/failing_macro_top_level/0.0/runs/failing_macro_top_level_2019-05-13T08-29-04/nodelist/failing_macro/network_run/nodelist/step_3 sample id <SampleId ('sample_1_1',)>, index <SampleIndex (1)>"}, "2019-05-13 06:29:08.230844": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "noderun", "function": "create_job", "lineno": 579, "message": "Creating job for node fastr:///networks/failing_macro_top_level/0.0/runs/failing_macro_top_level_2019-05-13T08-29-04/nodelist/failing_macro/network_run/nodelist/step_3 sample id <SampleId ('sample_1_2',)>, index <SampleIndex (2)>"}, "2019-05-13 06:29:08.231630": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "noderun", "function": "create_job", "lineno": 579, "message": "Creating job for node fastr:///networks/failing_macro_top_level/0.0/runs/failing_macro_top_level_2019-05-13T08-29-04/nodelist/failing_macro/network_run/nodelist/step_3 sample id <SampleId ('sample_1_3',)>, index <SampleIndex (3)>"}, "2019-05-13 06:29:08.232916": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "noderun", "function": "execute", "lineno": 473, "message": "Generating jobs for node \\"range\\" with dimensions: [source_1: 4]"}, "2019-05-13 06:29:08.234038": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "noderun", "function": "create_job", "lineno": 579, "message": "Creating job for node fastr:///networks/failing_macro_top_level/0.0/runs/failing_macro_top_level_2019-05-13T08-29-04/nodelist/failing_macro/network_run/nodelist/range sample id <SampleId ('sample_1_0',)>, index <SampleIndex (0)>"}, "2019-05-13 06:29:08.234668": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "noderun", "function": "create_job", "lineno": 579, "message": "Creating job for node fastr:///networks/failing_macro_top_level/0.0/runs/failing_macro_top_level_2019-05-13T08-29-04/nodelist/failing_macro/network_run/nodelist/range sample id <SampleId ('sample_1_1',)>, index <SampleIndex (1)>"}, "2019-05-13 06:29:08.235291": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "noderun", "function": "create_job", "lineno": 579, "message": "Creating job for node fastr:///networks/failing_macro_top_level/0.0/runs/failing_macro_top_level_2019-05-13T08-29-04/nodelist/failing_macro/network_run/nodelist/range sample id <SampleId ('sample_1_2',)>, index <SampleIndex (2)>"}, "2019-05-13 06:29:08.235806": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "noderun", "function": "create_job", "lineno": 579, "message": "Creating job for node fastr:///networks/failing_macro_top_level/0.0/runs/failing_macro_top_level_2019-05-13T08-29-04/nodelist/failing_macro/network_run/nodelist/range sample id <SampleId ('sample_1_3',)>, index <SampleIndex (3)>"}, "2019-05-13 06:29:08.306653": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "networkrun", "function": "execute", "lineno": 607, "message": "Waiting for execution to finish..."}, "2019-05-13 06:29:10.520761": {"process_name": "MainProcess", "thread_name": "CallbackProcessor-0", "level_name": "INFO", "module": "networkrun", "function": "job_finished", "lineno": 726, "message": "Finished job failing_macro_top_level_failing_macro_failing_network___step_2___sample_1_0 with status JobState.finished"}, "2019-05-13 06:29:10.534687": {"process_name": "MainProcess", "thread_name": "CallbackProcessor-0", "level_name": "INFO", "module": "networkrun", "function": "job_finished", "lineno": 726, "message": "Finished job failing_macro_top_level_failing_macro_failing_network___step_1___sample_1_3 with status JobState.failed"}, "2019-05-13 06:29:10.599692": {"process_name": "MainProcess", "thread_name": "CallbackProcessor-0", "level_name": "INFO", "module": "networkrun", "function": "job_finished", "lineno": 726, "message": "Finished job failing_macro_top_level_failing_macro_failing_network___step_1___sample_1_1 with status JobState.failed"}, "2019-05-13 06:29:10.604267": {"process_name": "MainProcess", "thread_name": "CallbackProcessor-0", "level_name": "INFO", "module": "networkrun", "function": "job_finished", "lineno": 726, "message": "Finished job failing_macro_top_level_failing_macro_failing_network___step_2___sample_1_3 with status JobState.failed"}, "2019-05-13 06:29:10.632215": {"process_name": "MainProcess", "thread_name": "CallbackProcessor-0", "level_name": "INFO", "module": "networkrun", "function": "job_finished", "lineno": 726, "message": "Finished job failing_macro_top_level_failing_macro_failing_network___step_3___sample_1_3 with status JobState.cancelled"}, "2019-05-13 06:29:10.646369": {"process_name": "MainProcess", "thread_name": "CallbackProcessor-0", "level_name": "INFO", "module": "networkrun", "function": "job_finished", "lineno": 726, "message": "Finished job failing_macro_top_level_failing_macro_failing_network___range___sample_1_3 with status JobState.cancelled"}, "2019-05-13 06:29:10.917547": {"process_name": "MainProcess", "thread_name": "CallbackProcessor-0", "level_name": "INFO", "module": "networkrun", "function": "job_finished", "lineno": 726, "message": "Finished job failing_macro_top_level_failing_macro_failing_network___step_1___sample_1_0 with status JobState.finished"}, "2019-05-13 06:29:10.951316": {"process_name": "MainProcess", "thread_name": "CallbackProcessor-0", "level_name": "INFO", "module": "networkrun", "function": "job_finished", "lineno": 726, "message": "Finished job failing_macro_top_level_failing_macro_failing_network___step_1___sample_1_2 with status JobState.finished"}, "2019-05-13 06:29:10.953954": {"process_name": "MainProcess", "thread_name": "CallbackProcessor-0", "level_name": "INFO", "module": "networkrun", "function": "job_finished", "lineno": 726, "message": "Finished job failing_macro_top_level_failing_macro_failing_network___step_2___sample_1_1 with status JobState.finished"}, "2019-05-13 06:29:10.967847": {"process_name": "MainProcess", "thread_name": "CallbackProcessor-0", "level_name": "INFO", "module": "networkrun", "function": "job_finished", "lineno": 726, "message": "Finished job failing_macro_top_level_failing_macro_failing_network___step_3___sample_1_1 with status JobState.cancelled"}, "2019-05-13 06:29:10.981624": {"process_name": "MainProcess", "thread_name": "CallbackProcessor-0", "level_name": "INFO", "module": "networkrun", "function": "job_finished", "lineno": 726, "message": "Finished job failing_macro_top_level_failing_macro_failing_network___range___sample_1_1 with status JobState.cancelled"}, "2019-05-13 06:29:10.984673": {"process_name": "MainProcess", "thread_name": "CallbackProcessor-0", "level_name": "INFO", "module": "networkrun", "function": "job_finished", "lineno": 726, "message": "Finished job failing_macro_top_level_failing_macro_failing_network___step_2___sample_1_2 with status JobState.failed"}, "2019-05-13 06:29:10.998679": {"process_name": "MainProcess", "thread_name": "CallbackProcessor-0", "level_name": "INFO", "module": "networkrun", "function": "job_finished", "lineno": 726, "message": "Finished job failing_macro_top_level_failing_macro_failing_network___step_3___sample_1_2 with status JobState.cancelled"}, "2019-05-13 06:29:11.012399": {"process_name": "MainProcess", "thread_name": "CallbackProcessor-0", "level_name": "INFO", "module": "networkrun", "function": "job_finished", "lineno": 726, "message": "Finished job failing_macro_top_level_failing_macro_failing_network___range___sample_1_2 with status JobState.cancelled"}, "2019-05-13 06:29:12.971911": {"process_name": "MainProcess", "thread_name": "CallbackProcessor-0", "level_name": "INFO", "module": "networkrun", "function": "job_finished", "lineno": 726, "message": "Finished job failing_macro_top_level_failing_macro_failing_network___step_3___sample_1_0 with status JobState.finished"}, "2019-05-13 06:29:15.047898": {"process_name": "MainProcess", "thread_name": "CallbackProcessor-0", "level_name": "INFO", "module": "networkrun", "function": "job_finished", "lineno": 726, "message": "Finished job failing_macro_top_level_failing_macro_failing_network___range___sample_1_0 with status JobState.finished"}, "2019-05-13 06:29:15.314604": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "networkrun", "function": "execute", "lineno": 618, "message": "Chunk execution finished!"}, "2019-05-13 06:29:15.317028": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "noderun", "function": "execute", "lineno": 473, "message": "Generating jobs for node \\"sum\\" with dimensions: [source_1: 4]"}, "2019-05-13 06:29:15.322112": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "noderun", "function": "create_job", "lineno": 579, "message": "Creating job for node fastr:///networks/failing_macro_top_level/0.0/runs/failing_macro_top_level_2019-05-13T08-29-04/nodelist/failing_macro/network_run/nodelist/sum sample id <SampleId ('sample_1_0',)>, index <SampleIndex (0)>"}, "2019-05-13 06:29:15.325070": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "noderun", "function": "create_job", "lineno": 579, "message": "Creating job for node fastr:///networks/failing_macro_top_level/0.0/runs/failing_macro_top_level_2019-05-13T08-29-04/nodelist/failing_macro/network_run/nodelist/sum sample id <SampleId ('sample_1_1',)>, index <SampleIndex (1)>"}, "2019-05-13 06:29:15.327458": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "noderun", "function": "create_job", "lineno": 579, "message": "Creating job for node fastr:///networks/failing_macro_top_level/0.0/runs/failing_macro_top_level_2019-05-13T08-29-04/nodelist/failing_macro/network_run/nodelist/sum sample id <SampleId ('sample_1_2',)>, index <SampleIndex (2)>"}, "2019-05-13 06:29:15.329930": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "noderun", "function": "create_job", "lineno": 579, "message": "Creating job for node fastr:///networks/failing_macro_top_level/0.0/runs/failing_macro_top_level_2019-05-13T08-29-04/nodelist/failing_macro/network_run/nodelist/sum sample id <SampleId ('sample_1_3',)>, index <SampleIndex (3)>"}, "2019-05-13 06:29:15.358572": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "networkrun", "function": "job_finished", "lineno": 726, "message": "Finished job failing_macro_top_level_failing_macro_failing_network___sum___sample_1_1 with status JobState.cancelled"}, "2019-05-13 06:29:15.377147": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "networkrun", "function": "job_finished", "lineno": 726, "message": "Finished job failing_macro_top_level_failing_macro_failing_network___sum___sample_1_2 with status JobState.cancelled"}, "2019-05-13 06:29:15.395206": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "networkrun", "function": "job_finished", "lineno": 726, "message": "Finished job failing_macro_top_level_failing_macro_failing_network___sum___sample_1_3 with status JobState.cancelled"}, "2019-05-13 06:29:15.395357": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "networkrun", "function": "execute", "lineno": 607, "message": "Waiting for execution to finish..."}, "2019-05-13 06:29:17.472866": {"process_name": "MainProcess", "thread_name": "CallbackProcessor-0", "level_name": "INFO", "module": "networkrun", "function": "job_finished", "lineno": 726, "message": "Finished job failing_macro_top_level_failing_macro_failing_network___sum___sample_1_0 with status JobState.finished"}, "2019-05-13 06:29:18.398990": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "networkrun", "function": "execute", "lineno": 618, "message": "Chunk execution finished!"}, "2019-05-13 06:29:18.433564": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "noderun", "function": "execute", "lineno": 473, "message": "Generating jobs for node \\"add\\" with dimensions: [source_a: 4]"}, "2019-05-13 06:29:18.470229": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "noderun", "function": "create_job", "lineno": 579, "message": "Creating job for node fastr:///networks/failing_macro_top_level/0.0/runs/failing_macro_top_level_2019-05-13T08-29-04/nodelist/add sample id <SampleId ('sample_1_0',)>, index <SampleIndex (0)>"}, "2019-05-13 06:29:18.477355": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "noderun", "function": "create_job", "lineno": 579, "message": "Creating job for node fastr:///networks/failing_macro_top_level/0.0/runs/failing_macro_top_level_2019-05-13T08-29-04/nodelist/add sample id <SampleId ('sample_1_1',)>, index <SampleIndex (1)>"}, "2019-05-13 06:29:18.483998": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "noderun", "function": "create_job", "lineno": 579, "message": "Creating job for node fastr:///networks/failing_macro_top_level/0.0/runs/failing_macro_top_level_2019-05-13T08-29-04/nodelist/add sample id <SampleId ('sample_1_2',)>, index <SampleIndex (2)>"}, "2019-05-13 06:29:18.491172": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "noderun", "function": "create_job", "lineno": 579, "message": "Creating job for node fastr:///networks/failing_macro_top_level/0.0/runs/failing_macro_top_level_2019-05-13T08-29-04/nodelist/add sample id <SampleId ('sample_1_3',)>, index <SampleIndex (3)>"}, "2019-05-13 06:29:18.500701": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "noderun", "function": "create_job", "lineno": 579, "message": "Creating job for node fastr:///networks/failing_macro_top_level/0.0/runs/failing_macro_top_level_2019-05-13T08-29-04/nodelist/sink sample id <SampleId ('sample_1_0',)>, index <SampleIndex (0)>"}, "2019-05-13 06:29:18.501319": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "noderun", "function": "create_job", "lineno": 579, "message": "Creating job for node fastr:///networks/failing_macro_top_level/0.0/runs/failing_macro_top_level_2019-05-13T08-29-04/nodelist/sink sample id <SampleId ('sample_1_1',)>, index <SampleIndex (1)>"}, "2019-05-13 06:29:18.501889": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "noderun", "function": "create_job", "lineno": 579, "message": "Creating job for node fastr:///networks/failing_macro_top_level/0.0/runs/failing_macro_top_level_2019-05-13T08-29-04/nodelist/sink sample id <SampleId ('sample_1_2',)>, index <SampleIndex (2)>"}, "2019-05-13 06:29:18.502629": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "noderun", "function": "create_job", "lineno": 579, "message": "Creating job for node fastr:///networks/failing_macro_top_level/0.0/runs/failing_macro_top_level_2019-05-13T08-29-04/nodelist/sink sample id <SampleId ('sample_1_3',)>, index <SampleIndex (3)>"}, "2019-05-13 06:29:18.535640": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "networkrun", "function": "job_finished", "lineno": 726, "message": "Finished job failing_macro_top_level___add___sample_1_1 with status JobState.cancelled"}, "2019-05-13 06:29:18.559113": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "networkrun", "function": "job_finished", "lineno": 726, "message": "Finished job failing_macro_top_level___add___sample_1_2 with status JobState.cancelled"}, "2019-05-13 06:29:18.582276": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "networkrun", "function": "job_finished", "lineno": 726, "message": "Finished job failing_macro_top_level___add___sample_1_3 with status JobState.cancelled"}, "2019-05-13 06:29:18.603105": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "networkrun", "function": "job_finished", "lineno": 726, "message": "Finished job failing_macro_top_level___sink___sample_1_1___0 with status JobState.cancelled"}, "2019-05-13 06:29:18.620780": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "networkrun", "function": "job_finished", "lineno": 726, "message": "Finished job failing_macro_top_level___sink___sample_1_2___0 with status JobState.cancelled"}, "2019-05-13 06:29:18.638765": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "networkrun", "function": "job_finished", "lineno": 726, "message": "Finished job failing_macro_top_level___sink___sample_1_3___0 with status JobState.cancelled"}, "2019-05-13 06:29:18.638943": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "networkrun", "function": "execute", "lineno": 607, "message": "Waiting for execution to finish..."}, "2019-05-13 06:29:20.637293": {"process_name": "MainProcess", "thread_name": "CallbackProcessor-0", "level_name": "INFO", "module": "networkrun", "function": "job_finished", "lineno": 726, "message": "Finished job failing_macro_top_level___add___sample_1_0 with status JobState.finished"}, "2019-05-13 06:29:22.753965": {"process_name": "MainProcess", "thread_name": "CallbackProcessor-0", "level_name": "INFO", "module": "networkrun", "function": "job_finished", "lineno": 726, "message": "Finished job failing_macro_top_level___sink___sample_1_0___0 with status JobState.finished"}, "2019-05-13 06:29:23.643993": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "networkrun", "function": "execute", "lineno": 618, "message": "Chunk execution finished!"}, "2019-05-13 06:29:24.754491": {"process_name": "MainProcess", "thread_name": "CallbackProcessor-0", "level_name": "INFO", "module": "executionplugin", "function": "process_callbacks", "lineno": 483, "message": "Callback processing thread for ProcessPoolExecution ended!"}, "2019-05-13 06:29:24.810174": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "networkrun", "function": "execute", "lineno": 620, "message": "####################################"}, "2019-05-13 06:29:24.810432": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "networkrun", "function": "execute", "lineno": 621, "message": "#    network execution FINISHED    #"}, "2019-05-13 06:29:24.810552": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "networkrun", "function": "execute", "lineno": 622, "message": "####################################"}, "2019-05-13 06:29:24.811089": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "simplereport", "function": "run_finished", "lineno": 26, "message": "===== RESULTS ====="}, "2019-05-13 06:29:24.811227": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "simplereport", "function": "run_finished", "lineno": 35, "message": "sink: 1 success / 3 failed"}, "2019-05-13 06:29:24.811327": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "INFO", "module": "simplereport", "function": "run_finished", "lineno": 36, "message": "==================="}, "2019-05-13 06:29:24.811419": {"process_name": "MainProcess", "thread_name": "MainThread", "level_name": "WARNING", "module": "simplereport", "function": "run_finished", "lineno": 48, "message": "There were failed samples in the run, to start debugging you can run:\\n\\n    fastr trace /tmp/fastr_failing_macro_top_level_2019-05-13T08-29-04_jnp24rgq/__sink_data__.json --sinks\\n\\nsee the debug section in the manual at https://fastr.readthedocs.io/en/develop/static/user_manual.html#debugging for more information."}}}	692	6519	failing_macro_top_level_2019-05-13T08-29-04/root	3
620813	failing_macro_top_level_2019-05-13T08-29-04/root/source_a/failing_macro_top_level___source_a___sample_1	failing_macro_top_level___source_a___sample_1		2019-05-13 06:29:07.461414	2019-05-13 06:29:07.461414	{"sample_id": ["sample_1"], "sample_index": [0], "errors": []}	692	6520	failing_macro_top_level_2019-05-13T08-29-04/root/source_a	2
620814	failing_macro_top_level_2019-05-13T08-29-04/root/source_b/failing_macro_top_level___source_b___sample_1	failing_macro_top_level___source_b___sample_1		2019-05-13 06:29:07.461414	2019-05-13 06:29:07.461414	{"sample_id": ["sample_1"], "sample_index": [0], "errors": []}	692	6521	failing_macro_top_level_2019-05-13T08-29-04/root/source_b	2
620815	failing_macro_top_level_2019-05-13T08-29-04/root/source_c/failing_macro_top_level___source_c___sample_1	failing_macro_top_level___source_c___sample_1		2019-05-13 06:29:07.461414	2019-05-13 06:29:07.461414	{"sample_id": ["sample_1"], "sample_index": [0], "errors": []}	692	6522	failing_macro_top_level_2019-05-13T08-29-04/root/source_c	2
620816	failing_macro_top_level_2019-05-13T08-29-04/root/const_add_right_hand_0/failing_macro_top_level___const_add_right_hand_0___id_0	failing_macro_top_level___const_add_right_hand_0___id_0		2019-05-13 06:29:07.461414	2019-05-13 06:29:07.461414	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	692	6526	failing_macro_top_level_2019-05-13T08-29-04/root/const_add_right_hand_0	2
620854	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sum/failing_macro_top_level_failing_macro_failing_network___sum___sample_1_1	failing_macro_top_level_failing_macro_failing_network___sum___sample_1_1		2019-05-13 06:29:17.465092	2019-05-13 06:29:17.465092	{"sample_id": ["sample_1_1"], "sample_index": [1], "errors": []}	692	6539	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sum	4
620855	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sum/failing_macro_top_level_failing_macro_failing_network___sum___sample_1_2	failing_macro_top_level_failing_macro_failing_network___sum___sample_1_2		2019-05-13 06:29:17.465092	2019-05-13 06:29:17.465092	{"sample_id": ["sample_1_2"], "sample_index": [2], "errors": []}	692	6539	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sum	4
620856	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sum/failing_macro_top_level_failing_macro_failing_network___sum___sample_1_3	failing_macro_top_level_failing_macro_failing_network___sum___sample_1_3		2019-05-13 06:29:17.465092	2019-05-13 06:29:17.465092	{"sample_id": ["sample_1_3"], "sample_index": [3], "errors": []}	692	6539	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sum	4
620853	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sum/failing_macro_top_level_failing_macro_failing_network___sum___sample_1_0	failing_macro_top_level_failing_macro_failing_network___sum___sample_1_0		2019-05-13 06:29:17.465092	2019-05-13 06:29:19.974458	{"sample_id": ["sample_1_0"], "sample_index": [0], "errors": []}	692	6539	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sum	2
620864	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sink_2/failing_macro_top_level_failing_macro_failing_network___sink_2___sample_1_3	failing_macro_top_level_failing_macro_failing_network___sink_2___sample_1_3		2019-05-13 06:29:19.974458	2019-05-13 06:29:19.974458	{"sample_id": ["sample_1_3"], "sample_index": [3], "errors": []}	692	6531	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sink_2	2
620865	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sink_3/failing_macro_top_level_failing_macro_failing_network___sink_3___sample_1_0	failing_macro_top_level_failing_macro_failing_network___sink_3___sample_1_0		2019-05-13 06:29:19.974458	2019-05-13 06:29:19.974458	{"sample_id": ["sample_1_0"], "sample_index": [0], "errors": []}	692	6532	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sink_3	2
620866	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sink_3/failing_macro_top_level_failing_macro_failing_network___sink_3___sample_1_1	failing_macro_top_level_failing_macro_failing_network___sink_3___sample_1_1		2019-05-13 06:29:19.974458	2019-05-13 06:29:19.974458	{"sample_id": ["sample_1_1"], "sample_index": [1], "errors": []}	692	6532	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sink_3	2
620867	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sink_3/failing_macro_top_level_failing_macro_failing_network___sink_3___sample_1_2	failing_macro_top_level_failing_macro_failing_network___sink_3___sample_1_2		2019-05-13 06:29:19.974458	2019-05-13 06:29:19.974458	{"sample_id": ["sample_1_2"], "sample_index": [2], "errors": []}	692	6532	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sink_3	2
620868	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sink_3/failing_macro_top_level_failing_macro_failing_network___sink_3___sample_1_3	failing_macro_top_level_failing_macro_failing_network___sink_3___sample_1_3		2019-05-13 06:29:19.974458	2019-05-13 06:29:19.974458	{"sample_id": ["sample_1_3"], "sample_index": [3], "errors": []}	692	6532	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sink_3	2
620869	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sink_4/failing_macro_top_level_failing_macro_failing_network___sink_4___sample_1_0	failing_macro_top_level_failing_macro_failing_network___sink_4___sample_1_0		2019-05-13 06:29:19.974458	2019-05-13 06:29:19.974458	{"sample_id": ["sample_1_0"], "sample_index": [0], "errors": []}	692	6533	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sink_4	2
620870	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sink_4/failing_macro_top_level_failing_macro_failing_network___sink_4___sample_1_1	failing_macro_top_level_failing_macro_failing_network___sink_4___sample_1_1		2019-05-13 06:29:19.974458	2019-05-13 06:29:19.974458	{"sample_id": ["sample_1_1"], "sample_index": [1], "errors": []}	692	6533	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sink_4	2
620871	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sink_4/failing_macro_top_level_failing_macro_failing_network___sink_4___sample_1_2	failing_macro_top_level_failing_macro_failing_network___sink_4___sample_1_2		2019-05-13 06:29:19.974458	2019-05-13 06:29:19.974458	{"sample_id": ["sample_1_2"], "sample_index": [2], "errors": []}	692	6533	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sink_4	2
620872	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sink_4/failing_macro_top_level_failing_macro_failing_network___sink_4___sample_1_3	failing_macro_top_level_failing_macro_failing_network___sink_4___sample_1_3		2019-05-13 06:29:19.974458	2019-05-13 06:29:19.974458	{"sample_id": ["sample_1_3"], "sample_index": [3], "errors": []}	692	6533	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sink_4	2
620840	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/step_1/failing_macro_top_level_failing_macro_failing_network___step_1___sample_1_3	failing_macro_top_level_failing_macro_failing_network___step_1___sample_1_3		2019-05-13 06:29:09.963361	2019-05-13 06:29:12.466034	{"sample_id": ["sample_1_3"], "sample_index": [3], "errors": [["FastrOutputValidationError", "Could not find result for output out_2", "/home/thomas/workspaces/fastr/fastr/execution/job.py", 961, "File /home/thomas/workspaces/fastr/fastr/execution/executionscript.py, line 185 in <module>\\n  main()\\nFile /home/thomas/workspaces/fastr/fastr/execution/executionscript.py, line 180 in main\\n  execute_job(joblist)\\nFile /home/thomas/workspaces/fastr/fastr/execution/executionscript.py, line 87 in execute_job\\n  job.execute()\\nFile /home/thomas/workspaces/fastr/fastr/execution/job.py, line 716 in execute\\n  if not self.validate_results(payload):\\nFile /home/thomas/workspaces/fastr/fastr/execution/job.py, line 961 in validate_results\\n  self.errors.append(exceptions.FastrOutputValidationError(message).excerpt())"], ["FastrValueError", "Output values are not valid!", "/home/thomas/workspaces/fastr/fastr/execution/job.py", 717, "File /home/thomas/workspaces/fastr/fastr/execution/executionscript.py, line 185 in <module>\\n  main()\\nFile /home/thomas/workspaces/fastr/fastr/execution/executionscript.py, line 180 in main\\n  execute_job(joblist)\\nFile /home/thomas/workspaces/fastr/fastr/execution/executionscript.py, line 87 in execute_job\\n  job.execute()\\nFile /home/thomas/workspaces/fastr/fastr/execution/job.py, line 717 in execute\\n  raise exceptions.FastrValueError('Output values are not valid!')"]], "__stdout__": "Namespace(fail_1=False, fail_2=True, in_1=9, in_2=9)\\nin 1  : 9\\nin 2  : 9\\nfail_1: False\\nfail_2: True\\nRESULT_1=[10]\\n", "__stderr__": "", "command": ["python", "/home/thomas/workspaces/fastr/fastr/resources/tools/fastr/util/1.0/bin/fail.py", "--in_1", "9", "--in_2", "9", "--fail_2"], "returncode": null, "time_elapsed": 0.04839468002319336, "hostinfo": {"os": {"system": "Linux", "is64bits": 1, "maxsize": 9223372036854775807, "version": {"distname": "Ubuntu", "version": "18.04", "id": "bionic", "libc": {"libname": "glibc", "version": "2.25"}}}, "uname": ["Linux", "thomas-xps", "4.15.0-47-generic", "#50-Ubuntu SMP Wed Mar 13 10:44:52 UTC 2019", "x86_64", "x86_64"], "username": "thomas", "cwd": "/home/thomas/workspaces", "python": {"version": "3.6.7", "branch": "", "compiler": "GCC 8.2.0", "implementation": "CPython", "revision": ""}, "envvar": {"CLUTTER_IM_MODULE": "xim", "LS_COLORS": "rs=0:di=01;34:ln=01;36:mh=00:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:mi=00:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=34;42:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arc=01;31:*.arj=01;31:*.taz=01;31:*.lha=01;31:*.lz4=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.tzo=01;31:*.t7z=01;31:*.zip=01;31:*.z=01;31:*.Z=01;31:*.dz=01;31:*.gz=01;31:*.lrz=01;31:*.lz=01;31:*.lzo=01;31:*.xz=01;31:*.zst=01;31:*.tzst=01;31:*.bz2=01;31:*.bz=01;31:*.tbz=01;31:*.tbz2=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.war=01;31:*.ear=01;31:*.sar=01;31:*.rar=01;31:*.alz=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.cab=01;31:*.wim=01;31:*.swm=01;31:*.dwm=01;31:*.esd=01;31:*.jpg=01;35:*.jpeg=01;35:*.mjpg=01;35:*.mjpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.webm=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt=01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.ogv=01;35:*.ogx=01;35:*.aac=00;36:*.au=00;36:*.flac=00;36:*.m4a=00;36:*.mid=00;36:*.midi=00;36:*.mka=00;36:*.mp3=00;36:*.mpc=00;36:*.ogg=00;36:*.ra=00;36:*.wav=00;36:*.oga=00;36:*.opus=00;36:*.spx=00;36:*.xspf=00;36:", "TERMINAL_EMULATOR": "JetBrains-JediTerm", "LC_MEASUREMENT": "nl_NL.UTF-8", "LESSCLOSE": "/usr/bin/lesspipe %s %s", "LC_PAPER": "nl_NL.UTF-8", "LC_MONETARY": "nl_NL.UTF-8", "XDG_MENU_PREFIX": "gnome-", "LANG": "en_US.UTF-8", "PROJECT_DIR": "/home/thomas/workspaces/pim", "DISPLAY": ":0", "OLDPWD": "/home/thomas", "GNOME_SHELL_SESSION_MODE": "ubuntu", "USERNAME": "thomas", "XDG_VTNR": "2", "GIO_LAUNCHED_DESKTOP_FILE_PID": "22190", "SSH_AUTH_SOCK": "/run/user/1000/keyring/ssh", "VIRTUAL_ENV": "/home/thomas/workspaces/venv_fastr_dev", "LC_NAME": "nl_NL.UTF-8", "XDG_SESSION_ID": "2", "USER": "thomas", "DESKTOP_SESSION": "ubuntu", "QT4_IM_MODULE": "xim", "TEXTDOMAINDIR": "/usr/share/locale/", "PWD": "/home/thomas/workspaces", "HOME": "/home/thomas", "TEXTDOMAIN": "im-config", "SSH_AGENT_PID": "3341", "QT_ACCESSIBILITY": "1", "XDG_SESSION_TYPE": "x11", "XDG_DATA_DIRS": "/usr/share/ubuntu:/usr/local/share:/usr/share:/var/lib/snapd/desktop", "HISTFILE": "/home/thomas/.PyCharm2018.3/config/terminal/history/history-136", "XDG_SESSION_DESKTOP": "ubuntu", "LC_ADDRESS": "nl_NL.UTF-8", "GJS_DEBUG_OUTPUT": "stderr", "LC_NUMERIC": "nl_NL.UTF-8", "GTK_MODULES": "gail:atk-bridge", "WINDOWPATH": "2", "SHELL": "/bin/bash", "TERM": "xterm-256color", "__INTELLIJ_COMMAND_HISTFILE__": "/home/thomas/.PyCharm2018.3/config/terminal/history/history-136", "QT_IM_MODULE": "ibus", "XMODIFIERS": "@im=ibus", "IM_CONFIG_PHASE": "2", "XDG_CURRENT_DESKTOP": "ubuntu:GNOME", "GPG_AGENT_INFO": "/run/user/1000/gnupg/S.gpg-agent:0:1", "GIO_LAUNCHED_DESKTOP_FILE": "/home/thomas/.local/share/applications/jetbrains-pycharm.desktop", "XDG_SEAT": "seat0", "SHLVL": "1", "LC_TELEPHONE": "nl_NL.UTF-8", "GDMSESSION": "ubuntu", "GNOME_DESKTOP_SESSION_ID": "this-is-deprecated", "LOGNAME": "thomas", "DBUS_SESSION_BUS_ADDRESS": "unix:path=/run/user/1000/bus", "XDG_RUNTIME_DIR": "/run/user/1000", "XAUTHORITY": "/run/user/1000/gdm/Xauthority", "XDG_CONFIG_DIRS": "/etc/xdg/xdg-ubuntu:/etc/xdg", "PATH": "/home/thomas/workspaces/venv_fastr_dev/bin:/home/thomas/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin", "LC_IDENTIFICATION": "nl_NL.UTF-8", "PS1": "(venv_fastr_dev) \\\\[\\\\e]0;\\\\u@\\\\h: \\\\w\\\\a\\\\]${debian_chroot:+($debian_chroot)}\\\\[\\\\033[01;32m\\\\]\\\\u@\\\\h\\\\[\\\\033[00m\\\\]:\\\\[\\\\033[01;34m\\\\]\\\\w\\\\[\\\\033[00m\\\\]\\\\$ ", "GJS_DEBUG_TOPICS": "JS ERROR;JS LOG", "SESSION_MANAGER": "local/thomas-xps:@/tmp/.ICE-unix/3246,unix/thomas-xps:/tmp/.ICE-unix/3246", "LESSOPEN": "| /usr/bin/lesspipe %s", "GTK_IM_MODULE": "ibus", "LC_TIME": "nl_NL.UTF-8", "BASH_FUNC_generate_command_executed_sequence%%": "() {  printf '\\\\e\\\\7'\\n}", "_": "/home/thomas/workspaces/venv_fastr_dev/bin/python"}, "drmaa": {"jobid": null, "taskid": null, "jobname": null}}, "input_hash": null, "output_hash": null, "tool_name": null, "tool_version": null}	692	6535	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/step_1	3
620838	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/step_1/failing_macro_top_level_failing_macro_failing_network___step_1___sample_1_1	failing_macro_top_level_failing_macro_failing_network___step_1___sample_1_1		2019-05-13 06:29:09.963361	2019-05-13 06:29:12.466034	{"sample_id": ["sample_1_1"], "sample_index": [1], "errors": [["FastrOutputValidationError", "Could not find result for output out_2", "/home/thomas/workspaces/fastr/fastr/execution/job.py", 961, "File /home/thomas/workspaces/fastr/fastr/execution/executionscript.py, line 185 in <module>\\n  main()\\nFile /home/thomas/workspaces/fastr/fastr/execution/executionscript.py, line 180 in main\\n  execute_job(joblist)\\nFile /home/thomas/workspaces/fastr/fastr/execution/executionscript.py, line 87 in execute_job\\n  job.execute()\\nFile /home/thomas/workspaces/fastr/fastr/execution/job.py, line 716 in execute\\n  if not self.validate_results(payload):\\nFile /home/thomas/workspaces/fastr/fastr/execution/job.py, line 961 in validate_results\\n  self.errors.append(exceptions.FastrOutputValidationError(message).excerpt())"], ["FastrValueError", "Output values are not valid!", "/home/thomas/workspaces/fastr/fastr/execution/job.py", 717, "File /home/thomas/workspaces/fastr/fastr/execution/executionscript.py, line 185 in <module>\\n  main()\\nFile /home/thomas/workspaces/fastr/fastr/execution/executionscript.py, line 180 in main\\n  execute_job(joblist)\\nFile /home/thomas/workspaces/fastr/fastr/execution/executionscript.py, line 87 in execute_job\\n  job.execute()\\nFile /home/thomas/workspaces/fastr/fastr/execution/job.py, line 717 in execute\\n  raise exceptions.FastrValueError('Output values are not valid!')"]], "__stdout__": "Namespace(fail_1=False, fail_2=True, in_1=1, in_2=1)\\nin 1  : 1\\nin 2  : 1\\nfail_1: False\\nfail_2: True\\nRESULT_1=[2]\\n", "__stderr__": "", "command": ["python", "/home/thomas/workspaces/fastr/fastr/resources/tools/fastr/util/1.0/bin/fail.py", "--in_1", "1", "--in_2", "1", "--fail_2"], "returncode": null, "time_elapsed": 0.037551164627075195, "hostinfo": {"os": {"system": "Linux", "is64bits": 1, "maxsize": 9223372036854775807, "version": {"distname": "Ubuntu", "version": "18.04", "id": "bionic", "libc": {"libname": "glibc", "version": "2.25"}}}, "uname": ["Linux", "thomas-xps", "4.15.0-47-generic", "#50-Ubuntu SMP Wed Mar 13 10:44:52 UTC 2019", "x86_64", "x86_64"], "username": "thomas", "cwd": "/home/thomas/workspaces", "python": {"version": "3.6.7", "branch": "", "compiler": "GCC 8.2.0", "implementation": "CPython", "revision": ""}, "envvar": {"CLUTTER_IM_MODULE": "xim", "LS_COLORS": "rs=0:di=01;34:ln=01;36:mh=00:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:mi=00:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=34;42:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arc=01;31:*.arj=01;31:*.taz=01;31:*.lha=01;31:*.lz4=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.tzo=01;31:*.t7z=01;31:*.zip=01;31:*.z=01;31:*.Z=01;31:*.dz=01;31:*.gz=01;31:*.lrz=01;31:*.lz=01;31:*.lzo=01;31:*.xz=01;31:*.zst=01;31:*.tzst=01;31:*.bz2=01;31:*.bz=01;31:*.tbz=01;31:*.tbz2=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.war=01;31:*.ear=01;31:*.sar=01;31:*.rar=01;31:*.alz=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.cab=01;31:*.wim=01;31:*.swm=01;31:*.dwm=01;31:*.esd=01;31:*.jpg=01;35:*.jpeg=01;35:*.mjpg=01;35:*.mjpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.webm=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt=01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.ogv=01;35:*.ogx=01;35:*.aac=00;36:*.au=00;36:*.flac=00;36:*.m4a=00;36:*.mid=00;36:*.midi=00;36:*.mka=00;36:*.mp3=00;36:*.mpc=00;36:*.ogg=00;36:*.ra=00;36:*.wav=00;36:*.oga=00;36:*.opus=00;36:*.spx=00;36:*.xspf=00;36:", "TERMINAL_EMULATOR": "JetBrains-JediTerm", "LC_MEASUREMENT": "nl_NL.UTF-8", "LESSCLOSE": "/usr/bin/lesspipe %s %s", "LC_PAPER": "nl_NL.UTF-8", "LC_MONETARY": "nl_NL.UTF-8", "XDG_MENU_PREFIX": "gnome-", "LANG": "en_US.UTF-8", "PROJECT_DIR": "/home/thomas/workspaces/pim", "DISPLAY": ":0", "OLDPWD": "/home/thomas", "GNOME_SHELL_SESSION_MODE": "ubuntu", "USERNAME": "thomas", "XDG_VTNR": "2", "GIO_LAUNCHED_DESKTOP_FILE_PID": "22190", "SSH_AUTH_SOCK": "/run/user/1000/keyring/ssh", "VIRTUAL_ENV": "/home/thomas/workspaces/venv_fastr_dev", "LC_NAME": "nl_NL.UTF-8", "XDG_SESSION_ID": "2", "USER": "thomas", "DESKTOP_SESSION": "ubuntu", "QT4_IM_MODULE": "xim", "TEXTDOMAINDIR": "/usr/share/locale/", "PWD": "/home/thomas/workspaces", "HOME": "/home/thomas", "TEXTDOMAIN": "im-config", "SSH_AGENT_PID": "3341", "QT_ACCESSIBILITY": "1", "XDG_SESSION_TYPE": "x11", "XDG_DATA_DIRS": "/usr/share/ubuntu:/usr/local/share:/usr/share:/var/lib/snapd/desktop", "HISTFILE": "/home/thomas/.PyCharm2018.3/config/terminal/history/history-136", "XDG_SESSION_DESKTOP": "ubuntu", "LC_ADDRESS": "nl_NL.UTF-8", "GJS_DEBUG_OUTPUT": "stderr", "LC_NUMERIC": "nl_NL.UTF-8", "GTK_MODULES": "gail:atk-bridge", "WINDOWPATH": "2", "SHELL": "/bin/bash", "TERM": "xterm-256color", "__INTELLIJ_COMMAND_HISTFILE__": "/home/thomas/.PyCharm2018.3/config/terminal/history/history-136", "QT_IM_MODULE": "ibus", "XMODIFIERS": "@im=ibus", "IM_CONFIG_PHASE": "2", "XDG_CURRENT_DESKTOP": "ubuntu:GNOME", "GPG_AGENT_INFO": "/run/user/1000/gnupg/S.gpg-agent:0:1", "GIO_LAUNCHED_DESKTOP_FILE": "/home/thomas/.local/share/applications/jetbrains-pycharm.desktop", "XDG_SEAT": "seat0", "SHLVL": "1", "LC_TELEPHONE": "nl_NL.UTF-8", "GDMSESSION": "ubuntu", "GNOME_DESKTOP_SESSION_ID": "this-is-deprecated", "LOGNAME": "thomas", "DBUS_SESSION_BUS_ADDRESS": "unix:path=/run/user/1000/bus", "XDG_RUNTIME_DIR": "/run/user/1000", "XAUTHORITY": "/run/user/1000/gdm/Xauthority", "XDG_CONFIG_DIRS": "/etc/xdg/xdg-ubuntu:/etc/xdg", "PATH": "/home/thomas/workspaces/venv_fastr_dev/bin:/home/thomas/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin", "LC_IDENTIFICATION": "nl_NL.UTF-8", "PS1": "(venv_fastr_dev) \\\\[\\\\e]0;\\\\u@\\\\h: \\\\w\\\\a\\\\]${debian_chroot:+($debian_chroot)}\\\\[\\\\033[01;32m\\\\]\\\\u@\\\\h\\\\[\\\\033[00m\\\\]:\\\\[\\\\033[01;34m\\\\]\\\\w\\\\[\\\\033[00m\\\\]\\\\$ ", "GJS_DEBUG_TOPICS": "JS ERROR;JS LOG", "SESSION_MANAGER": "local/thomas-xps:@/tmp/.ICE-unix/3246,unix/thomas-xps:/tmp/.ICE-unix/3246", "LESSOPEN": "| /usr/bin/lesspipe %s", "GTK_IM_MODULE": "ibus", "LC_TIME": "nl_NL.UTF-8", "BASH_FUNC_generate_command_executed_sequence%%": "() {  printf '\\\\e\\\\7'\\n}", "_": "/home/thomas/workspaces/venv_fastr_dev/bin/python"}, "drmaa": {"jobid": null, "taskid": null, "jobname": null}}, "input_hash": null, "output_hash": null, "tool_name": null, "tool_version": null}	692	6535	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/step_1	3
620844	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/step_2/failing_macro_top_level_failing_macro_failing_network___step_2___sample_1_3	failing_macro_top_level_failing_macro_failing_network___step_2___sample_1_3		2019-05-13 06:29:09.963361	2019-05-13 06:29:12.466034	{"sample_id": ["sample_1_3"], "sample_index": [3], "errors": [["FastrOutputValidationError", "Could not find result for output out_1", "/home/thomas/workspaces/fastr/fastr/execution/job.py", 961, "File /home/thomas/workspaces/fastr/fastr/execution/executionscript.py, line 185 in <module>\\n  main()\\nFile /home/thomas/workspaces/fastr/fastr/execution/executionscript.py, line 180 in main\\n  execute_job(joblist)\\nFile /home/thomas/workspaces/fastr/fastr/execution/executionscript.py, line 87 in execute_job\\n  job.execute()\\nFile /home/thomas/workspaces/fastr/fastr/execution/job.py, line 716 in execute\\n  if not self.validate_results(payload):\\nFile /home/thomas/workspaces/fastr/fastr/execution/job.py, line 961 in validate_results\\n  self.errors.append(exceptions.FastrOutputValidationError(message).excerpt())"], ["FastrValueError", "Output values are not valid!", "/home/thomas/workspaces/fastr/fastr/execution/job.py", 717, "File /home/thomas/workspaces/fastr/fastr/execution/executionscript.py, line 185 in <module>\\n  main()\\nFile /home/thomas/workspaces/fastr/fastr/execution/executionscript.py, line 180 in main\\n  execute_job(joblist)\\nFile /home/thomas/workspaces/fastr/fastr/execution/executionscript.py, line 87 in execute_job\\n  job.execute()\\nFile /home/thomas/workspaces/fastr/fastr/execution/job.py, line 717 in execute\\n  raise exceptions.FastrValueError('Output values are not valid!')"]], "__stdout__": "Namespace(fail_1=True, fail_2=False, in_1=9, in_2=9)\\nin 1  : 9\\nin 2  : 9\\nfail_1: True\\nfail_2: False\\nRESULT_2=[10]\\n", "__stderr__": "", "command": ["python", "/home/thomas/workspaces/fastr/fastr/resources/tools/fastr/util/1.0/bin/fail.py", "--in_1", "9", "--in_2", "9", "--fail_1"], "returncode": null, "time_elapsed": 0.03426718711853027, "hostinfo": {"os": {"system": "Linux", "is64bits": 1, "maxsize": 9223372036854775807, "version": {"distname": "Ubuntu", "version": "18.04", "id": "bionic", "libc": {"libname": "glibc", "version": "2.25"}}}, "uname": ["Linux", "thomas-xps", "4.15.0-47-generic", "#50-Ubuntu SMP Wed Mar 13 10:44:52 UTC 2019", "x86_64", "x86_64"], "username": "thomas", "cwd": "/home/thomas/workspaces", "python": {"version": "3.6.7", "branch": "", "compiler": "GCC 8.2.0", "implementation": "CPython", "revision": ""}, "envvar": {"CLUTTER_IM_MODULE": "xim", "LS_COLORS": "rs=0:di=01;34:ln=01;36:mh=00:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:mi=00:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=34;42:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arc=01;31:*.arj=01;31:*.taz=01;31:*.lha=01;31:*.lz4=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.tzo=01;31:*.t7z=01;31:*.zip=01;31:*.z=01;31:*.Z=01;31:*.dz=01;31:*.gz=01;31:*.lrz=01;31:*.lz=01;31:*.lzo=01;31:*.xz=01;31:*.zst=01;31:*.tzst=01;31:*.bz2=01;31:*.bz=01;31:*.tbz=01;31:*.tbz2=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.war=01;31:*.ear=01;31:*.sar=01;31:*.rar=01;31:*.alz=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.cab=01;31:*.wim=01;31:*.swm=01;31:*.dwm=01;31:*.esd=01;31:*.jpg=01;35:*.jpeg=01;35:*.mjpg=01;35:*.mjpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.webm=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt=01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.ogv=01;35:*.ogx=01;35:*.aac=00;36:*.au=00;36:*.flac=00;36:*.m4a=00;36:*.mid=00;36:*.midi=00;36:*.mka=00;36:*.mp3=00;36:*.mpc=00;36:*.ogg=00;36:*.ra=00;36:*.wav=00;36:*.oga=00;36:*.opus=00;36:*.spx=00;36:*.xspf=00;36:", "TERMINAL_EMULATOR": "JetBrains-JediTerm", "LC_MEASUREMENT": "nl_NL.UTF-8", "LESSCLOSE": "/usr/bin/lesspipe %s %s", "LC_PAPER": "nl_NL.UTF-8", "LC_MONETARY": "nl_NL.UTF-8", "XDG_MENU_PREFIX": "gnome-", "LANG": "en_US.UTF-8", "PROJECT_DIR": "/home/thomas/workspaces/pim", "DISPLAY": ":0", "OLDPWD": "/home/thomas", "GNOME_SHELL_SESSION_MODE": "ubuntu", "USERNAME": "thomas", "XDG_VTNR": "2", "GIO_LAUNCHED_DESKTOP_FILE_PID": "22190", "SSH_AUTH_SOCK": "/run/user/1000/keyring/ssh", "VIRTUAL_ENV": "/home/thomas/workspaces/venv_fastr_dev", "LC_NAME": "nl_NL.UTF-8", "XDG_SESSION_ID": "2", "USER": "thomas", "DESKTOP_SESSION": "ubuntu", "QT4_IM_MODULE": "xim", "TEXTDOMAINDIR": "/usr/share/locale/", "PWD": "/home/thomas/workspaces", "HOME": "/home/thomas", "TEXTDOMAIN": "im-config", "SSH_AGENT_PID": "3341", "QT_ACCESSIBILITY": "1", "XDG_SESSION_TYPE": "x11", "XDG_DATA_DIRS": "/usr/share/ubuntu:/usr/local/share:/usr/share:/var/lib/snapd/desktop", "HISTFILE": "/home/thomas/.PyCharm2018.3/config/terminal/history/history-136", "XDG_SESSION_DESKTOP": "ubuntu", "LC_ADDRESS": "nl_NL.UTF-8", "GJS_DEBUG_OUTPUT": "stderr", "LC_NUMERIC": "nl_NL.UTF-8", "GTK_MODULES": "gail:atk-bridge", "WINDOWPATH": "2", "SHELL": "/bin/bash", "TERM": "xterm-256color", "__INTELLIJ_COMMAND_HISTFILE__": "/home/thomas/.PyCharm2018.3/config/terminal/history/history-136", "QT_IM_MODULE": "ibus", "XMODIFIERS": "@im=ibus", "IM_CONFIG_PHASE": "2", "XDG_CURRENT_DESKTOP": "ubuntu:GNOME", "GPG_AGENT_INFO": "/run/user/1000/gnupg/S.gpg-agent:0:1", "GIO_LAUNCHED_DESKTOP_FILE": "/home/thomas/.local/share/applications/jetbrains-pycharm.desktop", "XDG_SEAT": "seat0", "SHLVL": "1", "LC_TELEPHONE": "nl_NL.UTF-8", "GDMSESSION": "ubuntu", "GNOME_DESKTOP_SESSION_ID": "this-is-deprecated", "LOGNAME": "thomas", "DBUS_SESSION_BUS_ADDRESS": "unix:path=/run/user/1000/bus", "XDG_RUNTIME_DIR": "/run/user/1000", "XAUTHORITY": "/run/user/1000/gdm/Xauthority", "XDG_CONFIG_DIRS": "/etc/xdg/xdg-ubuntu:/etc/xdg", "PATH": "/home/thomas/workspaces/venv_fastr_dev/bin:/home/thomas/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin", "LC_IDENTIFICATION": "nl_NL.UTF-8", "PS1": "(venv_fastr_dev) \\\\[\\\\e]0;\\\\u@\\\\h: \\\\w\\\\a\\\\]${debian_chroot:+($debian_chroot)}\\\\[\\\\033[01;32m\\\\]\\\\u@\\\\h\\\\[\\\\033[00m\\\\]:\\\\[\\\\033[01;34m\\\\]\\\\w\\\\[\\\\033[00m\\\\]\\\\$ ", "GJS_DEBUG_TOPICS": "JS ERROR;JS LOG", "SESSION_MANAGER": "local/thomas-xps:@/tmp/.ICE-unix/3246,unix/thomas-xps:/tmp/.ICE-unix/3246", "LESSOPEN": "| /usr/bin/lesspipe %s", "GTK_IM_MODULE": "ibus", "LC_TIME": "nl_NL.UTF-8", "BASH_FUNC_generate_command_executed_sequence%%": "() {  printf '\\\\e\\\\7'\\n}", "_": "/home/thomas/workspaces/venv_fastr_dev/bin/python"}, "drmaa": {"jobid": null, "taskid": null, "jobname": null}}, "input_hash": null, "output_hash": null, "tool_name": null, "tool_version": null}	692	6536	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/step_2	3
620848	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/step_3/failing_macro_top_level_failing_macro_failing_network___step_3___sample_1_3	failing_macro_top_level_failing_macro_failing_network___step_3___sample_1_3		2019-05-13 06:29:09.963361	2019-05-13 06:29:12.466034	{"sample_id": ["sample_1_3"], "sample_index": [3], "errors": []}	692	6537	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/step_3	4
620852	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/range/failing_macro_top_level_failing_macro_failing_network___range___sample_1_3	failing_macro_top_level_failing_macro_failing_network___range___sample_1_3		2019-05-13 06:29:09.963361	2019-05-13 06:29:12.466034	{"sample_id": ["sample_1_3"], "sample_index": [3], "errors": []}	692	6538	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/range	4
620837	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/step_1/failing_macro_top_level_failing_macro_failing_network___step_1___sample_1_0	failing_macro_top_level_failing_macro_failing_network___step_1___sample_1_0		2019-05-13 06:29:09.963361	2019-05-13 06:29:12.466034	{"sample_id": ["sample_1_0"], "sample_index": [0], "errors": []}	692	6535	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/step_1	2
620839	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/step_1/failing_macro_top_level_failing_macro_failing_network___step_1___sample_1_2	failing_macro_top_level_failing_macro_failing_network___step_1___sample_1_2		2019-05-13 06:29:09.963361	2019-05-13 06:29:12.466034	{"sample_id": ["sample_1_2"], "sample_index": [2], "errors": []}	692	6535	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/step_1	2
620842	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/step_2/failing_macro_top_level_failing_macro_failing_network___step_2___sample_1_1	failing_macro_top_level_failing_macro_failing_network___step_2___sample_1_1		2019-05-13 06:29:09.963361	2019-05-13 06:29:12.466034	{"sample_id": ["sample_1_1"], "sample_index": [1], "errors": []}	692	6536	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/step_2	2
620846	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/step_3/failing_macro_top_level_failing_macro_failing_network___step_3___sample_1_1	failing_macro_top_level_failing_macro_failing_network___step_3___sample_1_1		2019-05-13 06:29:09.963361	2019-05-13 06:29:12.466034	{"sample_id": ["sample_1_1"], "sample_index": [1], "errors": []}	692	6537	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/step_3	4
620850	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/range/failing_macro_top_level_failing_macro_failing_network___range___sample_1_1	failing_macro_top_level_failing_macro_failing_network___range___sample_1_1		2019-05-13 06:29:09.963361	2019-05-13 06:29:12.466034	{"sample_id": ["sample_1_1"], "sample_index": [1], "errors": []}	692	6538	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/range	4
620843	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/step_2/failing_macro_top_level_failing_macro_failing_network___step_2___sample_1_2	failing_macro_top_level_failing_macro_failing_network___step_2___sample_1_2		2019-05-13 06:29:09.963361	2019-05-13 06:29:12.466034	{"sample_id": ["sample_1_2"], "sample_index": [2], "errors": [["FastrOutputValidationError", "Could not find result for output out_1", "/home/thomas/workspaces/fastr/fastr/execution/job.py", 961, "File /home/thomas/workspaces/fastr/fastr/execution/executionscript.py, line 185 in <module>\\n  main()\\nFile /home/thomas/workspaces/fastr/fastr/execution/executionscript.py, line 180 in main\\n  execute_job(joblist)\\nFile /home/thomas/workspaces/fastr/fastr/execution/executionscript.py, line 87 in execute_job\\n  job.execute()\\nFile /home/thomas/workspaces/fastr/fastr/execution/job.py, line 716 in execute\\n  if not self.validate_results(payload):\\nFile /home/thomas/workspaces/fastr/fastr/execution/job.py, line 961 in validate_results\\n  self.errors.append(exceptions.FastrOutputValidationError(message).excerpt())"], ["FastrValueError", "Output values are not valid!", "/home/thomas/workspaces/fastr/fastr/execution/job.py", 717, "File /home/thomas/workspaces/fastr/fastr/execution/executionscript.py, line 185 in <module>\\n  main()\\nFile /home/thomas/workspaces/fastr/fastr/execution/executionscript.py, line 180 in main\\n  execute_job(joblist)\\nFile /home/thomas/workspaces/fastr/fastr/execution/executionscript.py, line 87 in execute_job\\n  job.execute()\\nFile /home/thomas/workspaces/fastr/fastr/execution/job.py, line 717 in execute\\n  raise exceptions.FastrValueError('Output values are not valid!')"]], "__stdout__": "Namespace(fail_1=True, fail_2=False, in_1=12, in_2=12)\\nin 1  : 12\\nin 2  : 12\\nfail_1: True\\nfail_2: False\\nRESULT_2=[13]\\n", "__stderr__": "", "command": ["python", "/home/thomas/workspaces/fastr/fastr/resources/tools/fastr/util/1.0/bin/fail.py", "--in_1", "12", "--in_2", "12", "--fail_1"], "returncode": null, "time_elapsed": 0.02717733383178711, "hostinfo": {"os": {"system": "Linux", "is64bits": 1, "maxsize": 9223372036854775807, "version": {"distname": "Ubuntu", "version": "18.04", "id": "bionic", "libc": {"libname": "glibc", "version": "2.25"}}}, "uname": ["Linux", "thomas-xps", "4.15.0-47-generic", "#50-Ubuntu SMP Wed Mar 13 10:44:52 UTC 2019", "x86_64", "x86_64"], "username": "thomas", "cwd": "/home/thomas/workspaces", "python": {"version": "3.6.7", "branch": "", "compiler": "GCC 8.2.0", "implementation": "CPython", "revision": ""}, "envvar": {"CLUTTER_IM_MODULE": "xim", "LS_COLORS": "rs=0:di=01;34:ln=01;36:mh=00:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:mi=00:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=34;42:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arc=01;31:*.arj=01;31:*.taz=01;31:*.lha=01;31:*.lz4=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.tzo=01;31:*.t7z=01;31:*.zip=01;31:*.z=01;31:*.Z=01;31:*.dz=01;31:*.gz=01;31:*.lrz=01;31:*.lz=01;31:*.lzo=01;31:*.xz=01;31:*.zst=01;31:*.tzst=01;31:*.bz2=01;31:*.bz=01;31:*.tbz=01;31:*.tbz2=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.war=01;31:*.ear=01;31:*.sar=01;31:*.rar=01;31:*.alz=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.cab=01;31:*.wim=01;31:*.swm=01;31:*.dwm=01;31:*.esd=01;31:*.jpg=01;35:*.jpeg=01;35:*.mjpg=01;35:*.mjpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.webm=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt=01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.ogv=01;35:*.ogx=01;35:*.aac=00;36:*.au=00;36:*.flac=00;36:*.m4a=00;36:*.mid=00;36:*.midi=00;36:*.mka=00;36:*.mp3=00;36:*.mpc=00;36:*.ogg=00;36:*.ra=00;36:*.wav=00;36:*.oga=00;36:*.opus=00;36:*.spx=00;36:*.xspf=00;36:", "TERMINAL_EMULATOR": "JetBrains-JediTerm", "LC_MEASUREMENT": "nl_NL.UTF-8", "LESSCLOSE": "/usr/bin/lesspipe %s %s", "LC_PAPER": "nl_NL.UTF-8", "LC_MONETARY": "nl_NL.UTF-8", "XDG_MENU_PREFIX": "gnome-", "LANG": "en_US.UTF-8", "PROJECT_DIR": "/home/thomas/workspaces/pim", "DISPLAY": ":0", "OLDPWD": "/home/thomas", "GNOME_SHELL_SESSION_MODE": "ubuntu", "USERNAME": "thomas", "XDG_VTNR": "2", "GIO_LAUNCHED_DESKTOP_FILE_PID": "22190", "SSH_AUTH_SOCK": "/run/user/1000/keyring/ssh", "VIRTUAL_ENV": "/home/thomas/workspaces/venv_fastr_dev", "LC_NAME": "nl_NL.UTF-8", "XDG_SESSION_ID": "2", "USER": "thomas", "DESKTOP_SESSION": "ubuntu", "QT4_IM_MODULE": "xim", "TEXTDOMAINDIR": "/usr/share/locale/", "PWD": "/home/thomas/workspaces", "HOME": "/home/thomas", "TEXTDOMAIN": "im-config", "SSH_AGENT_PID": "3341", "QT_ACCESSIBILITY": "1", "XDG_SESSION_TYPE": "x11", "XDG_DATA_DIRS": "/usr/share/ubuntu:/usr/local/share:/usr/share:/var/lib/snapd/desktop", "HISTFILE": "/home/thomas/.PyCharm2018.3/config/terminal/history/history-136", "XDG_SESSION_DESKTOP": "ubuntu", "LC_ADDRESS": "nl_NL.UTF-8", "GJS_DEBUG_OUTPUT": "stderr", "LC_NUMERIC": "nl_NL.UTF-8", "GTK_MODULES": "gail:atk-bridge", "WINDOWPATH": "2", "SHELL": "/bin/bash", "TERM": "xterm-256color", "__INTELLIJ_COMMAND_HISTFILE__": "/home/thomas/.PyCharm2018.3/config/terminal/history/history-136", "QT_IM_MODULE": "ibus", "XMODIFIERS": "@im=ibus", "IM_CONFIG_PHASE": "2", "XDG_CURRENT_DESKTOP": "ubuntu:GNOME", "GPG_AGENT_INFO": "/run/user/1000/gnupg/S.gpg-agent:0:1", "GIO_LAUNCHED_DESKTOP_FILE": "/home/thomas/.local/share/applications/jetbrains-pycharm.desktop", "XDG_SEAT": "seat0", "SHLVL": "1", "LC_TELEPHONE": "nl_NL.UTF-8", "GDMSESSION": "ubuntu", "GNOME_DESKTOP_SESSION_ID": "this-is-deprecated", "LOGNAME": "thomas", "DBUS_SESSION_BUS_ADDRESS": "unix:path=/run/user/1000/bus", "XDG_RUNTIME_DIR": "/run/user/1000", "XAUTHORITY": "/run/user/1000/gdm/Xauthority", "XDG_CONFIG_DIRS": "/etc/xdg/xdg-ubuntu:/etc/xdg", "PATH": "/home/thomas/workspaces/venv_fastr_dev/bin:/home/thomas/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin", "LC_IDENTIFICATION": "nl_NL.UTF-8", "PS1": "(venv_fastr_dev) \\\\[\\\\e]0;\\\\u@\\\\h: \\\\w\\\\a\\\\]${debian_chroot:+($debian_chroot)}\\\\[\\\\033[01;32m\\\\]\\\\u@\\\\h\\\\[\\\\033[00m\\\\]:\\\\[\\\\033[01;34m\\\\]\\\\w\\\\[\\\\033[00m\\\\]\\\\$ ", "GJS_DEBUG_TOPICS": "JS ERROR;JS LOG", "SESSION_MANAGER": "local/thomas-xps:@/tmp/.ICE-unix/3246,unix/thomas-xps:/tmp/.ICE-unix/3246", "LESSOPEN": "| /usr/bin/lesspipe %s", "GTK_IM_MODULE": "ibus", "LC_TIME": "nl_NL.UTF-8", "BASH_FUNC_generate_command_executed_sequence%%": "() {  printf '\\\\e\\\\7'\\n}", "_": "/home/thomas/workspaces/venv_fastr_dev/bin/python"}, "drmaa": {"jobid": null, "taskid": null, "jobname": null}}, "input_hash": null, "output_hash": null, "tool_name": null, "tool_version": null}	692	6536	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/step_2	3
620847	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/step_3/failing_macro_top_level_failing_macro_failing_network___step_3___sample_1_2	failing_macro_top_level_failing_macro_failing_network___step_3___sample_1_2		2019-05-13 06:29:09.963361	2019-05-13 06:29:12.466034	{"sample_id": ["sample_1_2"], "sample_index": [2], "errors": []}	692	6537	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/step_3	4
620851	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/range/failing_macro_top_level_failing_macro_failing_network___range___sample_1_2	failing_macro_top_level_failing_macro_failing_network___range___sample_1_2		2019-05-13 06:29:09.963361	2019-05-13 06:29:12.466034	{"sample_id": ["sample_1_2"], "sample_index": [2], "errors": []}	692	6538	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/range	4
620817	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/source_1/failing_macro_top_level_failing_macro_failing_network___source_1___sample_1_0	failing_macro_top_level_failing_macro_failing_network___source_1___sample_1_0		2019-05-13 06:29:09.963361	2019-05-13 06:29:09.963361	{"sample_id": ["sample_1_0"], "sample_index": [0], "errors": []}	692	6527	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/source_1	2
620818	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/source_1/failing_macro_top_level_failing_macro_failing_network___source_1___sample_1_1	failing_macro_top_level_failing_macro_failing_network___source_1___sample_1_1		2019-05-13 06:29:09.963361	2019-05-13 06:29:09.963361	{"sample_id": ["sample_1_1"], "sample_index": [1], "errors": []}	692	6527	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/source_1	2
620819	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/source_1/failing_macro_top_level_failing_macro_failing_network___source_1___sample_1_2	failing_macro_top_level_failing_macro_failing_network___source_1___sample_1_2		2019-05-13 06:29:09.963361	2019-05-13 06:29:09.963361	{"sample_id": ["sample_1_2"], "sample_index": [2], "errors": []}	692	6527	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/source_1	2
620820	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/source_1/failing_macro_top_level_failing_macro_failing_network___source_1___sample_1_3	failing_macro_top_level_failing_macro_failing_network___source_1___sample_1_3		2019-05-13 06:29:09.963361	2019-05-13 06:29:09.963361	{"sample_id": ["sample_1_3"], "sample_index": [3], "errors": []}	692	6527	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/source_1	2
620821	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/source_2/failing_macro_top_level_failing_macro_failing_network___source_2___sample_1_0	failing_macro_top_level_failing_macro_failing_network___source_2___sample_1_0		2019-05-13 06:29:09.963361	2019-05-13 06:29:09.963361	{"sample_id": ["sample_1_0"], "sample_index": [0], "errors": []}	692	6528	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/source_2	2
620822	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/source_2/failing_macro_top_level_failing_macro_failing_network___source_2___sample_1_1	failing_macro_top_level_failing_macro_failing_network___source_2___sample_1_1		2019-05-13 06:29:09.963361	2019-05-13 06:29:09.963361	{"sample_id": ["sample_1_1"], "sample_index": [1], "errors": []}	692	6528	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/source_2	2
620823	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/source_2/failing_macro_top_level_failing_macro_failing_network___source_2___sample_1_2	failing_macro_top_level_failing_macro_failing_network___source_2___sample_1_2		2019-05-13 06:29:09.963361	2019-05-13 06:29:09.963361	{"sample_id": ["sample_1_2"], "sample_index": [2], "errors": []}	692	6528	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/source_2	2
620824	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/source_2/failing_macro_top_level_failing_macro_failing_network___source_2___sample_1_3	failing_macro_top_level_failing_macro_failing_network___source_2___sample_1_3		2019-05-13 06:29:09.963361	2019-05-13 06:29:09.963361	{"sample_id": ["sample_1_3"], "sample_index": [3], "errors": []}	692	6528	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/source_2	2
620825	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/source_3/failing_macro_top_level_failing_macro_failing_network___source_3___sample_1_0	failing_macro_top_level_failing_macro_failing_network___source_3___sample_1_0		2019-05-13 06:29:09.963361	2019-05-13 06:29:09.963361	{"sample_id": ["sample_1_0"], "sample_index": [0], "errors": []}	692	6529	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/source_3	2
620826	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/source_3/failing_macro_top_level_failing_macro_failing_network___source_3___sample_1_1	failing_macro_top_level_failing_macro_failing_network___source_3___sample_1_1		2019-05-13 06:29:09.963361	2019-05-13 06:29:09.963361	{"sample_id": ["sample_1_1"], "sample_index": [1], "errors": []}	692	6529	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/source_3	2
620827	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/source_3/failing_macro_top_level_failing_macro_failing_network___source_3___sample_1_2	failing_macro_top_level_failing_macro_failing_network___source_3___sample_1_2		2019-05-13 06:29:09.963361	2019-05-13 06:29:09.963361	{"sample_id": ["sample_1_2"], "sample_index": [2], "errors": []}	692	6529	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/source_3	2
620828	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/source_3/failing_macro_top_level_failing_macro_failing_network___source_3___sample_1_3	failing_macro_top_level_failing_macro_failing_network___source_3___sample_1_3		2019-05-13 06:29:09.963361	2019-05-13 06:29:09.963361	{"sample_id": ["sample_1_3"], "sample_index": [3], "errors": []}	692	6529	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/source_3	2
620829	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/const_step_1_fail_2_0/failing_macro_top_level_failing_macro_failing_network___const_step_1_fail_2_0___id_0	failing_macro_top_level_failing_macro_failing_network___const_step_1_fail_2_0___id_0		2019-05-13 06:29:09.963361	2019-05-13 06:29:09.963361	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	692	6540	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/const_step_1_fail_2_0	2
620830	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/const_step_1_fail_2_0/failing_macro_top_level_failing_macro_failing_network___const_step_1_fail_2_0___id_1	failing_macro_top_level_failing_macro_failing_network___const_step_1_fail_2_0___id_1		2019-05-13 06:29:09.963361	2019-05-13 06:29:09.963361	{"sample_id": ["id_1"], "sample_index": [1], "errors": []}	692	6540	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/const_step_1_fail_2_0	2
620831	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/const_step_1_fail_2_0/failing_macro_top_level_failing_macro_failing_network___const_step_1_fail_2_0___id_2	failing_macro_top_level_failing_macro_failing_network___const_step_1_fail_2_0___id_2		2019-05-13 06:29:09.963361	2019-05-13 06:29:09.963361	{"sample_id": ["id_2"], "sample_index": [2], "errors": []}	692	6540	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/const_step_1_fail_2_0	2
620832	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/const_step_1_fail_2_0/failing_macro_top_level_failing_macro_failing_network___const_step_1_fail_2_0___id_3	failing_macro_top_level_failing_macro_failing_network___const_step_1_fail_2_0___id_3		2019-05-13 06:29:09.963361	2019-05-13 06:29:09.963361	{"sample_id": ["id_3"], "sample_index": [3], "errors": []}	692	6540	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/const_step_1_fail_2_0	2
620833	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/const_step_2_fail_1_0/failing_macro_top_level_failing_macro_failing_network___const_step_2_fail_1_0___id_0	failing_macro_top_level_failing_macro_failing_network___const_step_2_fail_1_0___id_0		2019-05-13 06:29:09.963361	2019-05-13 06:29:09.963361	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	692	6541	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/const_step_2_fail_1_0	2
620873	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sink_5/failing_macro_top_level_failing_macro_failing_network___sink_5___sample_1_0	failing_macro_top_level_failing_macro_failing_network___sink_5___sample_1_0		2019-05-13 06:29:19.974458	2019-05-13 06:29:19.974458	{"sample_id": ["sample_1_0"], "sample_index": [0], "errors": []}	692	6534	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sink_5	2
620874	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sink_5/failing_macro_top_level_failing_macro_failing_network___sink_5___sample_1_1	failing_macro_top_level_failing_macro_failing_network___sink_5___sample_1_1		2019-05-13 06:29:19.974458	2019-05-13 06:29:19.974458	{"sample_id": ["sample_1_1"], "sample_index": [1], "errors": []}	692	6534	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sink_5	2
620875	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sink_5/failing_macro_top_level_failing_macro_failing_network___sink_5___sample_1_2	failing_macro_top_level_failing_macro_failing_network___sink_5___sample_1_2		2019-05-13 06:29:19.974458	2019-05-13 06:29:19.974458	{"sample_id": ["sample_1_2"], "sample_index": [2], "errors": []}	692	6534	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sink_5	2
620876	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sink_5/failing_macro_top_level_failing_macro_failing_network___sink_5___sample_1_3	failing_macro_top_level_failing_macro_failing_network___sink_5___sample_1_3		2019-05-13 06:29:19.974458	2019-05-13 06:29:19.974458	{"sample_id": ["sample_1_3"], "sample_index": [3], "errors": []}	692	6534	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sink_5	2
620878	failing_macro_top_level_2019-05-13T08-29-04/root/add/failing_macro_top_level___add___sample_1_1	failing_macro_top_level___add___sample_1_1		2019-05-13 06:29:19.974458	2019-05-13 06:29:19.974458	{"sample_id": ["sample_1_1"], "sample_index": [1], "errors": []}	692	6524	failing_macro_top_level_2019-05-13T08-29-04/root/add	4
620879	failing_macro_top_level_2019-05-13T08-29-04/root/add/failing_macro_top_level___add___sample_1_2	failing_macro_top_level___add___sample_1_2		2019-05-13 06:29:19.974458	2019-05-13 06:29:19.974458	{"sample_id": ["sample_1_2"], "sample_index": [2], "errors": []}	692	6524	failing_macro_top_level_2019-05-13T08-29-04/root/add	4
620880	failing_macro_top_level_2019-05-13T08-29-04/root/add/failing_macro_top_level___add___sample_1_3	failing_macro_top_level___add___sample_1_3		2019-05-13 06:29:19.974458	2019-05-13 06:29:19.974458	{"sample_id": ["sample_1_3"], "sample_index": [3], "errors": []}	692	6524	failing_macro_top_level_2019-05-13T08-29-04/root/add	4
620882	failing_macro_top_level_2019-05-13T08-29-04/root/sink/failing_macro_top_level___sink___sample_1_1___0	failing_macro_top_level___sink___sample_1_1___0		2019-05-13 06:29:19.974458	2019-05-13 06:29:19.974458	{"sample_id": ["sample_1_1"], "sample_index": [1], "errors": []}	692	6525	failing_macro_top_level_2019-05-13T08-29-04/root/sink	4
620883	failing_macro_top_level_2019-05-13T08-29-04/root/sink/failing_macro_top_level___sink___sample_1_2___0	failing_macro_top_level___sink___sample_1_2___0		2019-05-13 06:29:19.974458	2019-05-13 06:29:19.974458	{"sample_id": ["sample_1_2"], "sample_index": [2], "errors": []}	692	6525	failing_macro_top_level_2019-05-13T08-29-04/root/sink	4
620884	failing_macro_top_level_2019-05-13T08-29-04/root/sink/failing_macro_top_level___sink___sample_1_3___0	failing_macro_top_level___sink___sample_1_3___0		2019-05-13 06:29:19.974458	2019-05-13 06:29:19.974458	{"sample_id": ["sample_1_3"], "sample_index": [3], "errors": []}	692	6525	failing_macro_top_level_2019-05-13T08-29-04/root/sink	4
620877	failing_macro_top_level_2019-05-13T08-29-04/root/add/failing_macro_top_level___add___sample_1_0	failing_macro_top_level___add___sample_1_0		2019-05-13 06:29:19.974458	2019-05-13 06:29:22.470705	{"sample_id": ["sample_1_0"], "sample_index": [0], "errors": []}	692	6524	failing_macro_top_level_2019-05-13T08-29-04/root/add	2
620881	failing_macro_top_level_2019-05-13T08-29-04/root/sink/failing_macro_top_level___sink___sample_1_0___0	failing_macro_top_level___sink___sample_1_0___0		2019-05-13 06:29:19.974458	2019-05-13 06:29:24.977399	{"sample_id": ["sample_1_0"], "sample_index": [0], "errors": []}	692	6525	failing_macro_top_level_2019-05-13T08-29-04/root/sink	2
620834	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/const_step_2_fail_1_0/failing_macro_top_level_failing_macro_failing_network___const_step_2_fail_1_0___id_1	failing_macro_top_level_failing_macro_failing_network___const_step_2_fail_1_0___id_1		2019-05-13 06:29:09.963361	2019-05-13 06:29:09.963361	{"sample_id": ["id_1"], "sample_index": [1], "errors": []}	692	6541	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/const_step_2_fail_1_0	2
620835	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/const_step_2_fail_1_0/failing_macro_top_level_failing_macro_failing_network___const_step_2_fail_1_0___id_2	failing_macro_top_level_failing_macro_failing_network___const_step_2_fail_1_0___id_2		2019-05-13 06:29:09.963361	2019-05-13 06:29:09.963361	{"sample_id": ["id_2"], "sample_index": [2], "errors": []}	692	6541	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/const_step_2_fail_1_0	2
620836	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/const_step_2_fail_1_0/failing_macro_top_level_failing_macro_failing_network___const_step_2_fail_1_0___id_3	failing_macro_top_level_failing_macro_failing_network___const_step_2_fail_1_0___id_3		2019-05-13 06:29:09.963361	2019-05-13 06:29:09.963361	{"sample_id": ["id_3"], "sample_index": [3], "errors": []}	692	6541	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/const_step_2_fail_1_0	2
620841	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/step_2/failing_macro_top_level_failing_macro_failing_network___step_2___sample_1_0	failing_macro_top_level_failing_macro_failing_network___step_2___sample_1_0		2019-05-13 06:29:09.963361	2019-05-13 06:29:12.466034	{"sample_id": ["sample_1_0"], "sample_index": [0], "errors": []}	692	6536	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/step_2	2
620845	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/step_3/failing_macro_top_level_failing_macro_failing_network___step_3___sample_1_0	failing_macro_top_level_failing_macro_failing_network___step_3___sample_1_0		2019-05-13 06:29:09.963361	2019-05-13 06:29:14.960987	{"sample_id": ["sample_1_0"], "sample_index": [0], "errors": []}	692	6537	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/step_3	2
620849	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/range/failing_macro_top_level_failing_macro_failing_network___range___sample_1_0	failing_macro_top_level_failing_macro_failing_network___range___sample_1_0		2019-05-13 06:29:09.963361	2019-05-13 06:29:17.465092	{"sample_id": ["sample_1_0"], "sample_index": [0], "errors": []}	692	6538	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/range	2
\.


--
-- Data for Name: link_node_refs; Type: TABLE DATA; Schema: public; Owner: pim
--

COPY public.link_node_refs (id, node_id, node_path, created) FROM stdin;
\.


--
-- Data for Name: links; Type: TABLE DATA; Schema: public; Owner: pim
--

COPY public.links (id, title, description, created, modified, custom_data, run_id, from_port_id, to_port_id, data_type, common_ancestor) FROM stdin;
859	link_88		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/qc_overview/t1_with_wm_outline/png_image	tissue_pipeline_2019-01-31T21-41-53/root/qc_overview/t1_with_wm_outline_sink/input	\N	tissue_pipeline_2019-01-31T21-41-53/root/qc_overview
860	link_89		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/qc_overview/t1_with_hammers/png_image	tissue_pipeline_2019-01-31T21-41-53/root/qc_overview/t1_with_hammers_sink/input	\N	tissue_pipeline_2019-01-31T21-41-53/root/qc_overview
861	link_80		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/segmentation/calculate_hard_segmentation/gm_image	tissue_pipeline_2019-01-31T21-41-53/root/output/gm_map_output/input	\N	tissue_pipeline_2019-01-31T21-41-53/root
862	link_81		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/segmentation/calculate_hard_segmentation/wm_image	tissue_pipeline_2019-01-31T21-41-53/root/output/wm_map_output/input	\N	tissue_pipeline_2019-01-31T21-41-53/root
863	link_82		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/combine_labels/hard_segment	tissue_pipeline_2019-01-31T21-41-53/root/output/multi_atlas_segm/input	\N	tissue_pipeline_2019-01-31T21-41-53/root
864	link_83		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/threshold_labels/image	tissue_pipeline_2019-01-31T21-41-53/root/output/hammer_brain_mask/input	\N	tissue_pipeline_2019-01-31T21-41-53/root
865	link_84		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/statistics/get_wm_volume/statistics	tissue_pipeline_2019-01-31T21-41-53/root/statistics/wm_volume/input	\N	tissue_pipeline_2019-01-31T21-41-53/root/statistics
866	link_85		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/statistics/get_gm_volume/statistics	tissue_pipeline_2019-01-31T21-41-53/root/statistics/gm_volume/input	\N	tissue_pipeline_2019-01-31T21-41-53/root/statistics
867	link_86		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/statistics/get_brain_volume/statistics	tissue_pipeline_2019-01-31T21-41-53/root/statistics/brain_volume/input	\N	tissue_pipeline_2019-01-31T21-41-53/root/statistics
868	link_87		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/qc_overview/t1_with_gm_outline/png_image	tissue_pipeline_2019-01-31T21-41-53/root/qc_overview/t1_with_gm_outline_sink/input	\N	tissue_pipeline_2019-01-31T21-41-53/root/qc_overview
869	link_39		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/const_threshold_labels_upper_threshold_0/output	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/threshold_labels/upper_threshold	\N	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG
870	link_38		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/combine_brains/hard_segment	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/threshold_labels/image	\N	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG
871	link_35		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": ["hammers_atlas"], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/label_registration/image	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/combine_labels/images	\N	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG
872	link_34		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/const_label_registration_threads_0/output	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/label_registration/threads	\N	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG
873	link_37		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/const_combine_labels_number_of_classes_0/output	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/combine_labels/number_of_classes	\N	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG
874	link_36		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/const_combine_labels_method_0/output	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/combine_labels/method	\N	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG
875	link_31		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/const_combine_brains_number_of_classes_0/output	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/combine_brains/number_of_classes	\N	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG
876	link_30		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/const_combine_brains_method_0/output	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/combine_brains/method	\N	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG
877	link_33		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/t1w_registration/transform	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/label_registration/transform	\N	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG
878	link_32		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/hammers_atlas/hammers_labels/output	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/label_registration/image	\N	tissue_pipeline_2019-01-31T21-41-53/root
879	link_28		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/const_hammers_brain_transform_threads_0/output	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/hammers_brain_transform/threads	\N	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG
880	link_29		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": ["hammers_atlas"], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/hammers_brain_transform/image	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/combine_brains/images	\N	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG
881	link_22		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/dilate_brainmask/image	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/t1w_registration/fixed_mask	\N	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG
882	link_23		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/brain_mask_registation/transform	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/t1w_registration/initial_transform	\N	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG
883	link_20		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/hammers_atlas/hammers_brains_r5/output	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/t1w_registration/moving_mask	\N	tissue_pipeline_2019-01-31T21-41-53/root
884	link_21		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/n4_bias_correction/corrected_image	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/t1w_registration/fixed_image	\N	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG
885	link_26		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/hammers_atlas/hammers_brains/output	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/hammers_brain_transform/image	\N	tissue_pipeline_2019-01-31T21-41-53/root
886	link_27		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/t1w_registration/transform	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/hammers_brain_transform/transform	\N	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG
887	link_24		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/const_t1w_registration_parameters_0/output	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/t1w_registration/parameters	\N	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG
888	link_25		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/const_t1w_registration_threads_0/output	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/t1w_registration/threads	\N	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG
889	link_54		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/statistics/const_get_wm_volume_flatten_0/output	tissue_pipeline_2019-01-31T21-41-53/root/statistics/get_wm_volume/flatten	\N	tissue_pipeline_2019-01-31T21-41-53/root/statistics
890	link_53		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/statistics/const_get_wm_volume_selection_0/output	tissue_pipeline_2019-01-31T21-41-53/root/statistics/get_wm_volume/selection	\N	tissue_pipeline_2019-01-31T21-41-53/root/statistics
891	link_52		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/statistics/const_get_wm_volume_volume_0/output	tissue_pipeline_2019-01-31T21-41-53/root/statistics/get_wm_volume/volume	\N	tissue_pipeline_2019-01-31T21-41-53/root/statistics
892	link_51		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/segmentation/fast_tissue_segmentation/white_matter	tissue_pipeline_2019-01-31T21-41-53/root/statistics/get_wm_volume/image	\N	tissue_pipeline_2019-01-31T21-41-53/root
893	link_50		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/combine_labels/hard_segment	tissue_pipeline_2019-01-31T21-41-53/root/statistics/get_wm_volume/label	\N	tissue_pipeline_2019-01-31T21-41-53/root
894	link_9		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/bet/mask_image	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/dilate_brainmask/image	\N	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG
895	link_8		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/convert_brain/image	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/threshold_brain/image	\N	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG
896	link_3		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/dicom_to_nifti/reformat_t1/reformatted_image	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/bet/image	\N	tissue_pipeline_2019-01-31T21-41-53/root
897	link_2		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/dicom_to_nifti/t1_dicom_to_nifti/image	tissue_pipeline_2019-01-31T21-41-53/root/dicom_to_nifti/reformat_t1/image	\N	tissue_pipeline_2019-01-31T21-41-53/root/dicom_to_nifti
898	link_1		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/dicom_to_nifti/const_t1_dicom_to_nifti_file_order_0/output	tissue_pipeline_2019-01-31T21-41-53/root/dicom_to_nifti/t1_dicom_to_nifti/file_order	\N	tissue_pipeline_2019-01-31T21-41-53/root/dicom_to_nifti
899	link_0		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/subjects/t1w_dicom/output	tissue_pipeline_2019-01-31T21-41-53/root/dicom_to_nifti/t1_dicom_to_nifti/dicom_image	\N	tissue_pipeline_2019-01-31T21-41-53/root
900	link_7		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/const_convert_brain_component_type_0/output	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/convert_brain/component_type	\N	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG
901	link_6		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/bet/brain_image	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/convert_brain/image	\N	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG
902	link_5		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/const_bet_fraction_threshold_0/output	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/bet/fraction_threshold	\N	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG
903	link_4		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/const_bet_bias_cleanup_0/output	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/bet/bias_cleanup	\N	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG
904	link_48		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/segmentation/calculate_hard_segmentation/gm_image	tissue_pipeline_2019-01-31T21-41-53/root/statistics/mask_hammers_labels/right_hand	\N	tissue_pipeline_2019-01-31T21-41-53/root
905	link_49		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/statistics/const_mask_hammers_labels_operator_0/output	tissue_pipeline_2019-01-31T21-41-53/root/statistics/mask_hammers_labels/operator	\N	tissue_pipeline_2019-01-31T21-41-53/root/statistics
906	link_44		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/segmentation/fast_tissue_segmentation/gray_matter	tissue_pipeline_2019-01-31T21-41-53/root/segmentation/calculate_hard_segmentation/gray_matter	\N	tissue_pipeline_2019-01-31T21-41-53/root/segmentation
907	link_45		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/segmentation/fast_tissue_segmentation/white_matter	tissue_pipeline_2019-01-31T21-41-53/root/segmentation/calculate_hard_segmentation/white_matter	\N	tissue_pipeline_2019-01-31T21-41-53/root/segmentation
908	link_46		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/segmentation/fast_tissue_segmentation/csf	tissue_pipeline_2019-01-31T21-41-53/root/segmentation/calculate_hard_segmentation/cerebral_spinal_fluid	\N	tissue_pipeline_2019-01-31T21-41-53/root/segmentation
909	link_47		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/combine_labels/hard_segment	tissue_pipeline_2019-01-31T21-41-53/root/statistics/mask_hammers_labels/left_hand	\N	tissue_pipeline_2019-01-31T21-41-53/root
910	link_40		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/dicom_to_nifti/reformat_t1/reformatted_image	tissue_pipeline_2019-01-31T21-41-53/root/segmentation/mask_t1_with_brain_mask/image1	\N	tissue_pipeline_2019-01-31T21-41-53/root
911	link_41		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/segmentation/const_mask_t1_with_brain_mask_operator1_0/output	tissue_pipeline_2019-01-31T21-41-53/root/segmentation/mask_t1_with_brain_mask/operator1	\N	tissue_pipeline_2019-01-31T21-41-53/root/segmentation
912	link_42		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/threshold_labels/image	tissue_pipeline_2019-01-31T21-41-53/root/segmentation/mask_t1_with_brain_mask/image2	\N	tissue_pipeline_2019-01-31T21-41-53/root
913	link_43		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/segmentation/mask_t1_with_brain_mask/output_image	tissue_pipeline_2019-01-31T21-41-53/root/segmentation/fast_tissue_segmentation/image	\N	tissue_pipeline_2019-01-31T21-41-53/root/segmentation
914	link_79		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/dicom_to_nifti/reformat_t1/reformatted_image	tissue_pipeline_2019-01-31T21-41-53/root/output/t1_nifti_images/input	\N	tissue_pipeline_2019-01-31T21-41-53/root
915	link_78		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/threshold_labels/image	tissue_pipeline_2019-01-31T21-41-53/root/qc_overview/t1_with_brain/segmentation	\N	tissue_pipeline_2019-01-31T21-41-53/root
916	link_71		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/dicom_to_nifti/reformat_t1/reformatted_image	tissue_pipeline_2019-01-31T21-41-53/root/qc_overview/t1_with_gm_outline/image	\N	tissue_pipeline_2019-01-31T21-41-53/root
917	link_70		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/dicom_to_nifti/reformat_t1/reformatted_image	tissue_pipeline_2019-01-31T21-41-53/root/qc_overview/t1_qc/image	\N	tissue_pipeline_2019-01-31T21-41-53/root
918	link_73		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/dicom_to_nifti/reformat_t1/reformatted_image	tissue_pipeline_2019-01-31T21-41-53/root/qc_overview/t1_with_wm_outline/image	\N	tissue_pipeline_2019-01-31T21-41-53/root
919	link_72		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/segmentation/calculate_hard_segmentation/gm_image	tissue_pipeline_2019-01-31T21-41-53/root/qc_overview/t1_with_gm_outline/segmentation	\N	tissue_pipeline_2019-01-31T21-41-53/root
920	link_75		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/dicom_to_nifti/reformat_t1/reformatted_image	tissue_pipeline_2019-01-31T21-41-53/root/qc_overview/t1_with_hammers/image	\N	tissue_pipeline_2019-01-31T21-41-53/root
921	link_74		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/segmentation/calculate_hard_segmentation/wm_image	tissue_pipeline_2019-01-31T21-41-53/root/qc_overview/t1_with_wm_outline/segmentation	\N	tissue_pipeline_2019-01-31T21-41-53/root
922	link_77		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/dicom_to_nifti/reformat_t1/reformatted_image	tissue_pipeline_2019-01-31T21-41-53/root/qc_overview/t1_with_brain/image	\N	tissue_pipeline_2019-01-31T21-41-53/root
923	link_76		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/combine_labels/hard_segment	tissue_pipeline_2019-01-31T21-41-53/root/qc_overview/t1_with_hammers/segmentation	\N	tissue_pipeline_2019-01-31T21-41-53/root
924	link_66		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/combine_brains/hard_segment	tissue_pipeline_2019-01-31T21-41-53/root/statistics/get_brain_volume/image	\N	tissue_pipeline_2019-01-31T21-41-53/root
925	link_67		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/statistics/const_get_brain_volume_volume_0/output	tissue_pipeline_2019-01-31T21-41-53/root/statistics/get_brain_volume/volume	\N	tissue_pipeline_2019-01-31T21-41-53/root/statistics
926	link_64		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/statistics/const_get_gm_volume_flatten_1/output	tissue_pipeline_2019-01-31T21-41-53/root/statistics/get_gm_volume/flatten	\N	tissue_pipeline_2019-01-31T21-41-53/root/statistics
927	link_65		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/combine_brains/hard_segment	tissue_pipeline_2019-01-31T21-41-53/root/statistics/get_brain_volume/label	\N	tissue_pipeline_2019-01-31T21-41-53/root
928	link_62		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/statistics/const_get_gm_volume_volume_1/output	tissue_pipeline_2019-01-31T21-41-53/root/statistics/get_gm_volume/volume	\N	tissue_pipeline_2019-01-31T21-41-53/root/statistics
929	link_63		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/statistics/const_get_gm_volume_selection_1/output	tissue_pipeline_2019-01-31T21-41-53/root/statistics/get_gm_volume/selection	\N	tissue_pipeline_2019-01-31T21-41-53/root/statistics
930	link_60		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/combine_labels/hard_segment	tissue_pipeline_2019-01-31T21-41-53/root/statistics/get_gm_volume/label	\N	tissue_pipeline_2019-01-31T21-41-53/root
931	link_61		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/segmentation/fast_tissue_segmentation/gray_matter	tissue_pipeline_2019-01-31T21-41-53/root/statistics/get_gm_volume/image	\N	tissue_pipeline_2019-01-31T21-41-53/root
932	link_68		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/statistics/const_get_brain_volume_selection_0/output	tissue_pipeline_2019-01-31T21-41-53/root/statistics/get_brain_volume/selection	\N	tissue_pipeline_2019-01-31T21-41-53/root/statistics
933	link_69		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/statistics/const_get_brain_volume_flatten_0/output	tissue_pipeline_2019-01-31T21-41-53/root/statistics/get_brain_volume/flatten	\N	tissue_pipeline_2019-01-31T21-41-53/root/statistics
934	link_13		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/dicom_to_nifti/reformat_t1/reformatted_image	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/n4_bias_correction/image	\N	tissue_pipeline_2019-01-31T21-41-53/root
935	link_12		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/const_dilate_brainmask_radius_0/output	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/dilate_brainmask/radius	\N	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG
936	link_11		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/const_dilate_brainmask_operation_type_0/output	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/dilate_brainmask/operation_type	\N	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG
937	link_10		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/const_dilate_brainmask_operation_0/output	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/dilate_brainmask/operation	\N	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG
938	link_17		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/const_brain_mask_registation_parameters_0/output	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/brain_mask_registation/parameters	\N	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG
939	link_16		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/dilate_brainmask/image	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/brain_mask_registation/fixed_image	\N	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG
940	link_15		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/hammers_atlas/hammers_brains_r5/output	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/brain_mask_registation/moving_image	\N	tissue_pipeline_2019-01-31T21-41-53/root
941	link_14		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/bet/mask_image	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/n4_bias_correction/mask	\N	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG
942	link_19		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/hammers_atlas/hammers_t1w/output	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/t1w_registration/moving_image	\N	tissue_pipeline_2019-01-31T21-41-53/root
943	link_18		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/const_brain_mask_registation_threads_0/output	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/brain_mask_registation/threads	\N	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG
944	link_91		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/qc_overview/t1_with_brain/png_image	tissue_pipeline_2019-01-31T21-41-53/root/qc_overview/t1_with_brain_sink/input	\N	tissue_pipeline_2019-01-31T21-41-53/root/qc_overview
945	link_90		2019-04-02 09:39:49.663001	2019-04-02 09:39:49.663028	{"collapse": [], "expand": false}	624	tissue_pipeline_2019-01-31T21-41-53/root/qc_overview/t1_qc/png_image	tissue_pipeline_2019-01-31T21-41-53/root/qc_overview/t1_qc_sink/input	\N	tissue_pipeline_2019-01-31T21-41-53/root/qc_overview
987	Link	Rutrum duis, ultriciespellentesque facilisi feugiat nisi lorem eros.	2019-04-07 10:43:17.692351	2019-04-07 10:43:17.692387	{}	663	test/root/sources/image/dicom_image	test/root/process/convert/dicom_image	undefined	test/root
988	Link	Magna porttitor a arcumorbi.	2019-04-07 10:43:17.692351	2019-04-07 10:43:17.692387	{}	663	test/root/process/constant/constant	test/root/process/convert/constant	undefined	test/root/process
989	Link	Nonumy imperdietaliquam faucibus egestas quammaecenas ultrices laoreetphasellus, euismod voluptua id ligula non interdum vehicula.	2019-04-07 10:43:17.692351	2019-04-07 10:43:17.692387	{}	663	test/root/process/convert/nifti_image	test/root/sinks/save_nifti/nifti_image	undefined	test/root
1485	link_0		2019-05-13 06:27:40.180785	2019-05-13 06:27:40.180861	{"expand": false, "collapse": []}	692	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/source_1/output	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/step_1/in_1	Int	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro
1486	link_1		2019-05-13 06:27:40.180785	2019-05-13 06:27:40.180861	{"expand": false, "collapse": []}	692	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/source_2/output	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/step_1/in_2	Int	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro
1487	link_2		2019-05-13 06:27:40.180785	2019-05-13 06:27:40.180861	{"expand": false, "collapse": []}	692	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/const_step_1_fail_2_0/output	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/step_1/fail_2	Boolean	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro
1488	link_3		2019-05-13 06:27:40.180785	2019-05-13 06:27:40.180861	{"expand": false, "collapse": []}	692	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/source_3/output	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/step_2/in_1	Int	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro
1489	link_4		2019-05-13 06:27:40.180785	2019-05-13 06:27:40.180861	{"expand": false, "collapse": []}	692	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/source_1/output	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/step_2/in_2	Int	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro
1490	link_5		2019-05-13 06:27:40.180785	2019-05-13 06:27:40.180861	{"expand": false, "collapse": []}	692	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/const_step_2_fail_1_0/output	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/step_2/fail_1	Boolean	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro
1491	link_6		2019-05-13 06:27:40.180785	2019-05-13 06:27:40.180861	{"expand": false, "collapse": []}	692	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/step_1/out_2	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/step_3/in_1	Int	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro
1492	link_7		2019-05-13 06:27:40.180785	2019-05-13 06:27:40.180861	{"expand": false, "collapse": []}	692	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/step_2/out_1	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/step_3/in_2	Int	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro
1493	link_8		2019-05-13 06:27:40.180785	2019-05-13 06:27:40.180861	{"expand": false, "collapse": []}	692	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/step_3/out_1	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/range/value	Int	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro
1494	link_9		2019-05-13 06:27:40.180785	2019-05-13 06:27:40.180861	{"expand": false, "collapse": []}	692	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/range/result	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sum/values	Int	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro
1495	link_10		2019-05-13 06:27:40.180785	2019-05-13 06:27:40.180861	{"expand": false, "collapse": []}	692	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/step_1/out_1	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sink_1/input	Int	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro
1496	link_11		2019-05-13 06:27:40.180785	2019-05-13 06:27:40.180861	{"expand": false, "collapse": []}	692	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/step_2/out_2	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sink_2/input	Int	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro
1497	link_12		2019-05-13 06:27:40.180785	2019-05-13 06:27:40.180861	{"expand": false, "collapse": []}	692	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/step_3/out_1	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sink_3/input	Int	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro
1498	link_13		2019-05-13 06:27:40.180785	2019-05-13 06:27:40.180861	{"expand": false, "collapse": []}	692	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/range/result	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sink_4/input	Int	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro
1499	link_14		2019-05-13 06:27:40.180785	2019-05-13 06:27:40.180861	{"expand": false, "collapse": []}	692	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sum/result	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sink_5/input	Int	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro
1500	link_0		2019-05-13 06:27:40.180785	2019-05-13 06:27:40.180861	{"expand": false, "collapse": []}	692	failing_macro_top_level_2019-05-13T08-29-04/root/source_a/output	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/source_1/input	Int	failing_macro_top_level_2019-05-13T08-29-04/root
1501	link_1		2019-05-13 06:27:40.180785	2019-05-13 06:27:40.180861	{"expand": false, "collapse": []}	692	failing_macro_top_level_2019-05-13T08-29-04/root/source_b/output	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/source_2/input	Int	failing_macro_top_level_2019-05-13T08-29-04/root
1502	link_2		2019-05-13 06:27:40.180785	2019-05-13 06:27:40.180861	{"expand": false, "collapse": []}	692	failing_macro_top_level_2019-05-13T08-29-04/root/source_c/output	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/source_3/input	Int	failing_macro_top_level_2019-05-13T08-29-04/root
1503	link_3		2019-05-13 06:27:40.180785	2019-05-13 06:27:40.180861	{"expand": false, "collapse": []}	692	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sink_5/output	failing_macro_top_level_2019-05-13T08-29-04/root/add/left_hand	Int	failing_macro_top_level_2019-05-13T08-29-04/root
1504	link_4		2019-05-13 06:27:40.180785	2019-05-13 06:27:40.180861	{"expand": false, "collapse": []}	692	failing_macro_top_level_2019-05-13T08-29-04/root/const_add_right_hand_0/output	failing_macro_top_level_2019-05-13T08-29-04/root/add/right_hand	Int	failing_macro_top_level_2019-05-13T08-29-04/root
1505	link_5		2019-05-13 06:27:40.180785	2019-05-13 06:27:40.180861	{"expand": false, "collapse": []}	692	failing_macro_top_level_2019-05-13T08-29-04/root/add/result	failing_macro_top_level_2019-05-13T08-29-04/root/sink/input	Int	failing_macro_top_level_2019-05-13T08-29-04/root
\.


--
-- Data for Name: nodes; Type: TABLE DATA; Schema: public; Owner: pim
--

COPY public.nodes (id, path, title, description, created, modified, custom_data, type, parent_id, run_id, level, aggregates) FROM stdin;
5966	test/root	Root	Root node	2019-04-07 10:43:17.676339	2019-04-07 10:43:17.676385	{}	group	\N	663	0	{0,0,0,0,0,0}
5592	tissue_pipeline_2019-01-31T21-41-53/root/segmentation	segmentation	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	NetworkStep	5591	624	1	{0,0,0,0,0,0}
5967	test/root/sources	Sources	Metusdonec cubilia variuscras dictumst, libero lobortis pretium.	2019-04-07 10:43:17.676339	2019-04-07 10:43:17.676385	{}	bat	5966	663	1	{0,0,0,0,0,0}
5968	test/root/process	Convert	Nulla leo, vehicula lacinia tempus cursus mazim.	2019-04-07 10:43:17.676339	2019-04-07 10:43:17.676385	{}	dust	5966	663	1	{0,0,0,0,0,0}
5969	test/root/sinks	Sinks	Id semvestibulum fames, diaminteger tincidunt takimata egestas tortor.	2019-04-07 10:43:17.676339	2019-04-07 10:43:17.676385	{}	opinions	5966	663	1	{0,0,0,0,0,0}
5970	test/root/sources/image	Source Image	Cubilia quam magna, ultrices duimauris exerci integer iusto.	2019-04-07 10:43:17.676339	2019-04-07 10:43:17.676385	{}	SourceNode	5967	663	2	{0,0,0,0,0,0}
5593	tissue_pipeline_2019-01-31T21-41-53/root/statistics	statistics	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	NetworkStep	5591	624	1	{0,0,0,0,0,0}
5594	tissue_pipeline_2019-01-31T21-41-53/root/dicom_to_nifti	dicom_to_nifti	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	NetworkStep	5591	624	1	{0,0,0,0,0,0}
5595	tissue_pipeline_2019-01-31T21-41-53/root/hammers_atlas	hammers_atlas	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	NetworkStep	5591	624	1	{0,0,0,0,0,0}
5596	tissue_pipeline_2019-01-31T21-41-53/root/subjects	subjects	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	NetworkStep	5591	624	1	{0,0,0,0,0,0}
5597	tissue_pipeline_2019-01-31T21-41-53/root/output	output	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	NetworkStep	5591	624	1	{0,0,0,0,0,0}
5598	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG	BET_and_REG	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	NetworkStep	5591	624	1	{0,0,0,0,0,0}
5599	tissue_pipeline_2019-01-31T21-41-53/root/qc_overview	qc_overview	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	NetworkStep	5591	624	1	{0,0,0,0,0,0}
5600	tissue_pipeline_2019-01-31T21-41-53/root/segmentation/fast_tissue_segmentation	fast_tissue_segmentation	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	NodeRun	5592	624	2	{0,0,0,0,0,0}
5601	tissue_pipeline_2019-01-31T21-41-53/root/segmentation/calculate_hard_segmentation	calculate_hard_segmentation	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	NodeRun	5592	624	2	{0,0,0,0,0,0}
5602	tissue_pipeline_2019-01-31T21-41-53/root/segmentation/mask_t1_with_brain_mask	mask_t1_with_brain_mask	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	NodeRun	5592	624	2	{0,0,0,0,0,0}
5662	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/const_combine_labels_method_0	const_combine_labels_method_0	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	ConstantNodeRun	5598	624	2	{0,0,0,0,0,0}
5663	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/const_combine_labels_number_of_classes_0	const_combine_labels_number_of_classes_0	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	ConstantNodeRun	5598	624	2	{0,0,0,0,0,0}
5664	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/threshold_labels	threshold_labels	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	NodeRun	5598	624	2	{0,0,0,0,0,0}
5665	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/const_threshold_labels_upper_threshold_0	const_threshold_labels_upper_threshold_0	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	ConstantNodeRun	5598	624	2	{0,0,0,0,0,0}
5666	tissue_pipeline_2019-01-31T21-41-53/root/qc_overview/t1_with_gm_outline_sink	t1_with_gm_outline_sink	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	SinkNodeRun	5599	624	2	{0,0,0,0,0,0}
5667	tissue_pipeline_2019-01-31T21-41-53/root/qc_overview/t1_with_wm_outline_sink	t1_with_wm_outline_sink	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	SinkNodeRun	5599	624	2	{0,0,0,0,0,0}
6520	failing_macro_top_level_2019-05-13T08-29-04/root/source_a	source_a	\N	2019-05-13 06:27:40.164058	2019-05-13 06:29:07.461414	{}	SourceNodeRun	6519	692	1	{0,0,1,0,0,0}
5668	tissue_pipeline_2019-01-31T21-41-53/root/qc_overview/t1_with_hammers_sink	t1_with_hammers_sink	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	SinkNodeRun	5599	624	2	{0,0,0,0,0,0}
5669	tissue_pipeline_2019-01-31T21-41-53/root/qc_overview/t1_qc_sink	t1_qc_sink	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	SinkNodeRun	5599	624	2	{0,0,0,0,0,0}
6523	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro	failing_macro	\N	2019-05-13 06:27:40.164058	2019-05-13 06:29:19.974458	{}	MacroNodeRun	6519	692	1	{0,0,47,4,9,0}
6524	failing_macro_top_level_2019-05-13T08-29-04/root/add	add	\N	2019-05-13 06:27:40.164058	2019-05-13 06:29:22.470705	{}	NodeRun	6519	692	1	{0,0,1,0,3,0}
6525	failing_macro_top_level_2019-05-13T08-29-04/root/sink	sink	\N	2019-05-13 06:27:40.164058	2019-05-13 06:29:24.977399	{}	SinkNodeRun	6519	692	1	{0,0,1,0,3,0}
5603	tissue_pipeline_2019-01-31T21-41-53/root/segmentation/const_mask_t1_with_brain_mask_operator1_0	const_mask_t1_with_brain_mask_operator1_0	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	ConstantNodeRun	5592	624	2	{0,0,0,0,0,0}
5604	tissue_pipeline_2019-01-31T21-41-53/root/statistics/wm_volume	wm_volume	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	SinkNodeRun	5593	624	2	{0,0,0,0,0,0}
5971	test/root/process/constant	Constant	Bibendumin antesuspendisse accumsan torquent soluta pulvinar, lectus augue lacinia assum sed mazim magnis mauris.	2019-04-07 10:43:17.676339	2019-04-07 10:43:17.676385	{}	ConstantNode	5968	663	2	{0,0,0,0,0,0}
5972	test/root/process/convert	DICOM to Nifti	Fringilla conubia himenaeos tincidunt etiam senectus, turpis nonummy takimata posuere quisaenean diaminteger platea in.	2019-04-07 10:43:17.676339	2019-04-07 10:43:17.676385	{}	ConverterNode	5968	663	2	{0,0,0,0,0,0}
5973	test/root/sinks/save_nifti	Save Nifti	Turpis tincidunt taciti imperdiet, possim condimentum sem muspellentesque rebum aenean pretium mazim.	2019-04-07 10:43:17.676339	2019-04-07 10:43:17.676385	{}	SinkNode	5969	663	2	{0,0,0,0,0,0}
5605	tissue_pipeline_2019-01-31T21-41-53/root/statistics/gm_volume	gm_volume	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	SinkNodeRun	5593	624	2	{0,0,0,0,0,0}
5606	tissue_pipeline_2019-01-31T21-41-53/root/statistics/brain_volume	brain_volume	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	SinkNodeRun	5593	624	2	{0,0,0,0,0,0}
5607	tissue_pipeline_2019-01-31T21-41-53/root/statistics/get_wm_volume	get_wm_volume	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	NodeRun	5593	624	2	{0,0,0,0,0,0}
5608	tissue_pipeline_2019-01-31T21-41-53/root/statistics/get_gm_volume	get_gm_volume	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	NodeRun	5593	624	2	{0,0,0,0,0,0}
5609	tissue_pipeline_2019-01-31T21-41-53/root/statistics/get_brain_volume	get_brain_volume	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	NodeRun	5593	624	2	{0,0,0,0,0,0}
5610	tissue_pipeline_2019-01-31T21-41-53/root/statistics/mask_hammers_labels	mask_hammers_labels	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	NodeRun	5593	624	2	{0,0,0,0,0,0}
5611	tissue_pipeline_2019-01-31T21-41-53/root/statistics/const_mask_hammers_labels_operator_0	const_mask_hammers_labels_operator_0	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	ConstantNodeRun	5593	624	2	{0,0,0,0,0,0}
5612	tissue_pipeline_2019-01-31T21-41-53/root/statistics/const_get_wm_volume_volume_0	const_get_wm_volume_volume_0	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	ConstantNodeRun	5593	624	2	{0,0,0,0,0,0}
5613	tissue_pipeline_2019-01-31T21-41-53/root/statistics/const_get_wm_volume_selection_0	const_get_wm_volume_selection_0	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	ConstantNodeRun	5593	624	2	{0,0,0,0,0,0}
5614	tissue_pipeline_2019-01-31T21-41-53/root/statistics/const_get_wm_volume_flatten_0	const_get_wm_volume_flatten_0	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	ConstantNodeRun	5593	624	2	{0,0,0,0,0,0}
5615	tissue_pipeline_2019-01-31T21-41-53/root/statistics/const_get_gm_volume_volume_0	const_get_gm_volume_volume_0	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	ConstantNodeRun	5593	624	2	{0,0,0,0,0,0}
5616	tissue_pipeline_2019-01-31T21-41-53/root/statistics/const_get_gm_volume_selection_0	const_get_gm_volume_selection_0	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	ConstantNodeRun	5593	624	2	{0,0,0,0,0,0}
5617	tissue_pipeline_2019-01-31T21-41-53/root/statistics/const_get_gm_volume_flatten_0	const_get_gm_volume_flatten_0	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	ConstantNodeRun	5593	624	2	{0,0,0,0,0,0}
5618	tissue_pipeline_2019-01-31T21-41-53/root/statistics/const_get_gm_volume_volume_1	const_get_gm_volume_volume_1	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	ConstantNodeRun	5593	624	2	{0,0,0,0,0,0}
5671	tissue_pipeline_2019-01-31T21-41-53/root/qc_overview/t1_qc	t1_qc	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	NodeRun	5599	624	2	{0,0,0,0,0,0}
6530	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sink_1	sink_1	\N	2019-05-13 06:27:40.164058	2019-05-13 06:29:19.974458	{}	SinkNodeRun	6523	692	2	{0,0,4,0,0,0}
6531	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sink_2	sink_2	\N	2019-05-13 06:27:40.164058	2019-05-13 06:29:19.974458	{}	SinkNodeRun	6523	692	2	{0,0,4,0,0,0}
6532	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sink_3	sink_3	\N	2019-05-13 06:27:40.164058	2019-05-13 06:29:19.974458	{}	SinkNodeRun	6523	692	2	{0,0,4,0,0,0}
6533	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sink_4	sink_4	\N	2019-05-13 06:27:40.164058	2019-05-13 06:29:19.974458	{}	SinkNodeRun	6523	692	2	{0,0,4,0,0,0}
6534	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sink_5	sink_5	\N	2019-05-13 06:27:40.164058	2019-05-13 06:29:19.974458	{}	SinkNodeRun	6523	692	2	{0,0,4,0,0,0}
5619	tissue_pipeline_2019-01-31T21-41-53/root/statistics/const_get_gm_volume_selection_1	const_get_gm_volume_selection_1	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	ConstantNodeRun	5593	624	2	{0,0,0,0,0,0}
5620	tissue_pipeline_2019-01-31T21-41-53/root/statistics/const_get_gm_volume_flatten_1	const_get_gm_volume_flatten_1	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	ConstantNodeRun	5593	624	2	{0,0,0,0,0,0}
6540	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/const_step_1_fail_2_0	const_step_1_fail_2_0	\N	2019-05-13 06:27:40.164058	2019-05-13 06:29:09.963361	{}	ConstantNodeRun	6523	692	2	{0,0,4,0,0,0}
6541	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/const_step_2_fail_1_0	const_step_2_fail_1_0	\N	2019-05-13 06:27:40.164058	2019-05-13 06:29:09.963361	{}	ConstantNodeRun	6523	692	2	{0,0,4,0,0,0}
5621	tissue_pipeline_2019-01-31T21-41-53/root/statistics/const_get_brain_volume_volume_0	const_get_brain_volume_volume_0	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	ConstantNodeRun	5593	624	2	{0,0,0,0,0,0}
5622	tissue_pipeline_2019-01-31T21-41-53/root/statistics/const_get_brain_volume_selection_0	const_get_brain_volume_selection_0	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	ConstantNodeRun	5593	624	2	{0,0,0,0,0,0}
5623	tissue_pipeline_2019-01-31T21-41-53/root/statistics/const_get_brain_volume_flatten_0	const_get_brain_volume_flatten_0	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	ConstantNodeRun	5593	624	2	{0,0,0,0,0,0}
5624	tissue_pipeline_2019-01-31T21-41-53/root/dicom_to_nifti/t1_dicom_to_nifti	t1_dicom_to_nifti	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	NodeRun	5594	624	2	{0,0,0,0,0,0}
5625	tissue_pipeline_2019-01-31T21-41-53/root/dicom_to_nifti/reformat_t1	reformat_t1	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	NodeRun	5594	624	2	{0,0,0,0,0,0}
5626	tissue_pipeline_2019-01-31T21-41-53/root/dicom_to_nifti/const_t1_dicom_to_nifti_file_order_0	const_t1_dicom_to_nifti_file_order_0	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	ConstantNodeRun	5594	624	2	{0,0,0,0,0,0}
5627	tissue_pipeline_2019-01-31T21-41-53/root/hammers_atlas/hammers_t1w	hammers_t1w	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	SourceNodeRun	5595	624	2	{0,0,0,0,0,0}
5628	tissue_pipeline_2019-01-31T21-41-53/root/hammers_atlas/hammers_brains_r5	hammers_brains_r5	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	SourceNodeRun	5595	624	2	{0,0,0,0,0,0}
5629	tissue_pipeline_2019-01-31T21-41-53/root/hammers_atlas/hammers_brains	hammers_brains	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	SourceNodeRun	5595	624	2	{0,0,0,0,0,0}
5630	tissue_pipeline_2019-01-31T21-41-53/root/hammers_atlas/hammers_labels	hammers_labels	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	SourceNodeRun	5595	624	2	{0,0,0,0,0,0}
6539	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sum	sum	\N	2019-05-13 06:27:40.164058	2019-05-13 06:29:19.974458	{}	NodeRun	6523	692	2	{0,0,1,0,3,0}
5631	tissue_pipeline_2019-01-31T21-41-53/root/subjects/t1w_dicom	t1w_dicom	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	SourceNodeRun	5596	624	2	{0,0,0,0,0,0}
5632	tissue_pipeline_2019-01-31T21-41-53/root/output/t1_nifti_images	t1_nifti_images	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	SinkNodeRun	5597	624	2	{0,0,0,0,0,0}
5633	tissue_pipeline_2019-01-31T21-41-53/root/output/gm_map_output	gm_map_output	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	SinkNodeRun	5597	624	2	{0,0,0,0,0,0}
5634	tissue_pipeline_2019-01-31T21-41-53/root/output/wm_map_output	wm_map_output	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	SinkNodeRun	5597	624	2	{0,0,0,0,0,0}
5635	tissue_pipeline_2019-01-31T21-41-53/root/output/multi_atlas_segm	multi_atlas_segm	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	SinkNodeRun	5597	624	2	{0,0,0,0,0,0}
5636	tissue_pipeline_2019-01-31T21-41-53/root/output/hammer_brain_mask	hammer_brain_mask	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	SinkNodeRun	5597	624	2	{0,0,0,0,0,0}
5637	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/bet	bet	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	NodeRun	5598	624	2	{0,0,0,0,0,0}
5638	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/convert_brain	convert_brain	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	NodeRun	5598	624	2	{0,0,0,0,0,0}
5639	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/threshold_brain	threshold_brain	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	NodeRun	5598	624	2	{0,0,0,0,0,0}
5640	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/dilate_brainmask	dilate_brainmask	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	NodeRun	5598	624	2	{0,0,0,0,0,0}
5641	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/n4_bias_correction	n4_bias_correction	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	NodeRun	5598	624	2	{0,0,0,0,0,0}
5642	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/brain_mask_registation	brain_mask_registation	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	NodeRun	5598	624	2	{0,0,0,0,0,0}
5643	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/t1w_registration	t1w_registration	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	NodeRun	5598	624	2	{0,0,0,0,0,0}
5644	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/hammers_brain_transform	hammers_brain_transform	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	NodeRun	5598	624	2	{0,0,0,0,0,0}
5645	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/label_registration	label_registration	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	NodeRun	5598	624	2	{0,0,0,0,0,0}
5646	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/combine_labels	combine_labels	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	NodeRun	5598	624	2	{0,0,0,0,0,0}
5647	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/combine_brains	combine_brains	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	NodeRun	5598	624	2	{0,0,0,0,0,0}
5648	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/const_bet_bias_cleanup_0	const_bet_bias_cleanup_0	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	ConstantNodeRun	5598	624	2	{0,0,0,0,0,0}
5649	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/const_bet_fraction_threshold_0	const_bet_fraction_threshold_0	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	ConstantNodeRun	5598	624	2	{0,0,0,0,0,0}
5650	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/const_convert_brain_component_type_0	const_convert_brain_component_type_0	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	ConstantNodeRun	5598	624	2	{0,0,0,0,0,0}
5651	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/const_dilate_brainmask_operation_0	const_dilate_brainmask_operation_0	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	ConstantNodeRun	5598	624	2	{0,0,0,0,0,0}
5652	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/const_dilate_brainmask_operation_type_0	const_dilate_brainmask_operation_type_0	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	ConstantNodeRun	5598	624	2	{0,0,0,0,0,0}
5653	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/const_dilate_brainmask_radius_0	const_dilate_brainmask_radius_0	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	ConstantNodeRun	5598	624	2	{0,0,0,0,0,0}
5654	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/const_brain_mask_registation_parameters_0	const_brain_mask_registation_parameters_0	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	ConstantNodeRun	5598	624	2	{0,0,0,0,0,0}
5655	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/const_brain_mask_registation_threads_0	const_brain_mask_registation_threads_0	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	ConstantNodeRun	5598	624	2	{0,0,0,0,0,0}
6519	failing_macro_top_level_2019-05-13T08-29-04/root	failing_macro_top_level		2019-05-13 06:27:40.164058	2019-05-13 06:29:24.977399	{}	NetworkRun	\N	692	0	{0,0,53,4,15,0}
5656	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/const_t1w_registration_parameters_0	const_t1w_registration_parameters_0	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	ConstantNodeRun	5598	624	2	{0,0,0,0,0,0}
5657	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/const_t1w_registration_threads_0	const_t1w_registration_threads_0	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	ConstantNodeRun	5598	624	2	{0,0,0,0,0,0}
5658	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/const_hammers_brain_transform_threads_0	const_hammers_brain_transform_threads_0	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	ConstantNodeRun	5598	624	2	{0,0,0,0,0,0}
5659	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/const_combine_brains_method_0	const_combine_brains_method_0	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	ConstantNodeRun	5598	624	2	{0,0,0,0,0,0}
5660	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/const_combine_brains_number_of_classes_0	const_combine_brains_number_of_classes_0	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	ConstantNodeRun	5598	624	2	{0,0,0,0,0,0}
5661	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/const_label_registration_threads_0	const_label_registration_threads_0	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	ConstantNodeRun	5598	624	2	{0,0,0,0,0,0}
5670	tissue_pipeline_2019-01-31T21-41-53/root/qc_overview/t1_with_brain_sink	t1_with_brain_sink	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	SinkNodeRun	5599	624	2	{0,0,0,0,0,0}
5672	tissue_pipeline_2019-01-31T21-41-53/root/qc_overview/t1_with_gm_outline	t1_with_gm_outline	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	NodeRun	5599	624	2	{0,0,0,0,0,0}
5673	tissue_pipeline_2019-01-31T21-41-53/root/qc_overview/t1_with_wm_outline	t1_with_wm_outline	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	NodeRun	5599	624	2	{0,0,0,0,0,0}
5674	tissue_pipeline_2019-01-31T21-41-53/root/qc_overview/t1_with_hammers	t1_with_hammers	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	NodeRun	5599	624	2	{0,0,0,0,0,0}
5675	tissue_pipeline_2019-01-31T21-41-53/root/qc_overview/t1_with_brain	t1_with_brain	\N	2019-04-02 09:39:49.647701	2019-04-02 09:39:49.64774	{}	NodeRun	5599	624	2	{0,0,0,0,0,0}
5591	tissue_pipeline_2019-01-31T21-41-53/root	tissue_pipeline		2019-04-02 09:39:49.647701	2019-04-03 10:16:01.588243	{}	NetworkRun	\N	624	0	{0,0,0,0,0,0}
6521	failing_macro_top_level_2019-05-13T08-29-04/root/source_b	source_b	\N	2019-05-13 06:27:40.164058	2019-05-13 06:29:07.461414	{}	SourceNodeRun	6519	692	1	{0,0,1,0,0,0}
6522	failing_macro_top_level_2019-05-13T08-29-04/root/source_c	source_c	\N	2019-05-13 06:27:40.164058	2019-05-13 06:29:07.461414	{}	SourceNodeRun	6519	692	1	{0,0,1,0,0,0}
6526	failing_macro_top_level_2019-05-13T08-29-04/root/const_add_right_hand_0	const_add_right_hand_0	\N	2019-05-13 06:27:40.164058	2019-05-13 06:29:07.461414	{}	ConstantNodeRun	6519	692	1	{0,0,1,0,0,0}
6527	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/source_1	source_1	\N	2019-05-13 06:27:40.164058	2019-05-13 06:29:09.963361	{}	SourceNodeRun	6523	692	2	{0,0,4,0,0,0}
6528	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/source_2	source_2	\N	2019-05-13 06:27:40.164058	2019-05-13 06:29:09.963361	{}	SourceNodeRun	6523	692	2	{0,0,4,0,0,0}
6529	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/source_3	source_3	\N	2019-05-13 06:27:40.164058	2019-05-13 06:29:09.963361	{}	SourceNodeRun	6523	692	2	{0,0,4,0,0,0}
6535	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/step_1	step_1	\N	2019-05-13 06:27:40.164058	2019-05-13 06:29:12.466034	{}	NodeRun	6523	692	2	{0,0,2,2,0,0}
6536	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/step_2	step_2	\N	2019-05-13 06:27:40.164058	2019-05-13 06:29:12.466034	{}	NodeRun	6523	692	2	{0,0,2,2,0,0}
6537	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/step_3	step_3	\N	2019-05-13 06:27:40.164058	2019-05-13 06:29:14.960987	{}	NodeRun	6523	692	2	{0,0,1,0,3,0}
6538	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/range	range	\N	2019-05-13 06:27:40.164058	2019-05-13 06:29:17.465092	{}	NodeRun	6523	692	2	{0,0,1,0,3,0}
6419	run_0_jobs/root	Root	Root node	2019-04-30 12:02:05.459174	2019-04-30 12:02:05.459216	{}	group	\N	687	0	{0,0,0,0,0,0}
6420	run_0_jobs/root/child_a	Gloria	Lectus ultriciespellentesque nullam, potenti velit eros nunc elit ridiculus inceptos.	2019-04-30 12:02:05.459174	2019-04-30 12:02:05.459216	{}	crust	6419	687	1	{0,0,0,0,0,0}
6421	run_0_jobs/root/child_b	Jaylon	Dignissim leo labore elitr in.	2019-04-30 12:02:05.459174	2019-04-30 12:02:05.459216	{}	anchor	6419	687	1	{0,0,0,0,0,0}
6422	run_0_jobs/root/child_a/child_a	Mckenzie	Wisi suscipit.	2019-04-30 12:02:05.459174	2019-04-30 12:02:05.459216	{}	nation	6420	687	2	{0,0,0,0,0,0}
6423	run_0_jobs/root/child_a/child_b	Noah	Lobortisetiam ametduis eget minulla justocras enimaliquam.	2019-04-30 12:02:05.459174	2019-04-30 12:02:05.459216	{}	hail	6420	687	2	{0,0,0,0,0,0}
6424	run_0_jobs/root/child_b/child_a	Marian	Mipellentesque molestie doming enimaliquam nulla eirmod, cursus luptatum a iriure aliquammauris venenatis.	2019-04-30 12:02:05.459174	2019-04-30 12:02:05.459216	{}	fetches	6421	687	2	{0,0,0,0,0,0}
6425	run_0_jobs/root/child_b/child_b	Mat	Option mollis.	2019-04-30 12:02:05.459174	2019-04-30 12:02:05.459216	{}	produce	6421	687	2	{0,0,0,0,0,0}
6426	run_0_jobs/root/child_b/child_c	Kyra	Odio mollis tortorvestibulum, exerci maecenas kasd quam.	2019-04-30 12:02:05.459174	2019-04-30 12:02:05.459216	{}	surfaces	6421	687	2	{0,0,0,0,0,0}
\.


--
-- Data for Name: out_ports; Type: TABLE DATA; Schema: public; Owner: pim
--

COPY public.out_ports (id, path, title, description, created, modified, custom_data, node_id) FROM stdin;
3268	tissue_pipeline_2019-01-31T21-41-53/root/segmentation/fast_tissue_segmentation/output_prefix	output_prefix	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "FilePrefix", "dimension_names": ["subjects"]}	5600
3269	tissue_pipeline_2019-01-31T21-41-53/root/segmentation/fast_tissue_segmentation/segmentation	segmentation	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "NiftiImageFileCompressed", "dimension_names": ["subjects"]}	5600
3270	tissue_pipeline_2019-01-31T21-41-53/root/segmentation/fast_tissue_segmentation/csf	csf	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "NiftiImageFileCompressed", "dimension_names": ["subjects"]}	5600
3271	tissue_pipeline_2019-01-31T21-41-53/root/segmentation/fast_tissue_segmentation/gray_matter	gray_matter	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "NiftiImageFileCompressed", "dimension_names": ["subjects"]}	5600
3272	tissue_pipeline_2019-01-31T21-41-53/root/segmentation/fast_tissue_segmentation/white_matter	white_matter	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "NiftiImageFileCompressed", "dimension_names": ["subjects"]}	5600
3273	tissue_pipeline_2019-01-31T21-41-53/root/segmentation/calculate_hard_segmentation/gm_image	gm_image	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "NiftiImageFileCompressed", "dimension_names": ["subjects"]}	5601
3274	tissue_pipeline_2019-01-31T21-41-53/root/segmentation/calculate_hard_segmentation/wm_image	wm_image	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "NiftiImageFileCompressed", "dimension_names": ["subjects"]}	5601
3275	tissue_pipeline_2019-01-31T21-41-53/root/segmentation/calculate_hard_segmentation/csf_image	csf_image	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "NiftiImageFileCompressed", "dimension_names": ["subjects"]}	5601
3276	tissue_pipeline_2019-01-31T21-41-53/root/segmentation/mask_t1_with_brain_mask/output_image	output_image	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "NiftiImageFileCompressed", "dimension_names": ["subjects"]}	5602
3277	tissue_pipeline_2019-01-31T21-41-53/root/segmentation/const_mask_t1_with_brain_mask_operator1_0/output	output	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "String", "dimension_names": ["const_mask_t1_with_brain_mask_operator1_0"]}	5603
3278	tissue_pipeline_2019-01-31T21-41-53/root/statistics/wm_volume/output	output	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "CsvFile", "dimension_names": []}	5604
3279	tissue_pipeline_2019-01-31T21-41-53/root/statistics/gm_volume/output	output	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "CsvFile", "dimension_names": []}	5605
3280	tissue_pipeline_2019-01-31T21-41-53/root/statistics/brain_volume/output	output	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "CsvFile", "dimension_names": []}	5606
3281	tissue_pipeline_2019-01-31T21-41-53/root/statistics/get_wm_volume/statistics	statistics	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "CsvFile", "dimension_names": ["subjects"]}	5607
3282	tissue_pipeline_2019-01-31T21-41-53/root/statistics/get_gm_volume/statistics	statistics	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "CsvFile", "dimension_names": ["subjects"]}	5608
3283	tissue_pipeline_2019-01-31T21-41-53/root/statistics/get_brain_volume/statistics	statistics	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "CsvFile", "dimension_names": ["subjects"]}	5609
3284	tissue_pipeline_2019-01-31T21-41-53/root/statistics/mask_hammers_labels/result	result	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "NiftiImageFileCompressed", "dimension_names": ["subjects"]}	5610
3285	tissue_pipeline_2019-01-31T21-41-53/root/statistics/const_mask_hammers_labels_operator_0/output	output	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "String", "dimension_names": ["const_mask_hammers_labels_operator_0"]}	5611
3286	tissue_pipeline_2019-01-31T21-41-53/root/statistics/const_get_wm_volume_volume_0/output	output	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "String", "dimension_names": ["const_get_wm_volume_volume_0"]}	5612
3287	tissue_pipeline_2019-01-31T21-41-53/root/statistics/const_get_wm_volume_selection_0/output	output	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "TxtFile", "dimension_names": ["const_get_wm_volume_selection_0"]}	5613
3288	tissue_pipeline_2019-01-31T21-41-53/root/statistics/const_get_wm_volume_flatten_0/output	output	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "Boolean", "dimension_names": ["const_get_wm_volume_flatten_0"]}	5614
3289	tissue_pipeline_2019-01-31T21-41-53/root/statistics/const_get_gm_volume_volume_0/output	output	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "String", "dimension_names": ["const_get_gm_volume_volume_0"]}	5615
3290	tissue_pipeline_2019-01-31T21-41-53/root/statistics/const_get_gm_volume_selection_0/output	output	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "TxtFile", "dimension_names": ["const_get_gm_volume_selection_0"]}	5616
3291	tissue_pipeline_2019-01-31T21-41-53/root/statistics/const_get_gm_volume_flatten_0/output	output	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "Boolean", "dimension_names": ["const_get_gm_volume_flatten_0"]}	5617
3292	tissue_pipeline_2019-01-31T21-41-53/root/statistics/const_get_gm_volume_volume_1/output	output	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "String", "dimension_names": ["const_get_gm_volume_volume_1"]}	5618
3293	tissue_pipeline_2019-01-31T21-41-53/root/statistics/const_get_gm_volume_selection_1/output	output	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "TxtFile", "dimension_names": ["const_get_gm_volume_selection_1"]}	5619
3294	tissue_pipeline_2019-01-31T21-41-53/root/statistics/const_get_gm_volume_flatten_1/output	output	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "Boolean", "dimension_names": ["const_get_gm_volume_flatten_1"]}	5620
3295	tissue_pipeline_2019-01-31T21-41-53/root/statistics/const_get_brain_volume_volume_0/output	output	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "String", "dimension_names": ["const_get_brain_volume_volume_0"]}	5621
3296	tissue_pipeline_2019-01-31T21-41-53/root/statistics/const_get_brain_volume_selection_0/output	output	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "TxtFile", "dimension_names": ["const_get_brain_volume_selection_0"]}	5622
3297	tissue_pipeline_2019-01-31T21-41-53/root/statistics/const_get_brain_volume_flatten_0/output	output	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "Boolean", "dimension_names": ["const_get_brain_volume_flatten_0"]}	5623
3298	tissue_pipeline_2019-01-31T21-41-53/root/dicom_to_nifti/t1_dicom_to_nifti/image	image	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "NiftiImageFileCompressed", "dimension_names": ["subjects"]}	5624
3299	tissue_pipeline_2019-01-31T21-41-53/root/dicom_to_nifti/reformat_t1/reformatted_image	reformatted_image	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "NiftiImageFileCompressed", "dimension_names": ["subjects"]}	5625
3300	tissue_pipeline_2019-01-31T21-41-53/root/dicom_to_nifti/const_t1_dicom_to_nifti_file_order_0/output	output	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "String", "dimension_names": ["const_t1_dicom_to_nifti_file_order_0"]}	5626
3301	tissue_pipeline_2019-01-31T21-41-53/root/hammers_atlas/hammers_t1w/output	output	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "NiftiImageFileCompressed", "dimension_names": ["hammers_atlas"]}	5627
3302	tissue_pipeline_2019-01-31T21-41-53/root/hammers_atlas/hammers_brains_r5/output	output	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "NiftiImageFileCompressed", "dimension_names": ["hammers_atlas"]}	5628
3303	tissue_pipeline_2019-01-31T21-41-53/root/hammers_atlas/hammers_brains/output	output	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "NiftiImageFileCompressed", "dimension_names": ["hammers_atlas"]}	5629
3304	tissue_pipeline_2019-01-31T21-41-53/root/hammers_atlas/hammers_labels/output	output	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "NiftiImageFileCompressed", "dimension_names": ["hammers_atlas"]}	5630
3305	tissue_pipeline_2019-01-31T21-41-53/root/subjects/t1w_dicom/output	output	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "DicomImageFile", "dimension_names": ["subjects"]}	5631
3306	tissue_pipeline_2019-01-31T21-41-53/root/output/t1_nifti_images/output	output	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "NiftiImageFileCompressed", "dimension_names": []}	5632
3307	tissue_pipeline_2019-01-31T21-41-53/root/output/gm_map_output/output	output	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "NiftiImageFile", "dimension_names": []}	5633
3308	tissue_pipeline_2019-01-31T21-41-53/root/output/wm_map_output/output	output	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "NiftiImageFile", "dimension_names": []}	5634
3309	tissue_pipeline_2019-01-31T21-41-53/root/output/multi_atlas_segm/output	output	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "NiftiImageFile", "dimension_names": []}	5635
3310	tissue_pipeline_2019-01-31T21-41-53/root/output/hammer_brain_mask/output	output	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "NiftiImageFile", "dimension_names": []}	5636
3311	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/bet/brain_image	brain_image	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "NiftiImageFileCompressed", "dimension_names": ["subjects"]}	5637
3312	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/bet/overlay_image	overlay_image	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "NiftiImageFileCompressed", "dimension_names": ["subjects"]}	5637
3313	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/bet/mask_image	mask_image	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "NiftiImageFileCompressed", "dimension_names": ["subjects"]}	5637
3314	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/bet/skull_image	skull_image	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "NiftiImageFileCompressed", "dimension_names": ["subjects"]}	5637
3315	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/bet/mesh_image	mesh_image	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "VTKMeshFile", "dimension_names": ["subjects"]}	5637
3316	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/convert_brain/image	image	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "NiftiImageFileCompressed", "dimension_names": ["subjects"]}	5638
3317	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/threshold_brain/image	image	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "NiftiImageFileCompressed", "dimension_names": ["subjects"]}	5639
3318	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/dilate_brainmask/image	image	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "NiftiImageFileCompressed", "dimension_names": ["subjects"]}	5640
3319	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/n4_bias_correction/corrected_image	corrected_image	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "NiftiImageFileCompressed", "dimension_names": ["subjects"]}	5641
3320	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/brain_mask_registation/directory	directory	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "Directory", "dimension_names": ["subjects", "hammers_atlas"]}	5642
3321	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/brain_mask_registation/transform	transform	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "ElastixTransformFile", "dimension_names": ["subjects", "hammers_atlas"]}	5642
3322	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/brain_mask_registation/log_file	log_file	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "ElastixLogFile", "dimension_names": ["subjects", "hammers_atlas"]}	5642
3323	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/t1w_registration/directory	directory	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "Directory", "dimension_names": ["subjects", "hammers_atlas"]}	5643
3324	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/t1w_registration/transform	transform	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "ElastixTransformFile", "dimension_names": ["subjects", "hammers_atlas"]}	5643
3325	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/t1w_registration/log_file	log_file	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "ElastixLogFile", "dimension_names": ["subjects", "hammers_atlas"]}	5643
3326	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/hammers_brain_transform/directory	directory	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "Directory", "dimension_names": ["subjects", "hammers_atlas"]}	5644
3327	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/hammers_brain_transform/image	image	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "NiftiImageFileCompressed", "dimension_names": ["subjects", "hammers_atlas"]}	5644
3328	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/hammers_brain_transform/points	points	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "NiftiImageFileCompressed", "dimension_names": ["subjects", "hammers_atlas"]}	5644
3329	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/hammers_brain_transform/determinant_of_jacobian	determinant_of_jacobian	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "NiftiImageFileCompressed", "dimension_names": ["subjects", "hammers_atlas"]}	5644
3330	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/hammers_brain_transform/jacobian_matrix	jacobian_matrix	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "NiftiImageFileCompressed", "dimension_names": ["subjects", "hammers_atlas"]}	5644
3331	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/hammers_brain_transform/log_file	log_file	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "TransformixLogFile", "dimension_names": ["subjects", "hammers_atlas"]}	5644
3332	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/label_registration/directory	directory	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "Directory", "dimension_names": ["subjects", "hammers_atlas"]}	5645
3333	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/label_registration/image	image	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "NiftiImageFileCompressed", "dimension_names": ["subjects", "hammers_atlas"]}	5645
3334	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/label_registration/points	points	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "NiftiImageFileCompressed", "dimension_names": ["subjects", "hammers_atlas"]}	5645
3335	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/label_registration/determinant_of_jacobian	determinant_of_jacobian	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "NiftiImageFileCompressed", "dimension_names": ["subjects", "hammers_atlas"]}	5645
3336	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/label_registration/jacobian_matrix	jacobian_matrix	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "NiftiImageFileCompressed", "dimension_names": ["subjects", "hammers_atlas"]}	5645
3337	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/label_registration/log_file	log_file	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "TransformixLogFile", "dimension_names": ["subjects", "hammers_atlas"]}	5645
3338	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/combine_labels/hard_segment	hard_segment	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "NiftiImageFileCompressed", "dimension_names": ["subjects"]}	5646
3339	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/combine_brains/hard_segment	hard_segment	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "NiftiImageFileCompressed", "dimension_names": ["subjects"]}	5647
3340	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/const_bet_bias_cleanup_0/output	output	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "Boolean", "dimension_names": ["const_bet_bias_cleanup_0"]}	5648
3341	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/const_bet_fraction_threshold_0/output	output	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "Float", "dimension_names": ["const_bet_fraction_threshold_0"]}	5649
3342	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/const_convert_brain_component_type_0/output	output	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "__PxCastConvert_0.3.2_interface__component_type__Enum__", "dimension_names": ["const_convert_brain_component_type_0"]}	5650
3343	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/const_dilate_brainmask_operation_0/output	output	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "__PxMorphology_0.3.2_interface__operation__Enum__", "dimension_names": ["const_dilate_brainmask_operation_0"]}	5651
3344	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/const_dilate_brainmask_operation_type_0/output	output	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "__PxMorphology_0.3.2_interface__operation_type__Enum__", "dimension_names": ["const_dilate_brainmask_operation_type_0"]}	5652
3345	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/const_dilate_brainmask_radius_0/output	output	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "Float", "dimension_names": ["const_dilate_brainmask_radius_0"]}	5653
3346	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/const_brain_mask_registation_parameters_0/output	output	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "ElastixParameterFile", "dimension_names": ["const_brain_mask_registation_parameters_0"]}	5654
3347	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/const_brain_mask_registation_threads_0/output	output	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "Int", "dimension_names": ["const_brain_mask_registation_threads_0"]}	5655
3348	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/const_t1w_registration_parameters_0/output	output	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "ElastixParameterFile", "dimension_names": ["const_t1w_registration_parameters_0"]}	5656
3349	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/const_t1w_registration_threads_0/output	output	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "Int", "dimension_names": ["const_t1w_registration_threads_0"]}	5657
3350	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/const_hammers_brain_transform_threads_0/output	output	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "Int", "dimension_names": ["const_hammers_brain_transform_threads_0"]}	5658
3351	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/const_combine_brains_method_0/output	output	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "__PxCombineSegmentations_0.3.2_interface__method__Enum__", "dimension_names": ["const_combine_brains_method_0"]}	5659
3352	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/const_combine_brains_number_of_classes_0/output	output	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "Int", "dimension_names": ["const_combine_brains_number_of_classes_0"]}	5660
3353	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/const_label_registration_threads_0/output	output	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "Int", "dimension_names": ["const_label_registration_threads_0"]}	5661
3354	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/const_combine_labels_method_0/output	output	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "__PxCombineSegmentations_0.3.2_interface__method__Enum__", "dimension_names": ["const_combine_labels_method_0"]}	5662
3355	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/const_combine_labels_number_of_classes_0/output	output	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "Int", "dimension_names": ["const_combine_labels_number_of_classes_0"]}	5663
3356	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/threshold_labels/image	image	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "NiftiImageFileCompressed", "dimension_names": ["subjects"]}	5664
3357	tissue_pipeline_2019-01-31T21-41-53/root/BET_and_REG/const_threshold_labels_upper_threshold_0/output	output	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "Float", "dimension_names": ["const_threshold_labels_upper_threshold_0"]}	5665
3358	tissue_pipeline_2019-01-31T21-41-53/root/qc_overview/t1_with_gm_outline_sink/output	output	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "PngImageFile", "dimension_names": []}	5666
3359	tissue_pipeline_2019-01-31T21-41-53/root/qc_overview/t1_with_wm_outline_sink/output	output	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "PngImageFile", "dimension_names": []}	5667
3360	tissue_pipeline_2019-01-31T21-41-53/root/qc_overview/t1_with_hammers_sink/output	output	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "PngImageFile", "dimension_names": []}	5668
3361	tissue_pipeline_2019-01-31T21-41-53/root/qc_overview/t1_qc_sink/output	output	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "PngImageFile", "dimension_names": []}	5669
3362	tissue_pipeline_2019-01-31T21-41-53/root/qc_overview/t1_with_brain_sink/output	output	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "PngImageFile", "dimension_names": []}	5670
3363	tissue_pipeline_2019-01-31T21-41-53/root/qc_overview/t1_qc/png_image	png_image	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "PngImageFile", "dimension_names": ["subjects"]}	5671
3364	tissue_pipeline_2019-01-31T21-41-53/root/qc_overview/t1_with_gm_outline/png_image	png_image	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "PngImageFile", "dimension_names": ["subjects"]}	5672
3365	tissue_pipeline_2019-01-31T21-41-53/root/qc_overview/t1_with_wm_outline/png_image	png_image	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "PngImageFile", "dimension_names": ["subjects"]}	5673
3366	tissue_pipeline_2019-01-31T21-41-53/root/qc_overview/t1_with_hammers/png_image	png_image	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "PngImageFile", "dimension_names": ["subjects"]}	5674
3367	tissue_pipeline_2019-01-31T21-41-53/root/qc_overview/t1_with_brain/png_image	png_image	\N	2019-04-02 09:39:49.661162	2019-04-02 09:39:49.661188	{"datatype": "PngImageFile", "dimension_names": ["subjects"]}	5675
3421	test/root/sources/image/dicom_image	dicom_image	\N	2019-04-07 10:43:17.690417	2019-04-07 10:43:17.690451	null	5970
3422	test/root/process/constant/constant	constant	\N	2019-04-07 10:43:17.690417	2019-04-07 10:43:17.690451	null	5971
3423	test/root/process/convert/nifti_image	nifti_image	\N	2019-04-07 10:43:17.690417	2019-04-07 10:43:17.690451	null	5972
4000	failing_macro_top_level_2019-05-13T08-29-04/root/source_a/output	output	\N	2019-05-13 06:27:40.178802	2019-05-13 06:27:40.178842	{"datatype": "Int", "dimension_names": ["source_a"]}	6520
4001	failing_macro_top_level_2019-05-13T08-29-04/root/source_b/output	output	\N	2019-05-13 06:27:40.178802	2019-05-13 06:27:40.178842	{"datatype": "Int", "dimension_names": ["source_b"]}	6521
4002	failing_macro_top_level_2019-05-13T08-29-04/root/source_c/output	output	\N	2019-05-13 06:27:40.178802	2019-05-13 06:27:40.178842	{"datatype": "Int", "dimension_names": ["source_c"]}	6522
4003	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/source_1/output	output	\N	2019-05-13 06:27:40.178802	2019-05-13 06:27:40.178842	{"datatype": "Int", "dimension_names": ["source_1"]}	6527
4004	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/source_2/output	output	\N	2019-05-13 06:27:40.178802	2019-05-13 06:27:40.178842	{"datatype": "Int", "dimension_names": ["source_2"]}	6528
4005	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/source_3/output	output	\N	2019-05-13 06:27:40.178802	2019-05-13 06:27:40.178842	{"datatype": "Int", "dimension_names": ["source_3"]}	6529
4006	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sink_1/output	output	\N	2019-05-13 06:27:40.178802	2019-05-13 06:27:40.178842	{"datatype": "Int", "dimension_names": []}	6530
4007	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sink_2/output	output	\N	2019-05-13 06:27:40.178802	2019-05-13 06:27:40.178842	{"datatype": "Int", "dimension_names": []}	6531
4008	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sink_3/output	output	\N	2019-05-13 06:27:40.178802	2019-05-13 06:27:40.178842	{"datatype": "Int", "dimension_names": []}	6532
4009	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sink_4/output	output	\N	2019-05-13 06:27:40.178802	2019-05-13 06:27:40.178842	{"datatype": "Int", "dimension_names": []}	6533
4010	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sink_5/output	output	\N	2019-05-13 06:27:40.178802	2019-05-13 06:27:40.178842	{"datatype": "Int", "dimension_names": []}	6534
4011	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/step_1/out_1	out_1	\N	2019-05-13 06:27:40.178802	2019-05-13 06:27:40.178842	{"datatype": "Int", "dimension_names": ["source_1"]}	6535
4012	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/step_1/out_2	out_2	\N	2019-05-13 06:27:40.178802	2019-05-13 06:27:40.178842	{"datatype": "Int", "dimension_names": ["source_1"]}	6535
4013	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/step_2/out_1	out_1	\N	2019-05-13 06:27:40.178802	2019-05-13 06:27:40.178842	{"datatype": "Int", "dimension_names": ["source_3"]}	6536
4014	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/step_2/out_2	out_2	\N	2019-05-13 06:27:40.178802	2019-05-13 06:27:40.178842	{"datatype": "Int", "dimension_names": ["source_3"]}	6536
4015	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/step_3/out_1	out_1	\N	2019-05-13 06:27:40.178802	2019-05-13 06:27:40.178842	{"datatype": "Int", "dimension_names": ["source_1"]}	6537
4016	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/step_3/out_2	out_2	\N	2019-05-13 06:27:40.178802	2019-05-13 06:27:40.178842	{"datatype": "Int", "dimension_names": ["source_1"]}	6537
4017	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/range/result	result	\N	2019-05-13 06:27:40.178802	2019-05-13 06:27:40.178842	{"datatype": "Int", "dimension_names": ["source_1"]}	6538
4018	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/sum/result	result	\N	2019-05-13 06:27:40.178802	2019-05-13 06:27:40.178842	{"datatype": "Int", "dimension_names": ["source_1"]}	6539
4019	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/const_step_1_fail_2_0/output	output	\N	2019-05-13 06:27:40.178802	2019-05-13 06:27:40.178842	{"datatype": "Boolean", "dimension_names": ["const_step_1_fail_2_0"]}	6540
4020	failing_macro_top_level_2019-05-13T08-29-04/root/failing_macro/const_step_2_fail_1_0/output	output	\N	2019-05-13 06:27:40.178802	2019-05-13 06:27:40.178842	{"datatype": "Boolean", "dimension_names": ["const_step_2_fail_1_0"]}	6541
4021	failing_macro_top_level_2019-05-13T08-29-04/root/add/result	result	\N	2019-05-13 06:27:40.178802	2019-05-13 06:27:40.178842	{"datatype": "Int", "dimension_names": ["source_a"]}	6524
4022	failing_macro_top_level_2019-05-13T08-29-04/root/sink/output	output	\N	2019-05-13 06:27:40.178802	2019-05-13 06:27:40.178842	{"datatype": "Int", "dimension_names": []}	6525
4023	failing_macro_top_level_2019-05-13T08-29-04/root/const_add_right_hand_0/output	output	\N	2019-05-13 06:27:40.178802	2019-05-13 06:27:40.178842	{"datatype": "Int", "dimension_names": ["const_add_right_hand_0"]}	6526
\.


--
-- Data for Name: runs; Type: TABLE DATA; Schema: public; Owner: pim
--

COPY public.runs (id, name, title, description, created, modified, custom_data, "user") FROM stdin;
624	tissue_pipeline_2019-01-31T21-41-53	tissue_pipeline_2019-01-31T21-41-53	Run of tissue_pipeline_2019-01-31T21-41-53 started at 2019-01-31 21:41:53.444197	2019-04-03 10:15:13.266687	2019-04-03 10:16:01.588243	{"workflow_engine": "fastr", "tmpdir": "/exports/lkeb-hpc/tkroes/BBMRI/temp/staging/fastr-bbmri-iris/"}	tkroes
692	failing_macro_top_level_2019-05-13T08-29-04	failing_macro_top_level_2019-05-13T08-29-04	Run of failing_macro_top_level_2019-05-13T08-29-04 started at 2019-05-13 08:29:04.715846	2019-05-13 06:29:04.844923	2019-05-13 06:29:24.977399	{"workflow_engine": "fastr", "tmpdir": "/tmp/fastr_failing_macro_top_level_2019-05-13T08-29-04_jnp24rgq"}	thomas
663	test	Test run	Run for testing	2019-04-07 11:15:20.047167	2019-04-07 11:15:20.047167	{}	simulator
687	run_0_jobs	Test run	Run for testing	2019-05-01 06:38:47.825356	2019-05-01 06:38:47.825356	{}	unit_test_2
\.


--
-- Name: aggregates_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pim
--

SELECT pg_catalog.setval('public.aggregates_id_seq', 1, false);


--
-- Name: in_ports_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pim
--

SELECT pg_catalog.setval('public.in_ports_id_seq', 4648, true);


--
-- Name: jobs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pim
--

SELECT pg_catalog.setval('public.jobs_id_seq', 620884, true);


--
-- Name: link_node_refs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pim
--

SELECT pg_catalog.setval('public.link_node_refs_id_seq', 1, false);


--
-- Name: links_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pim
--

SELECT pg_catalog.setval('public.links_id_seq', 1505, true);


--
-- Name: nodes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pim
--

SELECT pg_catalog.setval('public.nodes_id_seq', 6541, true);


--
-- Name: out_ports_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pim
--

SELECT pg_catalog.setval('public.out_ports_id_seq', 4023, true);


--
-- Name: runs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pim
--

SELECT pg_catalog.setval('public.runs_id_seq', 692, true);


--
-- Name: aggregates aggregates_pkey; Type: CONSTRAINT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.aggregates
    ADD CONSTRAINT aggregates_pkey PRIMARY KEY (id);


--
-- Name: in_ports in_ports_path_key; Type: CONSTRAINT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.in_ports
    ADD CONSTRAINT in_ports_path_key UNIQUE (path);


--
-- Name: in_ports in_ports_pkey; Type: CONSTRAINT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.in_ports
    ADD CONSTRAINT in_ports_pkey PRIMARY KEY (id);


--
-- Name: jobs jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.jobs
    ADD CONSTRAINT jobs_pkey PRIMARY KEY (id);


--
-- Name: link_node_refs link_node_refs_pkey; Type: CONSTRAINT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.link_node_refs
    ADD CONSTRAINT link_node_refs_pkey PRIMARY KEY (id);


--
-- Name: links links_pkey; Type: CONSTRAINT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.links
    ADD CONSTRAINT links_pkey PRIMARY KEY (id);


--
-- Name: nodes nodes_pkey; Type: CONSTRAINT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.nodes
    ADD CONSTRAINT nodes_pkey PRIMARY KEY (id);


--
-- Name: out_ports out_ports_path_key; Type: CONSTRAINT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.out_ports
    ADD CONSTRAINT out_ports_path_key UNIQUE (path);


--
-- Name: out_ports out_ports_pkey; Type: CONSTRAINT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.out_ports
    ADD CONSTRAINT out_ports_pkey PRIMARY KEY (id);


--
-- Name: runs runs_name_key; Type: CONSTRAINT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.runs
    ADD CONSTRAINT runs_name_key UNIQUE (name);


--
-- Name: runs runs_pkey; Type: CONSTRAINT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.runs
    ADD CONSTRAINT runs_pkey PRIMARY KEY (id);


--
-- Name: ix_aggregates_node_id; Type: INDEX; Schema: public; Owner: pim
--

CREATE INDEX ix_aggregates_node_id ON public.aggregates USING btree (node_id);


--
-- Name: ix_jobs_created; Type: INDEX; Schema: public; Owner: pim
--

CREATE INDEX ix_jobs_created ON public.jobs USING btree (created);


--
-- Name: ix_jobs_id; Type: INDEX; Schema: public; Owner: pim
--

CREATE INDEX ix_jobs_id ON public.jobs USING btree (id);


--
-- Name: ix_jobs_modified; Type: INDEX; Schema: public; Owner: pim
--

CREATE INDEX ix_jobs_modified ON public.jobs USING btree (modified);


--
-- Name: ix_jobs_node_id; Type: INDEX; Schema: public; Owner: pim
--

CREATE INDEX ix_jobs_node_id ON public.jobs USING btree (node_id);


--
-- Name: ix_jobs_node_path; Type: INDEX; Schema: public; Owner: pim
--

CREATE INDEX ix_jobs_node_path ON public.jobs USING btree (node_path);


--
-- Name: ix_jobs_path; Type: INDEX; Schema: public; Owner: pim
--

CREATE UNIQUE INDEX ix_jobs_path ON public.jobs USING btree (path);


--
-- Name: ix_jobs_run_id; Type: INDEX; Schema: public; Owner: pim
--

CREATE INDEX ix_jobs_run_id ON public.jobs USING btree (run_id);


--
-- Name: ix_jobs_status; Type: INDEX; Schema: public; Owner: pim
--

CREATE INDEX ix_jobs_status ON public.jobs USING btree (status);


--
-- Name: ix_link_node_refs_node_id; Type: INDEX; Schema: public; Owner: pim
--

CREATE INDEX ix_link_node_refs_node_id ON public.link_node_refs USING btree (node_id);


--
-- Name: ix_link_node_refs_node_path; Type: INDEX; Schema: public; Owner: pim
--

CREATE INDEX ix_link_node_refs_node_path ON public.link_node_refs USING btree (node_path);


--
-- Name: ix_nodes_created; Type: INDEX; Schema: public; Owner: pim
--

CREATE INDEX ix_nodes_created ON public.nodes USING btree (created);


--
-- Name: ix_nodes_id; Type: INDEX; Schema: public; Owner: pim
--

CREATE INDEX ix_nodes_id ON public.nodes USING btree (id);


--
-- Name: ix_nodes_modified; Type: INDEX; Schema: public; Owner: pim
--

CREATE INDEX ix_nodes_modified ON public.nodes USING btree (modified);


--
-- Name: ix_nodes_parent_id; Type: INDEX; Schema: public; Owner: pim
--

CREATE INDEX ix_nodes_parent_id ON public.nodes USING btree (parent_id);


--
-- Name: ix_nodes_path; Type: INDEX; Schema: public; Owner: pim
--

CREATE UNIQUE INDEX ix_nodes_path ON public.nodes USING btree (path);


--
-- Name: ix_nodes_run_id; Type: INDEX; Schema: public; Owner: pim
--

CREATE INDEX ix_nodes_run_id ON public.nodes USING btree (run_id);


--
-- Name: ix_nodes_type; Type: INDEX; Schema: public; Owner: pim
--

CREATE INDEX ix_nodes_type ON public.nodes USING btree (type);


--
-- Name: ix_runs_created; Type: INDEX; Schema: public; Owner: pim
--

CREATE INDEX ix_runs_created ON public.runs USING btree (created);


--
-- Name: ix_runs_modified; Type: INDEX; Schema: public; Owner: pim
--

CREATE INDEX ix_runs_modified ON public.runs USING btree (modified);


--
-- Name: aggregates aggregates_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.aggregates
    ADD CONSTRAINT aggregates_node_id_fkey FOREIGN KEY (node_id) REFERENCES public.nodes(id) ON DELETE CASCADE;


--
-- Name: in_ports in_ports_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.in_ports
    ADD CONSTRAINT in_ports_node_id_fkey FOREIGN KEY (node_id) REFERENCES public.nodes(id) ON DELETE CASCADE;


--
-- Name: jobs jobs_node_path_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.jobs
    ADD CONSTRAINT jobs_node_path_fkey FOREIGN KEY (node_path) REFERENCES public.nodes(path);


--
-- Name: jobs jobs_run_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.jobs
    ADD CONSTRAINT jobs_run_id_fkey FOREIGN KEY (run_id) REFERENCES public.runs(id) ON DELETE CASCADE;


--
-- Name: links links_common_ancestor_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.links
    ADD CONSTRAINT links_common_ancestor_fkey FOREIGN KEY (common_ancestor) REFERENCES public.nodes(path);


--
-- Name: links links_from_port_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.links
    ADD CONSTRAINT links_from_port_id_fkey FOREIGN KEY (from_port_id) REFERENCES public.out_ports(path);


--
-- Name: links links_run_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.links
    ADD CONSTRAINT links_run_id_fkey FOREIGN KEY (run_id) REFERENCES public.runs(id) ON DELETE CASCADE;


--
-- Name: links links_to_port_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.links
    ADD CONSTRAINT links_to_port_id_fkey FOREIGN KEY (to_port_id) REFERENCES public.in_ports(path);


--
-- Name: nodes nodes_parent_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.nodes
    ADD CONSTRAINT nodes_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES public.nodes(id);


--
-- Name: nodes nodes_run_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.nodes
    ADD CONSTRAINT nodes_run_id_fkey FOREIGN KEY (run_id) REFERENCES public.runs(id) ON DELETE CASCADE;


--
-- Name: out_ports out_ports_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.out_ports
    ADD CONSTRAINT out_ports_node_id_fkey FOREIGN KEY (node_id) REFERENCES public.nodes(id) ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

