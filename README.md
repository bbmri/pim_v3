# What is PIM?
PIM is a Pipeline Inspection and Monitoring webservice.
The novelty of PIM is that it provides an interactive, visualization-based web service to intuitively explore all information (e.g. structure, status and job details) on running processing pipelines.
PIM makes complex, long running processing pipelines (e.g. image or -omics) more insightful, irrespective of the level of technical expertise.
The interactivity that PIM provides through well-known information visualization paradigms (e.g. focus and context, coupled views and details on demand), allows for a variable level of detail, thus making it suitable for use by both pipeline developers and clinical researchers.

PIM is designed to be workflow engine agnostic; any engine can connect through a common REST API.

![alt text](screenshot.png)

# Requirements
This repository contains all the source files to reproduce the PIM webservice.
To make it easier to test, develop and deploy PIM, we encapsulated all functionality in Docker Compose environments. Thus, it becomes very easy to test and deploy PIM, as merely a dependency on Docker and Docker Compose is required: 
* [Docker](http://www.docker.com) (developed with version 18.09.1, build 4c52b90)
* [Docker Compose](https://docs.docker.com/compose/) Docker Compose (developed with version 1.23.1, build b02f1306)

At this point, PIM has been developed and tested on Linux ([Ubuntu 18.04](http://releases.ubuntu.com/18.04/)). PIM has not been tested on Windows and Mac.

# Organization
This repository is split up in the following way:
```.
├── app             # Frontend implementation
├── develop         # Development environment
├── plugins         # Example plugins for workflow engines
└── server          # Backend implementation (Python and Flask)
```

### App
The [/app](app) directory contains all the source files for the frontend implementation.
The frontend is a Single Page Application ([SPA](https://en.wikipedia.org/wiki/Single-page_application)) written in Javascript and [ReactJS](https://reactjs.org/).
For more information on the frontend check [this](app/README.md) guide.

### Develop
The [/develop](develop) directory contains a fully functional, self-contained Docker Compose environment that is used for development and testing.
Please consult [this](develop/README.md) guide for more information. 

### Plugins
The [/plugins](plugins) directory provides a [guide](plugins/howto/README.md) on how to connect a workflow engine to PIM.
This directory also contains an [example](/plugins/pimreporter.py) of a workflow engine plugin which interfaces with PIM.
It is a plugin for workflow engine [Fastr](https://www.frontiersin.org/article/10.3389/fict.2016.00015).

### Server
The [/server](server) directory contains the implementation for the PIM backend.
Please follow [these](server/docker/README.md) deployment instructions if you plan to host PIM yourself.

# Licensing
PIM has been made publicly available for non-commercial use under the [GPL3](https://www.gnu.org/licenses/gpl-3.0.nl.html) license.