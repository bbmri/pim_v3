/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React from 'react'
import {BrowserRouter} from 'react-router-dom'
import {withCookies} from 'react-cookie'
import {ThemeProvider} from 'styled-components'

// import 'semantic-ui-css'

import Router from './Router'

import Navigation from './Navigation'

import AppApi from 'api/api'

import Runs from './pages/runs/Runs'
import Run from './pages/run/Run'
import Jobs from './pages/jobs/Jobs'
import Job from './pages/job/Job'

class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            apiInfo: {
                lastUpdated: '',
                currentRevision: ''
            }
        };
    }

    componentDidMount() {
        AppApi.fetchApiInfo()
            .then((response) => {
                const apiInfo = {
                    apiInfo: {
                        lastUpdated: response.data.deployment.lastUpdated,
                        currentRevision: response.data.deployment.currentRevision
                    }
                };

                this.setState(apiInfo);
            });
    }

    render() {
        return <ThemeProvider theme={theme}>
            <BrowserRouter>
                <Navigation>
                    <Runs>
                        <Run>
                            <Jobs>
                                <Job>
                                    <Router {...this.props} {...this.state}/>
                                </Job>
                            </Jobs>
                        </Run>
                    </Runs>
                </Navigation>
            </BrowserRouter>
        </ThemeProvider>
    }
}

const colors = {
    focus: 'hsl(40, 85%, 55%)',
};

const theme = {
    groupRadius: 15,
    progress: {
        idle: 'hsl(210, 60%, 40%)',
        running: 'hsl(42, 58%, 45%)',
        success: 'hsl(120, 50%, 35%)',
        failed: 'hsl(8, 60%, 41%)',
        cancelled: 'hsl(278, 45%, 42%)',
        undefined: 'hsl(186, 45%, 50%)',
        uninitialized: 'hsl(0, 0%, 70%)',
        completed: 'hsl(0, 0%, 30%)',
        all: 'hsl(0, 0%, 60%)',
    },
    node: {
        focus: {
            color: colors.focus,
        },
        transition: {
            color: 'hsl(0, 0%, 75%)'
        },
        group: {
            padding: 50,
            background: {
                color: 'hsla(0, 0%, 0%, 0%)'
            },
            border: {
                radius: 30,
                width: 2
            },
            glow: {
                size: 40,
                opacity: 0.8
            }
        },
        leaf: {
            padding: 35,
            background: {
                color: 'hsl(0, 0%, 95%)'
            },
            border: {
                radius: 25,
                width: 2
            },
            glow: {
                size: 25,
                opacity: 0.8
            },
            hub: {
                anchor: {
                    size: 30
                }
            }
        }
    },
    link: {
        path: {
            inner: {
                width: 8,
                opacity: 1,
                color: 'hsl(0, 0%, 50%)',
            },
            outer: {
                width: 20,
                opacity: 1,
                color: 'hsl(0, 0%, 35%)',
            },
            border: {
                width: 40,
                opacity: 0.1,
                color: 'hsl(0, 0%, 20%)',
            },
            focus: {
                width: 45,
                opacity: 0.7,
                color: colors.focus,
            },
            hit: {
                width: 45,
                opacity: 0,
                color: 'hsl(0, 0%, 0%)',
            }
        }
    }

};

export default withCookies(App);