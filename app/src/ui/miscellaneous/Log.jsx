/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React, {Component} from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import HelpContext from 'ui/miscellaneous/HelpContext'
import VirtualizedList from 'ui/miscellaneous/VirtualizedList'

export default class Log extends Component {
    constructor(props) {
        super(props);

        this.items = React.createRef();

        this.state = {
            wrap: false,
            useRegularExpression: true,
            levelFilter: {
                'INFO': true,
                'WARNING': true,
                'ERROR': true
            },
            scroll: {
                atTop: false,
                atBottom: false
            }
        };
    }

    onWrapChange = (e, {checked}) => {
        this.setState({
            wrap: checked
        })
    };

    toggleUseRegularExpression() {
        this.setState({
            useRegularExpression: !this.state.useRegularExpression
        })
    }

    levelFilterToggle(level) {
        let levelFilter = this.state.levelFilter;

        levelFilter[level] = (!levelFilter[level]);

        this.setState({
            levelFilter: levelFilter
        })
    }

    toTop = () => {
        this.items.current.scrollToTop();
    };

    toBottom = () => {
        this.items.current.scrollToBottom();
    };

    onScrolled = (position, height) => {
        this.setState({
            scroll: {
                atTop: position === 0,
                atBottom: position === height,
            }
        });
    };

    renderItem = ({index, item, style}) => {
        const filter = this.props.filter;

        const segments = item.message.split(filter).join('#' + filter + '#').split('#', 100);
        const message = filter === '' ? item.message : segments.map((str, key) => (
            str === filter ? (<FilterMatch key={key}>{str}</FilterMatch>) : str
        ));

        const classes = {
            lineNumber: `cell line-number`,
            timestamp: `cell timestamp ${item.groupIndex > 0 ? 'hidden' : ''}`,
            level: `cell level ${item.groupIndex > 0 ? 'hidden' : ''}`,
            message: `cell message`
        };

        let properties = [];

        const ignoreAttributes = ['lineNumber', 'timestamp', 'message', 'groupIndex'];

        for (let attr in item) {
            if (item[attr] === undefined)
                continue;

            if (!ignoreAttributes.includes(attr))
                properties.push(`${attr}: ${item[attr]}`);
        }

        const divs = {
            lineNumber: <LineNumberCell>{item.lineNumber}</LineNumberCell>,
            timestamp: <TimestampCell className={classes.timestamp}>{item.timestamp}</TimestampCell>,
            level: <LevelCell className={classes.level}>[{item.level}]</LevelCell>,
            message: <MessageCell className={classes.message}>{message}</MessageCell>
        };

        return <HelpContext key={index} status={properties.join(', ')}>
            <LogItem style={{...style, height: 20}}>
                {item.lineNumber !== undefined ? divs.lineNumber : null}
                {item.timestamp !== undefined ? divs.timestamp : null}
                {item.level !== undefined ? divs.level : null}
                {item.message !== undefined ? divs.message : null}
            </LogItem>
        </HelpContext>
    };

    render() {
        const filterActive = this.props.filter !== '';
        const regExp       = new RegExp(this.props.filter);

        const items = this.props.items.reduce((items, logItem) => {
            if (Array.isArray(logItem.message)) {
                for (let message of logItem.message) {
                    if (!filterActive || regExp.test(message)) {
                        items.push({
                            lineNumber: items.length,
                            timestamp: logItem.timestamp,
                            level: logItem.level_name,
                            message: message,
                            module: logItem.module,
                            process: logItem.process_name,
                            thread: logItem.thread_name,
                            function: logItem.function,
                            groupIndex: logItem.message.indexOf(message)
                        });
                    }
                }
            } else {
                if (!filterActive || regExp.test(logItem.message)) {
                    items.push({
                        lineNumber: items.length,
                        timestamp: logItem.timestamp,
                        level: logItem.level_name,
                        message: logItem.message,
                        module: logItem.module,
                        process: logItem.process_name,
                        thread: logItem.thread_name,
                        function: logItem.function,
                        groupIndex: -1
                    });
                }
            }

            return items;
        }, []);

        return <LogItems style={{flex: 1}}>
            <VirtualizedList ref={this.items}
                             source={items}
                             overScanCount={1}
                             rowHeight={20}
                             renderItem={this.renderItem.bind(this)}
                             scrollToBottom={this.props.scrollToBottom}
                             onScrolled={this.onScrolled}/>
        </LogItems>
    }
}

Log.defaultProps = {
    filter: ''
};

Log.propTypes = {
    items: PropTypes.array.isRequired,
    // updating: PropTypes.bool.isRequired,
    // lastUpdated: PropTypes.string.isRequired,
    scrollToBottom: PropTypes.bool.isRequired,
    filter: PropTypes.string
};

const LogItems = styled.div`
    background-color: hsl(0, 0%, 10%);
    font-family: "Ubuntu Mono";
    font-size: 10pt;
`;

const LogItem = styled.div`
    color: hsla(0, 0%, 100%, 0.8);
    cursor: default;
    display: flex;
    align-items: center;
    :hover {
      background-color: hsla(0, 0%, 100%, 0.05);
    }
`;

const Cell = styled.div`
    padding: 0 5px 0 5px;
    height: 20px;
    white-space: nowrap;
`;

const LineNumberCell = styled(Cell)`
    min-width: 50px;
    text-align: right;
    color: hsla(0, 0%, 100%, 0.4);
    border-right: 1px solid hsla(0, 0%, 100%, 0.1);
    background-color: hsla(0, 0%, 100%, 0.05);
`;

const TimestampCell = styled(Cell)`
    color: hsla(0, 0%, 100%, 0.7);
`;

const LevelCell = styled(Cell)`
`;

const MessageCell = styled(Cell)`
    color: hsla(0, 0%, 100%, 0.7);
`;

const FilterMatch = styled.span`
    color: hsla(0, 0%, 0%, 0.8);
    background-color: hsla(0, 0%, 100%, 0.6);
`;