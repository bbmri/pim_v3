/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React from 'react'
import PropTypes from 'prop-types'

import styled from 'styled-components'

import PubSub from 'pubsub-js'

const getUuid = require('uuid-by-string');

import {
    Icon
} from 'semantic-ui-react'

import Node from './Node'

export default class JsonTree extends React.Component {
    _flattenJson(data) {
        let toReturn = {};

        for (let i in data) {
            if (!data.hasOwnProperty(i)) continue;

            if ((typeof data[i]) == 'object') {
                let flatObject = this._flattenJson(data[i]);
                for (let x in flatObject) {
                    if (!flatObject.hasOwnProperty(x)) continue;

                    toReturn[i + '.' + x] = flatObject[x];
                }
            } else {
                toReturn[i] = data[i];
            }
        }
        return toReturn;
    }

    _filterNodes = () => {
        const flatJson        = this._flattenJson(this.props.json);
        const upperCaseFilter = this.state.filter.toUpperCase();

        const filteredNodes = Object.keys(flatJson)
            .reduce((accum, key) => {
                let matches = {
                    key: key.toUpperCase().includes(upperCaseFilter),
                    value: typeof flatJson[key] === 'string' && flatJson[key].toUpperCase().includes(upperCaseFilter)
                };

                if (matches.key || matches.value) {
                    accum.push({
                        key: key,
                        value: flatJson[key]
                    });
                }

                return accum;
            }, []);

        return filteredNodes;
    };

    _id = (node) => {
        return node.title;
    };

    _children = (parent, node) => {
        if (node.data === null)
            return [];

        const keys      = Object.keys(node.data);
        const batchSize = 100;

        if (typeof node.data === 'object' && keys.length > 0) {
            if (keys.length > batchSize) {
                let batches = [];

                for (let i = 0; i < keys.length; i += batchSize) {
                    const batch = keys.slice(i, i + batchSize).reduce((accum, key) => {
                        accum[key] = node.data[key];
                        return accum;
                    }, {});

                    batches.push({
                        title: `${i}-${i + Object.keys(batch).length}`,
                        get path() {
                            return `${parent.path}/${this.title}`
                        },
                        get id() {
                            return getUuid(this.path);
                        },
                        data: batch,
                        expanded: true
                    });
                }

                return batches;
            } else {
                return Object.keys(node.data).map(key => {
                    return {
                        title: key,
                        get path() {
                            return `${parent.path}/${this.title}`
                        },
                        get id() {
                            return getUuid(this.path);
                        },
                        data: node.data[key],
                        expanded: true
                    };
                });
            }
        }

        return [];
    };

    _icon = (node, level, expanded, noChildren) => {
        let icon = '';

        if (noChildren > 0) {
            if (typeof node.data === 'object') {
                icon = expanded ? 'caret down' : 'caret right';
            }
        }

        return <Icon name={icon}/>;
    };

    _title = (node, level, expanded) => {
        let objectValue = '';

        if (typeof node.data === 'object')
            objectValue = '{}';

        if (Array.isArray(node.data))
            objectValue = '[]';

        const value = objectValue !== '' ? expanded ? '' : objectValue : node.data;

        return <div style={{cursor: 'default'}}>{`${node.title}: ${value}`}</div>;
    };

    render() {
        if (Object.keys(this.props.json).length === 0) {
            return <div/>;
        }

        const props = {
            ...this.props,
            onClick: this.props.onClick,
            onMouseEnter: this.props.onMouseEnter,
            onMouseLeave: this.props.onMouseLeave,
            expanded: this.props.expanded,
            id: this._id,
            children: this._children,
            icon: this._icon,
            title: this._title,
        };

        const json = {
            id: getUuid(this.props.title),
            path: this.props.title,
            title: this.props.title,
            data: this.props.json,
            expanded: true
        };

        return <StyledJsonTree>
            <StyledContent>
                <Node node={json} level={0} tree={true} {...props}/>
            </StyledContent>
        </StyledJsonTree>;
    }
}

JsonTree.propTypes = {
    json: PropTypes.object.isRequired,
    title: PropTypes.string.isRequired,
    onClick: PropTypes.func,
    onMouseEnter: PropTypes.func,
    onMouseLeave: PropTypes.func
};

const StyledJsonTree = styled.div`
`;

const StyledContent = styled.div`
    white-space: nowrap;
    overflow: auto;
`;