/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React from 'react'

import {
    Message,
    Icon
} from 'semantic-ui-react'

const FetchErrorMessage = ({subject, error}) => {
    try {
        const statusText = error.getIn(['response', 'statusText']);
        const message    = <span>of a <b style={{textTransform: 'lowercase'}}>{statusText}</b> error</span>;

        return <Message icon error>
            <Icon name='exclamation circle'/>
            <Message.Content>
                <Message.Header>{error.response.status} error</Message.Header>
                <p>{subject} cannot be displayed because {message}.</p>
            </Message.Content>
        </Message>
    } catch (e) {
        return <Message icon error>
            <Icon name='exclamation circle'/>
            <Message.Content>
                <Message.Header>{error.constructor.name}</Message.Header>
                <p>{subject} cannot be displayed: <b>{error.message}</b>.</p>
            </Message.Content>
        </Message>
    }

};

export default FetchErrorMessage;