/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React, {Component} from 'react'
import PropTypes from 'prop-types';

import {
    Table,
    Icon
} from 'semantic-ui-react'


export default class Footer extends Component {
    shouldComponentUpdate(nextProps, nextState) {
        const attributes = ['name', 'firstRow', 'lastRow', 'lastUpdated', 'updating'];

        for (let attribute of attributes) {
            if (this.props[attribute] !== nextProps[attribute]) {
                return true;
            }
        }

        return false;
    }

    render() {
        const lastUpdated = this.props.lastUpdated;
        const icon        = <Icon name='download' style={{color: `hsl(0, 0%, ${this.props.updating ? 60 : 80}%)`}}/>;
        const name        = <span style={{textTransform: 'capitalize'}}>{this.props.name}</span>;
        const range       = `${this.props.firstRow} - ${this.props.lastRow}`;
        const rowInfo     = <div>{name} {range} of {this.props.noRows} {this.props.name}s</div>;

        let updated = null;

        if (lastUpdated !== '') {
            const date = new Date(lastUpdated);

            const localDateString = date.toLocaleDateString('nl');
            const localTimeString = date.toLocaleTimeString('nl');

            updated =
                <div style={{color: 'grey'}}>{icon}&nbsp;Last updated on {localDateString} at {localTimeString}</div>
        }

        const row = <div style={{display: 'flex', alignItems: 'center'}}>

            <div style={{color: 'grey'}}>{rowInfo}</div>
            <div style={{flex: 1}}/>
            {updated}
        </div>;

        return (
            <Table.Footer>
                <Table.Row>
                    <Table.HeaderCell>
                        <div style={{display: 'flex', alignItems: 'center', justifyContent: 'center'}}>
                        <Icon name={'trash'} link fitted color='grey'/>
                            </div>
                    </Table.HeaderCell>
                    <Table.HeaderCell colSpan={this.props.colSpan}>{row}</Table.HeaderCell>
                </Table.Row>
            </Table.Footer>
        )
    }
}

Footer.propTypes = {
    name: PropTypes.string.isRequired,
    firstRow: PropTypes.number.isRequired,
    lastRow: PropTypes.number.isRequired,
    noRows: PropTypes.number.isRequired,
    lastUpdated: PropTypes.string.isRequired,
    updating: PropTypes.bool.isRequired,
    colSpan: PropTypes.number.isRequired,
};
