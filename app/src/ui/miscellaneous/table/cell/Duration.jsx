/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React, {Component} from 'react'
import PropTypes from 'prop-types'

import {
    Popup,
    Table
} from 'semantic-ui-react'

import HelpContext from 'ui/miscellaneous/HelpContext'

import DateTimeHelper from 'helpers/dateTime'

export default class Duration extends Component {
    status = () => {
        const subject = <span key='subject' style={{textTransform: 'capitalize'}}>{this.props.subject}</span>;

        const durationString = DateTimeHelper.millisecondsToIntervalDescription(this.props.duration, false);

        return <span>{subject} took {durationString}</span>;
    };

    render() {
        const durationString = DateTimeHelper.millisecondsToIntervalDescription(this.props.duration);

        return <HelpContext status={this.status.bind(this)} popup={this.status()}>
            <Table.Cell textAlign='right' style={{cursor: 'default'}}>
                <div className={this.props.transient ? 'semi-transparent' : ''}>{durationString}</div>
            </Table.Cell>
        </HelpContext>;
    }
}

Duration.defaultProps = {
    transient: false
};

Duration.propTypes = {
    duration: PropTypes.number.isRequired,
    subject: PropTypes.string.isRequired,
    transient: PropTypes.bool
};