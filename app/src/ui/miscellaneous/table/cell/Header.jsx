/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React, {Component} from 'react'
import PropTypes from 'prop-types'

import {
    Popup,
    Table
} from 'semantic-ui-react'

import HelpContext from 'ui/miscellaneous/HelpContext'

export default class Header extends Component {
    status() {
        const table = this.props.table;

        if (table.get('sortColumn') === '')
            return null;

        const sorted     = table.get('sortColumn') === this.props.sortColumn;
        const directions = ['ascending', 'descending'];
        const sortable   = this.props.sortColumn !== '';

        let sortDirection = directions[0];

        if (sorted) {
            sortDirection = table.get('sortDirection') === directions[0] ? directions[1] : directions[0];
        }

        const title = <span key='title' style={{textTransform: 'capitalize'}}><b>{this.props.title}</b></span>;
        const sort  = <span>, click to sort in <b>{sortDirection}</b> order</span>;

        return <div>{title}{sortable ? sort : null}</div>;
    }

    click() {
        if (this.props.sortColumn === '')
            return;

        this.props.actions.toggleSortColumn(this.props.sortColumn);
    }

    render() {
        const table      = this.props.table;
        const sortable   = table.get('sortColumn') !== '';
        const sortActive = sortable && (table.get('sortColumn') === this.props.sortColumn);

        const trigger = <Table.HeaderCell className={this.props.className}
                                          style={this.props.style}
                                          width={this.props.width}
                                          textAlign={this.props.textAlign}
                                          sorted={sortActive ? table.get('sortDirection') : null}
                                          onClick={this.click.bind(this)}>
            {this.props.title}
        </Table.HeaderCell>


        return <HelpContext status={this.status.bind(this)}>
            <Popup trigger={trigger}
                   content={this.status()}
                   position='top center'
                   size='mini'
                   inverted/>
        </HelpContext>
    }
}

Header.defaultProps = {
    textAlign: 'left',
    width: 1
};

Header.propTypes = {
    sortColumn: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    width: PropTypes.number
};