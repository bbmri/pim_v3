/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React, {Component} from 'react'
import PropTypes from 'prop-types'

import {
    Popup,
    Table
} from 'semantic-ui-react'

import HelpContext from 'ui/miscellaneous/HelpContext'

import DateTimeHelper from 'helpers/dateTime'

export default class DateTime extends Component {
    status = () => {
        const localDateString = DateTimeHelper.utcDateTimeStringToLocalDateString(this.props.dateTime, true);
        const localTimeString = DateTimeHelper.utcStringToLocalTimeString(this.props.dateTime);

        return <span>{this.props.statusPrefix} <b>{localDateString}</b> at <b>{localTimeString}</b></span>;
    };

    render() {
        const localDateTime   = DateTimeHelper.utcDateToLocalDate(new Date(this.props.dateTime.replace(/ /g, 'T')));
        const localDateString = localDateTime.toLocaleDateString('nl', {
            day: '2-digit',
            month: '2-digit',
            year: '2-digit'
        });

        const localTimeString = localDateTime.toLocaleTimeString('nl');

        const trigger = <Table.Cell textAlign='right' style={{cursor: 'default'}}>
                      <div style={{flex: 1, overflow: 'hidden', textOverflow: 'ellipsis'}}
                           className={this.props.transient ? 'semi-transparent' : ''}>
                          <span>{localDateString}</span>
                          &nbsp;
                          <span style={{fontWeight: 'bold'}}>{localTimeString}</span>
                      </div>
                  </Table.Cell>


        return <HelpContext status={this.status.bind(this)}><Popup
            trigger={trigger}
            content={this.status()}
            position='top center'
            size='mini'
            inverted/>
        </HelpContext>
    }
}

DateTime.defaultProps = {
    transient: false
};

DateTime.propTypes = {
    dateTime: PropTypes.string.isRequired,
    statusPrefix: PropTypes.string.isRequired
};