/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React, {Component} from 'react'
import PropTypes from 'prop-types'

import {
    Table
} from 'semantic-ui-react'

import HelpContext from 'ui/miscellaneous/HelpContext'

export default class String extends Component {
    constructor(props) {
        super(props);

        this.state = {
            hover: false
        }
    }

    onMouseOver() {
        this.setState({
            hover: true
        })
    }

    onMouseLeave() {
        this.setState({
            hover: false
        })
    }

    render() {
        const style = {
            cursor: 'default',
            textDecoration: this.state.hover ? 'underline' : 'none',
        };

        return <HelpContext status={() => this.props.status}>
            <Table.Cell textAlign={this.props.textAlign}
                        style={style}
                        onMouseOver={this.onMouseOver.bind(this)}
                        onMouseLeave={this.onMouseLeave.bind(this)}
                        onClick={() => this.props.onClick()}>
                <div className={`overflow-ellipsis ${this.props.transient ? 'semi-transparent' : ''}`}>{this.props.string}</div>
            </Table.Cell>
        </HelpContext>
    }
}

String.defaultProps = {
    string: '',
    status: '',
    transient: false,
    textAlign: 'left'
};

String.propTypes = {
    string: PropTypes.any.isRequired,
    status: PropTypes.oneOfType([
        PropTypes.func,
        PropTypes.string,
    ]),
    onClick: PropTypes.func.isRequired,
    transient: PropTypes.bool,
    textAlign: PropTypes.string
};