/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

export default class Node extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            hovered: false,
            focused: false,
            expanded: false
        };
    }

    children() {
        if (this.props.children === undefined)
            return [];

        return this.props.children(this, this.props.node);
    }

    noChildren() {
        return this.children().length;
    }

    hasChildren() {
        return this.noChildren() > 0;
    }

    isExpanded() {
        return this.props.expanded !== undefined ? this.props.expanded(this.props.node) : this.state.expanded;
    }

    onClick() {
        if (this.props.onClick !== undefined) {
            this.props.onClick(this.props.node);
        }

        if (this.hasChildren()) {
            this.setState({
                expanded: !this.state.expanded
            });
        }
    }

    onMouseEnter() {
        this.setState({
            hovered: true,
            focused: true
        });

        if (this.props.onMouseEnter !== undefined) {
            this.props.onMouseEnter(this.props.node, this.props.level, this.state.expanded, this.noChildren());
        }
    }

    onMouseLeave() {
        this.setState({
            hovered: false,
            focused: false
        });

        if (this.props.onMouseLeave !== undefined) {
            this.props.onMouseLeave(this.props.node, this.props.level, this.state.expanded, this.noChildren());
        }
    }

    icon() {
        if (this.props.icon !== undefined) {
            return this.props.icon(this.props.node, this.props.level);
        }

        return 'home';
    }

    render() {
        const indent = this.props.tree ? this.props.level * 12 : 0;

        let childItems = null;

        if (this.props.tree && this.hasChildren() && this.isExpanded()) {
            childItems = this.children().map(childNode => {
                return <Node key={this.props.id(childNode)}
                             {...this.props}
                             node={childNode}
                             level={this.props.level + 1}/>
            });

            childItems = <div>{childItems}</div>;
        }

        const children = <div className='children'>{childItems}</div>;

        let nodeItemClasses = ['node-item'];

        if (this.state.focused)
            nodeItemClasses.push('focused');

        let style = {};

        if (this.props.color !== undefined) {
            style.color = this.props.color(this.props.node);
        }

        const context = this.props.context !== undefined ? this.props.context(this.props.node, this.props.level) : null;

        return <StyleNodeItemGroup>
            <StyledNodeItem state={this.state}
                            onMouseEnter={this.onMouseEnter.bind(this)}
                            onMouseLeave={this.onMouseLeave.bind(this)}
                            onClick={this.onClick.bind(this)}
                            style={style}>
                <div style={{marginLeft: `${indent}px`}}/>
                {this.props.icon(this.props.node, this.props.level, this.state.expanded, this.noChildren())}
                {this.props.title(this.props.node, this.props.level, this.isExpanded())}
                {this.state.focused ? context : null}
            </StyledNodeItem>
            {children}
        </StyleNodeItemGroup>;
    }
}

Node.defaultProps = {
    node: PropTypes.object.isRequired,
    level: 0,
    tree: true
};

Node.propTypes = {
    children: PropTypes.func,
    expanded: PropTypes.func,
    node: PropTypes.object.isRequired,
    level: PropTypes.number.isRequired,
    tree: PropTypes.bool.isRequired
};

const StyleNodeItemGroup = styled.div`
    width: 100%;
    display: flex;
    flex-direction: column;
`;

const StyledNodeItem = styled.div`
    width: 100%;
    display: flex;
    flex-direction: row;
    font-size: 10pt;
    padding: 3px;
    border-radius: 4px;
    background-color: ${props => {
    if (props.state.hovered)
        return '#ecedee';

    return 'none'
}};
`;

