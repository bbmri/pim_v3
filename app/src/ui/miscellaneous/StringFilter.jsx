/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React, {Component} from 'react'
import PropTypes from 'prop-types'

import {
    Input,
    Icon,
    Button
} from 'semantic-ui-react'

import HelpContext from 'ui/miscellaneous/HelpContext'

class ClearFilter extends Button {
    render() {
        const disabled = this.props.filter === '';

        return <HelpContext status={`Clear the ${this.props.filterType} filter`}>
            <Button basic icon size={this.props.size} onClick={() => this.props.setFilter('')}>
                <Icon name={'remove'} disabled={disabled}/>
            </Button>
        </HelpContext>
    }
}

ClearFilter.propTypes = {
    filterType: PropTypes.string.isRequired,
    filter: PropTypes.string.isRequired,
    setFilter: PropTypes.func.isRequired
};

export default class StringFilter extends Component {
    constructor(props) {
        super(props);

        this.timer = null;

        this.state = {
            filter: props.filter
        };
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.filter !== prevProps.filter) {
            this.setState({
                filter: this.props.filter
            });
        }
    }

    setFilter = (filter, forceImmediateUpdate = false) => {
        this.setState({
            filter: filter
        });

        if (this.props.commitFilterInterval > 0) {
            if (this.timer !== null) {
                clearInterval(this.timer);
            }

            this.timer = setInterval(this.commitFilter.bind(this), this.props.commitFilterInterval);
        } else {
            this.props.setFilter(this.state.filter);
        }
    };

    onInputChanged(event, data) {
        this.setFilter(data.value);
    }

    commitFilter() {
        clearInterval(this.timer);

        if (this.props.setFilter !== undefined) {
            this.props.setFilter(this.state.filter);
        }
    }

    render() {
        const action = <ClearFilter filterType={this.props.name}
                                    filter={this.props.filter}
                                    setFilter={this.setFilter}
                                    size={this.props.size}/>;

        return <HelpContext status={this.props.status}>
            <Input icon={this.props.icon}
                   iconPosition='left'
                   fluid={this.props.fluid}
                   onChange={this.onInputChanged.bind(this)}
                   value={this.state.filter}
                   action={action}
                   placeholder={this.props.placeholder}
                   size={this.props.size}/>
        </HelpContext>
    }
}

StringFilter.defaultProps = {
    name: '',
    icon: '',
    filter: '',
    commitFilterInterval: 50,
    saveHistoryInterval: 1000,
    fluid: false,
    placeholder: 'Filter',
    status: '',
    size: 'small'
};

StringFilter.propTypes = {
    name: PropTypes.string.isRequired,
    icon: PropTypes.string.isRequired,
    filter: PropTypes.string.isRequired,
    setFilter: PropTypes.func.isRequired,
    commitFilterInterval: PropTypes.number.isRequired,
    saveHistoryInterval: PropTypes.number.isRequired,
    fluid: PropTypes.bool.isRequired,
    placeholder: PropTypes.string.isRequired,
    status: PropTypes.string.isRequired,
    size: PropTypes.string.isRequired
};