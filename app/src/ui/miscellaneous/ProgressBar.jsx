/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React, {Component} from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import * as d3 from 'd3'

import {NavigationConsumer} from 'ui/Navigation'
import PubSub from "pubsub-js";

class Bar extends Component {
    constructor(props) {
        super(props);

        this.barRef = React.createRef();
    }

    componentDidMount() {
        d3.select(this.barRef.current)
            .style('width', `${this.props.bar.width}%`);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.bar.width !== prevProps.bar.width) {
            const animate = prevProps.uninitialized.count === 0 && this.props.uninitialized.count === 0;

            d3.select(this.barRef.current)
                .transition()
                .duration(animate ? prevProps.bar.width === 0 ? 0 : 1000 : 0)
                .style('width', `${this.props.bar.width}%`);
        }
    }

    render() {
        const handlers = {
            onClick: (event) => {
                event.stopPropagation();

                if (this.props.onClick !== undefined) {
                    this.props.onClick(this.props.bar);
                }
            },
            onMouseOver: (event) => {
                event.stopPropagation();

                if (this.props.onMouseOver !== undefined)
                    this.props.onMouseOver(this.props.bar);

                if (this.props.status)
                    PubSub.publish('STATUS_SET', this.props.status(this.props.bar));
            },
            onMouseLeave: (event) => {
                event.stopPropagation();

                if (this.props.onMouseLeave !== undefined)
                    this.props.onMouseLeave(this.props.bar);

                if (this.props.status)
                    PubSub.publish('STATUS_UNSET');
            }
        };

        const bar = <StyledBar bar={this.props.bar}
                               clickable={this.props.onClick !== undefined}
                               {...handlers}
                               ref={this.barRef}>
            {this.props.bar.label !== undefined ? this.props.bar.label : ''}
        </StyledBar>;

        if (this.props.popup !== undefined)
            return this.props.popup(bar, this.props.bar);

        return bar;
    }
}

Bar.propTypes = {
    bar: PropTypes.object.isRequired,
    onClick: PropTypes.func,
    onMouseOver: PropTypes.func,
    onMouseLeave: PropTypes.func
};

const StyledBar = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    width: 0%;
    height: 100%;
    background-color: ${props => props.theme.progress[props.bar.type]};
    :hover {
      filter: ${props => props.bar.type !== 'uninitialized' && props.clickable ? 'brightness(120%)' : 'none'};
    }
`;

class ProgressBar extends Component {
    constructor(props) {
        super(props);

        this.barsDiv = React.createRef();
    }

    bars = (items, barsWidth = 100, minBarWidth = 10) => {
        let bars = [];

        const noJobsTotal = Object.values(items).reduce((a, b) => a + b.count, 0);

        let deflateBars = {};
        let inflateBars = {};

        const minBarWidthPercentage = minBarWidth / barsWidth;

        for (let itemId in items) {
            const item       = items[itemId];
            const percentage = item.count / noJobsTotal;

            if (percentage > 0) {
                if (percentage > minBarWidthPercentage) {
                    deflateBars[itemId] = percentage;
                } else {
                    inflateBars[itemId] = Math.max(minBarWidthPercentage, percentage);
                }
            }
        }

        const barPercentages        = {...deflateBars, ...inflateBars};
        const deflateBarsPercentage = Object.values(deflateBars).reduce((a, b) => a + b, 0);
        const inflateBarsPercentage = Object.values(inflateBars).reduce((a, b) => a + b, 0);
        const deflateScale          = (1 - inflateBarsPercentage) / deflateBarsPercentage;

        for (let deflateBarId in deflateBars) {
            barPercentages[deflateBarId] *= deflateScale;
        }

        let currentOffset = 0;

        for (let itemId in items) {
            const width = Object.keys(barPercentages).includes(itemId) ? barPercentages[itemId] * barsWidth : 0;

            bars.push({
                ...items[itemId],
                type: itemId,
                width: width,
                count: items[itemId].count,
                offset: currentOffset,
                percentage: items[itemId].count > 0 ? items[itemId].count / noJobsTotal : 0
            });

            currentOffset += width;
        }

        return bars;
    };

    render() {
        const handlers = {
            onClick: this.props.onClick,
            onMouseOver: this.props.onMouseOver,
            onMouseLeave: this.props.onMouseLeave,
            status: this.props.status,
            popup: this.props.popup
        };

        const bars = this.bars(this.props.items).map(bar => {
            return <Bar key={bar.type} bar={bar} uninitialized={this.props.items.uninitialized} {...handlers}/>;
        });

        return <StyledProgressBar radius={this.props.radius}>
            <StyledBars ref={this.barsDiv}>
                {bars}
            </StyledBars>
            <StyledEdge radius={this.props.radius}/>
            <StyledShadow radius={this.props.radius}/>
        </StyledProgressBar>;
    }
}

ProgressBar.defaultProps = {
    runId: '',
    nodeId: 'root',
    radius: 5
};

ProgressBar.propTypes = {
    runId: PropTypes.string.isRequired,
    nodeId: PropTypes.string,
    radius: PropTypes.number,
    onClick: PropTypes.func,
    onMouseOver: PropTypes.func,
    onMouseLeave: PropTypes.func,
    status: PropTypes.func,
    popup: PropTypes.func
};

export default props => {
    return <NavigationConsumer>
        {navigation => <ProgressBar {...props} navigation={navigation}/>}
    </NavigationConsumer>
};

const StyledProgressBar = styled.div`
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    display: flex;
    flex-direction: column;
    overflow: hidden;
    border-radius: ${props => props.radius}px;
`;

const StyledBars = styled.div`
    //position: absolute;
    //top: 0;
    //left: 0;
    display: flex;
    flex: 1;
    flex-direction: row;
    width: 100%;
`;

const StyledEdge = styled.div`
    position: absolute;
    z-index: 3;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    pointer-events: none;
    border: 1px solid hsl(0, 0%, 45%);
    border-radius: ${props => props.radius}px;
`;

const StyledShadow = styled.div`
    position: absolute;
    z-index: 5;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    pointer-events: none;
    box-shadow: inset 0 0 8px hsla(0, 0%, 0%, 0.5);
    border-radius: ${props => props.radius}px;
`;