/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React from 'react'
import PropTypes from 'prop-types'

import PubSub from 'pubsub-js'

import {
    Popup
} from 'semantic-ui-react'

export default class HelpContext extends React.Component {
    constructor(props) {
        super(props);

        this.hovering = false;

        this.state = {
            isOpen: false
        }
    }

    onMouseEnter(childProps) {
        event.stopPropagation();

        this.hovering = true;

        PubSub.publish('STATUS_SET', this._status());

        if (childProps.onMouseEnter !== undefined) {
            childProps.onMouseEnter();
        }

        // this.forceUpdate();
    }

    onMouseLeave(childProps) {
        event.stopPropagation();

        this.hovering = false;

        PubSub.publish('STATUS_UNSET');

        if (childProps.onMouseLeave !== undefined) {
            childProps.onMouseLeave();
        }
    }

    _status = () => {
        if (typeof this.props.status === 'string')
            return this.props.status;

        if (typeof this.props.status === 'function')
            return this.props.status();

        return '';
    };

    render() {
        if (this.hovering) {
            // PubSub.publish('STATUS_SET', this._status());
        }

        let children = React.Children.map(this.props.children, child => {
            return React.cloneElement(child, {
                onMouseEnter: () => this.onMouseEnter(child.props),
                onMouseLeave: () => this.onMouseLeave(child.props)
            });
        });

        children = <React.Fragment>
            {children}
        </React.Fragment>;

        if (this.props.showPopup) {
            const content = this.props.popup !== undefined ? this.props.popup : this._status();

            return <Popup trigger={children}
                          content={content}
                          position='top center'
                          size='mini'
                          flowing
                          open={this.state.isOpen}
                          inverted/>;
        }

        return children;
    }
}

HelpContext.defaultProps = {
    showPopup: false
};

HelpContext.propTypes = {
    status: PropTypes.oneOfType([
        PropTypes.func,
        PropTypes.string,
    ]),
    showPopup: PropTypes.bool,
    popup: PropTypes.node
};