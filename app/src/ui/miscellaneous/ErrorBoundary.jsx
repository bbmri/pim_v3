/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React from 'react'
import PropTypes from 'prop-types'

import * as Sentry from '@sentry/browser'

import HelpContext from 'ui/miscellaneous/HelpContext'

export default class ErrorBoundary extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            hasError: false,
            error: null,
            errorInfo: null
        };
    }

    componentDidCatch(error, errorInfo) {
        Sentry.withScope(scope => {
            Object.keys(errorInfo).forEach(key => {
                scope.setExtra(key, errorInfo[key]);
            });
            Sentry.captureException(error);
        });


        this.setState({
            hasError: true,
            error: error,
            errorInfo: errorInfo
        });
    }

    render() {
        if (this.state.hasError) {
            const sentryLink = <HelpContext status={'Submit a description of the bug in our bug reporting service'}>
                <a onClick={() => Sentry.showReportDialog()} style={{cursor: 'pointer'}}>here</a>
            </HelpContext>;

            const sentrySupport = Sentry.getCurrentHub().stack[0].client !== undefined;
            const report        = sentrySupport ? <p>Click {sentryLink} to submit a feedback report</p> : '';
            const message       = <span>A critical user interface error has occurred, unable to display this component.<br/>{report}</span>;

            return this.props.content(message);
        }

        return this.props.children;
    }
}

ErrorBoundary.propTypes = {
    content: PropTypes.func.isRequired,
};