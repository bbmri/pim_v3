/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React from 'react'
import PropTypes from 'prop-types'

import {
    Button
} from 'semantic-ui-react'

import HelpContext from 'ui/miscellaneous/HelpContext'

import * as jobHelper from 'helpers/job'

class Toggle extends React.Component {
    handleClick = (type) => {
        this.props.actions.toggleStatusFilter(type);
    };

    render() {
        const active            = this.props.value.enabled;
        const statusDescription = jobHelper.statusDescription(this.props.type, 2);
        const status            = `${active ? 'Hide' : 'Show'} ${this.props.filterable}s that ${statusDescription}`;
        const count             = this.props.value.count === undefined ? null : `: ${this.props.value.count}`;

        return <HelpContext status={status}>
            <Button active={active}
                    onClick={this.handleClick.bind(this, this.props.type)}>
                <div style={{textTransform: 'capitalize'}}>{this.props.type} {count}</div>
            </Button>
        </HelpContext>
    }
}

Toggle.propTypes = {
    type: PropTypes.string.isRequired,
    value: PropTypes.object.isRequired,
    filterable: PropTypes.string.isRequired,
    actions: PropTypes.object.isRequired
};


class Isolate extends React.Component {
    handleClick = (type) => {
        this.props.actions.setSoloStatusFilter(type);
    };

    render() {
        const status = `Only list ${this.props.type} jobs`;

        return <HelpContext status={status}>
            <Button icon='filter' onClick={this.handleClick.bind(this, this.props.type)}/>
        </HelpContext>
    }
}

Isolate.propTypes = {
    type: PropTypes.string.isRequired,
    value: PropTypes.object.isRequired,
    filterable: PropTypes.string.isRequired,
    actions: PropTypes.object.isRequired
};

class Type extends React.Component {
    render() {
        return <Button.Group basic compact size='mini'>
            <Toggle {...this.props}/>
            <Isolate {...this.props}/>
        </Button.Group>
    }
}

Type.propTypes = {
    type: PropTypes.string.isRequired,
    value: PropTypes.object.isRequired,
    filterable: PropTypes.string.isRequired,
    actions: PropTypes.object.isRequired
};

export default class TypeFilter extends React.Component {
    render() {
        const items = Object.keys(this.props.filter).join('#-#').split('#').map((key, index) => {
            if (key === '-')
                return <div key={`spacer_${index}`} style={{width: '6px'}}/>;

            return <Type key={key} type={key} value={this.props.filter[key]} {...this.props}/>;
        });

        return <div style={{display: 'flex'}}>
            {items}
        </div>
    }
}

TypeFilter.propTypes = {
    filter: PropTypes.object.isRequired,
    filterable: PropTypes.string.isRequired,
    actions: PropTypes.object.isRequired
};