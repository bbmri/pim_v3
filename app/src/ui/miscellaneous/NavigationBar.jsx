/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React from 'react'
import PropTypes from 'prop-types'

import {
    Breadcrumb,
    Segment
} from 'semantic-ui-react'

import HelpContext from 'ui/miscellaneous/HelpContext'

class NavigationBar extends React.Component {
    shouldComponentUpdate(nextProps, nextState) {
        return JSON.stringify(this.props.sections) !== JSON.stringify(nextProps.sections);
    }

    breadCrumbs() {
        const breadcrumbs = [];

        for (let sectionId in this.props.sections) {
            const section   = this.props.sections[sectionId];
            const label     = section.label === undefined ? false : section.label;
            const active    = section.active === undefined ? false : section.active;
            const hasAction = section.action !== undefined && typeof section.action === 'function';

            if (sectionId > 0 && sectionId < this.props.sections.length) {
                breadcrumbs.push(<Breadcrumb.Divider key={`divider_${sectionId}`} icon='right angle'/>);
            }

            const status = () => {
                if (section.status === undefined)
                    return label;
                else
                    return section.status();
            };

            breadcrumbs.push(<HelpContext key={`section_${sectionId}`} status={status}>
                <Breadcrumb.Section active={active} link={hasAction}
                                    onClick={hasAction ? () => section.action() : null}>
                    {label}
                </Breadcrumb.Section>
            </HelpContext>);
        }

        return breadcrumbs;
    }

    render() {
        const style = {
            ...this.props.style,
            boxShadow: 'none',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'space-between'
        };

        return <Segment {...this.props} secondary style={style}>
            <Breadcrumb>
                {this.breadCrumbs()}
            </Breadcrumb>
            {this.props.actions ? this.props.actions : null}
        </Segment>
    }
};

NavigationBar.propTypes = {
    sections: PropTypes.array.isRequired,
    actions: PropTypes.node
};

export default NavigationBar;