/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'

const style = {
    container: {
    },
    listWrapper: {
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        overflowX: 'scroll',
        overflowY: 'auto',
        position: 'absolute'
    },
    list: height => ({
        height,
        position: 'relative'
    }),
    item: (index, height) => ({
        height,
        left: 0,
        right: 0,
        top: height * index,
        position: 'absolute'
    })
};

export default class VirtualizedList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            scrollTop: 0,
            visibleHeight: 0
        };
    }

    componentDidMount() {
        this.getWrapper().addEventListener(
            'scroll',
            e => {
                this.setScrollPosition(e);
            },
            true
        );

        const visibleHeight = parseFloat(
            window
                .getComputedStyle(this.getWrapper(), null)
                .getPropertyValue('height')
        );

        this.setState({visibleHeight});
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', e => {
            this.setScrollPosition(e);
        });
    }

    getCount = () => {
        return this.props.source.length;
    };

    getScrollPosition = () => {
        return this.state.scrollTop;
    };

    getVisibleHeight = () => {
        return this.state.visibleHeight;
    };

    getHeight = () => {
        return this.getCount() * this.props.rowHeight;
    };

    getWrapper = () => {
        return ReactDOM.findDOMNode(this.listWrapper);
    };

    getDefaultHeightWidth = () => {
        return this.props.className ? {} : {height: '100%', width: '100%'};
    };

    scrollBottom = () => {
        const wrapper = this.getWrapper();
        return wrapper.scrollHeight - wrapper.clientHeight;
    };

    setScrollPosition = event => {
        const scrollTop = event.target.scrollTop;

        this.setState({
            scrollTop: scrollTop
        });

        if (this.props.onScrolled !== undefined) {
            this.props.onScrolled(scrollTop, this.scrollBottom());
        }
    };

    scrollToTop = () => {
        this.getWrapper().scrollTop = 0;
    };

    scrollToBottom = () => {
        const wrapper      = this.getWrapper();
        const scrollHeight = wrapper.scrollHeight;
        const height       = wrapper.clientHeight;
        const maxScrollTop = scrollHeight - height;

        ReactDOM.findDOMNode(wrapper).scrollTop = maxScrollTop > 0 ? maxScrollTop : 0;
    };

    checkIfVisible = index => {
        const elementPosition = index * this.props.rowHeight;

        return (
            elementPosition >
            this.getScrollPosition() -
            this.props.overScanCount * this.props.rowHeight &&
            elementPosition + this.props.rowHeight <
            this.getScrollPosition() +
            this.state.visibleHeight +
            this.props.overScanCount * this.props.rowHeight
        );
    };

    visibleItems = () => {
        return this.props.source.map(
            (_, index) =>
                this.checkIfVisible(index) &&
                this.props.renderItem({
                    index: index,
                    item: this.props.source[index],
                    style: style.item(index, this.props.rowHeight)
                })
        )
    };

    componentWillUpdate(nextProps) {
        const wrapper       = this.getWrapper();
        const scrollPos     = wrapper.scrollTop;
        const scrollBottom  = (wrapper.scrollHeight - wrapper.clientHeight);
        this.scrollAtBottom = (scrollBottom <= 0) || (scrollPos === scrollBottom);
    }

    componentDidUpdate() {
        if (this.props.scrollToBottom || this.scrollAtBottom) {
            this.scrollToBottom();
        }
    }

    renderList = () => {
        return <div style={style.container}
                    className={this.props.className}
                    ref={c => (this.container = c)}>
            <div style={style.listWrapper} ref={c => (this.listWrapper = c)}>
                <div style={style.list(this.getHeight())} ref={c => (this.list = c)}>
                    {this.visibleItems()}
                </div>
            </div>
        </div>
    };

    render = () => this.renderList();
}

VirtualizedList.propTypes = {
    renderItem: PropTypes.func,
    rowHeight: PropTypes.number,
    className: PropTypes.string,
    source: PropTypes.array.isRequired,
    overScanCount: PropTypes.number.isRequired,
    scrollToBottom: PropTypes.bool.isRequired,
    onScrolled: PropTypes.func
};