/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React from 'react';

const PageShell = Page => {
    return props => <Page {...props} />
        // <div className="page">
        //     <ReactCSSTransitionGroup
        //         transitionAppear={true}
        //         transitionAppearTimeout={600}
        //         transitionEnterTimeout={600}
        //         transitionLeaveTimeout={200}
        //         transitionName={props.match.path === '/thanks' ? 'SlideIn' : 'SlideOut'}
        //     >
        //         <img src={logo} alt=""/>
        //         <Page {...props} />
        //     </ReactCSSTransitionGroup>
        // </div>;
};
export default PageShell;