/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React, {Component} from 'react'
import PropTypes from 'prop-types';

import {
    Icon
} from 'semantic-ui-react'

import HelpContext from 'ui/miscellaneous/HelpContext'

export default class LastUpdated extends Component {
    render() {
        const lastUpdated = this.props.lastUpdated;
        const icon        = <Icon name='download' style={{color: `hsl(0, 0%, ${this.props.updating ? 60 : 80}%)`}}/>;

        if (lastUpdated !== '') {
            const date = new Date(lastUpdated);

            const localDateString = date.toLocaleDateString('nl');
            const localTimeString = date.toLocaleTimeString('nl');
            const name            = this.props.name.charAt(0).toUpperCase() + this.props.name.slice(1);

            return <HelpContext status={`${name} last updated on ${localDateString} at ${localTimeString}`}>
                <div style={{color: 'grey', cursor: 'default'}}>{icon}&nbsp;Last updated
                    on {localDateString} at {localTimeString}</div>
            </HelpContext>
        }

        return null;
    }
}

LastUpdated.propTypes = {
    name: PropTypes.string.isRequired,
    lastUpdated: PropTypes.string.isRequired,
    updating: PropTypes.bool.isRequired
};
