/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React from 'react'
import {Switch, Route, withRouter} from 'react-router-dom'

import RunPage from 'ui/pages/run/Page'
import RunsPage from 'ui/pages/runs/Page'
import JobsPage from 'ui/pages/jobs/Page'
import JobPage from 'ui/pages/job/Page'
import NotFoundPage from 'ui/pages/NotFound'
import {RunProvider} from "./pages/run/Run";

const NavigationContext = React.createContext({});

export const NavigationProvider = NavigationContext.Provider;
export const NavigationConsumer = NavigationContext.Consumer;

class Navigation extends React.Component {
    toRunsPage = () => {
        logger.debug(`Navigating to runs page`);

        this.props.history.push({pathname: `/runs`});
    };

    toGraphPage = ({runId = '', nodeId = ''}) => {
        logger.debug(`Navigating to ${runId} graph page`);

        const queryParams = [
            `nodeId=${nodeId}`
        ];

        this.props.history.push({
            pathname: `/runs/${runId}`,
            search: `?${queryParams.join('&')}`
        });
    };

    toJobsPage = ({runId = '', nodeId = 'root', filter = ['idle', 'running', 'success', 'failed', 'cancelled', 'undefined']}) => {
        logger.debug(`Navigating to ${runId} jobs page`);

        const queryParams = [
            `node=${nodeId}`,
            `statusFilter=${JSON.stringify(filter)}`
        ];

        this.props.history.push({
            pathname: `/runs/${runId}/jobs`,
            search: `?${queryParams.join('&')}`
        });
    };

    toJobPage = ({runId = '', jobId = 'root/master', view = 'tree'}) => {
        logger.debug(`Navigating to ${runId} job page ${jobId}`);

        const queryParams = [
            `path=${jobId}`,
            `view=${view}`
        ];

        this.props.history.push({
            pathname: `/runs/${runId}/job`,
            search: `?${queryParams.join('&')}`
        });
    };

    render() {
        const value = {
            toRunsPage: this.toRunsPage,
            toGraphPage: this.toGraphPage,
            toJobsPage: this.toJobsPage,
            toJobPage: this.toJobPage
        };

        return <NavigationProvider value={value}>
            {this.props.children}
        </NavigationProvider>
    }
}

export default withRouter(Navigation);