/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React from 'react'
import {Switch, Route, withRouter} from 'react-router-dom'

import RunPage from 'ui/pages/run/Page'
import RunsPage from 'ui/pages/runs/Page'
import JobsPage from 'ui/pages/jobs/Page'
import JobPage from 'ui/pages/job/Page'
import NotFoundPage from 'ui/pages/NotFound'

class Router extends React.Component {
    render() {
        const runsPage = () => <RunsPage {...this.props}/>;
        const runPage  = () => <RunPage {...this.props}/>;
        const jobsPage = () => <JobsPage {...this.props}/>;
        const jobPage  = () => <JobPage {...this.props}/>;

        return <div>
            <Switch>
                <Route exact path='/runs/:run/jobs' render={jobsPage}/>}
                <Route exact path='/runs/:run' render={runPage}/>}
                <Route exact path='/runs' render={runsPage}/>}
                <Route exact path='/runs/:run/job' render={jobPage}/>}
                <Route exact path='/' render={runsPage}/>}
                <Route component={NotFoundPage}/>
            </Switch>
        </div>
    }
}

export default withRouter(Router);