/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React, {Component} from 'react'

import {
    Label
} from 'semantic-ui-react'

import {RunsConsumer} from 'ui/pages/runs/Runs'
import {NavigationConsumer} from 'ui/Navigation'

import StringFilter from 'ui/miscellaneous/StringFilter'
import TypeFilter from 'ui/miscellaneous/TypeFilter'

class Filter extends Component {
    shouldComponentUpdate(nextProps, nextState, nextContext) {
        return nextProps.runs.data.get('filter') !== this.props.runs.data.get('filter');
    }

    onSetNameFilter = (nameFilter) => {
        this.props.runs.actions.setNameFilter(nameFilter, false);
        this.props.runs.actions.synchronize()
    };

    onSetUserFilter = (userFilter) => {
        this.props.runs.actions.setUserFilter(userFilter, false);
        this.props.runs.actions.synchronize()
    };

    render() {
        const filterStore  = this.props.runs.data.get('filter');
        const statusFilter = filterStore.get('status').toJS();

        let typeFilter = {};

        for (let type in statusFilter) {
            typeFilter[type] = {
                enabled: statusFilter[type]
            }
        }

        return <div style={{display: 'flex', flexDirection: 'column'}}>
            <div style={{display: 'flex'}}>
                <StringFilter name={'run name'}
                              placeholder='Filter by run name'
                              status='Filter runs by name'
                              icon={'tag'}
                              filter={filterStore.get('name')}
                              setFilter={this.onSetNameFilter}
                              commitFilterInterval={250}/>
                <div style={{width: '10px'}}/>
                <StringFilter name={'user'}
                              placeholder='Filter by user'
                              status='Filter runs by user'
                              icon={'user'}
                              filter={filterStore.get('user')}
                              setFilter={this.onSetUserFilter}
                              commitFilterInterval={250}/>
            </div>
            <div style={{height: '10px'}}/>
            <div style={{display: 'flex'}}>
                <div style={{
                    display: 'flex',
                    flexDirection: 'column',
                    justifyContent: 'center',
                    paddingRight: '5px'
                }}>
                    <Label pointing='right' size='tiny'>Filter by status</Label>
                </div>
                <TypeFilter filterable='run'
                            filter={typeFilter}
                            actions={this.props.runs.actions}
                            {...this.props}/>
            </div>
        </div>
    }
}

export default props => {
    return <NavigationConsumer>
        {navigation => <RunsConsumer>
            {runs => <Filter {...props} runs={runs} navigation={navigation}/>}
        </RunsConsumer>}
    </NavigationConsumer>
};