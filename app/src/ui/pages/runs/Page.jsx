/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React from 'react';

import Footer from 'ui/pages/footer/Footer'

import Container from './Container'
import Page from './../Page'

export default class RunsPage extends React.Component {
    render() {
        return <Page>
            <Container {...this.props}/>
            <Footer {...this.props}/>
        </Page>
    }
}