/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React, {Component} from 'react'
import {withRouter} from 'react-router-dom'

import Immutable from 'immutable'

const queryString = require('query-string');

import {
    Message,
    Segment
} from 'semantic-ui-react'

import {RunsConsumer} from 'ui/pages/runs/Runs'
import {NavigationConsumer} from 'ui/Navigation'

import ErrorBoundary from 'ui/miscellaneous/ErrorBoundary'
import NavigationBar from 'ui/miscellaneous/NavigationBar'
import FetchErrorMessage from 'ui/miscellaneous/FetchErrorMessage'
import Pagination from 'ui/pagination/Pagination'

import Progress from 'helpers/progress'

import PageHeader from './PageHeader'
import Filter from './Filter'
import Table from './table/Table'

class Container extends Component {
    componentDidMount() {
        const params = queryString.parse(this.props.location.search);

        if (params.nameFilter !== undefined) {
            this.props.runs.actions.setNameFilter(params.nameFilter);
        }

        if (params.userFilter !== undefined) {
            this.props.runs.actions.setUserFilter(params.userFilter);
        }

        if (params.statusFilter !== undefined) {
            const statusFilter = {};

            for (let statusFilterType of Progress.jobStatusTypes) {
                statusFilter[statusFilterType] = JSON.parse(params.statusFilter).includes(statusFilterType);
            }

            this.props.runs.actions.setStatusFilter(statusFilter);
        }

        this.updateQueryParameters();

        this.props.runs.actions.startSynchronization();
    }

    componentWillUnmount() {
        this.props.runs.actions.stopSynchronization();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (!Immutable.is(this.props.runs.data.get('filter'), prevProps.runs.data.get('filter'))) {
            this.updateQueryParameters();
        }
    }

    updateQueryParameters() {
        logger.debug('Updating query parameters');

        const filter          = this.props.runs.data.get('filter');
        const statusFilterMap = filter.get('status').toJS();
        const statusFilter    = [];

        for (let type in statusFilterMap) {
            if (statusFilterMap[type])
                statusFilter.push(type);
        }

        const params = [
            `nameFilter=${filter.get('name')}`,
            `userFilter=${filter.get('user')}`,
            `statusFilter=${JSON.stringify(statusFilter)}`
        ];

        this.props.history.replace({
            path: '/runs',
            search: `?${params.join('&')}`
        });
    }

    content() {
        const catchErrorUI = (message) => <Message attached='bottom' compact error>
            {message}
        </Message>;

        const table = this.props.runs.data.get('table');

        const pagination = {
            rowType: 'run',
            noPages: table.get('noPages'),
            pageSize: table.get('pageSize'),
            pageIndex: table.get('pageIndex'),
            noPagesPerView: table.get('noPagesPerView'),
            actions: {
                setPageSize: this.props.runs.actions.setPageSize.bind(this),
                setPageIndex: this.props.runs.actions.setPageIndex.bind(this),
                firstPage: this.props.runs.actions.firstPage.bind(this),
                previousPage: this.props.runs.actions.previousPage.bind(this),
                nextPage: this.props.runs.actions.nextPage.bind(this),
                lastPage: this.props.runs.actions.lastPage.bind(this)
            }
        };

        const fetchError = this.props.runs.data.getIn(['api', 'fetchError']);

        let content = null;

        if (fetchError === null) {
            content = <React.Fragment>
                <Filter {...this.props}/>
                <Table name={'run'}/>
                <Pagination {...pagination}/>
            </React.Fragment>
        } else {
            content = <FetchErrorMessage subject='Runs' error={fetchError}/>
        }

        return <ErrorBoundary content={catchErrorUI}>
            <Segment attached='bottom'>
                {content}
            </Segment>
        </ErrorBoundary>
    }

    render() {
        const sections = [
            {label: 'pim', status: () => 'Pipeline Inspection and Monitoring'},
            {label: 'runs', active: true},
        ];

        return <div style={{flexGrow: 1, display: 'flex', flexDirection: 'column'}}>
            <PageHeader {...this.props}/>
            <NavigationBar attached='top' sections={sections}/>
            {this.content()}
            <div style={{height: 0}}/>
        </div>
    }
}

const RunsWithRouter = withRouter(Container);

export default props => {
    return <NavigationConsumer>
        {navigation => <RunsConsumer>
            {runs => <RunsWithRouter {...props} runs={runs} navigation={navigation}/>}
        </RunsConsumer>}
    </NavigationConsumer>
};