/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React from 'react'
import styled from 'styled-components'

import {
    Modal,
    Button, Icon
} from 'semantic-ui-react'

import {RunsConsumer} from 'ui/pages/runs/Runs'

class RemoveRunsModal extends React.Component {
    state = {open: false};

    open = () => {
        this.setState({open: true})
    };

    close = () => {
        this.setState({open: false})
    };

    clickYes = () => {
        this.close();
        this.removeRuns();
    };

    clickNo = () => {
        this.close();
    };

    removeRuns = () => {
        this.props.runs.actions.removeSelected();
    };

    render() {
        const runsToRemove = this.props.runs.data.getIn(['table', 'pageRows']).filter(run => {
            return run.get('selected');
        }).map(run => {
            return <StyledRun>{run.get('name')}</StyledRun>
        });

        const trigger = <Icon name={'trash'} link fitted onClick={this.open}/>;

        return <Modal trigger={trigger} open={this.state.open} size='small' centered={true} dimmer='blurring'>
            <Modal.Header>Remove {runsToRemove.size > 1 ? runsToRemove.size : ''} run{runsToRemove.size > 1 ? 's' : ''}</Modal.Header>
            <Modal.Content>
                <Modal.Description>
                    <p>You are about to remove {runsToRemove.size > 1 ? 'these runs' : 'this run'}:</p>
                    <StyledRuns>
                        {runsToRemove}
                    </StyledRuns>
                    <p>Are you sure?</p>
                </Modal.Description>
            </Modal.Content>
            <Modal.Actions>
                <StyledActions>
                    <StyledActionsButtons>
                        <Button basic
                                compact
                                onClick={this.clickYes}>Yes</Button>
                        <StyledButtonSpacer/>
                        <Button basic
                                compact
                                onClick={this.clickNo}>No</Button>
                    </StyledActionsButtons>
                </StyledActions>
            </Modal.Actions>
        </Modal>;
    }
}

const StyledActions = styled.div`
    display: flex;
    justify-content: flex-end;
    align-items: center;
`;

const StyledActionsButtons = styled.div`
    display: flex;
`;

const StyledRuns = styled.ul`
    display: flex;
    flex-direction: column;
    align-items: flex-start;
`;

const StyledRun = styled.li`
`;

const StyledButtonSpacer = styled.div`
    width: 5px;
`;

export default props => {
    return <RunsConsumer>
        {runs => <RemoveRunsModal {...props} runs={runs}/>}
    </RunsConsumer>
};