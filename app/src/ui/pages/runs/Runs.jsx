/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React from 'react'

import {withCookies} from 'react-cookie'

import Immutable from 'immutable'

import AppApi from 'api/api'

const RunsContext = React.createContext({});

export const RunsProvider = RunsContext.Provider;
export const RunsConsumer = RunsContext.Consumer;

const defaultRuns = {
    config: {
        updateInterval: 2500,
        confirmRemove: true
    },
    table: {
        sortColumn: 'name',
        sortDirection: 'ascending',
        noPages: 0,
        pageSize: 10,
        pageIndex: 0,
        noPagesPerView: 5,
        pageRows: [],
        noRows: 0,
        noFilteredRows: 0
    },
    filter: {
        name: '',
        user: '',
        status: {
            idle: true,
            running: true,
            success: true,
            failed: true,
            cancelled: true,
            undefined: true
        }
    },
    api: {
        fetching: false,
        noFetches: 0,
        fetchResponse: null,
        fetchError: null,
        lastFetched: ''
    }
};

class Runs extends React.Component {
    constructor(props) {
        super(props);

        this.data             = Immutable.fromJS(defaultRuns);
        this.synchronizeTimer = null;
    }

    componentWillMount() {
        this.readCookies();
    }

    componentDidMount() {
        window.addEventListener('beforeunload', this.writeCookies.bind(this));
    }

    readCookies() {
        logger.debug('Loading settings from cookies');

        const defaultStatusFilter = {
            idle: true,
            running: true,
            success: true,
            failed: true,
            cancelled: true,
            undefined: false
        };

        this.setNameFilter(this.props.cookies.get('runsNameFilter') || '', false);
        this.setUserFilter(this.props.cookies.get('runsUserFilter') || '', false);
        this.setStatusFilter(this.props.cookies.get('runsStatusFilter') || defaultStatusFilter, false);
        this.setSortColumn(this.props.cookies.get('runsSortColumn') || 'modified', false);
        this.setSortDirection(this.props.cookies.get('runsSortDirection') || 'descending', false);
        this.setPageSize(parseInt(this.props.cookies.get('runsPageSize')) || 10, false);
        this.setPageIndex(parseInt(this.props.cookies.get('runsPageIndex')) || 0, false);
    }

    writeCookies() {
        logger.debug('Writing settings to cookies');

        const table  = this.data.get('table');
        const filter = this.data.get('filter');

        this.props.cookies.set('runsNameFilter', filter.get('name'));
        this.props.cookies.set('runsUserFilter', filter.get('user'));
        this.props.cookies.set('runsStatusFilter', filter.get('status'));
        this.props.cookies.set('runsSortColumn', table.get('sortColumn'));
        this.props.cookies.set('runsSortDirection', table.get('sortDirection'));
        this.props.cookies.set('runsPageSize', table.get('pageSize'));
        this.props.cookies.set('runsPageIndex', Math.max(0, table.get('pageIndex')).toString());
    }

    startSynchronization = () => {
        logger.debug(`Start synchronization`);

        if (this.synchronizeTimer !== null)
            clearInterval(this.synchronizeTimer);

        if (this.synchronizeTimer === null) {
            this.synchronizeTimer = setInterval(this.synchronize, this.data.getIn(['config', 'updateInterval']));
        }

        this.synchronize();
    };

    stopSynchronization = () => {
        logger.debug(`Stop synchronization`);

        if (this.synchronizeTimer !== null) {
            clearInterval(this.synchronizeTimer);
            this.synchronizeTimer = null;
        }
    };

    synchronize = () => {
        logger.debug(`Synchronizing`);

        if (this.data.getIn(['api', 'fetching']))
            return;

        this._fetchRuns();
    };

    setData = (data) => {
        this.data = null;
        this.data = data;
    };

    removeRun = (name) => {
        logger.debug(`Remove run: ${name}`);

        this.setFetching(true);

        AppApi.removeRun(name)
            .then(response => {
                this.setFetching(false);
                this.synchronize();
            });
    };

    setNameFilter = (nameFilter, synchronize = false) => {
        logger.debug(`Set name filter: ${nameFilter}`);

        this.setData(this.data.setIn(['filter', 'name'], nameFilter));

        this.setPageIndex(0, synchronize);

        if (synchronize) {
            this.synchronize();
        }
    };

    setUserFilter = (userFilter, synchronize = true) => {
        logger.debug(`Set user filter: ${userFilter}`);

        this._setTransient();

        this.setData(this.data.setIn(['filter', 'user'], userFilter));

        this.setPageIndex(0, synchronize);

        if (synchronize) {
            this.synchronize();
        }
    };

    toggleStatusFilter = (type, synchronize = true) => {
        logger.debug(`Toggle status filter`);

        this._setTransient();

        const status  = this.data.getIn(['filter', 'status', type]);
        const newData = this.data.setIn(['filter', 'status', type], !status);

        this.setData(newData);

        if (synchronize)
            this.synchronize();
    };

    setStatusFilter = (filter, synchronize = true) => {
        logger.debug(`Set status filter`);

        this._setTransient();

        this.setData(this.data.setIn(['filter', 'status'], Immutable.fromJS(filter)));

        if (synchronize)
            this.synchronize();
    };

    setSoloStatusFilter = (statusType, synchronize = true) => {
        logger.debug(`Set solo status filter`);

        this._setTransient();

        const newFilter = this.data.get('filter').update('status', item => item.map((value, type) => {
            return type === statusType;
        }));

        this.setData(this.data.set('filter', newFilter));

        if (synchronize)
            this.synchronize();
    };

    toggleSortColumn = (sortColumn, synchronize = true) => {
        logger.debug(`Toggle sort column`);

        let sortDirection = 'ascending';

        if (this.data.getIn(['table', 'sortColumn']) === sortColumn) {
            sortDirection = this.data.getIn(['table', 'sortDirection']) === 'ascending' ? 'descending' : 'ascending';
        }

        this.setSortColumn(sortColumn, false);
        this.setSortDirection(sortDirection, false);

        if (synchronize)
            this.synchronize();
    };

    setSortColumn = (sortColumn, synchronize = true) => {
        logger.debug(`Set sort column: ${sortColumn}`);

        this._setTransient();

        this.setData(this.data.setIn(['table', 'sortColumn'], sortColumn));

        this.setPageIndex(0, false);

        if (synchronize)
            this.synchronize();

        this.forceUpdate();
    };

    setSortDirection = (sortDirection, synchronize = true) => {
        logger.debug(`Set sort direction: ${sortDirection}`);

        this._setTransient();

        this.setData(this.data.setIn(['table', 'sortDirection'], sortDirection));

        this.setPageIndex(0, false);

        if (synchronize)
            this.synchronize();

        this.forceUpdate();
    };

    setPageSize = (pageSize, synchronize = true) => {
        logger.debug(`Set page size: ${pageSize}`);

        this._setTransient();

        this.setData(this.data.setIn(['table', 'pageSize'], pageSize));

        this.setPageIndex(0, false);

        if (synchronize)
            this.synchronize();

        this.forceUpdate();
    };

    setPageIndex = (pageIndex, synchronize = true) => {
        logger.debug(`Set page index: ${pageIndex}`);

        this._setTransient();

        this.setData(this.data.setIn(['table', 'pageIndex'], pageIndex));

        if (synchronize)
            this.synchronize();

        this.forceUpdate();
    };

    firstPage = (synchronize = true) => {
        logger.debug(`First page`);

        this._setTransient();

        this.setPageIndex(0);

        if (synchronize)
            this.synchronize();

        this.forceUpdate();
    };

    previousPage = (synchronize = true) => {
        logger.debug(`Previous page`);

        this._setTransient();
        this.setPageIndex(this.data.getIn(['table', 'pageIndex']) - 1);

        if (synchronize)
            this.synchronize();

        this.forceUpdate();
    };

    nextPage = (synchronize = true) => {
        logger.debug(`Next page`);

        this._setTransient();
        this.setPageIndex(this.data.getIn(['table', 'pageIndex']) + 1);

        if (synchronize)
            this.synchronize();

        this.forceUpdate();
    };

    lastPage = (synchronize = true) => {
        logger.debug(`Last page`);

        this._setTransient();
        this.setPageIndex(this.data.getIn(['table', 'noPages']) - 1);

        if (synchronize)
            this.synchronize();

        this.forceUpdate();
    };

    select = (name, selected) => {
        logger.debug(`Select row: ${name}`);

        this.setData(this.data.updateIn(['table', 'pageRows'], item => item.map(row => {
            if (row.get('name') === name) {
                row = row.set('selected', selected);
            }
            return row;
        })));

        this.forceUpdate();
    };

    selectAll = (selected) => {
        logger.debug(`Select all rows`);

        this.setData(this.data.updateIn(['table', 'pageRows'], item => item.map(row => {
            return row.set('selected', selected);
        })));

        this.forceUpdate();
    };

    removeSelected = () => {
        logger.debug(`Remove selected row(s)`);

        const runs = this.data.getIn(['table', 'pageRows']);

        let runsToRemove = runs.reduce((accum, run) => {
            if (run.get('selected'))
                accum.push(run.get('name'));

            return accum;
        }, []);

        this._setRemoving(runsToRemove);

        this.forceUpdate();

        for (let runToRemove of runsToRemove) {
            AppApi.removeRun(runToRemove);
        }
    };

    _fetchRuns = () => {
        logger.debug(`Fetching runs`);

        this._setApiFetching(true);

        const data         = this.data;
        const statusFilter = data.getIn(['filter', 'status']).toJS();
        const table        = this.data.get('table');
        const filter       = this.data.get('filter');

        let params = {
            nameFilter: filter.get('name'),
            userFilter: filter.get('user'),
            statusFilter: JSON.stringify(Object.keys(statusFilter).filter(type => statusFilter[type])),
            sortColumn: table.get('sortColumn'),
            sortDirection: table.get('sortDirection'),
            pageSize: table.get('pageSize'),
            pageIndex: table.get('pageIndex')
        };

        AppApi.fetchRuns(params)
            .then(response => {
                this._setApiSuccess(response);
            })
            .catch(error => {
                this._setApiError(error);
            });
    };

    _updatePagination = (noFilteredRows) => {
        const noPages   = Math.ceil(noFilteredRows / this.data.getIn(['table', 'pageSize']));
        const pageIndex = Math.min(Math.max(0, noPages - 1), this.data.getIn(['table', 'pageIndex']));

        let newTable = this.data.get('table');

        newTable = newTable.set('noPages', noPages);
        newTable = newTable.set('pageIndex', pageIndex);
        newTable = newTable.set('noFilteredRows', noFilteredRows);

        this.setData(this.data.set('table', newTable));
    };

    _setPageRows = (pageRows) => {
        let selection = this.data.getIn(['table', 'pageRows']).reduce((accum, row) => {
            if (row.get('selected'))
                accum.add(row.get('name'));

            return accum;
        }, new Set());

        for (let row of pageRows) {
            row.transient = false;
            row.removing  = false;
            row.selected  = selection.has(row.name);
        }

        this.setData(this.data.setIn(['table', 'pageRows'], Immutable.fromJS(pageRows)));
    };

    _setNoRows = (noRows) => {
        this.setData(this.data.setIn(['table', 'noRows'], noRows));
    };

    _setTransient = () => {
        this.setData(this.data.updateIn(['table', 'pageRows'], item => item.map(row => {
            return row.set('transient', true);
        })));
    };

    _setRemoving = (rows) => {
        this.setData(this.data.updateIn(['table', 'pageRows'], item => item.map(row => {
            if (rows.includes(row.get('name'))) {
                return row.set('removing', true);
            }
            return row;
        })));
    };

    _setApiFetching = (fetching) => {
        logger.debug(`Set API fetching`);

        this.setData(this.data.setIn(['api', 'fetching'], fetching));

        this.forceUpdate();
    };

    _setApiResponse = (apiResponse) => {
        logger.debug(`Set API response`);

        let api = this.data.get('api');

        api = api.set('fetching', false);
        api = api.set('noFetches', this.data.getIn(['api', 'noFetches']) + 1);
        api = api.set('fetchResponse', apiResponse);
        api = api.set('fetchError', null);
        api = api.set('lastFetched', new Date().toUTCString());

        this.setData(this.data.set('api', api));
    };

    _setApiSuccess = (response) => {
        this._setPageRows(response.data.runs);
        this._setNoRows(response.data.noRuns);
        this._updatePagination(response.data.noFilteredRuns);

        this._setApiFetching(false);
        this._setApiResponse(response);

        this.forceUpdate();
    };

    _setApiError = (fetchError) => {
        logger.debug(`Set API error`);

        let api = this.data.get('api');

        api = api.set('fetching', false);
        api = api.set('fetchResponse', null);
        api = api.set('fetchError', fetchError);
        api = api.set('lastFetched', new Date().toUTCString());

        this.setData(this.data.set('api', api));
    };

    render() {
        const value = {
            data: this.data,
            actions: {
                removeRun: this.removeRun,
                setNameFilter: this.setNameFilter,
                setUserFilter: this.setUserFilter,
                toggleStatusFilter: this.toggleStatusFilter,
                setStatusFilter: this.setStatusFilter,
                setSoloStatusFilter: this.setSoloStatusFilter,
                toggleSortColumn: this.toggleSortColumn,
                setSortColumn: this.setSortColumn,
                setSortDirection: this.setSortDirection,
                setPageSize: this.setPageSize,
                setPageIndex: this.setPageIndex,
                firstPage: this.firstPage,
                previousPage: this.previousPage,
                nextPage: this.nextPage,
                lastPage: this.lastPage,
                select: this.select,
                selectAll: this.selectAll,
                removeSelected: this.removeSelected,
                synchronize: this.synchronize,
                startSynchronization: this.startSynchronization,
                stopSynchronization: this.stopSynchronization,
                setConfirmRemove: this.setConfirmRemove,
                toggleConfirmRemove: this.toggleConfirmRemove
            }
        };

        return <RunsProvider value={value}>
            {this.props.children}
        </RunsProvider>
    }
}

export default withCookies(Runs);