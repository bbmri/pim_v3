/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React, {Component} from 'react'
import PropTypes from 'prop-types'

import {
    Table
} from 'semantic-ui-react'

import {RunsConsumer} from 'ui/pages/runs/Runs'

import DurationCell from 'ui/miscellaneous/table//cell/Duration'
import DateTimeCell from 'ui/miscellaneous/table//cell/DateTime'

import Selected from './cell/Selected'
import Status from './cell/Status'
import Name from './cell/Name'
import User from './cell/User'
import NoJobs from './cell/NoJobs'
import Progress from './cell/Progress'
import Jobs from './cell/Jobs'

class Row extends Component {
    shouldComponentUpdate(nextProps, nextState, nextContext) {
        return nextProps.run !== this.props.run;
    }

    render() {
        const run       = this.props.run;
        const created   = run.get('created').replace(/ /g, 'T');
        const modified  = run.get('modified').replace(/ /g, 'T');
        const duration  = new Date(modified) - new Date(created);
        const transient = this.props.run.get('transient') || this.props.run.get('removing');

        return <Table.Row key={run.get('name')} warning={run.get('selected') && !run.get('removing')}>
            <Selected key='selected' {...this.props} transient={transient}/>
            <Status key='status' {...this.props} transient={transient}/>
            <Name key='name' {...this.props} transient={transient}/>
            <User key='user' {...this.props} transient={transient}/>
            <NoJobs key='noJobs' {...this.props} transient={transient}/>
            <Progress key='progress' {...this.props} transient={transient}/>
            <Jobs key='jobs' {...this.props} transient={transient}/>
            <DurationCell key='duration' duration={duration} subject={'run'} transient={transient}/>
            <DateTimeCell key='created' dateTime={created} statusPrefix={'Run created on'}
                          transient={transient}/>
            <DateTimeCell key='modified' dateTime={modified} statusPrefix={'Run last modified on'}
                          transient={transient}/>
        </Table.Row>
    }
}

Row.propTypes = {
    run: PropTypes.object.isRequired
};

export default props => {
    return <RunsConsumer>
        {runs => <Row {...props} runs={runs}/>}
    </RunsConsumer>
};

