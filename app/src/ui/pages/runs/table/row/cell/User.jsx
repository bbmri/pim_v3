/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React, {Component} from 'react'
import PropTypes from 'prop-types'

import {RunsConsumer} from 'ui/pages/runs/Runs'

import StringCell from 'ui/miscellaneous/table/cell/String'

class User extends Component {
    status() {
        if (this.props.run.get('user') === this.props.runs.data.getIn(['filter', 'user'])) {
            return `Run is owned by ${this.props.run.get('user')}, click to remove filter`;
        }

        return `Run is owned by ${this.props.run.get('user')}, click to filter`;
    }

    onClick = () => {
        const active = this.props.run.get('user') === this.props.runs.data.getIn(['filter', 'user']);
        this.props.runs.actions.setUserFilter(active ? '' : this.props.run.get('user'))
    };

    render() {
        const filter          = this.props.runs.data.getIn(['filter', 'user']);
        const user            = this.props.run.get('user');
        const userLowerCase   = user.toLowerCase();
        const filterLowerCase = filter.toLowerCase();
        const segments        = userLowerCase.split(filterLowerCase);
        const filterLength    = filterLowerCase.length;
        const userMatch       = segments.join(Array(filterLength + 1).join("~"));

        let formattedName = [];

        for (let charIndex in user) {
            if (userMatch[charIndex] === '~') {
                formattedName.push(<span style={{fontWeight: 'bold'}} key={charIndex}>{user[charIndex]}</span>)
            } else {
                formattedName.push(user[charIndex]);
            }
        }

        return <StringCell string={formattedName}
                           status={this.status()}
                           onClick={this.onClick}
                           transient={this.props.transient}/>
    }
}

User.propTypes = {
    run: PropTypes.object.isRequired
};

export default props => {
    return <RunsConsumer>
        {runs => <User {...props} runs={runs}/>}
    </RunsConsumer>
};