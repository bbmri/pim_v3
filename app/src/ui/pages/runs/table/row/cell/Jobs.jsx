/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React, {Component} from 'react'
import PropTypes from 'prop-types'

import PubSub from 'pubsub-js'

import {
    Table,
    Popup
} from 'semantic-ui-react'

import {RunsConsumer} from 'ui/pages/runs/Runs'
import {NavigationConsumer} from 'ui/Navigation'

import ProgressBar from 'ui/miscellaneous/ProgressBar'

import * as jobHelper from 'helpers/job'

class Jobs extends Component {
    items() {
        const jobs   = this.props.run.get('aggregates').toJS();
        const noJobs = jobs.reduce((total, job) => total + job, 0);

        return {
            idle: {
                count: jobs[0]
            },
            running: {
                count: jobs[1]
            },
            success: {
                count: jobs[2]
            },
            failed: {
                count: jobs[3]
            },
            cancelled: {
                count: jobs[4]
            },
            undefined: {
                count: jobs[5]
            },
            uninitialized: {
                count: noJobs === 0 ? 1 : 0
            }
        };
    }

    render() {
        const cellStyle = {
            padding: '8px'
        };

        const containerStyle = {
            position: 'relative',
            height: '15px'
        };

        const handlers = {
            onClick: (bar) => {
                if (bar.type !== 'uninitialized')
                    this.props.navigation.toJobsPage({runId: this.props.run.get('name'), filter: [bar.type]})
            },
            status: (bar) => {
                if (bar.type === 'uninitialized') {
                    return 'No jobs have been scheduled (yet)';
                } else {
                    return `${jobHelper.statusTypeProgressDescription(bar)}, click to explore`;
                }
            },
            popup: (trigger, bar) => {
                const percentage = (100.0 * bar.percentage).toFixed(1);

                const content = bar.type === 'uninitialized' ? 'No jobs scheduled (yet)' : <div>
                    <b>{bar.count} {bar.type}</b> {percentage}%
                </div>;

                return <Popup trigger={trigger}
                              content={content}
                              position='top center'
                              size='mini'
                              inverted/>
            }
        };

        return <Table.Cell key='jobs' style={cellStyle}>
            <div style={containerStyle} className={this.props.transient ? 'desaturate' : ''}>
                <ProgressBar radius={5} items={this.items()} runId={this.props.run.get('name')} {...handlers}/>
            </div>
        </Table.Cell>
    }
}

Jobs.propTypes = {
    run: PropTypes.object.isRequired
};

export default props => {
    return <NavigationConsumer>
        {navigation => <RunsConsumer>
            {runs => <Jobs {...props} runs={runs} navigation={navigation}/>}
        </RunsConsumer>}
    </NavigationConsumer>
};