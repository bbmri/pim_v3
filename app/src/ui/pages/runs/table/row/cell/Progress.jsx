/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React, {Component} from 'react'
import PropTypes from 'prop-types'

import Numeral from 'numeral'

import {RunsConsumer} from 'ui/pages/runs/Runs'

import StringCell from 'ui/miscellaneous/table/cell/String'

class Progress extends Component {
    status() {
        const progress = (100.0 * this.props.run.get('progress')).toFixed(1);
        const noJobs   = this.props.run.get('aggregates').reduce((a, b) => a + b, 0);

        if (this.props.run.get('progress') === 1) {
            return `All ${new Numeral(noJobs).format('0,0')} jobs have completed`;
        }

        return `${progress}% of ${new Numeral(noJobs).format('0,0')} jobs have completed`;
    }

    render() {
        const progress = (100.0 * this.props.run.get('progress')).toFixed(1);

        return <StringCell string={`${progress}%`}
                           status={this.status()}
                           onClick={() => null}
                           transient={this.props.transient}
                           textAlign='right'/>
    }
}

Progress.propTypes = {
    run: PropTypes.object.isRequired
};

export default props => {
    return <RunsConsumer>
        {runs => <Progress {...props} runs={runs}/>}
    </RunsConsumer>
};