/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React, {Component} from 'react'

import {
    Table,
    Checkbox
} from 'semantic-ui-react'

import {RunsConsumer} from 'ui/pages/runs/Runs'

import HelpContext from 'ui/miscellaneous/HelpContext'

class Selected extends Component {
    shouldComponentUpdate(nextProps, nextState, nextContext) {
        if (nextProps.run.get('name') !== this.props.run.get('name'))
            return true;

        if (nextProps.run.get('selected') !== this.props.run.get('selected'))
            return true;

        return false;
    }

    status() {
        return `Click to ${this.props.run.get('selected') ? 'deselect' : 'select'} ${this.props.run.get('name')}`;
    }

    handleClick = (e, {checked}) => {
        this.props.runs.actions.select(this.props.run.get('name'), checked);
    };

    render() {
        return <HelpContext status={() => this.status()}>
            <Table.Cell key='selected'>
                <Checkbox checked={this.props.run.get('selected')}
                          onClick={this.handleClick}/>
            </Table.Cell>
        </HelpContext>
    }
}

export default props => {
    return <RunsConsumer>
        {runs => <Selected {...props} runs={runs}/>}
    </RunsConsumer>
};