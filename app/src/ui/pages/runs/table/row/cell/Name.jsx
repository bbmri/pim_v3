/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React, {Component} from 'react'
import PropTypes from 'prop-types'

import {
    Icon
} from 'semantic-ui-react'

import {NavigationConsumer} from 'ui/Navigation'

import StringCell from 'ui/miscellaneous/table/cell/String'
import HelpContext from 'ui/miscellaneous/HelpContext'

class Action extends Component {
    status() {
        return <span>{this.props.description}</span>;
    }

    render() {
        return <HelpContext status={this.status.bind(this)}>
            <Icon name={this.props.icon} link color={this.props.color} onClick={() => this.props.action()}/>
        </HelpContext>
    }
}

Action.defaultProps = {
    color: 'black'
};

Action.propTypes = {
    icon: PropTypes.string.isRequired,
    action: PropTypes.func.isRequired,
    color: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired
};

class Name extends Component {
    status() {
        return `${this.props.run.get('name')}, click to inspect graph`;
    }

    onClick = () => {
        this.props.navigation.toGraphPage({runId: this.props.run.get('name')})
    };

    render() {
        const filter       = this.props.runs.data.getIn(['filter', 'name']);
        const name         = this.props.run.get('name');
        const nameLC       = name.toLowerCase();
        const filterLC     = filter.toLowerCase();
        const segments     = nameLC.split(filterLC);
        const filterLength = filterLC.length;
        const nameMatch    = segments.join(Array(filterLength + 1).join("~"));

        let formattedName = [];

        for (let charIndex in name) {
            if (nameMatch[charIndex] === '~') {
                formattedName.push(<span style={{fontWeight: 'bold'}} key={charIndex}>{name[charIndex]}</span>)
            } else {
                formattedName.push(name[charIndex]);
            }
        }

        const string = <div style={{display: 'flex', alignItems: 'center'}}>
            <div style={{
                flex: 1,
                overflow: 'hidden',
                textOverflow: 'ellipsis'
            }}>
                {formattedName}
            </div>
        </div>;

        return <StringCell string={string}
                           status={this.status()}
                           onClick={this.onClick}
                           transient={this.props.transient}/>

    }
}

export default props => {
    return <NavigationConsumer>
            {navigation => <Name {...props} navigation={navigation}/>}
    </NavigationConsumer>
};