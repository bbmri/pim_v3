/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React, {Component} from 'react'

import {RunsConsumer} from 'ui/pages/runs/Runs'
import {NavigationConsumer} from 'ui/Navigation'

import StringCell from 'ui/miscellaneous/table/cell/String'

import * as jobHelper from 'helpers/job'

class Status extends Component {
    status() {
        const status = jobHelper.statusName(this.props.run.get('status'));
        return `${this.props.run.get('name')} ${jobHelper.statusDescription(status, 1)}, click to inspect log output`;
    }

    onClick = () => {
        this.props.navigation.toJobPage({runId: this.props.run.get('name'), view: 'log'})
    };

    render() {
        return <StringCell string={jobHelper.statusName(this.props.run.get('status'))}
                           status={this.status()}
                           onClick={this.onClick}
                           transient={this.props.transient}
                           textAlign='center'/>
    }
}

export default props => {
    return <NavigationConsumer>
        {navigation => <RunsConsumer>
            {runs => <Status {...props} runs={runs} navigation={navigation}/>}
        </RunsConsumer>}
    </NavigationConsumer>
};