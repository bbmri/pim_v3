/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React, {Component} from 'react'

import {
    Table,
    Checkbox
} from 'semantic-ui-react'

import {RunsConsumer} from 'ui/pages/runs/Runs'

import HelpContext from 'ui/miscellaneous/HelpContext'

class SelectAll extends Component {
    status() {
        if (this.allSelected())
            return `Click to deselect all runs on this page`;

        if (this.someSelected() || this.noneSelected())
            return `Click to select all runs on this page`;

        return '';
    }

    pageRows = () => {
        return this.props.runs.data.getIn(['table', 'pageRows']);
    };

    noSelected() {
        return this.pageRows().reduce((noSelected, run) => {
            if (run.get('selected'))
                return noSelected + 1;

            return noSelected;
        }, 0);
    }

    allSelected() {
        if (this.pageRows().size === 0)
            return false;

        return this.noSelected() === this.pageRows().size;
    }

    noneSelected() {
        return this.noSelected() === 0;
    }

    someSelected() {
        return !this.allSelected() && !this.noneSelected();
    }

    handleClick = (e, {checked}) => {
        this.props.runs.actions.selectAll(checked);
    };

    render() {
        return <HelpContext status={() => this.status()}>
            <Table.HeaderCell style={{width: 35}}>
                <Checkbox disabled={this.pageRows().size === 0}
                          checked={this.allSelected()}
                          indeterminate={this.someSelected()}
                          onClick={this.handleClick}/>
            </Table.HeaderCell>
        </HelpContext>
    }
}

export default props => {
    return <RunsConsumer>
        {runs => <SelectAll {...props} runs={runs}/>}
    </RunsConsumer>
};