/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React, {Component} from 'react'
import PropTypes from 'prop-types'

import Numeral from 'numeral'

import {RunsConsumer} from 'ui/pages/runs/Runs'
import {NavigationConsumer} from 'ui/Navigation'

import StringCell from 'ui/miscellaneous/table/cell/String'

class NoJobs extends Component {
    status() {
        if (this.props.run.get('noJobs') === 0) {
            return `No jobs have been scheduled yet`;
        }

        return `Run has ${new Numeral(this.props.run.get('noJobs')).format('0,0')} jobs, click to explore`;
    }

    onClick = () => {
        this.props.navigation.toJobsPage({
            runId: this.props.run.get('name'),
            filter: ['idle', 'running', 'success', 'failed', 'cancelled']
        })
    };

    render() {
        const noJobs = this.props.run.get('noJobs');

        return <StringCell string={noJobs >= 1000 ? new Numeral(noJobs).format('0.0 a') : noJobs.toString()}
                           status={this.status()}
                           onClick={this.onClick}
                           transient={this.props.transient}
                           textAlign='right'/>
    }
}

NoJobs.propTypes = {
    run: PropTypes.object.isRequired
};

export default props => {
    return <NavigationConsumer>
        {navigation => <RunsConsumer>
            {runs => <NoJobs {...props} runs={runs} navigation={navigation}/>}
        </RunsConsumer>}
    </NavigationConsumer>
};