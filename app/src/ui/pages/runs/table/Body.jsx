/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React, {Component} from 'react'

import {
    Table
} from 'semantic-ui-react'

import {RunsConsumer} from 'ui/pages/runs/Runs'

import Row from './row/Row'

class Body extends Component {
    shouldComponentUpdate(nextProps, nextState, nextContext) {
        return nextProps.runs.data.getIn(['table', 'pageRows']) !== this.props.runs.data.getIn(['table', 'pageRows']);
    }

    render() {
        let pageRows  = this.props.runs.data.getIn(['table', 'pageRows']);
        let tableRows = pageRows.map(run => {
            return <Row key={run.get('name')}
                        run={run}/>
        });

        if (pageRows.length === 0) {
            const noFilterMatchText   = <span>No runs found for query</span>;
            const noRunsAvailableText = `No runs to display`;
            const rowText             = this.props.runs.date.getIn(['filter', 'name']).length > 0 ? noFilterMatchText : noRunsAvailableText;

            return (
                <Table.Body>
                    <Table.Row warning key="no_runs">
                        <Table.Cell colSpan={10} textAlign="center">{rowText}</Table.Cell>
                    </Table.Row>
                </Table.Body>
            )
        }

        return <Table.Body>{tableRows}</Table.Body>
    }
}

export default props => {
    return <RunsConsumer>
        {runs => <Body {...props} runs={runs}/>}
    </RunsConsumer>
};
