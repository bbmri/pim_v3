/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React, {Component} from 'react'

import {
    Icon,
    Table
} from 'semantic-ui-react'

import {RunsConsumer} from 'ui/pages/runs/Runs'

import HelpContext from 'ui/miscellaneous/HelpContext'

import RemoveRunsModal from '../RemoveRuns'

class RemoveSelected extends Component {
    noSelected() {
        return this.props.runs.data.getIn(['table', 'pageRows']).reduce((noSelected, run) => {
            if (run.get('selected'))
                return noSelected + 1;

            return noSelected;
        }, 0);
    }

    status = () => {
        return `Remove selected run${this.noSelected() > 1 ? 's' : ''}`;
    };

    render() {
        let content = () => {
            if (this.noSelected() > 0) {
                if (this.props.runs.data.getIn(['config', 'confirmRemove'])) {
                    return <RemoveRunsModal trigger={<Icon name={'trash'} link fitted/>}/>
                } else {
                    return <Icon name={'trash'}
                                 link
                                 fitted
                                 onClick={() => this.props.runs.actions.removeSelected()}/>;
                }
            }

            return null;
        };

        return <HelpContext status={this.status}>
            <Table.HeaderCell>
                <div style={{display: 'flex', alignItems: 'center', justifyContent: 'center'}}>
                    {content()}
                </div>
            </Table.HeaderCell>
        </HelpContext>
    }
}

export default props => {
    return <RunsConsumer>
        {runs => <RemoveSelected {...props} runs={runs}/>}
    </RunsConsumer>
};