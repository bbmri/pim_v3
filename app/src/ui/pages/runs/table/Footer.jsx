/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React, {Component} from 'react'

import {
    Table
} from 'semantic-ui-react'

import {RunsConsumer} from 'ui/pages/runs/Runs'

import LastUpdated from 'ui/miscellaneous/LastUpdated'
import PageInfo from 'ui/pagination/PageInfo'

import RemoveSelected from './RemoveSelected'

class Footer extends Component {
    render() {
        const api = this.props.runs.data.get('api');

        return <Table.Footer>
            <Table.Row>
                <RemoveSelected {...this.props}/>
                <Table.HeaderCell colSpan={9}>
                    <div style={{display: 'flex', alignItems: 'center', width: '100%'}}>
                        <PageInfo rowType='run' table={this.props.runs.data.get('table')}/>
                        <div style={{flex: 1}}/>
                        <LastUpdated name='runs' lastUpdated={api.get('lastFetched')} updating={api.get('fetching')}/>
                    </div>
                </Table.HeaderCell>
            </Table.Row>
        </Table.Footer>
    }
}

export default props => {
    return <RunsConsumer>
        {runs => <Footer {...props} runs={runs}/>}
    </RunsConsumer>
};