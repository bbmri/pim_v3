/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React, {Component} from 'react'

import {
    Table
} from 'semantic-ui-react'

import {RunsConsumer} from 'ui/pages/runs/Runs'

import HeaderCell from 'ui/miscellaneous/table/cell/Header'

import SelectAll from './row/cell/SelectAll'

class Header extends Component {
    shouldComponentUpdate(nextProps, nextState, nextContext) {
        return nextProps.runs.data.get('table') !== this.props.runs.data.get('table');
    }

    render() {
        const props = {
            table: this.props.runs.data.get('table'),
            actions: this.props.runs.actions
        };

        const row = <Table.Row>
            <SelectAll/>
            <HeaderCell {...props} style={{width: 80}} sortColumn='status' title='Status' textAlign='center'/>
            <HeaderCell {...props} width={7} sortColumn='name' title='Name'/>
            <HeaderCell {...props} width={3} sortColumn='user' title='User'/>
            <HeaderCell {...props} style={{width: 70}} sortColumn='no_jobs' title='# Jobs'/>
            <HeaderCell {...props} style={{width: 85}} sortColumn='progress' title='Progress'/>
            <HeaderCell {...props} width={8} sortColumn='' title='Jobs'/>
            <HeaderCell {...props} style={{width: 100}} sortColumn='duration' title='Duration'/>
            <HeaderCell {...props} style={{width: 140}} sortColumn='created' title='Created'/>
            <HeaderCell {...props} style={{width: 140}} sortColumn='modified' title='Modified'/>
        </Table.Row>;

        return <Table.Header>{row}</Table.Header>;
    }
}

export default props => {
    return <RunsConsumer>
        {runs => <Header {...props} runs={runs}/>}
    </RunsConsumer>
};