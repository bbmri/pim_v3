/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React, {Component} from 'react'

import {
    Label
} from 'semantic-ui-react'

import {JobsConsumer} from 'ui/pages/jobs/Jobs'
import {NavigationConsumer} from 'ui/Navigation'

import TypeFilter from 'ui/miscellaneous/TypeFilter'

class Filter extends Component {
    shouldComponentUpdate(nextProps, nextState, nextContext) {
        if (nextProps.jobs.data.getIn(['config', 'aggregate']) !== this.props.jobs.data.getIn(['config', 'aggregate']))
            return true;

        return nextProps.jobs.data.getIn(['filter', 'status']) !== this.props.jobs.data.getIn(['filter', 'status']);
    }

    render() {
        const statusFilter = this.props.jobs.data.getIn(['filter', 'status']).toJS();
        const aggregate    = this.props.jobs.data.getIn(['config', 'aggregate']).toJS();

        let typeFilter = {};

        for (let type in statusFilter) {
            typeFilter[type] = {
                enabled: statusFilter[type],
                count: aggregate[type]
            }
        }

        return <div style={{display: 'flex', flexDirection: 'column'}}>
            <div style={{display: 'flex'}}>
                <div style={{display: 'flex', flexDirection: 'column', justifyContent: 'center', paddingRight: '5px'}}>
                    <Label pointing='right' size='tiny'>Filter by status</Label>
                </div>
                <TypeFilter filterable='job'
                            filter={typeFilter}
                            actions={this.props.jobs.actions}
                            {...this.props}/>
            </div>
        </div>
    }
}

export default props => {
    return <NavigationConsumer>
        {navigation => <JobsConsumer>
            {jobs => <Filter {...props} jobs={jobs} navigation={navigation}/>}
        </JobsConsumer>}
    </NavigationConsumer>
};