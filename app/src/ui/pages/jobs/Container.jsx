/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React, {Component} from 'react'
import {withRouter} from 'react-router-dom'

const queryString = require('query-string');

import {
    Segment,
    Message
} from 'semantic-ui-react'

import {JobsConsumer} from 'ui/pages/jobs/Jobs'
import {NavigationConsumer} from 'ui/Navigation'

import PageHeader from './PageHeader'
import Filter from './Filter'
import Table from './table/Table'

import ErrorBoundary from 'ui/miscellaneous/ErrorBoundary'
import NavigationBar from 'ui/miscellaneous/NavigationBar'
import FetchErrorMessage from 'ui/miscellaneous/FetchErrorMessage'
import Pagination from 'ui/pagination/Pagination'

import * as jobHelper from 'helpers/job'

class Container extends Component {
    componentDidMount() {
        logger.debug('Component did mount');

        const curParams = queryString.parse(this.props.location.search);

        if (this.props.match.params.run !== this.props.jobs.data.getIn(['config', 'runId'])) {
            this.props.jobs.actions.setRunId(this.props.match.params.run);
        }

        if (curParams.node !== undefined && curParams.node !== this.props.jobs.data.getIn(['config', 'nodeId'])) {
            this.props.jobs.actions.setNodeId(curParams.node);
        }

        if (curParams.statusFilter !== undefined) {
            const statusFilter = {};

            for (let statusFilterType of jobHelper.statusNames()) {
                statusFilter[statusFilterType] = JSON.parse(curParams.statusFilter).includes(statusFilterType);
            }

            this.props.jobs.actions.setStatusFilter(statusFilter);
        }

        this.updateQueryParameters();

        this.props.jobs.actions.startSynchronization();
    }

    componentWillUnmount() {
        this.props.jobs.actions.stopSynchronization();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        // logger.debug('Component did update');

        let updateHistory = false;

        if (this.props.jobs.data.getIn(['filter', 'status']) !== prevProps.jobs.data.getIn(['filter', 'status'])) {
            updateHistory = true;
        }

        if (this.props.jobs.data.getIn(['config', 'nodeId']) !== prevProps.jobs.data.getIn(['config', 'nodeId'])) {
            updateHistory = true;
        }

        if (updateHistory) {
            this.updateQueryParameters();
        }
    }

    updateQueryParameters() {
        logger.debug('Updating query parameters');

        const statusFilterMap = this.props.jobs.data.getIn(['filter', 'status']).toJS();
        const statusFilter    = [];

        for (let type in statusFilterMap) {
            if (statusFilterMap[type])
                statusFilter.push(type);
        }

        this.props.history.replace({
            path: '/jobs',
            search: `?node=${this.props.jobs.data.getIn(['config', 'nodeId'])}&statusFilter=${JSON.stringify(statusFilter)}`
        });
    }

    content() {
        const catchErrorUI = (message) => <Message attached='bottom' compact error>
            {message}
        </Message>;

        const table = this.props.jobs.data.get('table');

        const pagination = {
            rowType: 'job',
            noPages: table.get('noPages'),
            pageSize: table.get('pageSize'),
            pageIndex: table.get('pageIndex'),
            noPagesPerView: table.get('noPagesPerView'),
            actions: {
                setPageSize: this.props.jobs.actions.setPageSize,
                setPageIndex: this.props.jobs.actions.setPageIndex,
                firstPage: this.props.jobs.actions.firstPage,
                previousPage: this.props.jobs.actions.previousPage,
                nextPage: this.props.jobs.actions.nextPage,
                lastPage: this.props.jobs.actions.lastPage
            }
        };

        const fetchError = this.props.jobs.data.getIn(['api', 'fetchError']);

        let content = null;

        if (fetchError === null) {
            content = <React.Fragment>
                <Filter {...this.props}/>
                <Table name={'job'}/>
                <Pagination {...pagination}/>
            </React.Fragment>
        } else {
            content =
                <FetchErrorMessage subject={`${this.props.jobs.data.getIn(['config', 'runId'])} jobs`} error={fetchError}/>
        }

        return <ErrorBoundary content={catchErrorUI}>
            <Segment attached='bottom'>
                {content}
            </Segment>
        </ErrorBoundary>
    }

    render() {
        const runId = this.props.jobs.data.getIn(['config', 'runId']);

        const sections = [
            {label: 'pim', status: (props) => 'Pipeline Inspection and Monitoring'},
            {
                label: 'runs',
                action: () => this.props.navigation.toRunsPage(),
                status: (props) => 'Go the runs overview page'
            },
            {
                label: runId,
                action: () => this.props.navigation.toGraphPage({runId: runId}),
                status: (props) => 'Explore run in the graph viewer'
            },
            {label: 'jobs', active: true, status: (props) => `Viewing jobs for ${runId}`}
        ];

        return <div style={{flexGrow: 1, display: 'flex', flexDirection: 'column'}}>
            <PageHeader {...this.props}/>
            <NavigationBar attached='top' sections={sections}/>
            {this.content()}
            <div style={{height: 0}}/>
        </div>
    }
}

const ContainerWithRouter = withRouter(Container);

export default props => {
    return <NavigationConsumer>
        {navigation => <JobsConsumer>
            {jobs => <ContainerWithRouter {...props} jobs={jobs} navigation={navigation}/>}
        </JobsConsumer>}
    </NavigationConsumer>
};