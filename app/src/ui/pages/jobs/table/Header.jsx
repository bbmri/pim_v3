/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React, {Component} from 'react'

import {
    Table
} from 'semantic-ui-react'

import {JobsConsumer} from 'ui/pages/jobs/Jobs'

import HeaderCell from 'ui/miscellaneous/table/cell/Header'

class Header extends Component {
    render() {
        const props = {
            table: this.props.jobs.data.get('table'),
            actions: this.props.jobs.actions
        };

        const row = <Table.Row>
            <HeaderCell width={10} sortColumn='title' title='Name' {...props}/>
            <HeaderCell width={10} sortColumn='path' title='Node' {...props}/>
            <HeaderCell style={{width: 140}} sortColumn='status' title='Status' {...props}/>
            <HeaderCell style={{width: 100}} sortColumn='duration' title='Duration' {...props}/>
            <HeaderCell style={{width: 140}} sortColumn='created' title='Created' {...props}/>
            <HeaderCell style={{width: 140}} sortColumn='modified' title='Modified' {...props}/>
        </Table.Row>;

        return <Table.Header>{row}</Table.Header>;
    }
}

export default props => {
    return <JobsConsumer>
        {jobs => <Header {...props} jobs={jobs}/>}
    </JobsConsumer>
};