/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React, {Component} from 'react'

import {
    Table as SemanticUiTable
} from 'semantic-ui-react'

import Header from './Header'
import Body from './Body'
import Footer from './Footer'

export default class Table extends Component {
    render() {
        return <SemanticUiTable striped sortable selectable celled compact singleLine fixed size='small'>
            <Header/>
            <Body/>
            <Footer/>
        </SemanticUiTable>
    }
}