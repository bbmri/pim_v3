/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React, {Component} from 'react'

import {
    Table
} from 'semantic-ui-react'

import {JobsConsumer} from 'ui/pages/jobs/Jobs'

import Row from './row/Row'

class Body extends Component {
    render() {
        if (this.props.jobs.data.getIn(['api', 'noFetches']) === 0) {
            return null;
        }

        const pageRows = this.props.jobs.data.getIn(['table', 'pageRows']);

        if (pageRows.size === 0) {
            const noJobsAvailableText = `No jobs to display for `;

            return <Table.Body>
                <Table.Row warning key="no_jobs">
                    <Table.Cell key='no_jobs' colSpan="6" rowSpan={this.props.jobs.data.getIn(['table', 'pageSize'])}
                                textAlign="center">{noJobsAvailableText}
                        <b>{this.props.jobs.data.getIn(['config', 'nodeId'])}</b></Table.Cell>
                </Table.Row>
            </Table.Body>
        }

        let tableRows = pageRows.map(job => <Row key={job.get('path')} job={job} {...this.props}/>);

        return <Table.Body>{tableRows}</Table.Body>
    }
}

export default props => {
    return <JobsConsumer>
        {jobs => <Body {...props} jobs={jobs}/>}
    </JobsConsumer>
};