/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React, {Component} from 'react'

import {
    Table
} from 'semantic-ui-react'

import {JobsConsumer} from 'ui/pages/jobs/Jobs'

import LastUpdated from 'ui/miscellaneous/LastUpdated'
import PageInfo from 'ui/pagination/PageInfo'

class Footer extends Component {
    render() {
        const api = this.props.jobs.data.get('api');

        return <Table.Footer>
            <Table.Row>
                <Table.HeaderCell colSpan={6}>
                    <div style={{display: 'flex', alignItems: 'center', width: '100%'}}>
                        <PageInfo rowType='job' table={this.props.jobs.data.get('table')}/>
                        <div style={{flex: 1}}/>
                        <LastUpdated name='jobs' lastUpdated={api.get('lastFetched')} updating={api.get('fetching')}/>
                    </div>
                </Table.HeaderCell>
            </Table.Row>
        </Table.Footer>
    }
}

export default props => {
    return <JobsConsumer>
        {jobs => <Footer {...props} jobs={jobs}/>}
    </JobsConsumer>
};