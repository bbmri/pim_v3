/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React, {Component} from 'react'
import PropTypes from 'prop-types'

import {
    Table
} from 'semantic-ui-react'

import DateTimeCell from 'ui/miscellaneous/table/cell/DateTime'
import DurationCell from 'ui/miscellaneous/table/cell/Duration'

import TitleCell from './cell/Title'
import NodeCell from './cell/Node'
import StatusCell from './cell/Status'

export default class Row extends Component {
    shouldComponentUpdate(nextProps, nextState, nextContext) {
        return nextProps.job !== this.props.job;
    }

    render() {
        const transient = this.props.job.get('transient');
        const created   = this.props.job.get('created');
        const modified  = this.props.job.get('modified');
        const duration  = new Date(modified) - new Date(created);

        return <Table.Row key={this.props.job.get('path')}>
            <TitleCell {...this.props}/>
            <NodeCell {...this.props}/>
            <StatusCell {...this.props}/>
            <DurationCell subject={'job'} duration={duration} {...this.props} transient={transient}/>
            <DateTimeCell key='created' dateTime={created} statusPrefix={'Job created on'} transient={transient}/>
            <DateTimeCell key='modified' dateTime={modified} statusPrefix={'Job last modified on'}
                          transient={transient}/>
        </Table.Row>
    }
}

Row.propTypes = {
    job: PropTypes.object.isRequired
};