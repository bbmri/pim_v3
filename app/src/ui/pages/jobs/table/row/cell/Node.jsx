/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React, {Component} from 'react'
import PropTypes from 'prop-types'

import {JobsConsumer} from 'ui/pages/jobs/Jobs'
import {NavigationConsumer} from 'ui/Navigation'

import StringCell from 'ui/miscellaneous/table/cell/String'

class Node extends Component {
    viewNode() {
        const query = {
            runId: this.props.jobs.data.getIn(['config', 'runId']),
            nodeId: this.node()
        };

        this.props.navigation.toGraphPage(query);
    }

    status() {
        return `Click to inspect node in graph viewer`;
    }

    node() {
        return this.props.job.get('path').split('/').slice(1, -1).join('/');
    }

    render() {
        return <StringCell string={this.node()}
                           status={this.status()}
                           onClick={() => this.viewNode()}
                           transient={this.props.job.get('transient')}/>
    }
}

Node.propTypes = {
    job: PropTypes.object.isRequired
};

export default props => {
    return <NavigationConsumer>
        {navigation => <JobsConsumer>
            {jobs => <Node {...props} jobs={jobs} navigation={navigation}/>}
        </JobsConsumer>}
    </NavigationConsumer>
};