/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React, {Component} from 'react'
import PropTypes from 'prop-types'

import StringCell from 'ui/miscellaneous/table/cell/String'

import {NavigationConsumer} from 'ui/Navigation'

import * as jobHelper from 'helpers/job'

class Title extends Component {
    viewJob() {
        const query = {
            runId: this.props.jobs.data.getIn(['config', 'runId']),
            jobId: this.props.job.get('path').split('/').slice(1).join('/')
        };

        this.props.navigation.toJobPage({runId: query.runId, jobId: query.jobId});
    }

    title() {
        return this.props.job.get('title');
    }

    status() {
        const statusType        = jobHelper.statusName(this.props.job.get('status'));
        const statusDescription = jobHelper.statusDescription(statusType, 1);

        return `Job ${statusDescription}, click to inspect details`;
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        return nextProps.job !== this.props.job;
    }

    render() {
        return <StringCell string={this.title()}
                           status={this.status()}
                           onClick={() => this.viewJob()}
                           transient={this.props.job.get('transient')}/>
    }
}

Title.propTypes = {
    job: PropTypes.object.isRequired,
};

export default props => {
    return <NavigationConsumer>
        {navigation => <Title {...props} navigation={navigation}/>}
    </NavigationConsumer>
};