/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React, {Component} from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import {
    Popup,
    Table
} from 'semantic-ui-react'

import ProgressBar from 'ui/miscellaneous/ProgressBar'

import * as jobHelper from 'helpers/job'

import {JobsConsumer} from 'ui/pages/jobs/Jobs'

class Status extends Component {
    render() {
        const status = jobHelper.statusName(this.props.job.get('status'));

        let items = {};

        items[status] = {
            label: <StyledStateLabel>{status}</StyledStateLabel>,
            count: 1
        };

        const cellStyle = {
            padding: '8px'
        };

        const containerStyle = {
            position: 'relative',
            height: '15px'
        };

        const handlers = {
            status: (bar) => {
                return `Job ${jobHelper.statusDescription(bar.type, 1)}`;
            },
            popup: (trigger, bar) => {
                return <Popup trigger={trigger}
                              content={`Job ${jobHelper.statusDescription(bar.type, 1)}`}
                              position='top center'
                              size='mini'
                              inverted/>
            }
        };

        return <Table.Cell key='status' textAlign='center' style={cellStyle}>
            <div style={containerStyle} className={this.props.job.get('transient') ? 'desaturate' : ''}>
                <ProgressBar radius={5}
                             items={items}
                             runId={this.props.jobs.data.getIn(['config', 'runId'])}
                             {...handlers}/>
            </div>
        </Table.Cell>
    }
}

Status.propTypes = {
    job: PropTypes.object.isRequired
};

const StyledStateLabel = styled.div`
    color: white;
    font-size: 9px;
    font-weight: bold;
    pointer-events: none;
    cursor: default;
`;

export default props => {
    return <JobsConsumer>
        {jobs => <Status {...props} jobs={jobs}/>}
    </JobsConsumer>
};