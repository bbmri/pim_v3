/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React from 'react'

import {withCookies} from 'react-cookie'

import Immutable from 'immutable'

import AppApi from 'api/api'

const JobsContext = React.createContext({});

export const JobsProvider = JobsContext.Provider;
export const JobsConsumer = JobsContext.Consumer;

const defaultJobs = {
    config: {
        runId: '',
        nodeId: '',
        aggregate: {
            idle: 0,
            running: 0,
            success: 0,
            failed: 0,
            cancelled: 0,
            undefined: 0
        },
        updateInterval: 2500
    },
    table: {
        sortColumn: 'name',
        sortDirection: 'ascending',
        noPages: 0,
        pageSize: 10,
        pageIndex: 0,
        noPagesPerView: 5,
        pageRows: [],
        noRows: 0,
        noFilteredRows: 0
    },
    filter: {
        name: '',
        user: '',
        status: {
            idle: true,
            running: true,
            success: true,
            failed: true,
            cancelled: true,
            undefined: true
        }
    },
    api: {
        fetching: false,
        noFetches: 0,
        fetchResponse: null,
        fetchError: null,
        lastFetched: ''
    }
};

class Jobs extends React.Component {
    constructor(props) {
        super(props);

        this.data             = Immutable.fromJS(defaultJobs);
        this.synchronizeTimer = null;
    }

    componentWillMount() {
        this.readCookies();
    }

    componentDidMount() {
        window.addEventListener('beforeunload', this.writeCookies.bind(this));
    }

    readCookies() {
        logger.debug('Loading settings from cookies');

        this.setSortColumn(this.props.cookies.get('jobsSortColumn') || 'name', false);
        this.setSortDirection(this.props.cookies.get('jobsSortDirection') || 'ascending', false);
        this.setPageSize(parseInt(this.props.cookies.get('jobsPageSize')) || 10, false);
        this.setPageIndex(parseInt(this.props.cookies.get('jobsPageIndex')) || 0, false);
    }

    writeCookies() {
        logger.debug('Writing settings to cookies');

        const table = this.data.get('table');

        this.props.cookies.set('jobsSortColumn', table.get('sortColumn'));
        this.props.cookies.set('jobsSortDirection', table.get('sortDirection'));
        this.props.cookies.set('jobsPageSize', table.get('pageSize'));
        this.props.cookies.set('jobsPageIndex', Math.max(0, table.get('pageIndex')).toString());
    }

    startSynchronization = () => {
        logger.debug(`Start synchronization`);

        this.stopSynchronization();

        if (this.synchronizeTimer !== null)
            clearInterval(this.synchronizeTimer);

        if (this.synchronizeTimer === null) {
            this.synchronizeTimer = setInterval(this.synchronize, this.data.getIn(['config', 'updateInterval']));
        }
    };

    stopSynchronization = () => {
        logger.debug(`Stop synchronization`);

        if (this.synchronizeTimer !== null) {
            clearInterval(this.synchronizeTimer);
            this.synchronizeTimer = null;
        }
    };

    synchronize = () => {
        logger.debug(`Synchronizing`);

        if (this.data.getIn(['api', 'fetching']))
            return;

        this._fetchJobs();
    };

    setData = (data) => {
        this.data = null;
        this.data = data;
    };

    setRunId = (runId) => {
        logger.debug(`Set run identifier: ${runId}`);

        this.setData(this.data.setIn(['config', 'runId'], runId));
    };

    setNodeId = (nodeId) => {
        logger.debug(`Set node identifier: ${nodeId}`);

        this.setData(this.data.setIn(['config', 'nodeId'], nodeId));
    };

    toggleStatusFilter = (type, synchronize = true) => {
        logger.debug(`Toggle status filter`);

        this._setTransient();

        const status  = this.data.getIn(['filter', 'status', type]);
        const newData = this.data.setIn(['filter', 'status', type], !status);

        this.setData(newData);

        if (synchronize)
            this.synchronize();
    };

    setStatusFilter = (filter, synchronize = true) => {
        logger.debug(`Set status filter`);

        this._setTransient();

        this.setData(this.data.setIn(['filter', 'status'], Immutable.fromJS(filter)));

        if (synchronize)
            this.synchronize();
    };

    setSoloStatusFilter = (statusType, synchronize = true) => {
        logger.debug(`Set solo status filter`);

        this._setTransient();

        const newFilter = this.data.get('filter').update('status', item => item.map((value, type) => {
            return type === statusType;
        }));

        this.setData(this.data.set('filter', newFilter));

        if (synchronize)
            this.synchronize();
    };

    toggleSortColumn = (sortColumn, synchronize = true) => {
        logger.debug(`Toggle sort column`);

        let sortDirection = 'ascending';

        if (this.data.getIn(['table', 'sortColumn']) === sortColumn) {
            sortDirection = this.data.getIn(['table', 'sortDirection']) === 'ascending' ? 'descending' : 'ascending';
        }

        this.setSortColumn(sortColumn, false);
        this.setSortDirection(sortDirection, false);

        if (synchronize)
            this.synchronize();
    };

    setSortColumn = (sortColumn, synchronize = true) => {
        logger.debug(`Set sort column: ${sortColumn}`);

        this._setTransient();

        this.setData(this.data.setIn(['table', 'sortColumn'], sortColumn));

        this.setPageIndex(0, false);

        if (synchronize)
            this.synchronize();

        this.forceUpdate();
    };

    setSortDirection = (sortDirection, synchronize = true) => {
        logger.debug(`Set sort direction: ${sortDirection}`);

        this._setTransient();

        this.setData(this.data.setIn(['table', 'sortDirection'], sortDirection));

        this.setPageIndex(0, false);

        if (synchronize)
            this.synchronize();

        this.forceUpdate();
    };

    setPageSize = (pageSize, synchronize = true) => {
        logger.debug(`Set page size: ${pageSize}`);

        this._setTransient();

        this.setData(this.data.setIn(['table', 'pageSize'], pageSize));

        this.setPageIndex(0, false);

        if (synchronize)
            this.synchronize();

        this.forceUpdate();
    };

    setPageIndex = (pageIndex, synchronize = true) => {
        logger.debug(`Set page index: ${pageIndex}`);

        this._setTransient();

        this.setData(this.data.setIn(['table', 'pageIndex'], pageIndex));

        if (synchronize)
            this.synchronize();

        this.forceUpdate();
    };

    firstPage = (synchronize = true) => {
        logger.debug(`First page`);

        this._setTransient();

        this.setPageIndex(0);

        if (synchronize)
            this.synchronize();

        this.forceUpdate();
    };

    previousPage = (synchronize = true) => {
        logger.debug(`Previous page`);

        this._setTransient();
        this.setPageIndex(this.data.getIn(['table', 'pageIndex']) - 1);

        if (synchronize)
            this.synchronize();

        this.forceUpdate();
    };

    nextPage = (synchronize = true) => {
        logger.debug(`Next page`);

        this._setTransient();
        this.setPageIndex(this.data.getIn(['table', 'pageIndex']) + 1);

        if (synchronize)
            this.synchronize();

        this.forceUpdate();
    };

    lastPage = (synchronize = true) => {
        logger.debug(`Last page`);

        this._setTransient();
        this.setPageIndex(this.data.getIn(['table', 'noPages']) - 1);

        if (synchronize)
            this.synchronize();

        this.forceUpdate();
    };

    _fetchJobs = () => {
        logger.debug(`Fetching jobs`);

        const config = this.data.get('config');
        const table  = this.data.get('table');
        const filter = this.data.get('filter');

        if (config.get('runId') === '') {
            return;
        }

        let statusFilterMap = filter.get('status').toJS();
        let statusFilter    = [];

        for (let key in statusFilterMap) {
            if (statusFilterMap[key]) {
                statusFilter.push(key);
            }
        }

        const params = {
            runId: config.get('runId'),
            nodeId: config.get('nodeId'),
            recursive: true,
            filter: JSON.stringify(statusFilter),
            sortColumn: table.get('sortColumn'),
            sortDirection: table.get('sortDirection'),
            pageSize: table.get('pageSize'),
            pageIndex: table.get('pageIndex')
        };

        this._setApiFetching(true);

        AppApi.fetchJobs(params)
            .then(response => {
                this._setApiSuccess(response);
            })
            .catch(error => {
                this._setApiError(error);
            });

        this.forceUpdate();
    };

    _updatePagination = (noFilteredRows) => {
        const noPages   = Math.ceil(noFilteredRows / this.data.getIn(['table', 'pageSize']));
        const pageIndex = Math.min(Math.max(0, noPages - 1), this.data.getIn(['table', 'pageIndex']));

        let newTable = this.data.get('table');

        newTable = newTable.set('noPages', noPages);
        newTable = newTable.set('pageIndex', pageIndex);
        newTable = newTable.set('noFilteredRows', noFilteredRows);

        this.setData(this.data.set('table', newTable));
    };

    _setPageRows = (pageRows) => {
        let selection = this.data.getIn(['table', 'pageRows']).reduce((accum, row) => {
            if (row.get('selected'))
                accum.add(row.get('name'));

            return accum;
        }, new Set());

        for (let row of pageRows) {
            row.transient = false;
            row.removing  = false;
            row.selected  = selection.has(row.name);
        }

        this.setData(this.data.setIn(['table', 'pageRows'], Immutable.fromJS(pageRows)));
    };

    _setNoRows = (noRows) => {
        this.setData(this.data.setIn(['table', 'noRows'], noRows));
    };

    _setTransient = () => {
        this.setData(this.data.updateIn(['table', 'pageRows'], item => item.map(row => {
            return row.set('transient', true);
        })));
    };

    _setApiFetching = (fetching) => {
        logger.debug(`Set API fetching`);

        this.setData(this.data.setIn(['api', 'fetching'], fetching));

        this.forceUpdate();
    };

    _setApiResponse = (apiResponse) => {
        logger.debug(`Set API response`);

        let api = this.data.get('api');

        api = api.set('fetching', false);
        api = api.set('noFetches', this.data.getIn(['api', 'noFetches']) + 1);
        api = api.set('fetchResponse', apiResponse);
        api = api.set('fetchError', null);
        api = api.set('lastFetched', new Date().toUTCString());

        this.setData(this.data.set('api', api));
    };

    _setApiError = (fetchError) => {
        logger.debug(`Set API error`);

        let api = this.data.get('api');

        api = api.set('fetching', false);
        api = api.set('fetchResponse', null);
        api = api.set('fetchError', fetchError);
        api = api.set('lastFetched', new Date().toUTCString());

        this.setData(this.data.set('api', api));
    };

    _setApiSuccess = (response) => {
        logger.debug(`Set API success`);

        this._setPageRows(response.data.jobs);
        this._setNoRows(response.data.noJobs);
        this._updatePagination(response.data.noFilteredJobs);

        this._setApiFetching(false);
        this._setApiResponse(response);

        const aggregate = {
            idle: response.data.aggregate[0],
            running: response.data.aggregate[1],
            success: response.data.aggregate[2],
            failed: response.data.aggregate[3],
            cancelled: response.data.aggregate[4],
            undefined: response.data.aggregate[5]
        };

        this.setData(this.data.setIn(['config', 'aggregate'], Immutable.fromJS(aggregate)));

        this.forceUpdate();
    };

    render() {
        const value = {
            data: this.data,
            actions: {
                setRunId: this.setRunId,
                setNodeId: this.setNodeId,
                toggleStatusFilter: this.toggleStatusFilter,
                setStatusFilter: this.setStatusFilter,
                setSoloStatusFilter: this.setSoloStatusFilter,
                toggleSortColumn: this.toggleSortColumn,
                setSortColumn: this.setSortColumn,
                setSortDirection: this.setSortDirection,
                setPageSize: this.setPageSize,
                setPageIndex: this.setPageIndex,
                firstPage: this.firstPage,
                previousPage: this.previousPage,
                nextPage: this.nextPage,
                lastPage: this.lastPage,
                synchronize: this.synchronize,
                startSynchronization: this.startSynchronization,
                stopSynchronization: this.stopSynchronization
            }
        };

        return <JobsProvider value={value}>
            {this.props.children}
        </JobsProvider>
    }
}

export default withCookies(Jobs);