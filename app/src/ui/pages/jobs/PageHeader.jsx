/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React from 'react'

import {
    Icon,
    Header
} from 'semantic-ui-react'

export default class PageHeader extends React.Component {
    render() {
        return <Header icon textAlign='center' size='small'>
            <Icon name='tasks' color='black'/>Jobs
            <Header.Subheader>Inspect <b>{this.props.jobs.data.getIn(['config', 'runId'])}</b> jobs</Header.Subheader>
        </Header>
    }
}