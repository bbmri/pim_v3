/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React, {Component} from 'react'
import {withRouter} from 'react-router-dom'

import PubSub from 'pubsub-js'

const queryString = require('query-string');

import {
    Segment,
    Message,
    Button,
    Loader
} from 'semantic-ui-react'

import {JobConsumer} from './Job'
import {NavigationConsumer} from 'ui/Navigation'

import Log from 'ui/miscellaneous/Log'
import ErrorBoundary from 'ui/miscellaneous/ErrorBoundary'
import NavigationBar from 'ui/miscellaneous/NavigationBar'
import FetchErrorMessage from 'ui/miscellaneous/FetchErrorMessage'
import HelpContext from 'ui/miscellaneous/HelpContext'
import StringFilter from 'ui/miscellaneous/StringFilter'
import JsonTree from 'ui/miscellaneous/JsonTree'
import LastUpdated from 'ui/miscellaneous/LastUpdated'

import PageHeader from './PageHeader'

class Container extends Component {
    constructor(props) {
        super(props);

        this.state = {
            viewers: ['tree'],
            viewer: 'tree'
        };

        this.logViewerRef = React.createRef();
    }

    componentDidMount() {
        logger.debug(`Component did mount`);

        const curParams = queryString.parse(this.props.location.search);

        if (this.props.match.params.run !== this.props.job.data.getIn(['config', 'runId'])) {
            this.props.job.actions.setRunId(this.props.match.params.run);
        }

        if (curParams.path !== this.props.job.data.getIn(['config', 'jobId'])) {
            this.props.job.actions.setJobId(curParams.path);
        }

        if (curParams.view !== undefined) {
            switch (curParams.view) {
                case 'tree': {
                    this.state.viewer = 'tree';
                    break;
                }

                case 'log': {
                    this.state.viewer = 'log';
                    break;
                }

                case 'stdout': {
                    this.state.viewer = 'stdout';
                    break;
                }

                case 'stderr': {
                    this.state.viewer = 'stderr';
                    break;
                }
            }
        }

        this.props.job.actions.startSynchronization();
    }

    componentWillUnmount() {
        this.props.job.actions.stopSynchronization();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.state.viewer !== prevState.viewer) {
            this.updateQueryParameters();
        }
    }

    updateQueryParameters() {
        logger.debug('Updating query parameters');

        this.props.history.replace({
            path: '/job',
            search: `?path=${this.props.job.data.getIn(['config', 'jobId'])}&view=${this.state.viewer}`
        });
    }

    _hasLog = (type) => {
        const customData = this.props.job.data.getIn(['job', 'customData']);

        if (customData === undefined)
            return false;

        const log = customData.get(type);

        if (log === undefined)
            return false;

        if (Array.isArray(log)) {
            return log.length > 0;
        } else {
            if (typeof log === 'object') {
                return Object.keys(log).length > 0;
            }
        }

        return false;
    };

    _toolbar = () => {
        let buttons = [];

        const ViewerButton = ({viewer, label, status = ''}) => {
            return <HelpContext key={viewer} status={status}>
                <Button basic
                        compact
                        size='tiny'
                        onClick={() => this.setState({viewer: viewer})}
                        active={this.state.viewer === viewer}>
                    {label}
                </Button>
            </HelpContext>
        };

        buttons.push(<ViewerButton viewer='tree' label='JSON view' status={'Switch to JSON view'}/>);

        if (this._hasLog('__log__')) {
            buttons.push(<div style={{width: 5}}/>);
            buttons.push(<ViewerButton viewer='log' label='Log view' status='Switch to log output'/>);
        }

        if (this._hasLog('__stdout__')) {
            buttons.push(<div style={{width: 5}}/>);
            buttons.push(<ViewerButton viewer='stdout' label='Standard out viewer'
                                       status='Switch to standard output'/>);
        }

        if (this._hasLog('__stderr__')) {
            buttons.push(<div style={{width: 5}}/>);
            buttons.push(<ViewerButton viewer='stderr' label='Standard error viewer'
                                       status='Switch to standard error'/>);
        }

        return <Segment disabled={this.props.job.data.getIn(['api', 'noFetches']) === 0}
                        attached='top'
                        secondary
                        compact
                        style={{display: 'flex'}}>
            {buttons}
        </Segment>
    };

    _treeViewer = (job) => {
        const style = {
            flex: 1,
            minHeight: 0,
            overflow: 'auto'
        };

        const handlers = {
            onClick: (node) => {
                if (this.props.job.isExpanded(node.id) === undefined) {
                    this.props.job.setExpanded(node.id, true);
                } else {
                    this.props.job.setExpanded(node.id, !this.props.job.isExpanded(node.id));
                }
            },
            onMouseEnter: (node, level, expanded, noChildren) => {
                const canExpandCollapse = typeof node.data === 'object' || Array.isArray(node.data);

                if (canExpandCollapse && Object.keys(node.data).length > 0) {
                    PubSub.publish('STATUS_SET', `${node.title}, click to ${expanded ? 'collapse' : 'expand'}`);
                } else {
                    PubSub.publish('STATUS_SET', `${node.title}: ${node.data}`);
                }
            },
            onMouseLeave: (node, level, expanded, noChildren) => {
                PubSub.publish('STATUS_UNSET');
            },
            expanded: (node) => {
                return this.props.job.isExpanded(node.id);
            }
        };

        return <Segment attached style={style}>
            <JsonTree json={job} title={job.title} {...handlers}/>
        </Segment>
    };

    _logViewer = (logType) => {
        const log = this.props.job.data.getIn(['job', 'customData', `__${logType}__`]).toJS();

        const style = {
            flex: 1,
            minHeight: 0,
            overflow: 'auto',
            padding: 0,
            display: 'flex'
        };

        let items = [];

        if (Array.isArray(log)) {
            for (let line of log) {

                items.push({
                    message: line
                })
            }
        } else {
            if (typeof log === 'object') {
                const sorted = Object.keys(log).sort();

                for (let timestamp of sorted) {
                    items.push({
                        ...log[timestamp],
                        timestamp: timestamp.split('.')[0]
                    })
                }
            }
        }

        const viewer = <Segment attached style={style}>
            <Log items={items}
                 scrollToBottom={this.props.job.data.getIn(['api', 'noFetches']) === 1}
                 filter={this.props.job.data.getIn(['config', 'logViewer', 'filter'])}
                 ref={this.logViewerRef}/>
        </Segment>;

        const toolbar = <Segment attached
                                 compact
                                 secondary
                                 style={{display: 'flex', justifyContent: 'space-between', alignItems: 'center'}}>
            <HelpContext status={'Filter log messages'}>
                <StringFilter name='message'
                              icon='filter'
                              filter={this.props.job.data.getIn(['config', 'logViewer', 'filter'])}
                              setFilter={this.props.job.setLogFilter}
                              commitFilter={() => null}
                              commitInterval={5000}
                              size='mini'/>
            </HelpContext>
            <div>
                <HelpContext status={'Go to the beginning of the log output'}>
                    <Button basic
                            compact
                            size='tiny'
                            icon='angle double up'
                            onClick={() => this.logViewerRef.current.toTop()}/>
                </HelpContext>
                <HelpContext status={'Go to the end of the log output'}>
                    <Button basic
                            compact
                            size='tiny'
                            icon='angle double down'
                            onClick={() => this.logViewerRef.current.toBottom()}/>
                </HelpContext>
            </div>
        </Segment>;

        return [toolbar, viewer];
    };

    _content = () => {
        const api = this.props.job.data.get('api');
        const job = this.props.job.data.get('job').toJS();

        if (api.get('fetchError') !== null) {
            return <FetchErrorMessage subject={this.props.job.data.getIn(['config', 'jobId'])}
                                      error={api.get('fetchError')}/>
        }

        const viewerType = this._hasLog(`__${this.state.viewer}__`) ? this.state.viewer : 'tree';
        const viewer     = viewerType === 'tree' ? this._treeViewer(job) : this._logViewer(viewerType);

        const footer = <Segment attached='bottom' secondary
                                style={{display: 'flex', alignItems: 'center', justifyContent: 'flex-end'}}>
            <LastUpdated name='job' updating={api.get('fetching')} lastUpdated={api.get('lastFetched')}/>
        </Segment>;

        return <div style={{flex: 1, minHeight: 0, margin: 1, display: 'flex', flexDirection: 'column'}}>
            {this._toolbar()}
            {api.get('noFetches') > 0 ? viewer : <Segment attached><Loader active/></Segment>}
            {footer}
        </div>
    };

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        if (nextState !== this.state)
            return true;

        return nextProps.job.data !== this.props.job.data;
    }

    render() {
        const runId = this.props.job.data.getIn(['config', 'runId']);

        const sections = [
            {label: 'pim', status: (props) => 'Pipeline Inspection and Monitoring'},
            {
                label: 'runs',
                action: () => this.props.navigation.toRunsPage(),
                status: (props) => 'Go the runs overview page'
            },
            {
                label: runId,
                action: () => this.props.navigation.toGraphPage({runId: runId}),
                status: (props) => 'Explore run in the graph viewer'
            },
            {
                label: 'jobs',
                action: () => this.props.navigation.toJobsPage({runId: runId}),
                status: (props) => 'Go the jobs page'
            }
        ];

        const catchErrorUI = (message) => <Message attached='bottom' compact error>
            {message}
        </Message>;

        return <div style={{flexGrow: 1, display: 'flex', flexDirection: 'column'}}>
            <PageHeader {...this.props}/>
            <NavigationBar sections={sections} style={{marginBottom: 6}}/>
            <ErrorBoundary content={catchErrorUI}>
                {this._content()}
            </ErrorBoundary>
            <div style={{height: 6}}/>
        </div>
    }
}

const ContainerWithRouter = withRouter(Container);

export default props => {
    return <NavigationConsumer>
        {navigation => <JobConsumer>
            {job => <ContainerWithRouter {...props} job={job} navigation={navigation}/>}
        </JobConsumer>}
    </NavigationConsumer>
};