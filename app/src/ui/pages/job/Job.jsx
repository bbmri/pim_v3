/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React from 'react'

import {withCookies} from 'react-cookie'

import Immutable from 'immutable'

import AppApi from 'api/api'

const JobContext = React.createContext({});

export const JobProvider = JobContext.Provider;
export const JobConsumer = JobContext.Consumer;

const defaultJob = {
    config: {
        runId: '',
        jobId: '',
        updateInterval: 3000,
        autoUpdate: true,
        logViewer: {
            filter: ''
        }
    },
    job: {},
    expanded: {},
    api: {
        fetching: false,
        noFetches: 0,
        fetchResponse: null,
        fetchError: null,
        lastFetched: ''
    }
};

class Job extends React.Component {
    constructor(props) {
        super(props);

        this.data             = Immutable.fromJS(defaultJob);
        this.synchronizeTimer = null;
    }

    componentWillMount() {
        this.readCookies();
    }

    componentDidMount() {
        window.addEventListener('beforeunload', this.writeCookies.bind(this));
    }

    readCookies() {
        logger.debug('Loading settings from cookies');

        this.setAutoUpdate(this.props.cookies.get('jobAutoUpdate') === 'true');
    }

    writeCookies() {
        logger.debug('Writing settings to cookies');

        this.props.cookies.set('jobAutoUpdate', this.data.getIn(['config', 'autoUpdate']));
    }

    startSynchronization = () => {
        logger.debug(`Start synchronization`);

        if (this.synchronizeTimer !== null)
            clearInterval(this.synchronizeTimer);

        if (this.synchronizeTimer === null) {
            this.synchronizeTimer = setInterval(this.synchronize, this.data.getIn(['config', 'updateInterval']));
        }

        this.synchronize();
    };

    stopSynchronization = () => {
        logger.debug(`Stop synchronization`);

        if (this.synchronizeTimer !== null) {
            clearInterval(this.synchronizeTimer);
            this.synchronizeTimer = null;
        }
    };

    synchronize = () => {
        logger.debug(`Synchronizing`);

        if (this.data.getIn(['api', 'fetching']))
            return;

        this._fetchJob();
    };

    setData = (data) => {
        this.data = null;
        this.data = data;
    };

    setRunId = (runId) => {
        logger.debug(`Set run identifier: ${runId}`);

        this.setData(this.data.setIn(['config', 'runId'], runId));
    };

    setJobId = (jobId) => {
        logger.debug(`Set job identifier: ${jobId}`);

        this.setData(this.data.setIn(['config', 'jobId'], jobId));
    };

    setAutoUpdate = (autoUpdate) => {
        logger.debug(`Set auto update: ${autoUpdate}`);

        this.setData(this.data.setIn(['config', 'autoUpdate'], autoUpdate));
    };

    toggleAutoUpdate = (autoUpdate) => {
        logger.debug(`Toggle auto update: ${autoUpdate}`);

        this.setData(this.data.setIn(['config', 'autoUpdate'], !this.data.getIn(['config', 'autoUpdate'])));
    };

    setLogFilter = (logFilter) => {
        this.setData(this.data.setIn(['config', 'logViewer', 'filter'], logFilter));

        this.forceUpdate();
    };

    isExpanded = (nodeId) => {
        return this.data.getIn(['expanded', nodeId]);
    };

    setExpanded = (nodeId, expanded) => {
        this.setData(this.data.setIn(['expanded', nodeId], expanded));

        this.forceUpdate();
    };

    _fetchJob = () => {
        logger.debug(`Fetching`);

        const config = this.data.get('config');
        const api    = this.data.get('api');

        if (this.data.getIn(['config', 'runId']) === '' || this.data.getIn(['config', 'jobId']) === '')
            return;

        if (api.get('fetching'))
            return;

        const params = {
            runId: config.get('runId'),
            jobId: config.get('jobId')
        };

        this._setApiFetching(true);

        AppApi.fetchJob(params)
            .then(response => {
                this._setJob(response.data.job);
                this._setApiFetching(false);
                this._setApiResponse(response);

                this.forceUpdate();
            })
            .catch(error => {
                this._setApiError(error);
            });
    };

    _sanitizeJob = (job) => {
        for (let property in job) {
            if (job.hasOwnProperty(property)) {
                switch (typeof job[property]) {
                    case 'object': {
                        this._sanitizeJob(job[property])
                        break;
                    }

                    case 'string': {
                        const segments = job[property].split('\n');

                        if (segments.length > 1) {
                            job[property] = segments;
                        }

                        break;
                    }
                }
            }
        }

        return job;
    };

    _setJob = (job) => {
        logger.debug(`Set job`);

        const sanitizedJob = this._sanitizeJob(job);

        this.setData(this.data.set('job', Immutable.fromJS(sanitizedJob)));
    };

    _setApiFetching = (fetching) => {
        logger.debug(`Set API fetching`);

        this.setData(this.data.setIn(['api', 'fetching'], fetching));

        this.forceUpdate();
    };

    _setApiResponse = (apiResponse) => {
        logger.debug(`Set API response`);

        let api = this.data.get('api');

        api = api.set('fetching', false);
        api = api.set('noFetches', api.get('noFetches') + 1);
        api = api.set('fetchResponse', apiResponse);
        api = api.set('fetchError', null);
        api = api.set('lastFetched', new Date().toUTCString());

        this.setData(this.data.set('api', api));
    };

    _setApiError = (fetchError) => {
        logger.debug(`Set API error`);

        let api = this.data.get('api');

        api = api.set('fetching', false);
        api = api.set('fetchResponse', null);
        api = api.set('fetchError', fetchError);
        api = api.set('lastFetched', new Date().toUTCString());

        this.setData(this.data.set('api', api));
    };

    render() {
        const value = {
            data: this.data,
            actions: {
                setRunId: this.setRunId,
                setJobId: this.setJobId,
                synchronize: this.synchronize,
                startSynchronization: this.startSynchronization,
                stopSynchronization: this.stopSynchronization
            },
            setLogFilter: this.setLogFilter,
            setAutoUpdate: this.setAutoUpdate,
            toggleAutoUpdate: this.toggleAutoUpdate,
            isExpanded: this.isExpanded,
            setExpanded: this.setExpanded
        };

        return <JobProvider value={value}>
            {this.props.children}
        </JobProvider>
    }
}

export default withCookies(Job);