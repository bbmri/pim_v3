/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React from 'react'

import {
    Icon,
    Header
} from 'semantic-ui-react'

import {JobConsumer} from './Job'

class PageHeader extends React.Component {
    render() {
        return <Header icon textAlign='center' size='small'>
            <Icon name='cog' color='black'/>Inspect job
            <Header.Subheader>
                {this.props.job.data.getIn(['config', 'jobId']).split('/').slice(-1)}
            </Header.Subheader>
        </Header>
    }
}

export default props => {
    return <JobConsumer>
        {job => <PageHeader {...props} job={job}/>}
    </JobConsumer>
};