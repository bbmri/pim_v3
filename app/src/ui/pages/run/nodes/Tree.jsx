/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React from 'react'
import styled from 'styled-components'

import PubSub from 'pubsub-js'

import {
    Icon
} from 'semantic-ui-react'

const getUuid = require('uuid-by-string');

import {RunConsumer} from 'ui/pages/run/Run'

import Node from 'ui/miscellaneous/Node'

import Title from './Title'
import Context from './Context'

import {NavigationConsumer} from 'ui/Navigation'

class Tree extends React.Component {
    render() {
        if (this.props.run.data.get('nodes').size === 0) {
            return <div/>;
        }

        const props = {
            ...this.props,
            onClick: node => {
                if (node.children.length > 0) {
                    if (node.expanded) {
                        this.props.run.actions.collapseNode(node.id);
                    } else {
                        this.props.run.actions.expandNode(node.id);
                    }
                } else {
                    if (node.active) {
                        this.props.run.actions.zoomToNode(node.id);
                    } else {
                        this.props.run.actions.revealNode(node.id);
                    }
                }
            },
            onMouseEnter: node => {
                this.props.run.actions.nodeReceiveFocus(node.id);

                const noChildren = node.children.length;

                if (noChildren > 0) {
                    const action = node.expanded ? 'collapse' : 'expand';
                    PubSub.publish('STATUS_SET', `${node.title} has ${noChildren} child${noChildren > 1 ? 'ren' : ''}, click to ${action}`);
                } else {
                    PubSub.publish('STATUS_SET', `Click to zoom to ${node.title}`);
                }
            },
            onMouseLeave: node => {
                this.props.run.actions.nodeLooseFocus(node.id);

                PubSub.publish('STATUS_UNSET');
            },
            id: (node) => {
                return node.id;
            },
            children: (parent, node) => {
                return node.children.map(childNodeId => {
                    return this.props.run.data.getIn(['nodes', childNodeId]).toJS();
                });
            },
            icon: (node, level) => {
                let icon = '';

                if (level === 0) {
                    icon = 'home';
                } else {
                    if (node.children.length === 0) {
                        icon = '';
                    } else {
                        if (node.expanded)
                            icon = 'caret down';
                        else
                            icon = 'caret right';
                    }
                }

                return <Icon name={icon}/>;
            },
            title: (node, level) => {
                return <Title node={node}/>
            },
            context: (node, level) => {
                return <Context node={node} {...this.props}/>
            },
            color: node => {
                return this.props.run.nodeStatus(node.id).getIn(['jobs', 3]) > 0 ? 'hsl(0, 70%, 40%)' : 'black';
            },
            expanded: node => {
                return node.expanded;
            }
        };

        const rootNode = this.props.run.data.getIn(['nodes', getUuid('root')]).toJS();

        return <StyledNodesTree>
            <Node node={rootNode} level={0} tree={true} {...props}/>
        </StyledNodesTree>;
    }
}

export default props => {
    return <NavigationConsumer>
        {navigation => <RunConsumer>
            {run => <Tree {...props} run={run} navigation={navigation}/>}
        </RunConsumer>}
    </NavigationConsumer>
};

const StyledNodesTree = styled.div`
    width: 100%;
    //flex: 1;
    //min-height: 0;
    //max-height: 100%;
`;