/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React from 'react'
import styled from 'styled-components'

import {
    Icon,
    Message
} from 'semantic-ui-react'

import HelpContext from 'ui/miscellaneous/HelpContext'

import Node from '../../../miscellaneous/Node'
import Title from './Title'
import Context from './Context'

import {RunConsumer} from 'ui/pages/run/Run'
import PubSub from "pubsub-js";

class List extends React.Component {
    filterNodes() {
        return this.props.run.data.get('nodes').filter((value, key) => {
            const node_name = value.get('path').split('/').slice(-1)[0];
            return node_name.toUpperCase().includes(this.props.run.data.getIn(['filter', 'node']).toUpperCase());
        });
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (this.props.run.data.get('nodes') !== nextProps.run.data.get('nodes'))
            return true;

        return this.props.run.data.getIn(['filter', 'node']) !== nextProps.run.data.getIn(['filter', 'node']);
    }

    render() {
        const filteredNodes = this.filterNodes();

        if (filteredNodes.size === 0) {
            return <Message warning size='mini' style={{width: '100%', textAlign: 'center'}}>No nodes found that
                match <b>{this.props.run.data.getIn(['filter', 'node'])}</b></Message>
        }

        const props = {
            ...this.props,
            onMouseEnter: node => {
                this.props.run.actions.nodeReceiveFocus(node.id);

                if (!node.active) {
                    PubSub.publish('STATUS_SET', `${node.title} is hidden, click to reveal`);
                }
            },
            onMouseLeave: node => {
                this.props.run.actions.nodeLooseFocus(node.id);

                PubSub.publish('STATUS_UNSET');
            },
            onClick: node => {
                if (node.children.length > 0) {
                    if (node.expanded) {
                        this.props.run.actions.collapseNode(node.path);
                    } else {
                        this.props.run.actions.expandNode(node.path);
                    }
                } else {
                    if (node.active) {
                        this.props.run.actions.zoomToNode(node.id);
                    } else {
                        this.props.run.actions.revealNode(node.id);
                    }
                }
            },
            icon: (node, level) => {
                const onClick = (event) => {
                    event.stopPropagation();
                    this.props.run.actions.revealNode(node.id);
                };

                if (!node.active) {
                    return <Icon name='hide' onClick={onClick}/>;
                }

                return <Icon name='eye'/>
            },
            title: (node, level) => {
                return <Title node={node}/>
            },
            context: (node, level) => {
                return <Context node={node} {...this.props}/>
            },
            color: (node) => {
                return node.active ? 'black' : 'grey';
            }
        };

        const nodes = filteredNodes.valueSeq().map((node, nodeId) => {
            return <Node key={node.get('path')}
                         node={node.toJS()}
                         level={0}
                         tree={false}
                         {...props}/>
        });

        return <StyleNodesList>{nodes}</StyleNodesList>
    }
}

export default props => {
    return <RunConsumer>
        {run => <List {...props} run={run}/>}
    </RunConsumer>
};

const StyleNodesList = styled.div`
    width: 100%;
`;