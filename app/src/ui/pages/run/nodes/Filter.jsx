/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React, {Component} from 'react'

import StringFilter from 'ui/miscellaneous/StringFilter'

import {RunConsumer} from 'ui/pages/run/Run'

class Filter extends Component {
    setFilter = (filter) => {
        this.props.run.actions.setNodeFilter(filter);
        // this.props.run.actions.s(filter);
    };

    shouldComponentUpdate(nextProps, nextState) {
        return this.props.run.data.getIn(['filter', 'node']) !== nextProps.run.data.getIn(['filter', 'node']);
    }

    render() {
        return <StringFilter name={'nodes'}
                             placeholder='Search nodes'
                             status='Filter nodes by name'
                             icon={'search'}
                             filter={this.props.run.data.getIn(['filter', 'node'])}
                             setFilter={this.setFilter}
                             commitFilterInterval={50}
                             fluid={true}/>
    }
}

export default props => {
    return <RunConsumer>
        {run => <Filter {...props} run={run}/>}
    </RunConsumer>
};