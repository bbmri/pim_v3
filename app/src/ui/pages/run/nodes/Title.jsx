/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React, {Component} from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import _ from 'lodash'

import {RunConsumer} from 'ui/pages/run/Run'

class Title extends Component {
    title() {
        let nodeTitle = this.props.node.title;

        const nodeFilter = this.props.run.data.getIn(['filter', 'node']);

        if (nodeFilter !== '') {
            let filter = nodeFilter;

            if (filter !== '') {
                nodeTitle = _.map(nodeTitle.split(filter).join('#' + filter + '#').split('#', 100), (str, key) => (
                    str === filter ? (<span className='filterMatch' key={key}>{str}</span>) : str
                ));
            }
        }

        return nodeTitle;
    }

    render() {
        const noChildren = this.props.node.children.length;
        const counter    = noChildren > 0 ? <sup>({noChildren})</sup> : null;

        return <StyledTitle active={this.props.node.active}>{this.title()}{counter}</StyledTitle>
    }
}

Title.propTypes = {
    node: PropTypes.object.isRequired
};

export default props => {
    return <RunConsumer>
        {run => <Title {...props} run={run}/>}
    </RunConsumer>
};

const StyledTitle = styled.div`
    cursor: default;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    flex: 1;
`;