/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React, {Component} from 'react'

import {
    Button,
    Segment
} from 'semantic-ui-react'

import HelpContext from 'ui/miscellaneous/HelpContext'

import {RunConsumer} from 'ui/pages/run/Run'

class Toolbar extends Component {
    noGroupNodes() {
        return this.props.run.get('nodes').valueSeq()
            .reduce((accum, node) => {
                if (node.get('children').size > 0)
                    return accum + 1;
                return accum;
            }, 0);
    }

    noExpandedGroupNodes() {
        return this.props.run.get('nodes').valueSeq()
            .reduce((accum, node) => {
                if (node.get('active') && node.get('expanded'))
                    return accum + 1;
                return accum;
            }, 0);
    }

    noLevels() {
        return this.props.run.get('nodes').valueSeq()
            .reduce((accum, node) => {
                if (node.get('level') > accum)
                    return node.get('level');
                return accum;
            }, 0);
    }

    render() {
        const noGroupNodes         = this.noGroupNodes();
        const noExpandedGroupNodes = this.noExpandedGroupNodes();
        const canExpandAll         = noExpandedGroupNodes !== noGroupNodes;
        const canCollapseAll       = noExpandedGroupNodes >= 1;

        const levels = Array.apply(null, {length: this.noLevels()}).map(Number.call, Number).map(level => {
            return <HelpContext key={`level${level}`} status={`Expand all nodes until level ${level + 1}`}>
                <Button size='mini'
                        onClick={() => this.props.actions.setViewDepth(level + 1)}>{level + 1}</Button>
            </HelpContext>
        });

        return <Segment attached='bottom' secondary size='mini'>
            <div style={{display: 'flex'}}>
                <HelpContext key='expandAll' status={'Expand all nodes'}>
                    <Button basic
                            size='mini'
                            disabled={!canExpandAll}
                            onClick={() => this.props.actions.expandAll()}
                            style={{flex: 1}}>Expand all</Button>
                </HelpContext>
                <div key='spacerA' style={{width: 5}}/>
                <HelpContext key='collapseAll' status={'Collapse all nodes'}>
                    <Button basic
                            size='mini'
                            disabled={!canCollapseAll}
                            onClick={() => this.props.actions.collapseAll()}
                            style={{flex: 1}}>Collapse all</Button>
                </HelpContext>
                {this.noLevels() > 1 ? <React.Fragment>
                    <div style={{width: 5}}/>
                    <Button.Group basic size='mini'>
                        {levels}
                    </Button.Group>
                </React.Fragment> : null}
            </div>
        </Segment>
    }
}

export default props => {
    return <RunConsumer>
        {context => <Toolbar {...props} run={context.data} actions={context.actions}/>}
    </RunConsumer>
};