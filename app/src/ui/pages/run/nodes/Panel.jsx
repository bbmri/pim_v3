/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React from 'react'
import styled from 'styled-components'

import {
    Segment,
    Header,
    Message
} from 'semantic-ui-react'

import ErrorBoundary from 'ui/miscellaneous/ErrorBoundary'

import Tree from './Tree'
import Filter from '../nodes/Filter'
import List from '../nodes/List'
import Toolbar from './Toolbar'

import {RunConsumer} from 'ui/pages/run/Run'

class Panel extends React.Component {
    header() {
        return <Segment secondary attached='top'>
            <Header as='h3'>
                Tree view
                <Header.Subheader>Hierarchical pipeline view</Header.Subheader>
            </Header>
        </Segment>
    }

    list() {
        return <List/>;
    }

    tree() {
        return <Tree/>;
    }

    content() {
        const hasNodeFilter = this.props.run.data.getIn(['filter', 'node']).length > 0;

        const filter = <Segment secondary attached>
            <Filter {...this.props}/>
        </Segment>;

        const catchErrorUI = (message) => <Message style={{flex: 1}} attached='bottom' compact error>
            {message}
        </Message>;

        return <ErrorBoundary content={catchErrorUI}>
            {filter}
            <StyledSegment attached={hasNodeFilter ? 'bottom' : true} style={{padding: 0}}>
                <StyledScrollArea>
                    {hasNodeFilter ? this.list() : this.tree()}
                </StyledScrollArea>
            </StyledSegment>
            {hasNodeFilter ? null : <Toolbar {...this.props}/>}
        </ErrorBoundary>
    }

    shouldComponentUpdate(nextProps, nextState) {
        return this.props.run.data.getIn(['filter', 'node']) !== nextProps.run.data.getIn(['filter', 'node']);
    }

    render() {
        return <StyledPanel>
            {this.header()}
            {this.content()}
        </StyledPanel>
    }
}

const StyledSegment = styled(Segment)`
    flex: 1;
    display: flex;
    flex-direction: column;
    min-height: 0;
    padding: 0;
`;


const StyledScrollArea = styled.div`
    flex: 1 1 auto;
    overflow: auto;
    height: 0;
    padding: 10px;
`;

const StyledPanel = styled.div`
    display: flex;
    flex-direction: column;
    margin-left: 1px;
    margin-right: 5px;
    height: 100%;
`;

export default props => {
    return <RunConsumer>
        {run => <Panel {...props} run={run}/>}
    </RunConsumer>
};