/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React, {Component} from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import {RunConsumer} from 'ui/pages/run/Run'

import Action from './Action'

import {NavigationConsumer} from 'ui/Navigation'

class Context extends Component {
    isActive() {
        return this.props.node.active;
    }

    hasFailedJobs() {
        return this.props.run.nodeStatus(this.props.node.id).getIn(['jobs', 3]) > 0;
    }

    failedStatus() {
        if (!this.hasFailedJobs())
            return null;

        const params = {
            runId: this.props.run.data.getIn(['config', 'runId']),
            nodeId: this.props.node.path,
            filter: ['failed']
        };

        return <Action icon='exclamation circle'
                       status={'Inspect failed jobs'}
                       action={() => this.props.navigation.toJobsPage(params)}/>;
    }

    locateNode() {
        if (!this.isActive())
            return null;

        return <Action icon='search'
                       status={'Locate node in the graph view'}
                       action={() => this.props.run.actions.zoomToNode(this.props.node.id)}/>;
    }

    showConnected() {
        if (this.props.node.level === 0)
            return null;

        if (!this.isActive())
            return null;

        return <Action icon='plug'
                       status={'Zoom to connected nodes'}
                       action={() => this.props.run.actions.zoomToNodeConnections(this.props.node.id)}/>;
    }

    viewJobs() {
        const runId  = this.props.run.data.getIn(['config', 'runId']);
        const nodeId = this.props.node.path;

        return <Action icon='tasks'
                       status={'Inspect node jobs'}
                       action={() => this.props.navigation.toJobsPage({runId: runId, nodeId: nodeId})}/>;
    }

    render() {
        return <StyledContext>
            {this.failedStatus()}
            {this.locateNode()}
            {this.showConnected()}
            {this.viewJobs()}
        </StyledContext>
    }
}

Context.propTypes = {
    node: PropTypes.object.isRequired,
};

const StyledContext = styled.div`
    display: flex;
    margin-left: 10px;
`;

export default props => {
    return <NavigationConsumer>
        {navigation => <RunConsumer>
            {run => <Context {...props} run={run} navigation={navigation}/>}
        </RunConsumer>}
    </NavigationConsumer>
};
