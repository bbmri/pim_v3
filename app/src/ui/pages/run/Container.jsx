/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React, {Component} from 'react'
import {withRouter} from 'react-router-dom'
import styled from 'styled-components'

const getUuid = require('uuid-by-string');

import {RunConsumer} from 'ui/pages/run/Run'
import {NavigationConsumer} from 'ui/Navigation'

import Splitter from 'm-react-splitters'

import 'm-react-splitters/lib/splitters.css'

const queryString = require('query-string');

import NavigationBar from 'ui/miscellaneous/NavigationBar'

import TreePanel from './nodes/Panel'
import GraphPanel from './graph/Panel'

class Container extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        logger.debug(`Component did mount`);

        const params = queryString.parse(this.props.location.search);

        if (this.props.match.params.run !== this.props.run.data.getIn(['config', 'runId'])) {
            this.props.run.actions.load(this.props.match.params.run, params.nodeId !== undefined ? params.nodeId : 'root');
        } else {
            if (params.nodeId !== undefined && params.nodeId !== '') {
                this.props.run.actions.revealNode(getUuid(params.nodeId));
            }
        }

        this.props.history.replace({
            search: ''
        });

        this.props.run.actions.startSynchronization();
    }

    componentWillUnmount() {
        this.props.run.actions.stopSynchronization();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        const params = queryString.parse(this.props.location.search);

        if (this.props.match.params.run !== this.props.run.data.getIn(['config', 'runId'])) {
            this.props.run.actions.load(this.props.match.params.run, params.nodeId !== undefined ? params.nodeId : 'root');
        } else {

        }
    }

    content() {
        let props = {
            ...this.props,
        };

        return <StyledContent>
            <StyledSplitter>
                <Splitter position='vertical'
                          primaryPaneMinWidth='350px'
                          primaryPaneMaxWidth='600px'
                          primaryPaneWidth='350px'
                          dispatchResize={true}
                          postPoned={false}>
                    <TreePanel {...props}/>
                    <GraphPanel {...props}/>
                </Splitter>
            </StyledSplitter>
        </StyledContent>;
    }

    render() {
        const sections = [
            {label: 'pim', status: (props) => 'Pipeline Inspection and Monitoring'},
            {
                label: 'runs',
                action: () => this.props.navigation.toRunsPage(),
                status: (props) => 'Go the runs overview page'
            },
            {label: this.props.run.data.getIn(['config', 'runId'])},
            {
                label: 'graph',
                active: true,
                status: (props) => `Viewing ${this.props.run.data.getIn(['config', 'runId'])}`
            }
        ];

        return <StyledContainer>
            <NavigationBar sections={sections} style={{marginBottom: 6}}/>
            {this.content()}
            <div style={{height: 6}}/>
        </StyledContainer>
    }
}

const RunWithRouter = withRouter(Container);

export default props => {
    return <NavigationConsumer>
        {navigation => <RunConsumer>
            {run => <RunWithRouter {...props} run={run} navigation={navigation}/>}
        </RunConsumer>}
    </NavigationConsumer>
};

const StyledContainer = styled.div`
    flex: 1;
    display: flex;
    flex-direction: column;
    width: 100%;
    height: 100%;
    min-height: 0;
`;

const StyledContent = styled.div`
    flex: 1;
    height: 0;
    display: flex;
    flex-direction: column;
`;

const StyledSplitter = styled.div`
    width: 100%;
    height: 0;
    flex: 1;
`;