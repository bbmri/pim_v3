/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const getUuid = require('uuid-by-string');

import {RunConsumer} from 'ui/pages/run/Run'
import {NavigationConsumer} from 'ui/Navigation'

import {NodeProvider} from './visualization/actors/node/Node'

import {
    Segment,
    Header,
    Message
} from 'semantic-ui-react'

import ErrorBoundary from 'ui/miscellaneous/ErrorBoundary'

import Graph from './visualization/Graph'

import Toolbar from './Toolbar'

class Panel extends React.Component {
    rootNode = () => {
        return this.props.run.data.getIn(['nodes', getUuid('root')]);
    };

    header() {
        return <Segment attached='top' secondary style={{display: 'flex'}}>
            <Header as='h3'>
                Graph view
                <Header.Subheader>Directed graph view of the processing pipeline</Header.Subheader>
            </Header>
        </Segment>
    }

    content() {
        const catchErrorUI = (message) => <Message style={{flex: 1}} attached='bottom' compact error>
            {message}
        </Message>;

        // console.log(this.props.runStore.graph.updating)
        const graph = <Segment attached style={{flex: 1, padding: '0px', display: 'flex'}}>
            {/*<Loader size='medium' active={this.props.run.data.get('updating')}/>*/}
            <Graph {...this.props} style={{flex: 1}}/>
        </Segment>;

        // console.log(this.rootNode(), this.props.run.data.getIn(['nodes']).toJS())
        return <ErrorBoundary content={catchErrorUI}>
            {/*<Segment attached secondary size='mini'>
                <NodeProvider value={this.rootNode()}>
                    <Status/>
                </NodeProvider>
            </Segment>*/}
            {graph}
            <Toolbar {...this.props}/>
        </ErrorBoundary>
    }

    render() {
        return <StyledPanel>
            {this.header()}
            {this.content()}
        </StyledPanel>
    }
}

Panel.propTypes = {
    run: PropTypes.object.isRequired,
    navigation: PropTypes.object.isRequired
};

const StyledPanel = styled.div`
    display: flex;
    flex-direction: column;
    margin-left: 5px;
    margin-right: 1px;
    height: 100%;
`;

export default props => {
    return <NavigationConsumer>
        {navigation => <RunConsumer>
            {run => <Panel {...props} run={run} navigation={navigation}/>}
        </RunConsumer>}
    </NavigationConsumer>
};