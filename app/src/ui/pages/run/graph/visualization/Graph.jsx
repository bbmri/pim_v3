/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import PubSub from 'pubsub-js'

import {RunConsumer} from 'ui/pages/run/Run'
import {NavigationConsumer} from 'ui/Navigation'

import * as d3 from 'd3'

import Actors from './actors/Actors'

const defaultZoomTransition = {
    ease: 'easeLinear',
    duration: 1000,
    delay: 0
};

const GraphContext = React.createContext({});

export const GraphProvider = GraphContext.Provider;
export const GraphConsumer = GraphContext.Consumer;

class Graph extends React.Component {
    constructor(props) {
        super(props);

        this.canvasRef     = React.createRef();
        this.graphRef      = React.createRef();
        this.zoomRef       = React.createRef();
        this.zoomTimer     = null;
        this.zoomTransform = d3.zoomIdentity.translate(0, 0).scale(1);
    }

    componentDidMount() {
        this.zoom = d3.zoom()
            .on('start', () => this.props.run.actions.setZooming(true))
            .on('zoom', this.zoom)
            .on('end', () => this.props.run.actions.setZooming(false));

        if (this.props.run.data.get('viewCache').has('main')) {
            const zoomTransform = this.props.run.data.getIn(['viewCache', 'main', 'zoomTransform']).toJS();

            this.zoomTransform = d3.zoomIdentity.translate(zoomTransform.x, zoomTransform.y).scale(zoomTransform.k);

            d3.select(this.zoomRef.current).call(this.zoom.transform, this.zoomTransform);
        }

        d3.select(this.zoomRef.current).call(this.zoom).on('click', function () {
            d3.event.preventDefault();
        });

        this.listeners = {
            zoomToNodes: PubSub.subscribe('RUN_ZOOM_TO_NODES', this.zoomToNodes),
        };
    }

    componentWillUnmount() {
        for (let listener of Object.values(this.listeners)) {
            PubSub.unsubscribe(listener);
        }

        this.props.run.actions.setViewCache('main', {
            zoomTransform: {
                x: this.zoomTransform.x,
                y: this.zoomTransform.y,
                k: this.zoomTransform.k
            }
        });
    }

    zoom = () => {
        logger.debug(`Zoom`);

        d3.select(this.graphRef.current)
            .style('transform', `translate(${d3.event.transform.x}px, ${d3.event.transform.y}px) scale(${d3.event.transform.k})`);

        if (this.zoomTimer !== null)
            window.clearInterval(this.zoomTimer);

        this.zoomTimer = window.setInterval(this.onZoomPanTimeOut, 200);

        this.zoomTransform = d3.event.transform;
    };

    onZoomPanTimeOut = () => {
        window.clearInterval(this.zoomTimer);

        this.zoomTimer = null;

        PubSub.publish('RUN_ZOOM_TRANSFORM_CHANGED', this.zoomTransform);
    };

    setZoomRegion({zoomBox, transition = defaultZoomTransition}) {
        logger.debug(`Set zoom region`);

        const clientSize   = this.clientSize();
        const margin       = 300;
        const regionWidth  = zoomBox.size.width + 2 * margin;
        const regionHeight = zoomBox.size.height + 2 * margin;
        const scale        = 1 / Math.max(regionWidth / clientSize.width, regionHeight / clientSize.height);
        const x            = zoomBox.position.x + (zoomBox.size.width / 2);
        const y            = zoomBox.position.y + (zoomBox.size.height / 2);
        const translate    = [clientSize.width / 2 - scale * x, clientSize.height / 2 - scale * y];

        this.zoomTransform = d3.zoomIdentity.translate(translate[0], translate[1]).scale(scale);

        this.props.run.actions.setZooming(true);

        d3.select(this.zoomRef.current)
            .transition()
            .ease(d3[transition.ease])
            .duration(transition.duration)
            .delay(transition.delay)
            .call(this.zoom.transform, this.zoomTransform)
            .on('end', () => this.props.run.actions.setZooming(false));

        PubSub.publish('RUN_ZOOM_TRANSFORM_CHANGED', this.zoomTransform);
    }

    clientSize() {
        const canvasNode = d3.select(this.canvasRef.current).node();

        return {
            width: canvasNode.clientWidth,
            height: canvasNode.clientHeight
        };
    }

    zoomToNodes = (message, nodeIds) => {
        logger.debug(`Zoom to nodes`);

        let axes = {
            x: [],
            y: []
        };

        for (let nodeId of nodeIds) {
            const nodeLayout = () => this.props.run.data.getIn(['layout', 'nodes', nodeId]);

            if (nodeLayout() === null)
                continue;

            const layout = nodeLayout().toJS();

            axes.x.push(layout.position.x);
            axes.x.push(layout.position.x + layout.size.width);
            axes.y.push(layout.position.y);
            axes.y.push(layout.position.y + layout.size.height);
        }

        axes.x = axes.x.sort((a, b) => a - b);
        axes.y = axes.y.sort((a, b) => a - b);

        const axisBounds = (axis) => {
            return [axis[0], axis.slice(-1)[0]];
        };

        let bounds = {
            x: axisBounds(axes.x),
            y: axisBounds(axes.y)
        };

        const zoomBox = {
            position: {
                x: bounds.x[0],
                y: bounds.y[0]
            },
            size: {
                width: bounds.x[1] - bounds.x[0],
                height: bounds.y[1] - bounds.y[0]
            }
        };

        const transition = this.props.run.data.getIn(['transition', 'update']);

        this.setZoomRegion({
            zoomBox: zoomBox,
            transition: {
                ease: transition.get('ease'),
                duration: this.props.run.data.getIn(['config', 'animation']) ? transition.get('duration') : 0,
                delay: 0
            }
        });
    };

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        return nextProps.run.data.get('nodes') !== this.props.run.data.get('nodes');
    }

    render() {
        const value = {
            zoomTransform: this.zoomTransform
        };

        return <GraphProvider value={value}>
            <StyledCanvas ref={this.canvasRef}>
                <StyledZoom ref={this.zoomRef}>
                    <StyledGraph ref={this.graphRef}>
                        <Actors/>
                    </StyledGraph>
                </StyledZoom>
            </StyledCanvas>
        </GraphProvider>
    }
}

Graph.propTypes = {
    run: PropTypes.object.isRequired,
    navigation: PropTypes.object.isRequired
};

const StyledCanvas = styled.div`
    width: 100%;
    minHeight: 0;
    position: relative;
    overflow: hidden;
    background-color: hsla(0, 0%, 0%, 0);
`;

const StyledZoom = styled.div`
    width: 100%;
    height: 100%;
`;

const StyledGraph = styled.div`
    position: absolute;
    top: 0;
    left: 0;
`;

export default props => {
    return <NavigationConsumer>
        {navigation => <RunConsumer>
            {run => <Graph {...props} run={run} navigation={navigation}/>}
        </RunConsumer>}
    </NavigationConsumer>
};