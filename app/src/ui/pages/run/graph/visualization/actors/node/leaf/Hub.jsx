/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import Immutable from 'immutable'

import {RunConsumer} from '../../../../../Run'
import {NodeConsumer} from '../Node'

import Ports from './Ports'

class Hub extends React.Component {
    render() {
        let inPorts  = this.props.node.get('inPorts');
        let outPorts = this.props.node.get('outPorts');

        if (this.props.exclude !== undefined) {
            inPorts = inPorts.filter(inPort => {
                return !this.props.exclude.includes(inPort.get('name'));
            });

            outPorts = outPorts.filter(outPort => {
                return !this.props.exclude.includes(outPort.get('name'));
            });
        }

        if (this.props.canCollapse && this.props.run.data.getIn(['config', 'compactNode'])) {
            const noFromPorts = inPorts.size;
            const noToPorts   = outPorts.size;

            if (noFromPorts > 1) {
                inPorts = Immutable.fromJS({
                    __collapsed_in__: {
                        name: '__collapsed_in__',
                        title: 'Collapsed input ports',
                        description: 'All input ports collapsed into one',
                        connections: {
                            links: this.props.node.get('inPorts').reduce((accum, inPort) => {
                                return accum.concat(inPort.getIn(['connections', 'links']));
                            }, []),
                            nodes: this.props.node.get('inPorts').reduce((accum, inPort) => {
                                return accum.concat(inPort.getIn(['connections', 'nodes']));
                            }, []),
                        },
                        customData: {}
                    }
                });
            }

            if (noToPorts > 1) {
                outPorts = Immutable.fromJS({
                    __collapsed_out__: {
                        name: '__collapsed_out__',
                        title: 'Collapsed output ports',
                        description: 'All output ports collapsed into one',
                        connections: {
                            links: this.props.node.get('outPorts').reduce((accum, outPort) => {
                                return accum.concat(outPort.getIn(['connections', 'links']));
                            }, []),
                            nodes: this.props.node.get('outPorts').reduce((accum, outPort) => {
                                return accum.concat(outPort.getIn(['connections', 'nodes']));
                            }, []),
                        },
                        customData: {}
                    }
                });
            }
        } else {
            if (this.props.include !== undefined) {
                inPorts = inPorts.filter(inPort => {
                    return this.props.include.includes(inPort.get('name'));
                });

                outPorts = outPorts.filter(outPort => {
                    return this.props.include.includes(outPort.get('name'));
                });
            }
        }

        return <StyledHub>
            <Ports ports={inPorts} in={true} {...this.props}/>
            <StyledGutter/>
            <Ports ports={outPorts} in={false} {...this.props}/>
        </StyledHub>
    }
}

Hub.propTypes = {
    canCollapse: PropTypes.bool.isRequired,
    exclude: PropTypes.array,
    include: PropTypes.array
};

const StyledHub = styled.div`
    display: flex;
    align-items: flex-start;
    width: 100%;
    padding: ${props => props.theme.node.leaf.padding};
    background-color: hsl(0, 0%, 85%);
    border-top: 3px solid hsla(0, 0%, 0%, 0.1);
    border-bottom: 3px solid hsla(0, 0%, 0%, 0.1);
`;

const StyledGutter = styled.div`
    width: 100px;
`;

export default props => {
    return <RunConsumer>
        {run => <NodeConsumer>
            {node => <Hub {...props} run={run} node={node}/>}
        </NodeConsumer>}
    </RunConsumer>
};