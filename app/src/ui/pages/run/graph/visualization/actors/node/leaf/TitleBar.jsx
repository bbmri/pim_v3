/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React from 'react'
import styled from 'styled-components'

import {RunConsumer} from '../../../../../Run'
import {NodeConsumer} from '../Node'

import {statusName} from 'helpers/job'

class TitleBar extends React.Component {
    status = () => {
        return this.props.run.data.get('status');
    };

    percentage = () => {
        if (!this.status().has(this.props.node.get('id')))
            return '...';

        const percentage = this.status().getIn([this.props.node.get('id'), 'progress']) * 100.0;

        return `${percentage.toFixed(1)}%`;
    };

    state = () => {
        if (!this.status().has(this.props.node.get('id')))
            return 'uninitialized';

        return statusName(this.status().getIn([this.props.node.get('id'), 'state']));
    };

    render() {
        return <StyledTitleBar>
            <StyledTitle>
                <StyledName>
                    {this.props.node.get('title')}
                </StyledName>
            </StyledTitle>
        </StyledTitleBar>
    }
}

const StyledTitleBar = styled.div`
    width: 100%;
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    pointer-events: none;
    cursor: default;
    padding: 20px;
`;

const StyledTitle = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: flex-start;
    font-weight: bold;
    width: 100%;
`;

const StyledName = styled.div`
    font-size: 42pt;
    white-space: nowrap;
`;

const StyledGap = styled.div`
    height: 30px;
`;

const StyledPercentage = styled.div`
    padding: 20px;
    border-radius: 10px;
    background-color: ${props => {
        return props.theme.progress[props.state];    
    }};
    box-shadow: inset 0 0 30px hsla(0, 0%, 0%, 0.2);
    color: white;
    font-size: 38pt;
`;

export default props => {
    return <RunConsumer>
        {run => <NodeConsumer>
            {node => <TitleBar {...props} run={run} node={node}/>}
        </NodeConsumer>}
    </RunConsumer>
};