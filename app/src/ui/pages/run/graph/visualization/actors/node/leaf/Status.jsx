/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React from 'react'
import styled from 'styled-components'

import ProgressBar from 'ui/miscellaneous/ProgressBar'

import {NavigationConsumer} from 'ui/Navigation'
import {RunConsumer} from 'ui/pages/run/Run'
import {NodeConsumer} from '../Node'
import {StatusConsumer} from './../Status'

import * as nodeHelper from 'helpers/node'
import * as jobHelper from 'helpers/job'

class Status extends React.Component {
    shouldComponentUpdate(nextProps, nextState, nextContext) {
        return nextProps.status !== this.props.status;
    }

    nodeStateItems = () => {
        const stateName  = nodeHelper.stateName(this.props.status.get('state'));
        const percentage = this.props.status.get('progress') * 100.0;
        const label      = `${percentage.toFixed(1)}%`;

        return {
            [stateName]: {
                count: 1,
                label: <StyledStateLabel>{label}</StyledStateLabel>
            }
        }
    };

    nodeJobsItems = () => {
        const jobs   = this.props.status.get('jobs').toJS();
        const noJobs = this.props.status.getIn(['noJobs', 'all']);

        return {
            idle: {
                count: jobs[0]
            },
            running: {
                count: jobs[1]
            },
            success: {
                count: jobs[2]
            },
            failed: {
                count: jobs[3]
            },
            cancelled: {
                count: jobs[4]
            },
            undefined: {
                count: jobs[5]
            },
            uninitialized: {
                count: noJobs === 0 ? 1 : 0
            }
        }
    };

    /*
     nodeStateId = () => {
     console.log(this.nodeStatus(this.props).toJS())
     return this.nodeStatus(this.props).get('state');
     };

     nodeStateName = () => {
     return nodeHelper.stateName(this.nodeStateId(this.props));
     };

     nodeStateJobs = () => {
     return {
     [this.nodeStateName()]: 1
     }
     };*/

    render() {
        const handlers = {
            state: {
                onClick: (bar) => {
                    if (bar.type !== 'uninitialized')
                        this.props.navigation.toJobsPage({
                            runId: this.props.run.data.getIn(['config', 'runId']),
                            nodeId: this.props.node.get('path'),
                            filter: [bar.type]
                        });
                },
                status: (bar) => {
                    if (bar.type === 'uninitialized') {
                        return `${this.props.node.get('title')} does not contain jobs`;
                    } else {
                        return `${this.props.node.get('title')} ${jobHelper.statusDescription(bar.type, 1)}`;
                    }
                }
            },
            jobs: {
                onClick: (bar) => {
                    if (bar.type !== 'uninitialized')
                        this.props.navigation.toJobsPage({
                            runId: this.props.run.data.getIn(['config', 'runId']),
                            nodeId: this.props.node.get('path'),
                            filter: [bar.type]
                        });
                },
                status: (bar) => {
                    if (bar.type === 'uninitialized') {
                        return `${this.props.node.get('title')} does not contain jobs`;
                    } else {
                        return jobHelper.statusTypeProgressDescription(bar);
                    }
                }
            }
        };

        return <StyledStatus>
            <StyledState>
                <ProgressBar radius={16}
                             items={this.nodeStateItems()}
                             runId={this.props.run.data.getIn(['config', 'runId'])}
                             {...handlers.state}/>
            </StyledState>
            <StyledGap/>
            <StyledProgress>
                <ProgressBar radius={12}
                             items={this.nodeJobsItems()}
                             runId={this.props.run.data.getIn(['config', 'runId'])}
                             {...handlers.jobs}/>
            </StyledProgress>
        </StyledStatus>
    }
}

const StyledStatus = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    padding: ${props => props.theme.node.leaf.padding};
`;

const StyledState = styled.div`
    position: relative;
    height: 100px;
    width: 100%;
`;

const StyledStateLabel = styled.div`
    color: white;
    font-size: 42px;
    font-weight: bold;
    pointer-events: none;
    cursor: default;
`;

const StyledGap = styled.div`
    height: 15px;
`;

const StyledProgress = styled.div`
    position: relative;
    width: 100%;
    height: 30px;
`;

export default props => {
    return <NavigationConsumer>
        {navigation => <RunConsumer>
            {run => <NodeConsumer>
                {node => <StatusConsumer>
                    {status => <Status {...props} navigation={navigation} run={run} node={node} status={status}/>}
                </StatusConsumer>}
            </NodeConsumer>}
        </RunConsumer>}
    </NavigationConsumer>
};
