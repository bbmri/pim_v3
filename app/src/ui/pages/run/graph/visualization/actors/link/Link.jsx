/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import PubSub from 'pubsub-js'
import * as d3 from 'd3'

import {NavigationConsumer} from 'ui/Navigation'
import {RunConsumer} from 'ui/pages/run/Run'
import {ActorConsumer} from '../Actor'

class Link extends React.Component {
    constructor(props) {
        super(props);

        this.opacityRef = React.createRef();
        this.hitPath    = React.createRef();
        this.outerPath  = React.createRef();
        this.innerPath  = React.createRef();
        this.focusGroup = React.createRef();
        this.focusPath  = React.createRef();
    }

    componentDidMount() {
        this.hitPath.current.addEventListener('click', this.click);
        this.hitPath.current.addEventListener('mouseenter', this.mouseEnter);
        this.hitPath.current.addEventListener('mouseleave', this.mouseLeave);

        this.listeners = {
            runLayoutChanged: PubSub.subscribe('RUN_LAYOUT_CHANGED', this.onRunLayoutChanged),
            highlightLinks: PubSub.subscribe('RUN_HIGHLIGHT_LINKS', this.onHighlightLinks)
        };

        d3.select(this.opacityRef.current)
            .style('opacity', 0);

        requestAnimationFrame(() => {
            const transition = this.props.run.transition();

            d3.select(this.opacityRef.current)
                .style('opacity', 0)
                .transition()
                .ease(d3[transition.entrance.ease])
                .duration(transition.entrance.duration)
                .delay(this.entranceDelay())
                .style('opacity', 1);

            d3.select(this.opacityRef.current).selectAll('path')
                .attr('d', this.path(this.props));

            d3.select(this.focusGroup.current)
                .attr('opacity', 0);
        });
    }

    componentWillUnmount() {
        this.hitPath.current.removeEventListener('click', this.click);
        this.hitPath.current.removeEventListener('mouseenter', this.mouseEnter);
        this.hitPath.current.removeEventListener('mouseleave', this.mouseLeave);

        for (let listener of Object.values(this.listeners)) {
            PubSub.unsubscribe(listener);
        }
    }

    onRunLayoutChanged = (message, data) => {
        const transition = this.props.run.transition();

        d3.select(this.opacityRef.current).selectAll('path')
            .transition()
            .ease(d3[transition.update.ease])
            .duration(transition.update.duration)
            .attr('d', this.path(this.props));
    };

    onHighlightLinks = (message, data) => {
        if (data.links.includes(this.props.link.get('id'))) {
            const transition = this.props.run.transition()[`focus${data.highlight ? 'In' : 'Out'}`];

            d3.select(this.focusGroup.current)
                .transition()
                .ease(d3[transition.ease])
                .duration(transition.duration)
                .attr('opacity', data.highlight ? 1 : 0);
        }
    };

    entranceDelay = () => {
        return (this.props.link.get('layerId')) * this.props.run.transition().update.duration;
    };

    click = () => {
        this.props.run.actions.zoomToLink(this.props.link.get('id'));

        /*if (this.props.link.get('implicit')) {
         this.props.run.actions.expandLink(this.props.link.get('id'));
         } else {
         this.props.run.actions.zoomToLink(this.props.link.get('id'));
         }*/
    };

    mouseEnter = () => {
        this.props.run.actions.linkReceiveFocus(this.props.link.get('id'));
    };

    mouseLeave = () => {
        this.props.run.actions.linkLooseFocus(this.props.link.get('id'));
    };

    layout = (props) => {
        return props.run.data.getIn(['layout', 'links', this.props.link.get('id')]);
    };

    path = (props) => {
        const layout = this.layout(props);

        if (layout === undefined)
            return '';

        return layout.getIn(['geometry', 'svgPath']);
    };

    shouldComponentUpdate(nextProps, nextState) {
        return this.path(nextProps) !== this.path(this.props);
    }

    render() {
        const extent = 100000;

        const viewBox = {
            x: -extent / 2,
            y: -extent / 2,
            width: extent,
            height: extent
        };

        const svgParams = {
            width: extent,
            height: extent,
            viewBox: Object.values(viewBox).join(' ')
        };

        const style = {
            transform: `translate(-${extent / 2}px, -${extent / 2}px)`,
            pointerEvents: 'none',
            background: 'transparent'
        };

        return <svg width={viewBox.width}
                    height={viewBox.height}
                    viewBox={svgParams.viewBox}
                    overflow='visible'
                    style={style}>
            <StyledLink ref={this.opacityRef}>
                <StyledHitPath link={this.props.link} ref={this.hitPath}/>
                <StyledOuterPath link={this.props.link} ref={this.outerPath}/>
                <StyledInnerPath link={this.props.link} ref={this.innerPath}/>
                <StyledFocusGroup link={this.props.link} ref={this.focusGroup}>
                    <StyledFocusPath link={this.props.link} ref={this.focusPath}/>
                </StyledFocusGroup>
            </StyledLink>
        </svg>
    }
}

Link.propTypes = {
    link: PropTypes.object.isRequired
};

const StyledLink = styled.g`
    opacity: 0;
    pointer-events: none;
`;

const StyledHitPath = styled.path`
    pointer-events: stroke;
    fill: none;
    stroke: ${props => props.theme.link.path.hit.color};
    stroke-width: ${props => props.theme.link.path.hit.width};
    stroke-opacity: ${props => props.theme.link.path.hit.opacity};
`;

const StyledInnerPath = styled.path`
    pointer-events: none;
    fill: none;
    stroke: ${props => {
    return props.link.get('implicit') ? props.theme.link.path.inner.color : props.link.get('color');
}};
    stroke-width: ${props => {
    return props.link.get('implicit') ? 8 : props.theme.link.path.inner.width;
}};
    stroke-dasharray: ${props => {
    return props.link.get('implicit') ? '10, 10' : 'none';
}};
    stroke-opacity: ${props => props.theme.link.path.inner.opacity};
`;

const StyledOuterPath = styled.path`
    pointer-events: none;
    visibility: ${props => props.link.get('implicit') ? 'hidden' : 'visible'};
    fill: none;
    stroke: ${props => props.theme.link.path.outer.color};
    stroke-width: ${props => props.theme.link.path.outer.width};
    stroke-opacity: ${props => props.theme.link.path.outer.opacity};
`;

const StyledFocusGroup = styled.g`
`;

const StyledFocusPath = styled.path`
    pointer-events: none;
    fill: none;
    stroke: ${props => props.theme.link.path.focus.color};
    stroke-width: ${props => props.theme.link.path.focus.width};
    stroke-opacity: ${props => props.theme.link.path.focus.opacity};
`;

export default props => {
    return <NavigationConsumer>
        {navigation => <RunConsumer>
            {run => <ActorConsumer>
                {actor => <Link {...props} run={run} actor={actor} navigation={navigation}/>}
            </ActorConsumer>}
        </RunConsumer>}
    </NavigationConsumer>
};