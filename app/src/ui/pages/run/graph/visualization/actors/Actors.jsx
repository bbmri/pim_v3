/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React from 'react'
import styled from 'styled-components'

import PubSub from 'pubsub-js'

const getUuid = require('uuid-by-string');

import {RunConsumer} from 'ui/pages/run/Run'

import Actor from './Actor'
import Node from './node/Node'
import Leaf from './node/leaf/Leaf'
import Group from './node/group/Group'
import Transition from './node/Transition'
import Link from './link/Link'

class Actors extends React.Component {
    constructor(props) {
        super(props);

        this.layout = props.run.data.get('layout');
    }

    componentDidMount() {
        PubSub.publish('RUN_LAYOUT_CHANGED', {
            previous: this.props.run.data.get('layout'),
            current: this.props.run.data.get('layout'),
            appearDelayed: true
        });
    }

    allNodes = () => {
        return this.props.run.data.get('nodes');
    };

    activeNodes = () => {
        return this.allNodes().filter(node => node.get('active'));
    };

    links = () => {
        return this.props.run.data.get('links');
    };

    nodesChanged = (oldProps, newProps) => {
        return newProps.run.data.get('nodes') !== oldProps.run.data.get('nodes');
    };

    linksChanged = (oldProps, newProps) => {
        return newProps.run.data.get('links') !== oldProps.run.data.get('links');
    };

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        return this.nodesChanged(this.props, nextProps) || this.linksChanged(this.props, nextProps);
    }

    leafActors = () => {
        return this.activeNodes().reduce((accum, node) => {
            const isActive      = node.get('active');
            const isExpanded    = node.get('expanded');
            const key           = getUuid(`${node.get('path')}_leaf`);
            const appearDelayed = !Object.keys(this.layout.get('nodes')).includes(node.get('id'));

            if (isActive && !isExpanded) {
                accum.push(<Actor key={key} zIndex={1000}>
                    <Node node={node}>
                        <Leaf node={node}/>
                    </Node>
                </Actor>);
            }

            return accum;
        }, []);
    };

    groupActors = () => {
        return this.activeNodes().reduce((accum, node) => {
            const isActive   = node.get('active');
            const isExpanded = node.get('expanded');
            const key        = getUuid(`${node.get('path')}_group`);

            if (isActive && isExpanded) {
                accum.push(<Actor key={key} zIndex={node.get('level') + 1}>
                    <Node node={node}>
                        <Group/>
                    </Node>
                </Actor>);
            }

            return accum;
        }, []);
    };

    transitionActors = () => {
        return this.allNodes().reduce((accum, node) => {
            const key = getUuid(`${node.get('path')}_transition`);

            accum.push(<Actor key={key} zIndex={10000}>
                <Transition node={node}/>
            </Actor>);

            return accum;
        }, []);
    };

    linkActors = () => {
        return Object.values(this.links().reduce((accum, link) => {
            const fromInput = link.getIn(['from', 'layout']).toJS();
            const toInput   = link.getIn(['to', 'layout']).toJS();
            const id        = `${fromInput.nodePath}_${fromInput.portId}_${toInput.nodePath}_${toInput.portId}`;

            accum[id] = <Actor key={id} zIndex={link.get('implicit') ? 500 : 500}>
                <Link link={link}/>
            </Actor>;

            return accum;
        }, {}));
    };

    render() {
        const actors = this.leafActors().concat(this.groupActors()).concat(this.transitionActors()).concat(this.linkActors());

        return <StyledActors id='actors'>
            {actors}
        </StyledActors>;
    }
}

const StyledActors = styled.div`
`;

export default props => {
    return <RunConsumer>
        {run => <Actors  {...props} run={run}/>}
    </RunConsumer>
};