/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React from 'react'
import styled from 'styled-components'
import {transparentize} from 'polished'

import PubSub from 'pubsub-js'

import * as d3 from 'd3'

import {RunConsumer} from 'ui/pages/run/Run'
import {ActorConsumer} from '../Actor'

class Transition extends React.Component {
    constructor(props) {
        super(props);

        this.opacityRef   = React.createRef();
        this.transformRef = React.createRef();
        this.rectangleRef = React.createRef();
    }

    componentDidMount() {
        this.listeners = {
            runLayoutChanged: PubSub.subscribe('RUN_LAYOUT_CHANGED', this.onRunLayoutChanged)
        };
    }

    componentWillUnmount() {
        for (let listener of Object.values(this.listeners)) {
            PubSub.unsubscribe(listener);
        }
    }

    onRunLayoutChanged = (message, data) => {
        const transition = this.props.run.transition();

        const applyLayout = (layout, animate = true) => {
            const position = layout.get('position').toJS();
            const size     = layout.get('size').toJS();
            const duration = animate ? transition.update.duration : 0;

            d3.select(this.transformRef.current)
                .transition()
                .ease(d3[transition.update.ease])
                .duration(duration)
                .style('left', position.x)
                .style('top', position.y);

            d3.select(this.rectangleRef.current)
                .transition()
                .ease(d3[transition.update.ease])
                .duration(duration)
                .style('width', size.width)
                .style('height', size.height);
        };

        const fadeTransition = () => {
            d3.select(this.opacityRef.current)
                .transition()
                .duration(transition.update.duration * 0.5)
                .style('opacity', 1)
                .on('end', () => {
                    d3.select(this.opacityRef.current)
                        .transition()
                        .duration(transition.update.duration * 0.5)
                        .style('opacity', 0);
                });
        };

        if (this.nodeLayout(data.current) !== undefined && this.nodeLayout(data.previous) !== undefined) {
            if (this.nodeLayout(data.current).get('expanded') !== this.nodeLayout(data.previous).get('expanded')) {
                applyLayout(this.nodeLayout(data.previous), false);
                applyLayout(this.nodeLayout(data.current));
                fadeTransition();
            }
        }
    };

    nodeLayout = (layout) => {
        return layout.getIn(['nodes', this.props.node.get('id')]);
    };

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        return false;
    }

    render() {
        return <StyledOpacity ref={this.opacityRef}>
            <StyledTransform ref={this.transformRef}>
                <StyledRectangle node={this.props.node} ref={this.rectangleRef}/>
            </StyledTransform>
        </StyledOpacity>
    }
}

const StyledOpacity = styled.div`
    opacity: 0;
`;

const StyledTransform = styled.div`
    position: absolute;
    pointer-events: none;
`;

const StyledRectangle = styled.div`
    display: flex;
    pointer-events: none;    
    flex-direction: column;
    background-color: ${props => transparentize(0.6, props.theme.node.transition.color)};
    border: 10px dotted hsla(0, 0%, 0%, 0.4);
    border-radius: ${props => props.theme.node.group.border.radius}px;
`;

export default props => {
    return <RunConsumer>
        {run => <ActorConsumer>
            {actor => <Transition {...props} run={run} actor={actor}/>}
        </ActorConsumer>}
    </RunConsumer>
};