/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import HelpContext from 'ui/miscellaneous/HelpContext'

import {RunConsumer} from 'ui/pages/run/Run'

class Child extends React.Component {
    reveal = (event) => {
        event.stopPropagation();

        this.props.run.actions.revealNode(this.props.id);
    };

    render() {
        const node      = this.props.run.data.getIn(['nodes', this.props.id]);
        const nodeTitle = node.get('title');
        const group     = node.get('children').size > 0 ? ` [${node.get('children').size}]` : null;

        return <HelpContext status={`Click to reveal child node: ${nodeTitle}`}>
            <StyledChild onClick={this.reveal}>
                {nodeTitle}{group}
            </StyledChild>
        </HelpContext>
    }
}

Child.propTypes = {
    id: PropTypes.string.isRequired
};

const StyledChild = styled.div`
    padding: 8px 20px 8px 20px;
    margin: 10px;
    background-color: hsl(0, 0%, 80%);
    border: none;
    color: hsl(0, 0%, 10%);
    font-size: 32px;
    font-weight: 700;
    text-align: center;
    border-radius: 12px;
    cursor: pointer;
    box-shadow: inset 0 0 10px hsla(0, 0%, 0%, 0.0);
    &:hover {
      background-color: hsl(0, 0%, 85%);
    }
`;

export default props => {
    return <RunConsumer>
        {run => <Child {...props} run={run}/>}
    </RunConsumer>
};