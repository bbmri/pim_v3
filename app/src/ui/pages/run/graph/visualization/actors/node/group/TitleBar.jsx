/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React from 'react'
import styled from 'styled-components'

import {RunConsumer} from '../../../../../Run'
import {NodeConsumer} from '../Node'

class TitleBar extends React.Component {
    status = () => {
        return this.props.run.data.get('status');
    };

    percentage = () => {
        if (!this.status().has(this.props.node.get('id')))
            return '';

        const percentage = this.status().getIn([this.props.node.get('id'), 'progress']) * 100.0;

        return `${percentage.toFixed(1)}%`;
    };

    render() {
        return <StyledTitleBar>
            <StyledTitle>
                <StyledName>
                    {this.props.node.get('title')}
                </StyledName>
                <StyledPercentage>
                    {this.percentage()}&nbsp;
                </StyledPercentage>
            </StyledTitle>
        </StyledTitleBar>
    }
}

const StyledTitleBar = styled.div`
    display: flex;
    justify-content: space-between;
    font-size: 2.7rem;
    font-weight: bold;
    width: 100%;
    padding: 20px;
`;

const StyledTitle = styled.div`
    width: 100%;
    display: flex;
    justify-content: space-between;
    font-size: 64pt;
    font-weight: bold;
    pointer-events: none;
    cursor: default;
    overflow: hidden;
    line-height: normal;
`;

const StyledName = styled.div`
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    //white-space: nowrap;
`;

const StyledPercentage = styled.div`
`;

export default props => {
    return <RunConsumer>
        {run => <NodeConsumer>
            {node => <TitleBar {...props} run={run} node={node}/>}
        </NodeConsumer>}
    </RunConsumer>
};