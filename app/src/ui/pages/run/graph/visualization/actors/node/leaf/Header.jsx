/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React from 'react'
import styled from 'styled-components'

import {RunConsumer} from '../../../../../Run'
import {NodeConsumer} from '../Node'

import TitleBar from './TitleBar'
import Port from './port/Port'

class Header extends React.Component {
    render() {
        const virtualInPort  = this.props.node.getIn(['inPorts', '__in__']);
        const virtualOutPort = this.props.node.getIn(['outPorts', '__out__']);

        return <StyledHeader>
                <StyledVirtualInputPort className='ports in'>
                    <Port port={virtualInPort} in={true}/>
                </StyledVirtualInputPort>
                <TitleBar {...this.props}/>
                <StyledVirtualOutputPort className='ports out'>
                    <Port port={virtualOutPort} in={false}/>
                </StyledVirtualOutputPort>
        </StyledHeader>
    }
}

const StyledHeader = styled.div`
    display: flex;
    align-items: center;
    border-radius: ${props => props.theme.node.leaf.border.radius}px ${props => props.theme.node.leaf.border.radius}px 0 0;
    padding: ${props => props.theme.node.leaf.padding};
    min-width: 500px;
`;

const StyledVirtualInputPort = styled.div`
    padding-right: 20px;
`;

const StyledVirtualOutputPort = styled.div`
    padding-left: 20px;
`;

export default props => {
    return <RunConsumer>
        {run => <NodeConsumer>
            {node => <Header {...props} run={run} node={node}/>}
        </NodeConsumer>}
    </RunConsumer>
};