/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import {RunConsumer} from 'ui/pages/run/Run'

const ActorContext = React.createContext({});

export const ActorProvider = ActorContext.Provider;
export const ActorConsumer = ActorContext.Consumer;

class Actor extends React.Component {
    render() {
        const value = {
        };

        return <ActorProvider value={value}>
            <StyledActor zIndex={this.props.zIndex}>
                {this.props.children}
            </StyledActor>
        </ActorProvider>
    }
}

Actor.propTypes = {
    zIndex: PropTypes.number.isRequired
};

const StyledActor = styled.div`
    position: absolute;
    left: 0;
    top: 0;
    z-index: ${props => props.zIndex};
    pointer-events: none;
`;

export default props => {
    return <RunConsumer>
        {run => <Actor {...props} run={run}/>}
    </RunConsumer>
};