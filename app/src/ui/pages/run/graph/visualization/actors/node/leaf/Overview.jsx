/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React from 'react'
import styled from 'styled-components'
import PubSub from 'pubsub-js'

const getUuid = require('uuid-by-string');

import {
    Button,
    Popup
} from 'semantic-ui-react'

import HelpContext from 'ui/miscellaneous/HelpContext'

import {NavigationConsumer} from 'ui/Navigation'
import {RunConsumer} from 'ui/pages/run/Run'
import {GraphConsumer} from '../../../Graph'
import {NodeConsumer} from '../Node'
import * as d3 from "d3";

class Overview extends React.Component {
    constructor(props) {
        super(props);

        this.overviewRef  = React.createRef();
        this.previousZoom = null;

        this.zoomRange = {
            min: 0,
            max: 0.1
        };

        this.state = {
            visible: false
        }
    }

    componentDidMount() {
        this.onZoomTransformChanged('RUN_ZOOM_TRANSFORM_CHANGED', this.props.graph.zoomTransform);

        this.listeners = {
            zoomChanged: PubSub.subscribe('RUN_ZOOM_TRANSFORM_CHANGED', this.onZoomTransformChanged)
        };

        this.overviewRef.current.addEventListener('mouseenter', this.mouseEnter);
        this.overviewRef.current.addEventListener('mouseleave', this.mouseLeave);
    }

    componentWillUnmount() {
        for (let listener of Object.values(this.listeners)) {
            PubSub.unsubscribe(listener);
        }

        this.overviewRef.current.removeEventListener('mouseenter', this.mouseEnter);
        this.overviewRef.current.removeEventListener('mouseleave', this.mouseLeave);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        let opacity = null;

        if (this.state.visible && !prevState.visible)
            opacity = 1;

        if (!this.state.visible && prevState.visible)
            opacity = 0;

        console.log(this.state.visible)
        d3.select(this.overviewRef.current)
            .style('pointer-events', this.state.visible ? 'all' : 'none');

        const transition = this.props.run.transition();

        if (opacity !== null) {
            d3.select(this.overviewRef.current)
                .transition()
                .ease(d3[transition.entrance.ease])
                .duration(transition.entrance.duration)
                .style('opacity', opacity);
        }
    }

    mouseEnter = () => {
        this.props.run.actions.nodeReceiveFocus(this.props.node.get('id'));
    };

    mouseLeave = () => {
        this.props.run.actions.nodeLooseFocus(this.props.node.get('id'));
    };

    isItemVisible = (zoomFactor) => {
        const zoomRange = this.zoomRange;
        return zoomFactor > zoomRange.min && zoomFactor <= zoomRange.max;
    };

    onZoomTransformChanged = (message, data) => {
        if (this.previousZoom !== null) {
            const visibility = {
                current: this.isItemVisible(data.k),
                previous: this.isItemVisible(this.previousZoom.k)
            };

            const visibilityChanged = visibility.current !== visibility.previous;

            let newState = JSON.parse(JSON.stringify(this.state));

            newState.visible = visibility.current;

            if (visibilityChanged) {
                this.setState(newState);
            }
        } else {
            let newState = JSON.parse(JSON.stringify(this.state));

            newState.visible = this.isItemVisible(data.k);

            this.setState(newState);
        }

        this.previousZoom = JSON.parse(JSON.stringify(data));
    };

    render() {
        return <StyledOverview ref={this.overviewRef}/>;
    }
}

const StyledOverview = styled.div`
    position: absolute;
    z-index: 10;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    opacity: 0;
    background-color: ${props => props.theme.node.leaf.background.color};
    border: 1px solid hsl(0, 0%, 50%);
`;

export default props => {
    return <NavigationConsumer>
        {navigation => <RunConsumer>
            {run => <GraphConsumer>
                {graph => <NodeConsumer>
                    {node => <Overview  {...props} node={node} run={run} graph={graph} navigation={navigation}/>}
                </NodeConsumer>}
            </GraphConsumer>}
        </RunConsumer>}
    </NavigationConsumer>
};




// import React from 'react'
// import styled from 'styled-components'
//
// import PubSub from 'pubsub-js'
//
// import * as d3 from 'd3'
//
// import {RunConsumer} from 'ui/pages/run/Run'
// import {GraphConsumer} from '../../../Graph'
// import {NodeConsumer} from '../Node'
//
// class Overview extends React.Component {
//     constructor(props) {
//         super(props);
//
//         this.focusRef      = React.createRef();
//         this.previousZoom  = null;
//         this.zoomThreshold = 0.15;
//     }
//
//     componentDidMount() {
//         this.updateVisibility(this.props.graph.zoomTransform, false);
//
//         this.listeners = {
//             zoomChanged: PubSub.subscribe('RUN_ZOOM_TRANSFORM_CHANGED', this.onZoomTransformChanged)
//         }
//     }
//
//     componentWillUnmount() {
//         for (let listener of Object.values(this.listeners)) {
//             PubSub.unsubscribe(listener);
//         }
//     }
//
//     onZoomTransformChanged = (message, data) => {
//         if (this.previousZoom !== null) {
//             if (data.k < this.zoomThreshold && this.previousZoom.k > this.zoomThreshold) {
//                 this.updateVisibility(data);
//             }
//
//             if (data.k > this.zoomThreshold && this.previousZoom.k < this.zoomThreshold) {
//                 this.updateVisibility(data);
//             }
//         }
//     };
//
//     onMouseOver = (event) => {
//         this.props.run.actions.nodeReceiveFocus(this.props.node.get('id'));
//
//     };
//
//     onMouseLeave = (event) => {
//         this.props.run.actions.nodeLooseFocus(this.props.node.get('id'));
//     };
//
//     onClick = (event) => {
//         event.stopPropagation();
//
//         if (this.canExpand()) {
//             this.props.run.actions.expandNode(this.props.node.get('id'));
//         } else {
//             this.props.run.actions.zoomToNode(this.props.node.get('id'));
//         }
//     };
//
//     setVisible = (opacity, animate) => {
//         const transition = this.props.run.transition();
//
//         d3.select(this.focusRef.current)
//             .transition()
//             .ease(d3[transition.entrance.ease])
//             .duration(animate ? transition.entrance.duration : 0)
//             .style('opacity', opacity);
//     };
//
//     updateVisibility = (zoomTransform, animate = true) => {
//         if (this.previousZoom === null) {
//             this.setVisible(zoomTransform.k < this.zoomThreshold ? 1 : 0, false);
//         } else {
//             if (zoomTransform.k < this.zoomThreshold && this.previousZoom.k > this.zoomThreshold) {
//                 this.setVisible(1, animate);
//             }
//
//             if (zoomTransform.k > this.zoomThreshold && this.previousZoom.k < this.zoomThreshold) {
//                 this.setVisible(0, animate);
//             }
//         }
//
//         this.previousZoom = JSON.parse(JSON.stringify(zoomTransform));
//     };
//
//     canExpand = () => {
//         return this.props.node.get('children').size > 0;
//     };
//
//     render() {
//         const handlers = {
//             onMouseOver: this.onMouseOver,
//             onMouseLeave: this.onMouseLeave,
//             onClick: this.onClick
//         };
//
//         return <StyledOverview ref={this.focusRef} {...handlers} {...this.props}>
//             <StyledTitle>{this.props.node.get('title')}</StyledTitle>
//             <StyledPercentage>{this.props.node.getIn(['status', 'progress'])}%</StyledPercentage>
//         </StyledOverview>
//     }
// }
//
// const StyledOverview = styled.div`
//     border-radius: ${props => props.theme.node.leaf.border.radius}px;
//     line-height: normal;
//     pointer-events: all;
//     position: absolute;
//     left: -${props => props.theme.node.leaf.border.width / 2}px;
//     right: -${props => props.theme.node.leaf.border.width / 2}px;
//     top: -${props => props.theme.node.leaf.border.width / 2}px;
//     bottom: -${props => props.theme.node.leaf.border.width / 2}px;
//     z-index: 5;
//     background-color: ${props => props.theme.node.leaf.background.color};
//     border: 2px solid hsl(0, 0%, 50%);
//     display: flex;
//     flex-direction: column;
//     align-items: center;
//     justify-content: center;
//     padding: 40px;
//     //visibility: hidden;
// `;
//
// const StyledTitle = styled.div`
//     font-size: 60pt;
//     font-weight: bold;
//     color: hsl(0, 0%, 20%);
//     cursor: default;
//     min-width: 0;
//     width: 100%;
//     white-space: nowrap;
//     overflow: hidden;
//     text-overflow: ellipsis;
//     text-align: center;
// `;
//
// const StyledPercentage = styled.div`
//     font-size: 40pt;
//     font-weight: bold;
//     color: hsl(0, 0%, 40%);
//     cursor: default;
//     text-align: center;
// `;
//
// export default props => {
//     return <RunConsumer>
//         {run => <GraphConsumer>
//             {graph => <NodeConsumer>
//                 {node => <Overview {...props} run={run} graph={graph} node={node}/>}
//             </NodeConsumer>}
//         </GraphConsumer>}
//     </RunConsumer>
// };