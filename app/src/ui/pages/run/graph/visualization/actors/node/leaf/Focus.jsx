/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React from 'react'

import styled from 'styled-components'
import {transparentize} from 'polished'

import PubSub from 'pubsub-js'

import * as d3 from 'd3'

import {RunConsumer} from 'ui/pages/run/Run'
import {GraphConsumer} from '../../../Graph'
import {NodeConsumer} from '../Node'

class Focus extends React.Component {
    constructor(props) {
        super(props);

        this.focusRef = React.createRef();
    }

    componentDidMount() {
        d3.select(this.focusRef.current)
            .style('opacity', 0);

        this.listeners = {
            highlightNodes: PubSub.subscribe('RUN_HIGHLIGHT_NODES', this.onHighlightNodes)
        }
    }

    componentWillUnmount() {
        for (let listener of Object.values(this.listeners)) {
            PubSub.unsubscribe(listener);
        }
    }

    onHighlightNodes = (message, data) => {
        if (!data.nodes.includes(this.props.node.get('id')))
            return;

        const transition = this.props.run.transition()[`focus${data.highlight ? 'In' : 'Out'}`];

        d3.select(this.focusRef.current)
            .transition()
            .ease(d3[transition.ease])
            .duration(transition.duration)
            .style('opacity', data.highlight ? 1 : 0);
    };

    render() {
        return <StyledFocus ref={this.focusRef} {...this.props}>
        </StyledFocus>
    }
}

const StyledFocus = styled.div`
    pointer-events: none;
    position: absolute;
    left: -${props => props.theme.node.leaf.border.width / 2}px;
    right: -${props => props.theme.node.leaf.border.width / 2}px;
    top: -${props => props.theme.node.leaf.border.width / 2}px;
    bottom: -${props => props.theme.node.leaf.border.width / 2}px;
    z-index: 0;
    border-radius: ${props => props.theme.node.leaf.border.radius}px;
    border: ${props => props.theme.node.leaf.border.width}px solid ${props => props.theme.node.focus.color};
    box-shadow: ${props => {
        const glow = props.theme.node.leaf.glow;
        return `0 0 ${glow.size}px ${glow.size}px ${transparentize(1 - glow.opacity, props.theme.node.focus.color)}}`;
    }};
`;

export default props => {
    return <RunConsumer>
        {run => <GraphConsumer>
            {graph => <NodeConsumer>
                {node => <Focus {...props} run={run} graph={graph} node={node}/>}
            </NodeConsumer>}
        </GraphConsumer>}
    </RunConsumer>
};