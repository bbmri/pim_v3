/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React from 'react'
import styled from 'styled-components'
import PubSub from 'pubsub-js'

const getUuid = require('uuid-by-string');

import {
    Button,
    Popup
} from 'semantic-ui-react'

import HelpContext from 'ui/miscellaneous/HelpContext'

import {NavigationConsumer} from 'ui/Navigation'
import {RunConsumer} from 'ui/pages/run/Run'
import {GraphConsumer} from '../../../Graph'
import {NodeConsumer} from '../Node'

class Context extends React.Component {
    constructor(props) {
        super(props);

        this.hitRef       = React.createRef();
        this.buttonsRef   = React.createRef();
        this.previousZoom = null;

        this.zoomRange = {
            min: 0.2,
            max: 1000.0
        };

        this.state = {
            visible: false,
            hovering: false
        }
    }

    componentDidMount() {
        this.onZoomTransformChanged('RUN_ZOOM_TRANSFORM_CHANGED', this.props.graph.zoomTransform);

        this.listeners = {
            zoomChanged: PubSub.subscribe('RUN_ZOOM_TRANSFORM_CHANGED', this.onZoomTransformChanged)
        };

        this.hitRef.current.addEventListener('mouseenter', this.mouseEnter);
        this.hitRef.current.addEventListener('mouseleave', this.mouseLeave);
    }

    componentWillUnmount() {
        for (let listener of Object.values(this.listeners)) {
            PubSub.unsubscribe(listener);
        }

        this.hitRef.current.removeEventListener('mouseenter', this.mouseEnter);
        this.hitRef.current.removeEventListener('mouseleave', this.mouseLeave);
    }

    mouseEnter = () => {
        let newState = JSON.parse(JSON.stringify(this.state));

        newState.hovering = true;

        this.setState(newState);
    };

    mouseLeave = () => {
        let newState = JSON.parse(JSON.stringify(this.state));

        newState.hovering = false;

        this.setState(newState);
    };

    isItemVisible = (zoomFactor) => {
        const zoomRange = this.zoomRange;
        return zoomFactor > zoomRange.min && zoomFactor <= zoomRange.max;
    };

    onZoomTransformChanged = (message, data) => {
        if (this.previousZoom !== null) {
            const visibility = {
                current: this.isItemVisible(data.k),
                previous: this.isItemVisible(this.previousZoom.k)
            };

            const visibilityChanged = visibility.current !== visibility.previous;

            let newState = JSON.parse(JSON.stringify(this.state));

            newState.visible = visibility.current;

            if (visibilityChanged) {
                this.setState(newState);
            }
        } else {
            let newState = JSON.parse(JSON.stringify(this.state));

            newState.visible = this.isItemVisible(data.k);

            this.setState(newState);
        }

        this.previousZoom = JSON.parse(JSON.stringify(data));
    };

    jobs = (event) => {
        this.props.navigation.toJobsPage({
            runId: this.props.run.data.getIn(['config', 'runId']),
            nodeId: this.props.node.get('path')
        });

        event.stopPropagation();
    };

    expand = (event) => {
        this.props.run.actions.expandNode(getUuid(this.props.node.get('path')));

        event.stopPropagation();
    };

    zoomToConnectedNodes = (event) => {
        this.props.run.actions.zoomToNodeConnections(this.props.node.get('id'));

        event.stopPropagation();
    };

    zoomToInput = (event) => {
        this.props.run.actions.zoomToNodeInputNodes(this.props.node.get('id'));

        event.stopPropagation();
    };

    zoomToParent = (event) => {
        const segments       = this.props.node.get('path').split('/');
        const parentNodePath = segments.slice(0, -1).join('/');

        this.props.run.actions.zoomToNode(getUuid(parentNodePath));

        event.stopPropagation();
    };

    zoomToOutput = (event) => {
        this.props.run.actions.zoomToNodeOutputNodes(this.props.node.get('id'));

        event.stopPropagation();
    };

    popup = (trigger, status) => {
        return <Popup trigger={trigger}
                      content={status}
                      position='top center'
                      size='mini'
                      inverted/>
    };

    content() {
        const isRoot      = this.props.node.get('level') === 0;
        const isExpanded  = this.props.node.get('expanded');
        const isLeaf      = this.props.node.get('children').size === 0 && !isExpanded;
        const canExpand   = this.props.node.get('children').size > 0 && !isExpanded;
        const connections = this.props.node.get('connections');

        const connected = {
            in: connections.get('fromNodes').size > 0,
            out: connections.get('toNodes').size > 0
        };

        const buttonSize = 'massive';
        const secondary  = true;
        const basic      = false;

        const buttons = {
            zoomToInput: <HelpContext status='Click to zoom to input nodes'>
                <Button secondary={secondary}
                        basic={basic}
                        onClick={this.zoomToInput}
                        icon='double angle left'
                        size={buttonSize}/>
            </HelpContext>,
            zoomToParent: <HelpContext status='Click to zoom to parent'>
                <Button onClick={this.zoomToParent}
                        icon='double angle up'
                        size={buttonSize}/>
            </HelpContext>,
            zoomToOutput: <HelpContext status='Click to zoom to output nodes'>
                <Button secondary={secondary}
                        basic={basic}
                        onClick={this.zoomToOutput}
                        icon='double angle right'
                        size={buttonSize}/>
            </HelpContext>,
            viewJobs: <HelpContext status='View jobs'>
                <Button onClick={this.jobs}
                        icon='tasks'
                        size={buttonSize}/>
            </HelpContext>,
            expandNode: <HelpContext
                status={`Group has ${this.props.node.get('children').size} children, click to expand`}>
                <Button icon='expand arrows alternate'
                        onClick={this.expand}
                        status={`Click to expand ${this.props.node.get('title')}`}
                        size={buttonSize}/>
            </HelpContext>,
            zoomToConnectedNodes: <HelpContext status='Zoom to connected nodes'>
                <Button icon='plug'
                        onClick={this.zoomToConnectedNodes}
                        status={`Zoom to connected nodes of ${this.props.node.get('title')}`}
                        size={buttonSize}/>
            </HelpContext>
        };

        return <StyledContext>
            {this.state.visible ? <StyledButtons ref={this.buttonsRef}>
                <Button.Group secondary={secondary} basic={basic} size={buttonSize}>
                    {connected.in ? this.popup(buttons.zoomToInput, 'Click to zoom to input nodes') : null}
                    {!isRoot ? buttons.zoomToParent : null}
                    {canExpand ? buttons.expandNode : null}
                    {buttons.viewJobs}
                    {isLeaf ? buttons.zoomToConnectedNodes : null}
                    {connected.out ? this.popup(buttons.zoomToOutput, 'Click to zoom to output nodes') : null}
                </Button.Group>
            </StyledButtons> : null}
        </StyledContext>
    }

    render() {
        const context = this.content();

        return <StyledHitRegion ref={this.hitRef}>
            {this.state.hovering ? context : null}
        </StyledHitRegion>;
    }
}

export default props => {
    return <NavigationConsumer>
        {navigation => <RunConsumer>
            {run => <GraphConsumer>
                {graph => <NodeConsumer>
                    {node => <Context  {...props} node={node} run={run} graph={graph} navigation={navigation}/>}
                </NodeConsumer>}
            </GraphConsumer>}
        </RunConsumer>}
    </NavigationConsumer>
};

const StyledHitRegion = styled.div`
    position: absolute;
    z-index: 0;
    left: -150px;
    right: -150px;
    top: -120px;
    bottom: -150px;
    background-color: transparent;
    opacity: 1;
`;

const StyledContext = styled.div`
    position: absolute;
    width: 100%;
    height: 100%;
    pointer-events: none;
`;

const StyledButtons = styled.div`
    position: absolute;
    padding: 20px;
    left: 50%;
    transform: translate(-50%, 0%);
    pointer-events: all;
    display: flex;
`;