/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React from 'react'
import styled from 'styled-components'
import {darken} from 'polished'

import PubSub from 'pubsub-js'

import * as d3 from 'd3'

import {RunConsumer} from 'ui/pages/run/Run'
import {ActorConsumer} from '../../Actor'
import {NodeConsumer} from '../Node'

import HelpContext from 'ui/miscellaneous/HelpContext'

import Header from './Header'
import Focus from './Focus'

class Group extends React.Component {
    constructor(props) {
        super(props);

        this.opacityRef = React.createRef();
        this.contentRef = React.createRef();
    }

    componentDidMount() {
        this.listeners = {
            runLayoutChanged: PubSub.subscribe('RUN_LAYOUT_CHANGED', this.onRunLayoutChanged)
        };
    }

    componentWillUnmount() {
        for (let listener of Object.values(this.listeners)) {
            PubSub.unsubscribe(listener);
        }
    }

    onRunLayoutChanged = (message, data) => {
        this.applyLayout(data.current, this.nodeLayout(data.previous) !== undefined);
    };

    nodeLayout = (layout) => {
        return layout.getIn(['nodes', this.props.node.get('id')]);
    };

    applyLayout = (layout, animate = true) => {
        if (this.nodeLayout(layout) === undefined)
            return;

        const transition = this.props.run.transition();
        const size       = this.nodeLayout(layout).get('size');

        d3.select(this.contentRef.current)
            .transition()
            .ease(d3[transition.update.ease])
            .duration(animate ? transition.update.duration : 0)
            .style('width', size.get('width'))
            .style('height', size.get('height'));
    };

    status = () => {
        return `Click to collapse to ${this.props.node.get('title')}`;
    };

    onMouseOver = (event) => {
        this.props.run.actions.nodeReceiveFocus(this.props.node.get('id'));
    };

    onMouseLeave = (event) => {
        this.props.run.actions.nodeLooseFocus(this.props.node.get('id'));
    };

    onClick = (event) => {
        this.props.run.actions.collapseNode(this.props.node.get('id'));
    };

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        return false;
    }

    render() {
        const elementId = `${this.props.node.get('path').split('/').join('_')}_group`;

        const handlers = {
            onMouseOver: this.onMouseOver,
            onMouseLeave: this.onMouseLeave,
            onClick: this.onClick
        };

        return <HelpContext status={this.status}>
        <StyledGroup id={elementId}
                            {...handlers}
                            node={this.props.node}
                            ref={this.opacityRef}>
            <StyledContent ref={this.contentRef}>
                <Header {...this.props}/>
                <Focus {...this.props}/>
            </StyledContent>
        </StyledGroup>
        </HelpContext>
    }
}

const StyledGroup = styled.div`
    display: flex;
    flex-direction: column;
    border-radius: ${props => props.theme.node.group.border.radius}px;
    background-color: hsla(0, 0%, 80%, 0.5);//${props => darken(props.node.get('level') / 10, `hsl(0, 0%, 100%)`)};
    border: 10px dotted hsla(0, 0%, 0%, 0.3);
    pointer-events: all;
    box-shadow: 80px 80px 100px hsla(0, 0%, 50%, 0.2);
`;

const StyledContent = styled.div`
    
`;

export default props => {
    return <RunConsumer>
        {run => <ActorConsumer>
            {actor => <NodeConsumer>
                {node => <Group {...props} run={run} node={node} actor={actor}/>}
            </NodeConsumer>}
        </ActorConsumer>}
    </RunConsumer>
};