/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React from 'react'
import styled from 'styled-components'

import {RunConsumer} from 'ui/pages/run/Run'
import {ActorConsumer} from '../Actor'

import PubSub from 'pubsub-js'

import * as d3 from 'd3'

const NodeContext = React.createContext({});

export const NodeProvider = NodeContext.Provider;
export const NodeConsumer = NodeContext.Consumer;

import Status from './Status'

class Node extends React.Component {
    constructor(props) {
        super(props);

        this.opacityRef   = React.createRef();
        this.transformRef = React.createRef();
    }

    componentDidMount() {
        this.listeners = {
            runLayoutChanged: PubSub.subscribe('RUN_LAYOUT_CHANGED', this.onRunLayoutChanged)
        };

        requestAnimationFrame(() => {
            const transition = this.props.run.transition();

            d3.select(this.opacityRef.current)
                .transition()
                .ease(d3[transition.entrance.ease])
                .duration(transition.entrance.duration)
                .delay(this.entranceDelay())
                .style('opacity', 1);
        });
    }

    componentWillUnmount() {
        for (let listener of Object.values(this.listeners)) {
            PubSub.unsubscribe(listener);
        }
    }

    entranceDelay() {
        return Math.max(1, this.props.node.get('level') - 1) * this.props.run.transition().update.duration;
    }

    onRunLayoutChanged = (message, data) => {
        this.applyLayout(data.current, this.nodeLayout(data.previous) !== undefined);
    };

    nodeLayout = (layout) => {
        return layout.getIn(['nodes', this.props.node.get('id')]);
    };

    applyLayout = (layout, animate = true) => {
        if (this.nodeLayout(layout) === undefined)
            return;

        const transition = this.props.run.transition();
        const position   = this.nodeLayout(layout).get('position');

        d3.select(this.transformRef.current)
            .transition()
            .ease(d3[transition.update.ease])
            .duration(animate ? transition.update.duration : 0)
            .style('left', position.get('x'))
            .style('top', position.get('y'));
    };

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        return false;
    }

    render() {
        return <NodeProvider value={this.props.node}>
            <Status>
                <StyledOpacity ref={this.opacityRef}>
                    <StyledTransform ref={this.transformRef}>
                        {this.props.children}
                    </StyledTransform>
                </StyledOpacity>
            </Status>
        </NodeProvider>
    }
}

const StyledOpacity = styled.div`
    opacity: 0;
`;

const StyledTransform = styled.div`
    position: absolute;
    left: 0;
    top: 0;
`;

export default props => {
    return <RunConsumer>
        {run => <ActorConsumer>
            {actor => <Node {...props} run={run} actor={actor}/>}
        </ActorConsumer>}
    </RunConsumer>
};