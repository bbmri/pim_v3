/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import {NodeConsumer} from '../Node'

import Port from './port/Port'

class Ports extends React.Component {
    render() {
        let ports = this.props.ports.valueSeq().map(port => {
            return <Port key={port.get('title')} port={port} {...this.props}/>
        });

        const className = `ports ${this.props.in ? 'in' : 'out'}`;

        return <StyledPorts className={className} inPort={this.props.in}>
            {ports}
        </StyledPorts>
    }
}

Ports.propTypes = {
    ports: PropTypes.object.isRequired,
    in: PropTypes.bool.isRequired
};

const StyledPorts = styled.div`
    flex: 1;
    margin: -5px;
    display: flex;
    flex-direction: column;
    align-items: ${props => props.inPort ? 'flex-start' : 'flex-end'};
`;

export default props => {
    return <NodeConsumer>
        {node => <Ports {...props} node={node}/>}
    </NodeConsumer>
};