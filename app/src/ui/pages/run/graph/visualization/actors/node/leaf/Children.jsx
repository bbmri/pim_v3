/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React from 'react'
import styled from 'styled-components'

import {NodeConsumer} from '../Node'

import Child from './Child'

class Children extends React.Component {
    shouldComponentUpdate(nextProps, nextState, nextContext) {
        return nextProps.node.get('children') !== this.props.node.get('children');
    }

    render() {
        let children = this.props.node.get('children').valueSeq().map(child => {
            return <Child key={child} {...this.props} id={child}/>
        });

        return <StyledChildren>
            {children}
        </StyledChildren>
    }
}

const StyledChildren = styled.div`
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    //margin: -6px;
    max-height: 750px;
    overflow-y: auto;
    padding: ${props => props.theme.node.leaf.padding};
    background-color: hsl(0, 0%, 90%);
    border-top: 3px solid hsla(0, 0%, 0%, 0.1);
    border-bottom: 3px solid hsla(0, 0%, 0%, 0.1);
`;

export default props => {
    return <NodeConsumer>
        {node => <Children {...props} node={node}/>}
    </NodeConsumer>
};