/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React from 'react'
import Immutable from 'immutable'

import {RunConsumer} from 'ui/pages/run/Run'
import {NodeConsumer} from './Node'

import * as nodeHelper from 'helpers/node'

const StatusContext = React.createContext({});

export const StatusProvider = StatusContext.Provider;
export const StatusConsumer = StatusContext.Consumer;

class Status extends React.Component {
    nodeStatus = () => {
        const status = this.props.run.data.getIn(['status', this.props.node.get('id')]);

        if (status !== undefined)
            return status;

        return Immutable.fromJS({
            jobs: [0, 0, 0, 0, 0, 0],
            noJobs: {
                completed: 0,
                all: 0
            },
            progress: 0,
            state: nodeHelper.state.uninitialized
        })
    };

    render() {
        return <StatusProvider value={this.nodeStatus()}>
            {this.props.children}
        </StatusProvider>
    }
}

export default props => {
    return <RunConsumer>
        {run => <NodeConsumer>
            {node => <Status {...props} run={run} node={node}/>}
        </NodeConsumer>}
    </RunConsumer>
};