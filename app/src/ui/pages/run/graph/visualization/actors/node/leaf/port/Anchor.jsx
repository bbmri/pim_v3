/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React from 'react'

import styled from 'styled-components'

import {
    Icon
} from 'semantic-ui-react'

import {NodeConsumer} from '../../Node'
import {PortConsumer} from './Port'

class Anchor extends React.Component {
    render() {
        if (this.props.port.virtual) {
            return <StyledImplicitAnchor className='anchor'>
                <StyledAnchorIcon connected={this.props.port.connected}>
                    <Icon name='plus' fitted size='large'/>
                </StyledAnchorIcon>
            </StyledImplicitAnchor>
        }

        return <StyledAnchor className='anchor'
                             collapsed={this.props.port.collapsed}
                             connected={this.props.port.connected}/>
    }
}

const StyledImplicitAnchor = styled.div`
    text-align: center;
`;

const StyledAnchorIcon = styled.div`
    padding: 3px;
    opacity: ${props => props.connected ? 1 : 0};
`;

const StyledAnchor = styled.div`
    width: ${props => props.theme.node.leaf.hub.anchor.size}px;
    height: ${props => props.theme.node.leaf.hub.anchor.size}px;
    border-radius: ${props => props.theme.node.leaf.hub.anchor.size}px;
    border: 5px solid hsl(0, 0%, 40%);
    background-color: ${props => props.collapsed ? `hsl(0, 0%, 40%)` : props.connected ? `hsl(0, 0%, 100%)` : `hsl(0, 0%, 70%)`};
    //box-shadow: inset 0 0 3px hsla(0, 0%, 0%, 30%);
    box-shadow: inset 0 0 3px  hsla(0, 0%, 0%, 0.5);
    text-align: center;
    font-weight: bold;
    font-size: 30pt;
`;

export default props => {
    return <NodeConsumer>
        {node => <PortConsumer>
            {port => <Anchor {...props} node={node} port={port}/>}
        </PortConsumer>}
    </NodeConsumer>
};