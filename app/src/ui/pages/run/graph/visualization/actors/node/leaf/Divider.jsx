/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React from 'react'
import styled from 'styled-components'

export default class Divider extends React.Component {
    render() {
        return <StyledDivider/>
    }
}

const StyledDivider = styled.div`
    margin: 0 ${props => props.theme.leafPadding} 0 ${props => props.theme.leafPadding};
    height: 1px;
    background-image: linear-gradient(to right, hsla(0, 0%, 0%, 0%) 0%, hsla(0, 0%, 0%, 35%) 50%, hsla(0, 0%, 0%, 0%) 100%);
`;