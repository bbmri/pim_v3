/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import TitleBar from './TitleBar'

export default class Header extends React.Component {
    render() {
        return <StyledHeader className='header' {...this.props}>
            <TitleBar/>
            {/*<Toolbar {...this.props}/>*/}
        </StyledHeader>
    }
}

Header.propTypes = {
    node: PropTypes.object.isRequired
};

const StyledHeader = styled.div`
    padding: ${props => props.theme.node.group.padding};
    border-radius: ${props => {
        const borderRadius = props.theme.node.group.border.radius;
        return `${borderRadius}px ${borderRadius}px 0 0`;
    }};
    //background-color: hsla(0, 0%, 85%, 0.5);
    overflow: hidden;
    text-overflow: ellipsis;
    //background-image: linear-gradient(to bottom, ${props => props.node.get('color')} 0px, ${props => props.node.get('color')} ${props => props.theme.node.group.stripThickness}px, ${props => props.theme.node.group.background.color} ${props => props.theme.node.group.stripThickness}px);
`;