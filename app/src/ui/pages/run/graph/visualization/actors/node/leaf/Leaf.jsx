/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React from 'react'
import styled from 'styled-components'
import PubSub from 'pubsub-js'

import {RunConsumer} from 'ui/pages/run/Run'
import {ActorConsumer} from '../../Actor'
import {NodeConsumer} from '../Node'

import HelpContext from 'ui/miscellaneous/HelpContext'

import Header from './Header'
import Status from './Status'
import Children from './Children'
import Hub from './Hub'
import Focus from './Focus'
import Divider from './Divider'
import Context from './Context'
import Overview from './Overview'

class Leaf extends React.Component {
    state = {
        hovering: false
    };

    onClick = (event) => {
        if (this.canExpand()) {
            this.props.run.actions.expandNode(this.props.node.get('id'));
        } else {
            this.props.run.actions.zoomToNode(this.props.node.get('id'));
        }
    };

    onMouseOver = (event) => {
        this.props.run.actions.nodeReceiveFocus(this.props.node.get('id'));
    };

    onMouseLeave = (event) => {
        this.props.run.actions.nodeLooseFocus(this.props.node.get('id'));

        PubSub.publish('STATUS_UNSET');
    };

    status = () => {
        const noChildren = this.props.node.get('children').size;

        if (noChildren > 0) {
            const action = this.props.node.get('expanded') ? 'collapse' : 'expand';
            return `${this.props.node.get('title')} has ${noChildren} child${noChildren > 1 ? 'ren' : ''}, click to ${action}`;
        } else {
            return `Click to zoom to ${this.props.node.get('title')}`;
        }
    };

    canExpand = () => {
        return this.props.node.get('children').size > 0;
    };

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        return false;
    }

    render() {
        const elementId   = `${this.props.node.get('path').split('/').join('_')}_leaf`;
        const isRoot      = this.props.node.get('level') === 0;
        const isExpanded  = this.props.node.get('expanded');
        const isLeaf      = this.props.node.get('children').size === 0 && !isExpanded;
        const hasChildren = this.props.node.get('children').size > 0;

        const handlers = {
            onClick: this.onClick,
            onMouseOver: this.onMouseOver,
            onMouseLeave: this.onMouseLeave
        };

        return <HelpContext status={this.status}>
            <StyledLeaf id={elementId}
                        node={this.props.node}
                        {...handlers}>
                <Header/>
                <StyledDivider/>
                {hasChildren && !isExpanded ? <Children {...this.props}/> : null}
                {!isRoot && isLeaf ? <Hub {...this.props} canCollapse={false} exclude={['__in__', '__out__']}/> : null}
                <StyledDivider/>
                {/*<Context {...this.props}/>*/}
                {/*<Overview/>*/}
                <Status/>
                <Focus/>
            </StyledLeaf>
        </HelpContext>
    }
}

const StyledLeaf = styled.div`
    border-radius: ${props => props.theme.node.leaf.border.radius}px;
    line-height: normal;
    pointer-events: all;
    position: relative;
    background-color: ${props => props.theme.node.leaf.background.color};
    border: 2px solid hsl(0, 0%, 40%);
    box-shadow: 25px 25px 25px hsla(0, 0%, 50%, 0.6);
`;

const StyledDivider = styled.div`
    //border-top: 7px solid hsla(0, 0%, 0%, 0.3);
    //height: 3px;
    //background-color: hsl(0, 0%, 80%);
    //box-shadow: inset 0 0 2px hsla(0, 0%, 0%, 0.5);
    //border-radius: 2px;
    /*margin: ${props => {
        return `0 ${props.theme.node.leaf.padding} 0 ${props.theme.node.leaf.padding}`;    
    }};*/
`;

export default props => {
    return <RunConsumer>
        {run => <ActorConsumer>
            {actor => <NodeConsumer>
                {node => <Leaf {...props} run={run} node={node} actor={actor}/>}
            </NodeConsumer>}
        </ActorConsumer>}
    </RunConsumer>
};