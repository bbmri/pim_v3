/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import {transparentize} from 'polished'

import {
    Popup
} from 'semantic-ui-react'

import {RunConsumer} from '../../../../../../Run'
import {NodeConsumer} from '../../Node'

import Anchor from './Anchor'
import Label from './Label'

import HelpContext from 'ui/miscellaneous/HelpContext'

const PortContext = React.createContext({});

export const PortProvider = PortContext.Provider;
export const PortConsumer = PortContext.Consumer;

class Port extends React.Component {
    constructor(props) {
        super(props);

        this.container = React.createRef();
    }

    onClick = (event) => {
        if (this.props.port.get('name').includes('collapsed')) {
            if (this.props.in) {
                this.props.run.actions.zoomToNodeInputNodes(this.props.node.get('id'));
            } else {
                this.props.run.actions.zoomToNodeOutputNodes(this.props.node.get('id'));
            }
        } else {
            if (this.props.in) {
                this.props.run.actions.zoomToInportConnectedNodes(this.props.node.get('id'), this.props.port.get('name'));
            } else {
                this.props.run.actions.zoomToOutportConnectedNodes(this.props.node.get('id'), this.props.port.get('name'));
            }
        }

        event.stopPropagation();
    };

    status = () => {
        event.stopPropagation();

        if (this.props.port.get('name') === '__in__')
            return 'Click to zoom to connected input nodes';

        if (this.props.port.get('name') === '__out__')
            return 'Click to zoom to connected output nodes';

        if (this.props.port.get('name').includes('collapsed')) {
            return `Click to expand ports hub`;
        } else {
            const prefix = `${this.props.in ? 'Input' : 'Output'} port:`;
            const title  = this.props.port.get('title');

            if (!this.connected())
                return `${prefix} title`;

            return `${prefix} ${title}, click to zoom to connected node(s)`;
        }
    };

    noLinks = () => {
        const links = this.props.port.getIn(['connections', 'links']);
        return links.size;
    };

    connected = () => {
        return this.noLinks() > 0;
    };

    render() {
        const props = {
            inPort: this.props.in,
            port: this.props.port,
            collapsed: this.props.port.get('name').includes('collapsed'),
            connected: this.connected(),
            virtual: ['__in__', '__out__'].includes(this.props.port.get('name'))
        };

        const anchor = <Anchor key='anchor'/>;

        let items = [anchor];

        if (!props.virtual)
            items.push(<Label key='label'/>);

        if (!this.props.in) {
            items = items.reverse();
        }

        const handlers = this.connected() ? {
            onClick: this.onClick
        } : null;

        const port = <StyledPort id={this.props.port.get('name')}
                                 connected={props.connected}
                                 virtual={this.props.virtual}
                                 {...handlers}>
            {items}
        </StyledPort>;

        return <PortProvider value={props}>
            <HelpContext status={this.status}>
                {port}
            </HelpContext>
        </PortProvider>
    }
}

Port.propTypes = {
    port: PropTypes.object.isRequired,
    in: PropTypes.bool.isRequired
};

const StyledPort = styled.div`
    display: flex;
    align-items: center;
    margin: 5px;
    padding: 4px;
    pointer-events: all;
    border-radius: 15px;
    cursor: ${props => props.connected ? 'pointer' : 'default'};
    &:hover {
      background-color: ${props => (props.connected && !props.virtual) ? transparentize(0.4, props.theme.node.focus.color) : 'none'};
    }
`;

export default props => {
    return <RunConsumer>
        {run => <NodeConsumer>
            {node => <Port {...props} run={run} node={node}/>}
        </NodeConsumer>}
    </RunConsumer>
};