/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React from 'react'

import styled from 'styled-components'

import {NodeConsumer} from '../../Node'
import {PortConsumer} from './Port'

class Label extends React.Component {
    render() {
        let classes = ['label'];

        classes.push(this.props.port.inPort ? 'in' : 'out');

        if (this.props.port.connected)
            classes.push('connected');

        if (this.props.port.collapsed)
            classes.push('collapsed');

        const noConnections = this.props.port.port.getIn(['connections', 'links']).size;
        const title = this.props.port.collapsed ? new Array(noConnections + 1).join('•') : this.props.port.port.get('title');
        // const title = this.props.port.collapsed ? '' : this.props.port.port.get('title');

        return <StyledLabel className={classes.join(' ')} {...this.props}>
            {title}
        </StyledLabel>
    }
}

const StyledLabel = styled.div`
    display: flex;
    align-items: center;
    line-height: normal;
    color: hsl(0, 0%, 20%);
    font-size: 32px;
    font-weight: 700;
    padding:${props => props.port.inPort ? '0 6px 0 15px' : '0 15px 0 6px'};
`;

export default props => {
    return <NodeConsumer>
        {node => <PortConsumer>
            {port => <Label {...props} node={node} port={port}/>}
        </PortConsumer>}
    </NodeConsumer>
};