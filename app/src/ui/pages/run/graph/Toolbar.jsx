/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React, {Component} from 'react'
import PropTypes from 'prop-types'

const getUuid = require('uuid-by-string');

import {
    Button,
    Segment,
    Checkbox
} from 'semantic-ui-react'

import {RunConsumer} from 'ui/pages/run/Run'
import {NavigationConsumer} from 'ui/Navigation'

import HelpContext from 'ui/miscellaneous/HelpContext'
import LastUpdated from 'ui/miscellaneous/LastUpdated'

class Toolbar extends Component {
    render() {
        return <Segment attached='bottom' secondary size='mini'>
            <div style={{display: 'flex', alignItems: 'center'}}>
                <HelpContext status='Zoom to the extents of the graph'>
                    <Button basic
                            size='mini'
                            onClick={() => this.props.run.actions.zoomToNode(getUuid('root'))}>Zoom extents</Button>
                </HelpContext>
                <div style={{width: 10}}/>
                <HelpContext status='Automatically zoom to nodes when expanding/collapsing nodes'>
                    <Checkbox label='Auto-zoom'
                              checked={this.props.run.data.getIn(['config', 'autoZoom'])}
                              onClick={() => this.props.run.actions.toggleAutoZoom()}/>
                </HelpContext>
                <div style={{width: 10}}/>
                <HelpContext status='Animate in between graph states'>
                    <Checkbox label='Animations'
                              checked={this.props.run.data.getIn(['config', 'animation'])}
                              onClick={() => this.props.run.actions.toggleAnimation()}/>
                </HelpContext>
                <div style={{flex: 1}}/>
                {/*<HelpContext status='Toggle compact nodes on/off'>
                    <Checkbox label='Compact nodes'
                              checked={this.props.run.data.getIn(['config', 'compactNode'])}
                              onClick={() => this.props.run.actions.toggleCompactNode()}/>
                </HelpContext>*/}
                {/*<LastUpdated name='status' lastUpdated={this.props.run.data.getIn(['api', 'lastFetched'])} updating={this.props.run.data.getIn(['api', 'status', 'fetching'])}/>*/}
            </div>
        </Segment>
    }
}

Toolbar.propTypes = {
    run: PropTypes.object.isRequired,
    navigation: PropTypes.object.isRequired
};

export default props => {
    return <NavigationConsumer>
        {navigation => <RunConsumer>
            {run => <Toolbar {...props} run={run} navigation={navigation}/>}
        </RunConsumer>}
    </NavigationConsumer>
};