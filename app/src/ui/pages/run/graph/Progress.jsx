/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React, {Component} from 'react'

import {RunConsumer} from 'ui/pages/run/Run'
import {NodeProvider} from './visualization/actors/node/Node'

import ProgressBar from 'ui/miscellaneous/ProgressBar'

class Progress extends Component {
    rootNode = () => {
        return this.props.run.data.getIn(['nodes', 'root']);
    };

    render() {
        return <NodeProvider value={this.rootNode()}>
            {this.props.children}
        </NodeProvider>;
    }
}

export default props => {
    return <RunConsumer>
        {run => <Progress {...props} run={run}/>}
    </RunConsumer>
};