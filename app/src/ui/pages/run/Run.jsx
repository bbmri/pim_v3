/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React from 'react'

import {withCookies} from 'react-cookie'

import PubSub from 'pubsub-js'

import Immutable from 'immutable'

const getUuid = require('uuid-by-string');

import AppApi from 'api/api'

import MeasureTime from 'helpers/measureTime'
import * as nodeHelper from 'helpers/node'
import * as jobHelper from 'helpers/job'
import * as linkHelper from 'helpers/link'

import Layouter from 'visualization/layouter'

const RunContext = React.createContext({});

export const RunProvider = RunContext.Provider;
export const RunConsumer = RunContext.Consumer;

const defaultApi = {
    fetching: false,
    noFetches: 0,
    error: null,
    response: null,
    lastFetched: ''
};

const defaultRun = {
    config: {
        runId: '',
        autoZoom: true,
        animation: true,
        compactNode: false,
        updateInterval: 2500
    },
    nodes: {},
    links: {},
    layout: {
        nodes: {},
        links: {},
        updating: false
    },
    status: {},
    filter: {
        node: '',
    },
    viewCache: {},
    transition: {
        entrance: {
            duration: 300,
            ease: 'easeQuad',
            delay: 0
        },
        update: {
            duration: 650,
            ease: 'easeQuad',
            delay: 0
        },
        exit: {
            duration: 50,
            ease: 'easeQuad',
            delay: 0
        },
        focusIn: {
            duration: 80,
            ease: 'easeQuad',
            delay: 0
        },
        focusOut: {
            duration: 150,
            ease: 'easeQuad',
            delay: 0
        }
    },
    editMode: false,
    api: {
        graph: {
            ...defaultApi
        },
        status: {
            ...defaultApi
        }
    }
};

class Run extends React.Component {
    constructor(props) {
        super(props);

        this.data = Immutable.fromJS(defaultRun);

        this.layoutDirty      = false;
        this.suspendFocus     = false;
        this.suspendTimer     = null;
        this.synchronizeTimer = true;
    }

    componentWillMount() {
        this.readCookies();
    }

    componentDidMount() {
        window.addEventListener('beforeunload', this.writeCookies.bind(this));
    }

    readCookies() {
        logger.debug(`Loading settings from cookies`);

        this.setAutoZoom(this.props.cookies.get('runAutoZoom') === 'true');
        this.setAnimation(this.props.cookies.get('runAnimation') === 'true');
    }

    writeCookies = () => {
        logger.debug(`Writing settings to cookies`);

        this.props.cookies.set('runAutoZoom', this.data.getIn(['config', 'autoZoom']));
        this.props.cookies.set('runAnimation', this.data.getIn(['config', 'animation']));
    };

    startSynchronization = () => {
        logger.debug(`Start synchronization`);

        if (this.synchronizeTimer !== null) {
            clearInterval(this.synchronizeTimer);
            this.synchronizeTimer = null;
        }

        if (this.synchronizeTimer === null) {
            this.synchronizeTimer = setInterval(this.synchronize, this.data.getIn(['config', 'updateInterval']));
        }

        // this.synchronize();
    };

    stopSynchronization = () => {
        logger.debug(`Stop synchronization`);

        if (this.synchronizeTimer !== null) {
            clearInterval(this.synchronizeTimer);
            this.synchronizeTimer = null;
        }
    };

    synchronize = () => {
        logger.debug(`Synchronizing`);

        if (this.data.getIn(['api', 'status', 'fetching']))
            return;

        this._synchronizeStatus();
    };

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.layoutDirty) {
            this.layoutDirty = false;
            this._computeLayout();
        }
    }

    setAutoZoom = (autoZoom) => {
        logger.debug(`Set automatic zoom`);

        this._setData(this.data.setIn(['config', 'autoZoom'], autoZoom));

        this.forceUpdate();

        if (this.data.getIn(['config', 'autoZoom']))
            PubSub.publish('RUN_ZOOM_TO_NODES', [getUuid('root')]);
    };

    toggleAutoZoom = () => {
        logger.debug(`Toggle automatic zoom`);

        this.setAutoZoom(!this.data.getIn(['config', 'autoZoom']));

        if (this.data.getIn(['config', 'autoZoom']))
            PubSub.publish('RUN_ZOOM_TO_NODES', [getUuid('root')]);
    };

    setAnimation = (animation) => {
        logger.debug(`Set animation`);

        this._setData(this.data.setIn(['config', 'animation'], animation));

        this.forceUpdate();
    };

    toggleAnimation = () => {
        logger.debug(`Toggle animation`);

        this.setAnimation(!this.data.getIn(['config', 'animation']));
    };

    toggleCompactNode = () => {
        logger.debug(`Toggle compact node`);

        this.setCompactNode(!this.data.getIn(['config', 'compactNode']));
    };

    setCompactNode = (compactNode) => {
        logger.debug(`Set compact node`);

        this._setData(this._updateGraph(this.data.setIn(['config', 'compactNode'], compactNode)));
    };

    expandNode = (nodeId) => {
        logger.debug(`Expand node`);

        this.unhighlightAll();

        this._setData(this._updateGraph(this.data.setIn(['nodes', nodeId, 'expanded'], true)));

        if (this.data.getIn(['config', 'autoZoom'])) {
            PubSub.publish('RUN_ZOOM_TO_NODES', [nodeId]);
        }
    };

    collapseNode = (nodeId) => {
        logger.debug(`Collapse node`);

        this.unhighlightAll();

        this._setData(this._updateGraph(this.data.setIn(['nodes', nodeId, 'expanded'], false)));

        const segments = this.data.getIn(['nodes', nodeId, 'path']).split('/');

        let zoomNodePath = 'root';

        if (segments.length > 1) {
            zoomNodePath = segments.slice(0, -1).join('/');
        }

        if (this.data.getIn(['config', 'autoZoom'])) {
            PubSub.publish('RUN_ZOOM_TO_NODES', [getUuid(zoomNodePath)]);
        }
    };

    toggleNodeExpanded = (nodeId) => {
        logger.debug(`Toggle node expanded`);

        this.unhighlightAll();

        const expanded = this.data.getIn(['nodes', nodeId, 'expanded']);

        this._setData(this._updateGraph(this.data.setIn(['nodes', nodeId, 'expanded'], !expanded)));

        if (this.data.getIn(['config', 'autoZoom'])) {
            PubSub.publish('RUN_ZOOM_TO_NODES', [nodeId]);
        }
    };

    expandAll = () => {
        logger.debug(`Expand all nodes`);

        this.setViewDepth(100);
    };

    collapseAll = () => {
        logger.debug(`Collapse all nodes`);

        this.unhighlightAll();

        this._setData(this._updateGraph(this.data.update('nodes', item => item.map(node => {
            if (node.get('children').size > 0) {
                node = node.set('expanded', false);
            }

            return node;
        }))));

        if (this.data.getIn(['config', 'autoZoom']))
            PubSub.publish('RUN_ZOOM_TO_NODES', [getUuid('root')]);
    };

    expandNodeHub = (nodeId) => {
        logger.debug('Expand node hub');

        this.unhighlightAll();

        this._setData(this._updateGraph(this.data.setIn(['nodes', nodeId, 'hubExpanded'], true)));

        if (this.data.getIn(['config', 'autoZoom']))
            PubSub.publish('RUN_ZOOM_TO_NODES', [nodeId]);
    };

    collapseNodeHub = (nodeId) => {
        logger.debug('Collapse node hub');

        this.unhighlightAll();

        this._setData(this._updateGraph(this.data.setIn(['nodes', nodeId, 'hubExpanded'], false)));

        if (this.data.getIn(['config', 'autoZoom']))
            PubSub.publish('RUN_ZOOM_TO_NODES', [nodeId]);
    };

    toggleNodeHubExpanded = (nodeId) => {
        logger.debug('Toggle node hub expanded');

        this.unhighlightAll();

        const hubExpanded = this.data.getIn(['nodes', nodeId, 'hubExpanded']);

        this._setData(this._updateGraph(this.data.setIn(['nodes', nodeId, 'hubExpanded'], !hubExpanded)));

        if (this.data.getIn(['config', 'autoZoom']))
            PubSub.publish('RUN_ZOOM_TO_NODES', [nodeId]);
    };

    expandLink = (linkId) => {
        logger.debug(`Expand link ${linkId}`);

        this.unhighlightAll();

        const link       = this.data.getIn(['links', linkId]);
        const fromNodeId = getUuid(link.getIn(['from', 'layout', 'nodePath']));
        const toNodeId   = getUuid(link.getIn(['to', 'layout', 'nodePath']));

        let newData = this.data;

        newData = newData.setIn(['nodes', fromNodeId, 'expanded'], true);
        newData = newData.setIn(['nodes', toNodeId, 'expanded'], true);

        this._setData(this._updateGraph(newData));

        if (this.data.getIn(['config', 'autoZoom']))
            PubSub.publish('RUN_ZOOM_TO_NODES', [fromNodeId, toNodeId]);
    };

    setViewDepth = (viewDepth) => {
        logger.debug(`Set view depth`);

        this.unhighlightAll();

        this._setData(this._updateGraph(this.data.update('nodes', item => item.map(node => {
                const level       = node.get('level');
                const hasChildren = node.get('children').size > 0;

                return node.set('expanded', level < viewDepth && hasChildren)
            }
        ))));

        if (this.data.getIn(['config', 'autoZoom']))
            PubSub.publish('RUN_ZOOM_TO_NODES', [getUuid('root')]);
    };

    revealNode = (nodeId) => {
        logger.debug(`Reveal ${nodeId}`);

        this._initiateFocusSuspend();
        this.unhighlightAll();

        let newData = this.data;

        const nodeToReveal = this.data.getIn(['nodes', nodeId]);

        if (nodeToReveal === undefined)
            return;

        const segments = nodeToReveal.get('path').split('/');

        for (let i = 1; i < segments.length; i++) {
            const parentNodeId = segments.slice(0, i).join('/');

            newData = newData.setIn(['nodes', getUuid(parentNodeId), 'expanded'], true);
        }

        this._setData(this._updateGraph(newData));

        PubSub.publish('RUN_HIGHLIGHT_NODES', {
            nodes: [nodeId],
            highlight: true
        });

        if (this.data.getIn(['config', 'autoZoom']))
            PubSub.publish('RUN_ZOOM_TO_NODES', [nodeId]);
    };

    nodeReceiveFocus = (nodeId, highlightDependencies = true) => {
        if (this.zooming || this.suspendFocus)
            return;

        logger.debug(`Node receive focus`);

        this.unhighlightAllNodes();

        PubSub.publish('RUN_HIGHLIGHT_NODES', {
            nodes: [nodeId],
            highlight: true
        });

        if (highlightDependencies) {
            const node            = this.data.getIn(['nodes', nodeId]);
            const nodeConnections = node.get('connections').toJS();
            const highlightNodes  = nodeConnections.fromNodes.concat(nodeConnections.toNodes);

            PubSub.publish('RUN_HIGHLIGHT_NODES', {
                nodes: highlightNodes,
                highlight: true
            });

            PubSub.publish('RUN_HIGHLIGHT_LINKS', {
                links: nodeConnections.links,
                highlight: true
            });
        }
    };

    nodeLooseFocus = (nodeId) => {
        if (this.zooming || this.suspendFocus)
            return;

        logger.debug(`Node loose focus`);

        const nodeConnections = this.data.getIn(['nodes', nodeId, 'connections']).toJS();
        const highlightNodes  = [nodeId].concat(nodeConnections.fromNodes).concat(nodeConnections.toNodes);

        PubSub.publish('RUN_HIGHLIGHT_NODES', {
            nodes: highlightNodes,
            highlight: false
        });

        PubSub.publish('RUN_HIGHLIGHT_LINKS', {
            links: nodeConnections.links,
            highlight: false
        });
    };

    linkReceiveFocus = (linkId) => {
        if (this.zooming || this.suspendFocus)
            return;

        logger.debug(`Link receive focus`);

        this.unhighlightAllLinks();

        const link       = this.data.getIn(['links', linkId]);
        const fromNodeId = link.getIn(['from', 'layout', 'nodeId']);
        const toNodeId   = link.getIn(['to', 'layout', 'nodeId']);

        PubSub.publish('RUN_HIGHLIGHT_NODES', {
            nodes: [fromNodeId, toNodeId],
            highlight: true
        });

        PubSub.publish('RUN_HIGHLIGHT_LINKS', {
            links: [linkId],
            highlight: true
        });

        if (link.get('implicit')) {
            PubSub.publish('STATUS_SET', `Click to zoom to link nodes`);
        } else {
            PubSub.publish('STATUS_SET', `Data type: ${link.get('dataType') === '' ? 'undefined' : link.get('dataType')}, click to zoom`);
        }
    };

    linkLooseFocus = (linkId) => {
        if (this.zooming || this.suspendFocus)
            return;

        logger.debug(`Node loose focus`);

        const link       = this.data.getIn(['links', linkId]);
        const fromNodeId = link.getIn(['from', 'layout', 'nodeId']);
        const toNodeId   = link.getIn(['to', 'layout', 'nodeId']);

        PubSub.publish('RUN_HIGHLIGHT_NODES', {
            nodes: [fromNodeId, toNodeId],
            highlight: false
        });

        PubSub.publish('RUN_HIGHLIGHT_LINKS', {
            links: [linkId],
            highlight: false
        });

        PubSub.publish('STATUS_UNSET');
    };

    unhighlightAll = () => {
        logger.debug(`Unhighlight all`);

        this.unhighlightAllNodes();
        this.unhighlightAllLinks();
    };

    unhighlightAllNodes = () => {
        logger.debug(`Unhighlight all nodes`);

        PubSub.publish('RUN_HIGHLIGHT_NODES', {
            nodes: this.data.get('nodes').keySeq().toJS(),
            highlight: false
        });
    };

    unhighlightAllLinks = () => {
        logger.debug(`Unhighlight all links`);

        PubSub.publish('RUN_HIGHLIGHT_LINKS', {
            links: this.data.get('links').keySeq().toJS(),
            highlight: false
        });
    };

    zoomToNode = (nodeId = getUuid('root')) => {
        logger.debug(`Zoom to node`);

        this._initiateFocusSuspend();
        this.unhighlightAll();

        PubSub.publish('RUN_HIGHLIGHT_NODES', {
            nodes: [nodeId],
            highlight: true
        });

        PubSub.publish('RUN_ZOOM_TO_NODES', [nodeId]);
    };

    zoomToNodes = (nodeIds) => {
        logger.debug(`Zoom to nodes`);

        PubSub.publish('RUN_ZOOM_TO_NODES', nodeIds);
    };

    zoomToNodeConnections = (nodeId) => {
        logger.debug(`Zoom to node connections`);

        const data      = this.data;
        const fromNodes = data.getIn(['nodes', nodeId, 'connections', 'fromNodes']).toJS();
        const toNodes   = data.getIn(['nodes', nodeId, 'connections', 'toNodes']).toJS();
        const nodes     = fromNodes.concat(toNodes);//.concat([nodeId]);

        this._initiateFocusSuspend();
        this.unhighlightAll();

        PubSub.publish('RUN_HIGHLIGHT_NODES', {
            nodes: nodes,
            highlight: true
        });

        PubSub.publish('RUN_ZOOM_TO_NODES', nodes);
    };

    zoomToNodeInputNodes = (nodeId) => {
        logger.debug(`Zoom to input nodes`);

        const node  = this.data.getIn(['nodes', nodeId]);
        const nodes = node.getIn(['connections', 'fromNodes']).toJS();

        this._initiateFocusSuspend();
        this.unhighlightAll();

        PubSub.publish('RUN_HIGHLIGHT_NODES', {
            nodes: nodes,
            highlight: true
        });

        PubSub.publish('RUN_ZOOM_TO_NODES', nodes);
    };

    zoomToNodeOutputNodes = (nodeId) => {
        logger.debug(`Zoom to output nodes`);

        const node  = this.data.getIn(['nodes', nodeId]);
        const nodes = node.getIn(['connections', 'toNodes']).toJS();

        this._initiateFocusSuspend();
        this.unhighlightAll();

        PubSub.publish('RUN_HIGHLIGHT_NODES', {
            nodes: nodes,
            highlight: true
        });

        PubSub.publish('RUN_ZOOM_TO_NODES', nodes);
    };

    zoomToInportConnectedNodes = (nodeId, portName) => {
        logger.debug(`Zoom to input port connected nodes`);

        const node  = this.data.getIn(['nodes', nodeId]);
        const nodes = node.getIn(['inPorts', portName, 'connections', 'nodes']).toJS();

        this._initiateFocusSuspend();
        this.unhighlightAll();

        PubSub.publish('RUN_HIGHLIGHT_NODES', {
            nodes: nodes,
            highlight: true
        });

        PubSub.publish('RUN_ZOOM_TO_NODES', nodes);
    };

    zoomToOutportConnectedNodes = (nodeId, portName) => {
        logger.debug(`Zoom to output port connected nodes`);

        const node  = this.data.getIn(['nodes', nodeId]);
        const nodes = node.getIn(['outPorts', portName, 'connections', 'nodes']).toJS();

        this._initiateFocusSuspend();
        this.unhighlightAll();

        PubSub.publish('RUN_HIGHLIGHT_NODES', {
            nodes: nodes,
            highlight: true
        });

        PubSub.publish('RUN_ZOOM_TO_NODES', nodes);
    };

    zoomToLink = (linkId) => {
        const link       = this.data.getIn(['links', linkId]);
        const fromNodeId = link.getIn(['from', 'layout', 'nodeId']);
        const toNodeId   = link.getIn(['to', 'layout', 'nodeId']);

        PubSub.publish('RUN_ZOOM_TO_NODES', [fromNodeId, toNodeId]);
    };

    load = (runId, nodeId = '') => {
        logger.debug(`Loading ${runId}`);

        if (this.data.getIn(['config', 'runId']) === runId)
            return;

        console.log('load')
        this._setData(Immutable.fromJS(defaultRun).setIn(['config', 'runId'], runId));

        this.forceUpdate();

        this._fetchGraph(getUuid(nodeId === '' ? 'root' : nodeId));
    };

    setNodeFilter = (nodeFilter) => {
        logger.debug(`Set node filter: ${nodeFilter}`);

        this._setData(this.data.setIn(['filter', 'node'], nodeFilter));

        this.forceUpdate();
    };

    transition = () => {
        let transition = this.data.get('transition');

        if (!this.data.getIn(['config', 'animation'])) {
            transition = transition.map((item, type) => {
                return item.set('duration', 0).set('delay', 0);
            });
        }

        return transition.toJS();
    };

    setZooming = (zooming) => {
        logger.debug(`Set zooming: ${zooming}`);

        this.zooming = zooming;
    };

    setViewCache = (name, viewCache) => {
        logger.debug(`Set view cache`, viewCache);

        this._setData(this.data.setIn(['viewCache', name], Immutable.fromJS(viewCache)));
    };

    nodeLayout = (nodeId) => {
        return this.data.getIn(['layout', 'nodes', nodeId]);
    };

    nodeStatus = (nodeId) => {
        const status = this.data.getIn(['status', nodeId]);

        if (status !== undefined)
            return status;

        return Immutable.fromJS({
            jobs: [0, 0, 0, 0, 0, 0],
            noJobs: {
                completed: 0,
                all: 0
            },
            progress: 0
        })
    };

    _initiateFocusSuspend() {
        this.suspendFocus = true;

        if (this.suspendTimer !== null)
            window.clearInterval(this.suspendTimer);

        this.suspendTimer = window.setInterval(this._onEndSuspendFocus, this.transition().update.duration * 2);
    }

    _onEndSuspendFocus = () => {
        this.suspendFocus = false;
    };

    _setData = (data) => {
        this.data = null;
        this.data = data;
    };

    _setLayoutDirty = (layoutDirty = true) => {
        logger.debug(`Set layout dirty`);

        this.setState({});

        this.layoutDirty = layoutDirty;
    };

    _fetchGraph = (revealNodeId = '') => {
        this._setApiFetching();

        AppApi.fetchGraph(this.data.getIn(['config', 'runId']))
            .then(response => {
                let nodes = {};

                for (let nodeId in response.data.nodes) {
                    const apiNode = response.data.nodes[nodeId];

                    const params = {
                        id: apiNode.path,
                        level: apiNode.level,
                        title: nodeId === 'root' ? this.data.getIn(['config', 'runId']) : apiNode.title,
                        description: apiNode.description,
                        path: apiNode.path,
                        type: apiNode.type,
                        children: apiNode.children,
                        inPorts: apiNode.inPorts,
                        outPorts: apiNode.outPorts
                    };

                    const node = nodeHelper.createNode(params);

                    nodes[node.id] = node;
                }

                nodes[getUuid('root')].expanded = true;

                let links = {};

                for (let linkId in response.data.links) {
                    const apiLink = response.data.links[linkId];

                    const params = {
                        title: apiLink.title,
                        description: apiLink.description,
                        dataType: apiLink.dataType,
                        customData: apiLink.customData,
                        fromNode: apiLink.fromNode,
                        fromPort: apiLink.fromPort,
                        toNode: apiLink.toNode,
                        toPort: apiLink.toPort
                    };

                    const link = linkHelper.createLink(params);

                    links[link.id] = link;
                }

                let newData = this.data;

                newData = newData.set('nodes', Immutable.fromJS(nodes));
                newData = newData.set('links', Immutable.fromJS(links));

                this._setData(this._updateGraph(newData));

                if (revealNodeId !== '')
                    this.revealNode(revealNodeId);

                this._setApiSuccess('graph', response);

                this.forceUpdate();
            })
            .catch(error => {
                logger.debug(error);

                this._setApiError('graph', error);

                this.forceUpdate();
            });

        this._setApiFetching(false);
    };

    _closestActiveAncestor = (nodePath, nodes) => {
        const segments = nodePath.split('/');

        if (nodes.getIn([getUuid(nodePath), 'active']))
            return nodePath;

        for (let i = 1; i < segments.length; i++) {
            const ancestorId = segments.slice(0, -i).join('/');

            if (nodes.getIn([getUuid(ancestorId), 'active']))
                return ancestorId;
        }

        return '';
    };

    _updateGraph = (state) => {
        // Determine which nodes are active
        let newState = state.update('nodes', item => item.map(node => {
                const level    = node.get('level');
                const segments = node.get('path').split('/');

                // Leaf is active by default
                let active = true;

                // Ignore root level
                if (level > 0) {

                    // Walk up the hierarchy
                    for (let l = 0; l < level; l++) {
                        const parentNodeId       = segments.slice(0, level - l).join('/');
                        const parentNodeExpanded = state.getIn(['nodes', getUuid(parentNodeId), 'expanded']);

                        // If one of the parent nodes is inactive the node is inactive
                        if (!parentNodeExpanded) {
                            active = false;
                            break;
                        }
                    }
                }

                const connections = {
                    fromNodes: [],
                    toNodes: [],
                    links: []
                };

                let newNode = node;

                newNode = newNode.set('active', active);
                newNode = newNode.set('connections', Immutable.fromJS(connections));

                return newNode;
            }
        ));

        const nodeTypes = newState.get('nodes').reduce((accum, node) => {
            if (accum[node.get('type')] === undefined)
                accum[node.get('type')] = `hsl(${Object.keys(accum).length * 30}, 30%, ${45}%)`;
            return accum;
        }, {});

        // Update node colors
        newState = newState.update('nodes', item => item.map(node => {
            return node.set('color', nodeTypes[node.get('type')]);
        }));

        // Reset node and port connections
        newState = newState.update('nodes', item => item.map(node => {
            const defaultPortConnections = {
                links: [],
                nodes: []
            };

            node = node.update('inPorts', item => item.map(inPort => {
                return inPort.set('connections', Immutable.fromJS(defaultPortConnections));
            }));

            node = node.update('outPorts', item => item.map(outPort => {
                return outPort.set('connections', Immutable.fromJS(defaultPortConnections));
            }));

            const defaultNodeConnections = {
                fromNodes: [],
                toNodes: [],
                links: []
            };

            return node.set('connections', Immutable.fromJS(defaultNodeConnections));
        }));

        // Enumerate data types
        newState = newState.set('dataTypes', newState.get('links').reduce((accum, link) => {
            const dataType  = link.get('dataType');
            const lightness = 60;

            if (dataType === '') {
                return accum.set('', `hsl(0, 0%, ${lightness}%)`);
            }

            if (!accum.has(dataType)) {
                return accum.set(dataType, `hsl(${accum.size * 30}, 60%, ${lightness}%)`);
            }

            return accum;
        }, Immutable.Map({})));


        // Update links
        newState = newState.update('links', item => item.map(link => {
            const nodes              = newState.get('nodes');
            const inputFromNodePath  = link.getIn(['from', 'input', 'nodePath']);
            const inputFromPortId    = link.getIn(['from', 'input', 'portId']);
            const inputToNodePath    = link.getIn(['to', 'input', 'nodePath']);
            const inputToPortId      = link.getIn(['to', 'input', 'portId']);
            const layoutFromNodePath = this._closestActiveAncestor(inputFromNodePath, nodes);
            const layoutToNodePath   = this._closestActiveAncestor(inputToNodePath, nodes);

            if ((layoutFromNodePath !== inputFromNodePath) || (layoutToNodePath !== inputToNodePath)) {
                link = link.setIn(['from', 'layout', 'nodeId'], getUuid(layoutFromNodePath));
                link = link.setIn(['from', 'layout', 'nodePath'], layoutFromNodePath);
                link = link.setIn(['from', 'layout', 'portId'], '__out__');
                link = link.setIn(['to', 'layout', 'nodeId'], getUuid(layoutToNodePath));
                link = link.setIn(['to', 'layout', 'nodePath'], layoutToNodePath);
                link = link.setIn(['to', 'layout', 'portId'], '__in__');
            } else {
                link = link.setIn(['from', 'layout', 'nodeId'], getUuid(inputFromNodePath));
                link = link.setIn(['from', 'layout', 'nodePath'], inputFromNodePath);
                link = link.setIn(['from', 'layout', 'portId'], inputFromPortId);
                link = link.setIn(['to', 'layout', 'nodeId'], getUuid(inputToNodePath));
                link = link.setIn(['to', 'layout', 'nodePath'], inputToNodePath);
                link = link.setIn(['to', 'layout', 'portId'], inputToPortId);
            }

            link = link.set('active', false);
            link = link.set('color', newState.getIn(['dataTypes', link.get('dataType')]));

            if (layoutFromNodePath !== layoutToNodePath) {
                link = link.set('active', true);

                if ((layoutFromNodePath !== inputFromNodePath) || (layoutToNodePath !== inputToNodePath)) {
                    link = link.setIn(['from', 'layout', 'nodeId'], getUuid(layoutFromNodePath));
                    link = link.setIn(['from', 'layout', 'nodePath'], layoutFromNodePath);
                    link = link.setIn(['from', 'layout', 'portId'], '__out__');
                    link = link.setIn(['to', 'layout', 'nodeId'], getUuid(layoutToNodePath));
                    link = link.setIn(['to', 'layout', 'nodePath'], layoutToNodePath);
                    link = link.setIn(['to', 'layout', 'portId'], '__in__');
                } else {
                    link = link.setIn(['from', 'layout', 'nodeId'], getUuid(inputFromNodePath));
                    link = link.setIn(['from', 'layout', 'nodePath'], inputFromNodePath);
                    link = link.setIn(['from', 'layout', 'portId'], inputFromPortId);
                    link = link.setIn(['to', 'layout', 'nodeId'], getUuid(inputToNodePath));
                    link = link.setIn(['to', 'layout', 'nodePath'], inputToNodePath);
                    link = link.setIn(['to', 'layout', 'portId'], inputToPortId);
                }
            } else {
                link = link.setIn(['from', 'layout', 'nodeId'], '');
                link = link.setIn(['from', 'layout', 'nodePath'], '');
                link = link.setIn(['from', 'layout', 'portId'], '');
                link = link.setIn(['to', 'layout', 'nodeId'], '');
                link = link.setIn(['to', 'layout', 'nodePath'], '');
                link = link.setIn(['to', 'layout', 'portId'], '');
            }

            link = link.set('implicit', link.getIn(['from', 'layout', 'portId']) === '__out__' || link.getIn(['to', 'layout', 'portId']) === '__in__');

            return link;
        }));

        newState = newState.update('links', item => item.map(link => {
            link = link.set('visible', false);

            return link;
        }));

        const activeLinks = newState.get('links').filter(link => link.get('active'));

        // Update node/port/link connection info
        newState = activeLinks.reduce((accum, activeLink) => {
            if (!activeLink.get('active'))
                return accum;

            const linkId       = activeLink.get('id');
            const fromNodeId   = activeLink.getIn(['from', 'layout', 'nodeId']);
            const toNodeId     = activeLink.getIn(['to', 'layout', 'nodeId']);
            const fromPortId   = activeLink.getIn(['from', 'layout', 'portId']);
            const toPortId     = activeLink.getIn(['to', 'layout', 'portId']);
            const fromNodePath = activeLink.getIn(['from', 'layout', 'nodePath']);
            const toNodePath   = activeLink.getIn(['to', 'layout', 'nodePath']);

            if (fromNodePath !== toNodePath) {
                accum = accum.updateIn(['nodes', fromNodeId, 'connections', 'links'], links => links.push(linkId));
                accum = accum.updateIn(['nodes', fromNodeId, 'connections', 'toNodes'], toNodes => toNodes.push(toNodeId));

                accum = accum.updateIn(['nodes', fromNodeId, 'outPorts', fromPortId, 'connections', 'links'], links => links.push(linkId));
                accum = accum.updateIn(['nodes', fromNodeId, 'outPorts', fromPortId, 'connections', 'nodes'], nodes => nodes.push(toNodeId));

                accum = accum.updateIn(['nodes', toNodeId, 'connections', 'links'], links => links.push(linkId));
                accum = accum.updateIn(['nodes', toNodeId, 'connections', 'fromNodes'], fromNodes => fromNodes.push(fromNodeId));

                accum = accum.updateIn(['nodes', toNodeId, 'inPorts', toPortId, 'connections', 'links'], links => links.push(linkId));
                accum = accum.updateIn(['nodes', toNodeId, 'inPorts', toPortId, 'connections', 'nodes'], nodes => nodes.push(fromNodeId));
            }

            return accum;
        }, newState);

        this._setLayoutDirty();

        return newState;
    };

    _computeLayout = () => {
        logger.debug('Computing layout');

        let timer = new MeasureTime();

        Layouter.compute(this.data)
            .then(layout => {
                logger.debug('Layout computed');

                timer.lap('Compute layout');

                this._setLayout({
                    nodes: layout,
                    links: this._computeLinks(layout)
                });

                this.forceUpdate();
            })
            .catch(error => {
                logger.debug(error)
            });
    };

    static _computeLinkGeometry(fromAnchor, toAnchor) {
        let dockPoints = [{...fromAnchor}, {...toAnchor}];

        if (dockPoints[0].x > dockPoints[1].x) {
            const tempDockPoint = dockPoints[0];

            dockPoints[0] = dockPoints[1];
            dockPoints[1] = tempDockPoint;
        }

        const deltaX       = dockPoints[1].x - dockPoints[0].x;
        const offset       = 40;
        const minRadius    = (deltaX - 2 * offset) / 2;
        const eccentricity = 8;
        const radius       = Math.min(minRadius, deltaX / eccentricity);

        const controlPoints = [
            {
                x: dockPoints[0].x + offset + radius,
                y: dockPoints[0].y
            },
            {
                x: dockPoints[1].x - offset - radius,
                y: dockPoints[1].y
            }
        ];

        let v1 = {
            x: controlPoints[1].x - controlPoints[0].x,
            y: controlPoints[1].y - controlPoints[0].y
        };

        let v2 = {
            x: -v1.x,
            y: -v1.y
        };

        function normalize(vector) {
            const length = Math.sqrt(Math.pow(vector.x, 2) + Math.pow(vector.y, 2));
            vector.x /= length;
            vector.y /= length;
        }

        normalize(v1);
        normalize(v2);

        function pointAlongVector(vector, point, distance) {
            return {
                x: point.x + distance * vector.x,
                y: point.y + distance * vector.y
            }
        }

        const p0 = {
            x: dockPoints[0].x,
            y: dockPoints[0].y
        };

        let p1 = {
            x: p0.x + offset,
            y: p0.y
        };

        let p2 = {
            x: p1.x + radius,
            y: p1.y
        };

        let p3 = pointAlongVector(v1, p2, radius);

        let p7 = {
            x: dockPoints[1].x,
            y: dockPoints[1].y
        };

        let p6 = {
            x: p7.x - offset,
            y: p7.y
        };

        let p5 = {
            x: p6.x - radius,
            y: p6.y
        };

        let p4 = pointAlongVector(v2, p5, radius);

        let svgPath = '';

        svgPath += 'M' + p0.x + ' ' + p0.y;
        svgPath += ' ' + p1.x + ' ' + p1.y;
        svgPath += ' ' + 'Q ' + p2.x + ' ' + p2.y;
        svgPath += ' ' + p3.x + ' ' + p3.y;
        svgPath += ' L' + p4.x + ' ' + p4.y;
        svgPath += ' Q' + p5.x + ' ' + p5.y;
        svgPath += ' ' + p6.x + ' ' + p6.y;
        svgPath += ' L' + p7.x + ' ' + p7.y;

        let textCenter = {
            x: p1.x + (p6.x - p1.x) / 2,
            y: p1.y + (p6.y - p1.y) / 2
        };

        return {
            svgPath: svgPath,
            text: {
                position: textCenter,
                rotation: Math.atan((p5.y - p2.y) / (p5.x - p2.x)) * (180.0 / Math.PI),
                width: 100
            }
        };
    }

    _computeLinks(layout) {
        return this.data.get('links').reduce((accum, link) => {
            // if (!link.get('active'))
            //     return accum;

            if (layout[link.getIn(['from', 'layout', 'nodeId'])] === undefined)
                return accum;

            if (layout[link.getIn(['to', 'layout', 'nodeId'])] === undefined)
                return accum;

            const getAnchor = (link, type) => {
                const nodeId       = link.getIn([type, 'layout', 'nodeId']);
                const portId       = link.getIn([type, 'layout', 'portId']);
                const nodeLayout   = layout[nodeId];
                const portPosition = nodeLayout[`${type === 'from' ? 'out' : 'in'}Ports`][portId];

                return {
                    x: portPosition.x,
                    y: portPosition.y
                }
            };

            const anchor = {
                from: getAnchor(link, 'from'),
                to: getAnchor(link, 'to')
            };

            const paths = {
                from: `${link.getIn(['from', 'layout', 'nodeId'])}/${link.getIn(['from', 'layout', 'portId'])}`,
                to: `${link.getIn(['to', 'layout', 'nodeId'])}/${link.getIn(['to', 'layout', 'portId'])}`
            };

            const linkId   = link.get('id');
            const geometry = Run._computeLinkGeometry(anchor.from, anchor.to);

            accum[linkId] = {
                id: linkId,
                fromAnchor: anchor.from,
                toAnchor: anchor.to,
                geometry: geometry,
                implicit: link.get('implicit'),
                layerId: Math.max(paths.from.split('/').length, paths.to.split('/').length) + 1,
                color: this.data.getIn(['dataTypes', link.get('dataType')])
            };

            return accum;
        }, {});
    }

    _synchronizeStatus = () => {
        if (this.zooming)
            return;

        logger.debug(`Synchronize state`);

        this._setApiFetching(true);

        AppApi.fetchStatus(this.data.getIn(['config', 'runId']), this.data.getIn(['api', 'status', 'lastFetched']))
            .then(response => {
                const status = Object.keys(response.data.status).reduce((accum, nodePath) => {
                    accum[getUuid(nodePath)] = {
                        jobs: response.data.status[nodePath],
                        get noJobs() {
                            return {
                                completed: this.jobs[jobHelper.status.success] + this.jobs[jobHelper.status.failed],
                                all: this.jobs.reduce((accum, job) => {
                                    return accum + job;
                                }, 0)
                            }
                        },
                        get progress() {
                            return this.noJobs.completed > 0 ? (this.jobs[2] + this.jobs[3]) / this.noJobs.all : 0;
                        },
                        get state() {
                            if (this.noJobs.all === 0)
                                return nodeHelper.state.uninitialized;

                            if (this.jobs[jobHelper.status.failed] > 0)
                                return nodeHelper.state.failed;

                            if (this.jobs[jobHelper.status.cancelled] > 0)
                                return nodeHelper.state.cancelled;

                            if (this.jobs[jobHelper.status.running] > 0)
                                return nodeHelper.state.running;

                            if (this.jobs[jobHelper.status.idle] === this.noJobs.all)
                                return nodeHelper.state.idle;

                            if (this.jobs[jobHelper.status.success] > 0)
                                return nodeHelper.state.success;

                            if (this.jobs[jobHelper.status.idle] > 0)
                                return nodeHelper.state.idle;

                            return nodeHelper.state.undefined;
                        }
                    };

                    return accum;
                }, {});

                this._setApiSuccess('status', response);
                this._setData(this.data.set('status', this.data.get('status').merge(Immutable.fromJS(status))));

                this.forceUpdate();
            })
            .catch(error => {
                this._setApiError('status', error);
            });

        this._setApiFetching(false);
    };

    _setLayout = (layout) => {
        logger.debug(`Set layout`);

        const newLayout = Immutable.fromJS(layout);

        PubSub.publish('RUN_LAYOUT_CHANGED', {
            previous: this.data.get('layout'),
            current: newLayout,
            appearDelayed: true
        });

        this._setData(this.data.set('layout', newLayout));
    };

    _setApiFetching = (fetching) => {
        logger.debug(`Set API fetching`);

        this._setData(this.data.setIn(['api', 'fetching'], fetching));
    };

    _setApiSuccess = (type, response) => {
        logger.debug(`Set ${type} API success`);

        let api = this.data.getIn(['api', type]);

        api = api.set('fetching', false);
        api = api.set('noFetches', this.data.getIn(['api', 'noFetches']) + 1);
        api = api.set('error', null);
        api = api.set('response', Immutable.fromJS(response));
        api = api.set('lastFetched', new Date().toUTCString());

        this._setData(this.data.setIn(['api', type], api));
    };

    _setApiError = (type, error) => {
        logger.debug(`Set ${type} API error`);

        let api = this.data.getIn(['api', type]);

        api = api.set('fetching', false);
        api = api.set('error', Immutable.fromJS(error));
        api = api.set('response', null);
        api = api.set('lastFetched', new Date().toUTCString());

        this._setData(this.data.setIn(['api', type], api));
    };

    render() {
        const value = {
            data: this.data,
            transition: this.transition,
            nodeLayout: this.nodeLayout,
            nodeStatus: this.nodeStatus,
            actions: {
                load: this.load,
                setNodeFilter: this.setNodeFilter,
                expandNode: this.expandNode,
                collapseNode: this.collapseNode,
                toggleNodeExpanded: this.toggleNodeExpanded,
                expandAll: this.expandAll,
                collapseAll: this.collapseAll,
                expandNodeHub: this.expandNodeHub,
                collapseNodeHub: this.collapseNodeHub,
                toggleNodeHubExpanded: this.toggleNodeHubExpanded,
                expandLink: this.expandLink,
                setViewDepth: this.setViewDepth,
                revealNode: this.revealNode,
                setAutoZoom: this.setAutoZoom,
                toggleAutoZoom: this.toggleAutoZoom,
                setAnimation: this.setAnimation,
                toggleAnimation: this.toggleAnimation,
                setCompactNode: this.setCompactNode,
                toggleCompactNode: this.toggleCompactNode,
                nodeReceiveFocus: this.nodeReceiveFocus,
                nodeLooseFocus: this.nodeLooseFocus,
                linkReceiveFocus: this.linkReceiveFocus,
                linkLooseFocus: this.linkLooseFocus,
                unhighlightAll: this.unhighlightAll,
                unhighlightAllNodes: this.unhighlightAllNodes,
                unhighlightAllLinks: this.unhighlightAllLinks,
                zoomToNode: this.zoomToNode,
                zoomToNodes: this.zoomToNodes,
                zoomToNodeConnections: this.zoomToNodeConnections,
                zoomToNodeInputNodes: this.zoomToNodeInputNodes,
                zoomToNodeOutputNodes: this.zoomToNodeOutputNodes,
                zoomToInportConnectedNodes: this.zoomToInportConnectedNodes,
                zoomToOutportConnectedNodes: this.zoomToOutportConnectedNodes,
                zoomToLink: this.zoomToLink,
                setZooming: this.setZooming,
                synchronize: this.synchronize,
                startSynchronization: this.startSynchronization,
                stopSynchronization: this.stopSynchronization,
                setViewCache: this.setViewCache
            }
        };

        return <RunProvider value={value}>
            {this.props.children}
        </RunProvider>
    }
}

export default withCookies(Run);