/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React, {Component} from 'react'
import PropTypes from 'prop-types'

import {
    Menu
} from 'semantic-ui-react'

import HelpContext from 'ui/miscellaneous/HelpContext'

export default class CurrentRevision extends Component {
    render() {
        const repoLink        = `https://gitlab.com/bbmri/pim/commit/${this.props.currentRevision}`;
        const currentRevision = this.props.currentRevision;
        const revision        = currentRevision === null ? 'No revision ID' : currentRevision.substring(0, 7);
        const status          = currentRevision === null ? 'There is currently no revision information available' : `Explore Git revision ${revision}`;

        return <HelpContext status={status}>
            <Menu.Item name='currentRevision'>
                <a href={repoLink}>{revision}</a>
            </Menu.Item>
        </HelpContext>
    }
}

CurrentRevision.propTypes = {
    currentRevision: PropTypes.string
};