/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React, {Component} from 'react'
import PropTypes from 'prop-types'

import {
    Menu
} from 'semantic-ui-react'

import HelpContext from 'ui/miscellaneous/HelpContext'

import DateTimeHelper from 'helpers/dateTime'

export default class LastDeployed extends Component {
    status() {
        const lastDeployed    = this.props.lastDeployed;
        const localDateString = DateTimeHelper.utcDateTimeStringToLocalDateString(lastDeployed, true);
        const localTimeString = DateTimeHelper.utcStringToLocalTimeString(lastDeployed);

        return <span>PIM was last deployed on {localDateString} at {localTimeString}</span>
    }

    render() {
        const lastDeployed = this.props.lastDeployed;

        if (lastDeployed === null) {
            return <HelpContext status={'There is currently no deployment information available'}>
                <Menu.Item name='lastDeployed'>
                    No deployment info
                </Menu.Item>
            </HelpContext>
        }

        const localDateString = DateTimeHelper.utcDateTimeStringToLocalDateString(lastDeployed, true);

        return <HelpContext status={this.status.bind(this)}>
            <Menu.Item name='lastDeployed'>
                Deployed on {localDateString}
            </Menu.Item>
        </HelpContext>
    }
}

LastDeployed.propTypes = {
    lastDeployed: PropTypes.string
};