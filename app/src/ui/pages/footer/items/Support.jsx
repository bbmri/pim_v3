/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React, {Component} from 'react'

import {
    Menu,
    Icon
} from 'semantic-ui-react'

import * as Sentry from '@sentry/browser'

import HelpContext from 'ui/miscellaneous/HelpContext'

export default class Support extends Component {
    support = () => {
        Sentry.showReportDialog({ eventId: '14bad9a2e3774046977a21440ddb39b2' })
    };

    render() {
        return <HelpContext status={() => 'Submit error/feedback through our reporting service'}>
            <Menu.Item name='support' onClick={this.support}>
                <Icon fitted name='wpforms' link color='black'/>
            </Menu.Item>
        </HelpContext>
    }
}