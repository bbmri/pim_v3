/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React, {Component} from 'react'
import PubSub from 'pubsub-js'

import {
    Menu
} from 'semantic-ui-react'

export default class Status extends Component {
    state = {status: null};

    componentDidMount() {
        PubSub.subscribe('STATUS_SET', this.onStatusSet.bind(this));
        PubSub.subscribe('STATUS_UNSET', this.onStatusUnset.bind(this));
    }

    componentWillUnmount() {
        PubSub.unsubscribe('STATUS_SET');
        PubSub.unsubscribe('STATUS_UNSET');
    }

    shouldComponentUpdate(nextProps, nextState) {
        return nextState !== this.state;
    }

    onStatusSet(message, data) {
        this.setState({
            status: data === null ? <span>&nbsp;</span> : data
        });
    }

    onStatusUnset(message, data) {
        this.setState({
            status: null
        });
    }

    render() {
        let status = this.state.status;

        if (this.state.status === null) {
            return <Menu.Item name='status' style={{flex: 1}}>
                <span>Ready</span>
            </Menu.Item>
        }

        return <Menu.Item name='status' style={{flex: 1, overflow: 'hidden', textOverflow: 'ellipsis'}}>
            <span style={{flex: 1, overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'noWrap'}}>{status}</span>
        </Menu.Item>
    }
}