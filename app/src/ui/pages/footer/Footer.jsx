/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React, {Component} from 'react'
import {withRouter} from 'react-router-dom'

import {
    Menu
} from 'semantic-ui-react'

import {
    isFirefox
} from 'react-device-detect'

import Home from './items/Home'
import Status from './items/Status'
import LastDeployed from './items/LastDeployed'
import CurrentRevision from './items/CurrentRevision'
import ApiDocumentation from './items/ApiDocumentation'
import Support from './items/Support'

import ErrorBoundary from 'ui/miscellaneous/ErrorBoundary'
import IncompatibleBrowser from 'ui/compatibility/IncompatibleBrowser'
import * as Sentry from "@sentry/browser";

class Footer extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const menuStyle = {
            display: 'flex',
            backgroundColor: '#f3f4f5',
            boxShadow: 'none'
        };

        const errorStyle = {
            ...menuStyle,
            border: 'none',
            backgroundColor: '#transparent',
            color: '#9f3a38',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            width: '100%'
        };

        const catchErrorUI = (message) => <Menu size='small'
                                                style={{
                                                    ...menuStyle,
                                                    backgroundColor: '#fff6f6',
                                                    borderColor: '#e0b4b4'
                                                }}
                                                compact>
            <Menu.Item style={errorStyle}>
                {message}
            </Menu.Item>
        </Menu>;

        const sentrySupport = Sentry.getCurrentHub().stack[0].client !== undefined;

        return <div className='footer'>
            {/*{isFirefox ? <IncompatibleBrowser/> : null}*/}
            <ErrorBoundary content={catchErrorUI}>
                <Menu size='small' style={menuStyle} compact>
                    <Home/>
                    <Status/>
                    <LastDeployed lastDeployed={this.props.apiInfo.lastUpdated}/>
                    <CurrentRevision currentRevision={this.props.apiInfo.currentRevision}/>
                    <ApiDocumentation/>
                    {/*{sentrySupport ? <Support/> : null}*/}
                </Menu>
            </ErrorBoundary>
        </div>
    }
}

export default withRouter(Footer);
