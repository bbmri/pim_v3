/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React from 'react'

import {
    Message,
    Icon
} from 'semantic-ui-react'

import {
    browserName,
    browserVersion
} from 'react-device-detect'

export default class IncompatibleBrowser extends React.Component {
    render() {
        return <Message compact warning size='tiny' style={{width: '100%'}}>
            <Message.Content>
                <Icon name='exclamation'/>{browserName} {browserVersion} is not (yet) officially supported, you may experience problems with the user interface.
            </Message.Content>
        </Message>
    }
};