/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React, {Component} from 'react'
import PropTypes from 'prop-types'

import {
    Dropdown
} from 'semantic-ui-react'

import HelpContext from 'ui/miscellaneous/HelpContext'

import PageSize from './PageSize'

export default class PageSizes extends Component {
    status() {
        if (this.props.noPages === 0)
            return null;
        
        return <span>Show {this.props.pageSize} {this.props.rowType}s per page (Click to change)</span>;
    }
    
    render() {
        const disabled = this.props.noPages === 0;
        
        return <HelpContext status={this.status.bind(this)}>
            <Dropdown item text={this.props.pageSize + ` ${this.props.rowType}s/page`} disabled={disabled} compact={false} upward={true}>
                <Dropdown.Menu>
                    <PageSize {...this.props} pageSize={5}/>
                    <PageSize {...this.props} pageSize={10}/>
                    <PageSize {...this.props} pageSize={25}/>
                    <PageSize {...this.props} pageSize={50}/>
                    {/*<PageSize {...this.props} pageSize={100}/>*/}
                    {/*<PageSize {...this.props} pageSize={250}/>*/}
                </Dropdown.Menu>
            </Dropdown>
        </HelpContext>
    }
}

PageSizes.propTypes = {
    rowType: PropTypes.string.isRequired,
    noPages: PropTypes.number.isRequired,
    pageSize: PropTypes.number.isRequired,
    actions: PropTypes.object.isRequired
};