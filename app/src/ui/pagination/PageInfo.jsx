/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React, {Component} from 'react'
import PropTypes from 'prop-types'

export default class PageInfo extends Component {
    render() {
        const name     = <span style={{textTransform: 'capitalize'}}>{this.props.rowType}</span>;
        const firstRow = this.props.table.get('pageIndex') * this.props.table.get('pageSize');
        const lastRow  = firstRow + this.props.table.get('pageRows').size;
        const range    = `${firstRow + 1} - ${lastRow}`;
        const rowInfo  = <div>{name} {range} of {this.props.table.get('noFilteredRows')} {this.props.rowType}s</div>;

        return <div style={{color: 'grey'}}>{rowInfo}</div>
    }
}

PageInfo.propTypes = {
    rowType: PropTypes.string.isRequired,
    table: PropTypes.object.isRequired
};