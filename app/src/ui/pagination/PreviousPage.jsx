/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React, {Component} from 'react'
import PropTypes from 'prop-types'

import {
    Menu,
    Icon
} from 'semantic-ui-react'

import HelpContext from 'ui/miscellaneous/HelpContext'

export default class PreviousPage extends Component {
    status() {
        if (this.props.noPages === 0)
            return null;
        
        if (this.props.pageIndex === 0)
            return null;
        
        return <span>Go to the previous page (page {this.props.pageIndex} of {this.props.noPages})</span>;
    }
    
    render() {
        let disabled = this.props.pageIndex === 0;
        
        if (this.props.noPages === 0)
            disabled = true;
        
        return <HelpContext status={this.status.bind(this)}>
            <Menu.Item onClick={() => this.props.actions.previousPage()} disabled={disabled}>
                <Icon name='angle left'/>
            </Menu.Item>
        </HelpContext>
    }
}

PreviousPage.propTypes = {
    noPages: PropTypes.number.isRequired,
    pageIndex: PropTypes.number.isRequired,
    actions: PropTypes.object.isRequired
};