/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React, {Component} from 'react'
import PropTypes from 'prop-types'

import {
    Menu
} from 'semantic-ui-react'

import PageSizes from './PageSizes'
import FirstPage from './FirstPage'
import PreviousPage from './PreviousPage'
import Page from './Page'
import NextPage from './NextPage'
import LastPage from './LastPage'

export default class Pagination extends Component {
    shouldComponentUpdate(nextProps, nextState) {
        const attributes = ['noPages', 'pageSize', 'pageIndex', 'noPagesPerView'];

        for (let attribute of attributes) {
            if (this.props[attribute] !== nextProps[attribute]) {
                return true;
            }
        }

        return false;
    }

    render() {
        let pages = Array.from(Array(this.props.noPages).keys()).map((pageIndex) => {
            // console.log(pageIndex)
            return <Page key={pageIndex} {...this.props} targetPageIndex={pageIndex}/>
        });

        // Limit the amount of page buttons at a time
        const viewIndex        = Math.floor(this.props.pageIndex / this.props.noPagesPerView);
        const pageSegmentStart = (viewIndex) * this.props.noPagesPerView;
        const pageSegmentEnd   = (viewIndex + 1) * this.props.noPagesPerView;

        pages.splice(pageSegmentEnd, this.props.noPages - pageSegmentEnd);
        pages.splice(0, pageSegmentStart);

        return <Menu floated='right' pagination size='tiny' style={{boxShadow: 'none'}}>
            <PageSizes {...this.props}/>
            <FirstPage {...this.props}/>
            <PreviousPage {...this.props}/>
            {pages}
            <NextPage {...this.props}/>
            <LastPage {...this.props}/>
        </Menu>
    }
}

Pagination.propTypes = {
    rowType: PropTypes.string.isRequired,
    noPages: PropTypes.number.isRequired,
    pageSize: PropTypes.number.isRequired,
    pageIndex: PropTypes.number.isRequired,
    noPagesPerView: PropTypes.number.isRequired,
    actions: PropTypes.object.isRequired
};