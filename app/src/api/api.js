/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import axios from 'axios'

class AppApi {
    static fetchApiInfo() {
        logger.debug('Fetching API info');

        return axios.get(`/api/info`);
    }

    static fetchRuns({nameFilter = '', userFilter = '', statusFilter = '', sortColumn = '', sortDirection = 'ascending', pageSize = 10, pageIndex = 0}) {
        logger.debug('Fetching runs');

        const nameFilterArg    = `name_filter=${nameFilter}`;
        const userFilterArg    = `user_filter=${userFilter}`;
        const statusFilterArg  = `status_filter=${statusFilter}`;
        const sortColumnArg    = `sort_column=${sortColumn}`;
        const sortDirectionArg = `sort_direction=${sortDirection}`;
        const pageSizeArg      = `page_size=${pageSize}`;
        const pageIndexArg     = `page_index=${pageIndex}`;

        return axios.get(`/api/runs?${nameFilterArg}&${userFilterArg}&${statusFilterArg}&${sortColumnArg}&${sortDirectionArg}&${pageSizeArg}&${pageIndexArg}`);
    }

    static removeRun(runId) {
        logger.debug(`Removing run`);

        return axios.delete(`/api/runs/${runId}`);
    }

    static fetchGraph(runId) {
        logger.debug(`Fetching graph`);

        return axios.get(`/api/runs/${runId}/graph`);
    }

    /***
     * Fetches status updates from the server (only nodes for which the status has been updated are yielded)
     * @param runId
     * @param statusUpdated When the aggregates were last updated
     * @returns {AxiosPromise<any>}
     */
    static fetchStatus(runId, statusUpdated) {
        logger.debug(`Fetching status`);

        const url = `/api/runs/${runId}/status?reference_timestamp=${statusUpdated}`;

        return axios.get(url);
    }

    static fetchJobs({runId = '', nodeId = 'root', filter = [], sortColumn = '', sortDirection = 'ascending', pageSize = 10, pageIndex = 0}) {
        logger.debug(`Fetching jobs`);

        const nodeArg          = `node=${nodeId}`;
        const filterArg        = `filter=${filter}`;
        const sortColumnArg    = `sort_column=${sortColumn}`;
        const sortDirectionArg = `sort_direction=${sortDirection}`;
        const pageSizeArg      = `page_size=${pageSize}`;
        const pageIndexArg     = `page_index=${pageIndex}`;
        const url              = `/api/runs/${runId}/jobs?${nodeArg}&${filterArg}&${sortColumnArg}&${sortDirectionArg}&${pageSizeArg}&${pageIndexArg}`;

        return axios.get(url);
    }

    static fetchJob({runId = '', jobId = ''}) {
        logger.debug(`Fetching job`);

        return axios.get(`/api/runs/${runId}/job?path=${jobId}`);
    }
}

export default AppApi;