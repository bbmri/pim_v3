/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import Numeral from 'numeral'

export const status = {
    'idle': 0,
    'running': 1,
    'success': 2,
    'failed': 3,
    'cancelled': 4,
    'uninitialized': 5,
    'undefined': 6
};

export function statusName(statusIndex) {
    try {
        for (let statusName in status) {
            if (status[statusName] === statusIndex)
                return statusName;
        }
    } catch (error) {
        console.log(error);
    }
}

export function statusNames() {
    return Object.keys(status);
}

export function statusDescription(type, count) {
    const descriptions = {
        single: {
            idle: 'is idle',
            running: 'is running',
            success: 'has succeeded',
            failed: 'has failed',
            cancelled: 'is cancelled',
            undefined: 'is undefined'
        },
        multiple: {
            idle: 'are idle',
            running: 'are running',
            success: 'have succeeded',
            failed: 'have failed',
            cancelled: 'are cancelled',
            undefined: 'are undefined'
        }
    };

    return descriptions[count === 1 ? 'single' : 'multiple'][type];
}

export function statusTypeProgressDescription(item) {
    const description = statusDescription(item.type, item.count);

    return `${new Numeral(item.count).format('0,0')} job${item.count === 1 ? '' : 's'} ${description}`;
}