/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

const getUuid = require('uuid-by-string');

import createPort from './port'

export const state = {
    'idle': 0,
    'running': 1,
    'success': 2,
    'failed': 3,
    'cancelled': 4,
    'uninitialized': 5,
    'undefined': 6
};

export function stateName(statusIndex) {
    try {
        for (let stateName in state) {
            if (state[stateName] === statusIndex)
                return stateName;
        }
    } catch (error) {
        console.log(error);
    }
}

const defaultNode = {
    modified: 0,
    id: '',
    path: '',
    level: 0,
    title: 0,
    type: '',
    color: 'hsl(0, 0%, 0%)',
    children: [],
    expanded: false,
    hubExpanded: false,
    active: false,
    inPorts: {},
    outPorts: {},
    connections: {
        fromNodes: [],
        toNodes: [],
        links: []
    },
    state: state.uninitialized
};

export function createNode({id = '', level = 0, title = '', description = '', path = '', type = '', children = [], layerId = 0, inPorts = {}, outPorts = {}}) {
    let node = JSON.parse(JSON.stringify(defaultNode));

    node.id          = getUuid(id);
    node.level       = level;
    node.title       = title;
    node.description = description;
    node.path        = path;
    node.type        = type;
    node.children    = children.map(child => getUuid(child));
    node.layerId     = layerId;

    for (let inPortId in inPorts) {
        node.inPorts[inPortId] = createPort(inPorts[inPortId]);
    }

    for (let outPortId in outPorts) {
        node.outPorts[outPortId] = createPort(outPorts[outPortId]);
    }

    node.inPorts.__in__ = createPort({
        name: '__in__',
        title: '',
        description: 'Virtual input port',
    });

    node.outPorts.__out__ = createPort({
        name: '__out__',
        title: '',
        description: 'Virtual output port',
    });

    return node;
}