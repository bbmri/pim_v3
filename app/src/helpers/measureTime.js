/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

export default class MeasureTime {
    constructor() {
        this.timeStart = performance.now();
    }
    
    capitalizeEventName(eventName) {
        return eventName.charAt(0).toUpperCase() + eventName.slice(1);
    }
    
    lap(eventName, restart = true) {
        const interval = performance.now() - this.timeStart;
        
        logger.debug(`${this.capitalizeEventName(eventName)} took ${interval.toFixed(1)} ms`);
        
        if (restart)
            this.timeStart = performance.now();
    }
}