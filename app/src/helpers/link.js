/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

const uuidv4  = require('uuid/v4');
const getUuid = require('uuid-by-string');

const defaultLink = {
    id: '',
    title: '',
    description: '',
    dataType: '',
    customData: {},
    active: false,
    implicit: false,
    layerId: 0,
    from: {
        layout: {
            nodeId: '',
            nodePath: '',
            portId: '',
        },
        input: {
            nodePath: '',
            nodeId: '',
            portId: ''
        }
    },
    to: {
        layout: {
            nodeId: '',
            nodePath: '',
            portId: ''
        },
        input: {
            nodePath: '',
            nodeId: '',
            portId: ''
        }
    }
};

export function createLink({id = '', title = '', description = '', dataType = '', customData = {}, fromNode = '', fromPort = '', toNode = '', toPort = ''}) {
    let link = JSON.parse(JSON.stringify(defaultLink));

    link.id          = uuidv4();
    link.title       = title;
    link.description = description;
    link.dataType    = dataType === null ? '' : dataType;
    link.customData  = customData;
    link.layerId     = Math.min(fromNode.split('/').length, toNode.split('/').length) - 1;
    link.from.input  = {
        nodeId: getUuid(fromNode),
        nodePath: fromNode,
        portId: fromPort
    };

    link.to.input = {
        nodeId: getUuid(toNode),
        nodePath: toNode,
        portId: toPort
    };

    return link;
}