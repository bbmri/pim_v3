/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

export default {
    linkReceivedFocus: 'PIM_LINK_RECEIVED_FOCUS',       // Callback event which is invoked when a link received focus
    linkLostFocus: 'PIM_LINK_LOST_FOCUS',               // Callback event which is invoked when a link lost focus
    linkHighlight: 'PIM_LINK_HIGHLIGHT',                // Action to highlight a link
    linkUnhighlight: 'PIM_LINK_UNHIGHLIGHT',            // Action to unhighlight a link
    nodeReceivedFocus: 'PIM_NODE_RECEIVED_FOCUS',       // Callback event which is invoked when a node received focus
    nodeLostFocus: 'PIM_NODE_LOST_FOCUS',               // Callback event which is invoked when a node lost focus
    nodeHighlight: 'PIM_NODE_HIGHLIGHT',                // Action to highlight a node
    nodeUnhighlight: 'PIM_NODE_UNHIGHLIGHT',            // Action to unhighlight a node
    groupExpand: 'PIM_GROUP_EXPAND',                    // Action to expand a compound node
    groupCollapse: 'PIM_GROUP_COLLAPSE',                // Action to collapse a compound node
    revealNode: 'PIM_REVEAL_NODE',                      // Action to reveal a hidden node
    expandAllNodes: 'PIM_EXPAND_ALL_NODES',             // Action to expand all nodes in the graph
    collapseAllNodes: 'PIM_COLLAPSE_ALL_NODES',         // Action to collapse all nodes in the graph
    zoomNodeLinkage: 'PIM_ZOOM_NODE_LINKAGE',           // Action to zoom to the node and its connected nodes
    zoomLink: 'PIM_ZOOM_LINK',                          // Action to zoom to the link and its connected nodes
    nodeViewFailedJobs: 'PIM_NODE_VIEW_FAILED_JOBS',    // Action to explore node failed jobs
    nodeViewJobs: 'PIM_NODE_VIEW_JOBS'                  // Action to explore node jobs
};