/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

/**
 *
 */
export default class DateTime {

    /**
     * Convert UTC date to local date
     * @param {Date} date UTC date
     * @returns {Date} Local date
     */
    static utcDateToLocalDate(date) {
        let newDate = new Date(date.getTime() + date.getTimezoneOffset() * 60 * 1000);

        let offset = date.getTimezoneOffset() / 60;
        let hours  = date.getHours();

        newDate.setHours(hours - offset);

        return newDate;
    }

    /**
     * Converts from UTC string to local date string
     * @param {string} utcDateTime Universal date time string
     * @param {boolean} long Whether to produce a long date string
     * @returns {string}
     */
    static utcDateTimeStringToLocalDateString(utcDateTime, long = false) {
        const localDateTime = DateTime.utcDateToLocalDate(new Date(utcDateTime));

        if (long)
            return localDateTime.toLocaleDateString('er', {day: 'numeric', month: 'long', year: 'numeric'});

        return localDateTime.toLocaleDateString('nl', {day: '2-digit', month: '2-digit', year: '2-digit'});
    }

    /**
     * Converts from UTC string to local time string
     * @param {string} utc Universal time string
     * @returns {string}
     */
    static utcStringToLocalTimeString(utc) {
        const localDateTime = DateTime.utcDateToLocalDate(new Date(utc));

        return localDateTime.toLocaleTimeString('nl');
    }

    /**
     * Convert interval in milliseconds to human readable string
     * @param {number} milliseconds Interval in milliseconds
     * @param {boolean} short Whether to produce a small description
     * @returns {string} Interval in human readable string
     */
    static millisecondsToIntervalDescription(milliseconds, short = true) {
        /*if (milliseconds === 0) {
            return '0 seconds';
        }*/

        function numberEnding(number) {
            return (number > 1) ? 's' : '';
        }

        function pad(num, size) {
            let s = num + "";
            while (s.length < size) s = "0" + s;
            return s;
        }

        let temp   = Math.floor(milliseconds / 1000);
        let result = [];

        const days = Math.floor((temp %= 31536000) / 86400);

        if (days) {
            result.push(days + ' day' + numberEnding(days));
        }

        const hours = Math.floor((temp %= 86400) / 3600);

        if (hours) {
            result.push(hours + ' hour' + numberEnding(hours));
        }

        const minutes = Math.floor((temp %= 3600) / 60);

        if (minutes) {
            result.push(minutes + ' minute' + numberEnding(minutes));
        }

        const seconds = temp % 60;

        // if (seconds) {
            result.push(`${seconds} seconds`);
        // }

        if (short) {
            return `${pad(days, 2)}:${pad(hours, 2)}:${pad(minutes, 2)}:${pad(seconds, 2)}`;
        }

        if (result.length > 1) {
            result[result.length - 1] = `and ${result[result.length - 1]}`;
        }

        return result.join(', ').replace(', and', ' and');
    }
}