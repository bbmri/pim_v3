/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

const defaultPort = {
    name: false,
    title: true,
    description: 0,
    customData: {},
    connections: {
        links: [],
        nodes: []
    }
};

export default function createPort(data) {
    let port     = JSON.parse(JSON.stringify(defaultPort));
    let dataCopy = JSON.parse(JSON.stringify(data));

    port.name        = dataCopy.name;
    port.title       = dataCopy.title;
    port.description = dataCopy.description;
    port.customData  = dataCopy.customData !== undefined ? dataCopy.customData : {};

    return port;
}