/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import React from 'react'
import {render} from 'react-dom'
import {CookiesProvider} from 'react-cookie'

import * as Sentry from '@sentry/browser'

if (process.env.SENTRY_DSN !== undefined && process.env.SENTRY_DSN !== '') {
    try {
        const sentry = Sentry.init({
            dsn: process.env.SENTRY_DSN
        });
    } catch (error) {
        console.log(`Unable to initialize Sentry with DSN: ${process.env.SENTRY_DSN}`);
    }
}

import 'helpers/logger'

import App from 'ui/App.jsx'

import 'styles/main.less'

render(<CookiesProvider>
    <App/>
    </CookiesProvider>,
    document.getElementById('app')
);