/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import * as d3 from 'd3'

const getUuid = require('uuid-by-string');

import MeasureTime from 'helpers/measureTime'

export default class KGraph {
    static portsFromDOM(nodeElement, node, type, compactNode) {
        const noPorts  = node.get(`${type}Ports`).size - 1;
        const portsDiv = nodeElement.selectAll(`.ports.${type}`);

        return node.get(`${type}Ports`).reduce((ports, port, id) => {
            let portId = '';

            if (id === `__${type}__`) {
                portId = `__${type}__`;
            } else {
                if (!compactNode) {
                    portId = id;
                } else {
                    portId = noPorts > 1 ? `__collapsed_${type}__` : id;
                }
            }

            const portDiv   = portsDiv.select(`#${portId}`);
            const anchorDiv = portDiv.select('.anchor');

            if (portDiv !== null) {
                ports[id] = {
                    x: anchorDiv.node().offsetLeft + anchorDiv.node().offsetWidth / 2,
                    y: anchorDiv.node().offsetTop + anchorDiv.node().offsetHeight / 2,
                };
            }

            return ports;
        }, {});
    }

    static buildNodeTree({node, parent = null, run}) {
        let kGraphNode = {};

        const position = {x: 0, y: 0};//node.get('layout') === null ? {x: 0, y: 0} : node.getIn(['layout', 'position']).toJS();

        kGraphNode.id       = node.get('path');
        kGraphNode.x        = position.x;
        kGraphNode.y        = position.y;
        kGraphNode.width    = 100;
        kGraphNode.height   = 100;
        kGraphNode.ports    = [];
        kGraphNode.children = [];
        kGraphNode.node     = node;

        kGraphNode.layout = {
            position: {
                x: 0,
                y: 0
            },
            size: {
                width: 0,
                height: 0
            },
            inPorts: {},
            outPorts: {}
        };

        const nodeIsRoot           = node.get('level') === 0;
        const nodeIsExpanded       = node.get('expanded');
        const nodeIsLeaf           = node.get('children').size === 0 && !nodeIsExpanded;
        const nodeIsCollapsedGroup = node.get('children').size > 0 && !nodeIsExpanded;
        const nodeIsExpandedGroup  = node.get('children').size > 0 && nodeIsExpanded;
        const elementId            = `#${node.get('path')}`.split('/').join('_');

        const nodesSelection = d3.select(`#actors`);
        const leafNodeDiv    = nodesSelection.select(`${elementId}_leaf`);
        const groupNodeDiv   = nodesSelection.select(`${elementId}_group`);

        // Obtain leaf node size and port positions
        if (nodeIsLeaf || nodeIsCollapsedGroup) {
            kGraphNode.width  = leafNodeDiv.node().clientWidth;
            kGraphNode.height = leafNodeDiv.node().clientHeight;

            kGraphNode.layout.size = {
                width: leafNodeDiv.node().clientWidth,
                height: leafNodeDiv.node().clientHeight
            };

            if (!nodeIsRoot) {
                const compactNode = run.getIn(['config', 'compactNode']);

                kGraphNode.layout.inPorts  = KGraph.portsFromDOM(leafNodeDiv, node, 'in', compactNode);
                kGraphNode.layout.outPorts = KGraph.portsFromDOM(leafNodeDiv, node, 'out', compactNode);
            }
        }

        if (nodeIsExpandedGroup) {
            const headerNode     = groupNodeDiv.select('.header').node();
            const paddingOverall = 150;

            kGraphNode.padding = {
                left: paddingOverall,
                top: paddingOverall + headerNode.clientHeight,
                right: paddingOverall,
                bottom: paddingOverall
            };
        }

        if (nodeIsExpandedGroup) {
            for (let childNodeId of node.get('children').toJS()) {
                kGraphNode.children.push(this.buildNodeTree({
                    node: run.getIn(['nodes', childNodeId]),
                    parent: null,
                    run: run
                }));
            }
        }

        return kGraphNode;
    }

    static generate(run) {
        let timer = new MeasureTime();

        let kGraph = KGraph.buildNodeTree({
            node: run.getIn(['nodes', getUuid('root')]),
            run: run
        });

        kGraph.edges = [];

        timer.lap('build node tree');

        return kGraph;
    }
}