/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

import klay from 'klayjs'

const getUuid = require('uuid-by-string');

import KGraph from './kgraph'

export default class Layouter {
    /**
     *
     * @param node
     * @param nodeLayouts
     * @param offset
     */
    static flatten(node, nodeLayouts, offset = {x: 0, y: 0}) {
        const isRoot          = node.id === 'root';
        const noChildren      = node.children.length;
        const id              = node.node.get('id');
        const isExpanded      = node.node.get('expanded');
        const isGroup         = noChildren > 0;
        const isExpandedGroup = isGroup && isExpanded;
        const isCollapsedRoot = isRoot && !isExpanded;

        let nodeLayout = JSON.parse(JSON.stringify(node.layout));

        nodeLayout.position = {
            x: offset.x + node.x,
            y: offset.y + node.y
        };

        nodeLayout.size = {
            width: isCollapsedRoot ? node.layout.size.width : node.width,
            height: isCollapsedRoot ? node.layout.size.height : node.height
        };

        nodeLayout.expanded = isExpanded;

        Object.keys(nodeLayout.inPorts).map(function (key) {
            nodeLayout.inPorts[key].x += nodeLayout.position.x;
            nodeLayout.inPorts[key].y += nodeLayout.position.y;
        });

        Object.keys(nodeLayout.outPorts).map(function (key) {
            nodeLayout.outPorts[key].x += nodeLayout.position.x;
            nodeLayout.outPorts[key].y += nodeLayout.position.y;
        });

        if (isExpandedGroup) {
            nodeLayout.size = {
                width: node.width,
                height: node.height
            }
        }

        nodeLayouts[id] = nodeLayout;

        for (let child of node.children) {
            Layouter.flatten(child, nodeLayouts, {
                x: nodeLayout.position.x + node.padding.left,
                y: nodeLayout.position.y + node.padding.top
            });
        }
    }

    /**
     *
     * @param run
     * @returns {Promise<any>}
     */
    static generateGraph(run) {
        return new Promise(function (resolve, reject) {
            try {
                resolve(KGraph.generate(run));
            } catch (error) {
                reject(error);
            }
        });
    }

    /**
     *
     * @param kgraph
     * @returns {Promise<any>}
     */
    static computeGraphLayout(kgraph) {
        return new Promise(function (resolve, reject) {
            const layoutOptions = {
                algorithm: 'de.cau.cs.kieler.klay.layered',
                spacing: 300,
                borderSpacing: 0,
                layoutHierarchy: true,
                intCoordinates: true,
                direction: 'RIGHT',

                // UNDEFINED, POLYLINE, ORTHOGONAL, SPLINES
                // edgeRouting: 'ORTHOGONAL',

                // NETWORK_SIMPLEX, LONGEST_PATH, COFFMAN_GRAHAM, INTERACTIVE, STRETCH_WIDTH, MIN_WIDTH
                // nodeLayering: 'NETWORK_SIMPLEX',

                // SIMPLE, INTERACTIVE
                nodePlace: 'SIMPLE',

                // GREEDY, DEPTH_FIRST, INTERACTIVE
                // cycleBreaking: 'DEPTH_FIRST'
            };

            klay.layout({
                graph: kgraph,
                options: layoutOptions,
                success: (graph) => {
                    let nodeLayouts = {};

                    let offset = {
                        x: 0,
                        y: 0
                    };

                    if (graph.node.get('expanded')) {
                        offset = {
                            x: -graph.width / 2,
                            y: -graph.height / 2
                        };
                    } else {
                        offset = {
                            x: -graph.layout.size.width / 2,
                            y: -graph.layout.size.height / 2
                        };
                    }

                    Layouter.flatten(graph, nodeLayouts, offset);

                    resolve(nodeLayouts);
                },
                error: (error) => {
                    reject(error);
                }
            });
        });
    }

    static edges(graph) {
        return graph.get('links').reduce((edges, link) => {
            const nodes         = graph.get('nodes');
            const fromNodeId    = link.getIn(['from', 'layout', 'nodePath']);
            const toNodeId      = link.getIn(['to', 'layout', 'nodePath']);
            const fromSegments  = fromNodeId.split('/');
            const toSegments    = toNodeId.split('/');
            const minNoSegments = Math.min(fromSegments.length, toSegments.length);

            if (link.get('active')) {
                for (let i = 0; i < minNoSegments; i++) {
                    const containerNodeId = (i === 0) ? fromSegments[0] : fromSegments.slice(0, i).join('/');
                    const containerNode   = nodes.get(getUuid(containerNodeId));

                    if (!containerNode.get('active') || !containerNode.get('expanded'))
                        break;

                    if (fromSegments[i] !== toSegments[i]) {
                        const edgeFromNodeId = fromSegments.slice(0, i + 1).join('/');
                        const edgeToNodeId   = toSegments.slice(0, i + 1).join('/');
                        const linkId         = `${edgeFromNodeId}_${edgeToNodeId}`;

                        const ids = edges.reduce((edgeIds, edge) => {
                            edgeIds.push(edge.id);
                            return edgeIds;
                        }, []);

                        if (!ids.includes(linkId)) {
                            edges.push({
                                id: linkId,
                                source: edgeFromNodeId,
                                target: edgeToNodeId
                            });
                        }

                        return edges;
                    }
                }
            }

            return edges;
        }, []);
    }

    /**
     *
     * @param run
     * @returns {Promise<any>}
     */
    static async compute(graph) {
        let myGraph = await Layouter.generateGraph(graph);

        myGraph.edges = Layouter.edges(graph);

        // console.log(run.graph.toJS())
        // console.log(graph.edges)

        const layout = await Layouter.computeGraphLayout(myGraph);

        return layout;
    }
}