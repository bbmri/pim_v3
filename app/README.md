# Frontend
This directory contains the implementation of the PIM frontend.

## Organization
The frontend files are organized as follows:
```
.
├── src                         # Frontend source files
│   ├── api                     # Wraps 
│   ├── helpers                 # Various Javascript helper classes
│   ├── styles                  # CSS styles
│   ├── ui                      # React UI components
│   │   ├── compatibility       # Compatibility components
│   │   ├── miscellaneous       # Miscellaneous components
│   │   ├── pages               # SPA pages
│   │   │   ├── footer          # Page footer
│   │   │   ├── job             # Job details page
│   │   │   ├── jobs            # Pipeline jobs page
│   │   │   ├── run             # Run page
│   │   │   └── runs            # Runs overview page
│   │   └── pagination          # Pagination components
│   └── visualization           # Visualization helpers (e.g. layout)

```