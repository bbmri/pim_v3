/*
 * Copyright (c) T. Kroes (t.kroes@lumc.nl), 2019.
 *
 * PIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

const webpack           = require('webpack');
const path              = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const UglifyJsPlugin    = require('uglifyjs-webpack-plugin');
const Visualizer        = require('webpack-visualizer-plugin');

module.exports = {
    entry: ['babel-polyfill', './src/index.js'],
    context: __dirname,
    // devtool: 'source-map',
    optimization: {
        minimize: true,
        minimizer: [
            new UglifyJsPlugin({
                uglifyOptions: {
                    parallel: true,
                    compress: {
                        ecma: 6,
                    }
                }
            })
        ],
        usedExports: true,
        sideEffects: true
    },
    output: {
        filename: 'bundle.js',
        sourceMapFilename: 'bundle.map',
        path: path.resolve(__dirname, './dist'),
        publicPath: '/'
    },
    resolve: {
        extensions: ['.js', '.jsx'],
        modules: ['src', 'src/components', 'node_modules'],
        alias: {
            'api': path.resolve(__dirname, 'src/api/'),
            'helpers': path.resolve(__dirname, 'src/helpers/'),
            'ui': path.resolve(__dirname, 'src/ui/'),
            'styles': path.resolve(__dirname, 'src/styles/'),
            'visualization': path.resolve(__dirname, 'src/visualization/')
        }
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            cacheDirectory: true,
                            cacheCompression: false
                        }
                    }
                ]
            },
            {
                test: /\.css$/,
                loader: "style-loader!css-loader"
            },
            {
                test: /\.less$/,
                use: [
                    {loader: "style-loader"},
                    {loader: "css-loader"},
                    {loader: "less-loader"}
                ]
            },
            {
                test: /\.(png|jp(e*)g|svg|gif)$/,
                use: [{
                    loader: 'url-loader',
                    options: {
                        limit: 8000, // Convert images < 8kb to base64 strings
                        name: 'images/[hash]-[name].[ext]'
                    }
                }]
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: 'PIM',
            filename: 'index.html',
            template: 'index.template.ejs',
            hash: false,
            favicon: './favicon.ico'
        }),
        new webpack.EnvironmentPlugin(['SENTRY_DSN'])
        // new Visualizer()
    ],
    devServer: {
        port: 8080,
        host: 'localhost',
        contentBase: __dirname,
        historyApiFallback: true,
        noInfo: false,
        // hot: true,
        stats: 'minimal',
        inline: true,
        publicPath: 'http://localhost:8080',
        proxy: {
            '/api': {
                target: 'http://pim_flask:5000',
                secure: false
            },
            '/swaggerui': {
                target: 'http://pim_flask:5000',
                secure: false
            }
        }
    },
    node: {
        fs: 'empty'
    }
};
