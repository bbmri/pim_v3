#!/usr/bin/env bash

LANG=en_us_8859_1

touch deploy_info

echo -n > deploy_info
python -c 'import datetime; print(datetime.datetime.utcnow())' >> deploy_info
git rev-parse HEAD >> deploy_info