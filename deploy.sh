#!/usr/bin/env bash

echo "Deploying PIM to" https://$2

# Bring the site down
if [ -f "/home/ubuntu/pim/docker-compose.yml" ]; then
   cd ~/pim/server/docker/site && sudo docker-compose down -v
fi

# Docker system cleanup (remove dangling containers images)
sudo docker system prune -f

# Docker volumes can clog the vm, so remove any dangling or orphaned volumes
sudo docker volume ls -qf dangling=true | xargs -r sudo docker volume rm

# Add Gitlab server to known hosts
ssh-keyscan -t rsa gitlab.com > ~/.ssh/known_hosts

if [ -d "/home/ubuntu/pim" ]; then
    cd ~/pim && git pull
else
    cd ~ && git clone git@gitlab.com:bbmri/pim.git
fi

# Checkout develop branch in case of development deployment
if [ $1 = "develop" ]; then
    cd ~/pim && git checkout develop
fi

# Checkout master branch in case of production deployment
if [ $1 = "production" ]; then
    cd ~/pim && git checkout master
fi

# Run script that writes deployment info (datetime and git revision hash) to file
cd ~/pim && sh deploy_info.sh

# Install the node modules
cd ~/pim/app && npm install

# Run production bundling tools
cd ~/pim/app && export SENTRY_DSN=$3 && npm run "build"

# Build PIM flask image locally
cd ~/pim/server && sudo docker build -t pim_flask .

# Create PIM docker network
sudo docker network create nginx-proxy

# Bring up the reverse proxy
cd ~/pim/server/docker/reverse-proxy && sudo docker-compose up -d

# Bring up PIM
cd ~/pim/server/docker/site && export PIM_ROOT=/home/ubuntu/pim PIM_DATA=/home/ubuntu/pim-data PIM_HOST=$2 LETSENCRYPT_EMAIL=$4 && sudo -E docker-compose up -d

# Verify that the site is up and running
wget -qO- --timeout=0 --waitretry=1 --tries=60 --retry-connrefused $2 &> /dev/null