# PIM architecture
PIM is a webservice for progress monitoring and debugging of long running (image or -omics) processing pipelines.
PIM visualizes the complex structure and real-time status of processing pipelines and provides interactive tools for debugging.    
This guide will explain how a workflow engine must connect to PIM (the image below provides a schematic overview).

![alt text](architecture.jpg)

## How to connect a workflow engine to PIM
Connecting a workflow engine to PIM is a straightforward process.
The exchange of pipeline and status information is established with the REST API that PIM implements.
The workflow engine uses the API to:
1. [Register](#register-pipeline-run) a pipeline instance (run)
2. [Update](#update-status) pipeline status

Note: the REST API documentation, located at ```PIM_LOCATION/api/documentation``` provides additional information about the various REST endpoints (e.g. the JSON schema that needs to be followed). 

### Register pipeline run
To register a run with PIM, a workflow engine sends a run (see [example](data/pipeline.json)) to the ```PIM_LOCATION/api/runs``` REST endpoint in an HTTP POST request.
This run is formatted in JSON, and contains a graph which consists of a collection of nodes that are connected by links.
The graph is organized in a hierarchy, which means that collections of nodes can be nested to form a group, thereby providing a customizable level of detail (by expanding/collapsing nodes) in the graphical pipeline viewer in PIM.

### Update status
To update status, a workflow engine sends one or more jobs (see [example](data/jobs.json)) to the ```PIM_LOCATION/api/runs/RUN_NAME/jobs``` REST endpoint in an HTTP PUT request.

The job status is specified in an integer format where:
```
{
    0: idle,
    1: running,
    2: success,
    3: failed,
    4: cancelled,
    5: undefined
}
```
Each job contains a ```customData``` field for optional meta data (e.g. job environment and log output).
The PIM frontend implements special viewers for inspection of log output. These can be enabled by built-in keys:
- For [timestamped](data/log.json) log output use ```__log__```
- For multiline output like standard out/error use ```__stdout__``` and ```__stderr__``` respectively

#### Master job
Each run that is registered in PIM is equipped with a master job.
This job is a direct child of the ```root``` node and represents the status (which is idle by default) of the entire pipeline: 
```
{
    "path": "root/master",
    "title": "Job that represents the entire pipeline status",
    "description": "This is the master job",
    "status": 0,
    "customData": {}
  }
```
It is the responsibility of the workflow engine to update the status of the master job.
