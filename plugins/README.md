# Plugins
This directory contains all files related to workflow engine plugins.

Consult [this](howto/README.md) guide if you desire to connect your own workflow engine to PIM.

## Organization
The files are organized as follows:
```
├── howto                       # Contains a guide on how to create a workflow engine plugin
│   ├── data                    # Example data
│   │   ├── log.json            # Example of time-stamped log output formatting
│   │   ├── jobs.json           # Example of a collection of jobs formatted in JSON according to the PIM schema
│   │   └── pipeline.json       # Example of a JSON formatted pipeline according to the PIM schema
├── pimreporter.py              # Example FASTR plugin written in Python
```